package com.consoft.xenia.repository;

import com.consoft.xenia.domain.Question;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Question entity.
 */
@SuppressWarnings("unused")
public interface QuestionRepository extends JpaRepository<Question,Long> {

    //@Query("select r.id from Response r where r.response = :response")
    List<Question> findAllByType(@Param("type") String type);
    
}
