package com.consoft.xenia.repository;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InviaCarrPosOutput entity.
 */
@SuppressWarnings("unused")
public interface InviaCarrPosOutputRepository extends JpaRepository<InviaCarrPosOutput,Long> {

}
