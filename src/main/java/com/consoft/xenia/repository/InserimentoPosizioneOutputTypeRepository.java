package com.consoft.xenia.repository;

import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InserimentoPosizioneOutputType entity.
 */
@SuppressWarnings("unused")
public interface InserimentoPosizioneOutputTypeRepository extends JpaRepository<InserimentoPosizioneOutputType,Long> {

}
