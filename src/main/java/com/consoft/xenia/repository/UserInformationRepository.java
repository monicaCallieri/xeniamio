package com.consoft.xenia.repository;

import com.consoft.xenia.domain.User;
import com.consoft.xenia.domain.UserInformation;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the UserInformation entity.
 */
@SuppressWarnings("unused")
public interface UserInformationRepository extends JpaRepository<UserInformation,Long> {
    
     @Query("select userInformation from UserInformation userInformation where userInformation.user.login = ?#{principal.username}")
    List<UserInformation> findByUserIsCurrentUser();

    @Query("select userInformation from UserInformation userInformation where userInformation.user.login = ?#{principal.username} and userInformation.question.id = :id")
    UserInformation findByUser(@Param("id") Long id);

}
