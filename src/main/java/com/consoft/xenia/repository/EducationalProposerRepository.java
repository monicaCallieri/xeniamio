package com.consoft.xenia.repository;

import com.consoft.xenia.domain.EducationalProposer;
import java.time.LocalDate;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the EducationalProposer entity.
 */
@SuppressWarnings("unused")
public interface EducationalProposerRepository extends JpaRepository<EducationalProposer,Long> {
    
    List<EducationalProposer> findByStartdateGreaterThanEqual(@Param("startdate") LocalDate startdate);

}
