package com.consoft.xenia.repository;

import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneInputType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InserimentoPosizioneInputType entity.
 */
@SuppressWarnings("unused")
public interface InserimentoPosizioneInputTypeRepository extends JpaRepository<InserimentoPosizioneInputType,Long> {

}
