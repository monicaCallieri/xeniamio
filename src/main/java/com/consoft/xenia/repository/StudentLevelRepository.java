package com.consoft.xenia.repository;

import com.consoft.xenia.domain.StudentLevel;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the StudentLevel entity.
 */
@SuppressWarnings("unused")
public interface StudentLevelRepository extends JpaRepository<StudentLevel,Long> {

}
