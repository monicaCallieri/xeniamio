package com.consoft.xenia.repository;

import com.consoft.xenia.domain.Course;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Course entity.
 */
@SuppressWarnings("unused")
public interface CourseRepository extends JpaRepository<Course,Long> {

    @Query("select distinct course from Course course join fetch course.lecturer where course.lecturer.user.login = ?#{principal.username}")
    List<Course> findByLecturerIsCurrentUser();
    
    @Query("select distinct course from Course course "
            + "join fetch course.ids as sc "
            + "join fetch sc.student s"
            + " where s.user.login = ?#{principal.username}"
    )
    List<Course> findByUserIsCurrentUser();
}
