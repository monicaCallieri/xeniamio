package com.consoft.xenia.repository;

import com.consoft.xenia.domain.StudentIntentions;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the StudentIntentions entity.
 */
@SuppressWarnings("unused")
public interface StudentIntentionsRepository extends JpaRepository<StudentIntentions,Long> {

    @Query("select studentIntentions from StudentIntentions studentIntentions where studentIntentions.user.login = ?#{principal.username}")
    List<StudentIntentions> findByUserIsCurrentUser();
    
    @Query("select studentIntentions from StudentIntentions studentIntentions where studentIntentions.user.login = ?#{principal.username} and studentIntentions.state =:state")
    List<StudentIntentions> findByUserIsCurrentUserState(@Param("state") Enum state);

    @Query("select distinct studentIntentions from StudentIntentions studentIntentions")
    List<StudentIntentions> findAllWithEagerRelationships();

    @Query("select studentIntentions from StudentIntentions studentIntentions where studentIntentions.id =:id")
    StudentIntentions findOneWithEagerRelationships(@Param("id") Long id);

}
