package com.consoft.xenia.repository;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.DatiSingVersLista;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DatiSingVersLista entity.
 */
@SuppressWarnings("unused")
public interface DatiSingVersListaRepository extends JpaRepository<DatiSingVersLista,Long> {

}
