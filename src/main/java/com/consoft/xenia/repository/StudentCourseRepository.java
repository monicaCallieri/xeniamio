package com.consoft.xenia.repository;

import com.consoft.xenia.domain.StudentCourse;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the StudentCourse entity.
 */
@SuppressWarnings("unused")
public interface StudentCourseRepository extends JpaRepository<StudentCourse,Long> {
    @Query("select distinct studentCourse from StudentCourse studentCourse inner join fetch studentCourse.course where studentCourse.course.lecturer.user.login = ?#{principal.username}")
    List<StudentCourse> findByUserIsCurrentUser();

    
    //@Query("select count(studentCourse.course) from StudentCourse studentCourse inner join fetch studentCourse.course where studentCourse.course.id =:courseId and studentCourse.course.lecturer.user.login = ?#{principal.username}")
    @Query("select count(studentCourse.course) from StudentCourse studentCourse inner join studentCourse.course where studentCourse.course.id= :courseId and studentCourse.course.lecturer.user.login = ?#{principal.username}")
    Long findTotalStudentByUserIsCurrentUser(@Param("courseId") Long courseId);
    
    @Query("select distinct studentCourse from StudentCourse studentCourse inner join fetch studentCourse.course "
            + " where studentCourse.course.lecturer.user.login = ?#{principal.username} "
            + " and studentCourse.course.code =:courseCode ")
    List<StudentCourse> findAllByCourseCode(@Param("courseCode") String courseCode);
}
