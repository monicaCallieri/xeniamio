package com.consoft.xenia.repository;

import com.consoft.xenia.domain.PlacementTest;
import com.consoft.xenia.domain.Student;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the PlacementTest entity.
 */
@SuppressWarnings("unused")
public interface PlacementTestRepository extends JpaRepository<PlacementTest,Long> {

    @Query("select distinct student from Student student inner join fetch student.placementTests")
    List<Student> findAllWithEagerRelationships();
    @Query("select student from Student student inner join fetch student.placementTests where student.id =:id")
    List<Student> findOneWithEagerRelationships(@Param("id") Long id);

    //@Query("select distinct student from Student student inner join fetch student.placement_tests  ")
    @Query("select distinct student from Student student inner join fetch student.placementTests p where p.scheduleTime =:date ")
    List<Student> findAllByDate(@Param("date") ZonedDateTime date);

    @Query("select distinct placement_test from PlacementTest placement_test where placement_test.scheduleTime =:date ")
    List<PlacementTest> findPlacementTestByDate(@Param("date") LocalDate date);
   
    List<PlacementTest> findAllByScheduleTimeBetween(@Param("zdtStartDate") ZonedDateTime zdtStartDate,@Param("zdtEndDate") ZonedDateTime zdtEndDate);
}
