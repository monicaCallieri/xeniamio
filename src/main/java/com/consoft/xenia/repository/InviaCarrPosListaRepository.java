package com.consoft.xenia.repository;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosLista;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InviaCarrPosLista entity.
 */
@SuppressWarnings("unused")
public interface InviaCarrPosListaRepository extends JpaRepository<InviaCarrPosLista,Long> {

}
