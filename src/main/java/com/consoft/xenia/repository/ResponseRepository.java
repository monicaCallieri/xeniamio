package com.consoft.xenia.repository;

import com.consoft.xenia.domain.Response;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Response entity.
 */
@SuppressWarnings("unused")
public interface ResponseRepository extends JpaRepository<Response,Long> {

    //@Query("select response, id from Response order by general_information_id ")
    @Query(value = "SELECT * FROM Response order by question_id", nativeQuery = true)
    List<Response> findAllOrderByIdQuestion();
    
    //@Query("select response, id, general_information_id from Response where general_information_id=:general_information_id")
    //List<Response> findAllOrderByIdQuestion(@Param("general_information_id") Long general_information_id);
    
    @Query("select r.id from Response r where r.response = :response")
    Long findIdByResponse (@Param("response") String response );
    
    //List<Response> findAllByType(@Param("type") String type);
    
    @Query("select response from Response response where response.question.type =:type")
    List<Response> findAllByTypeA(@Param("type") String type);
}
