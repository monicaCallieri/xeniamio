package com.consoft.xenia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.consoft.xenia.domain.enumeration.TypeOfResponse;

/**
 * The Question entity.
 */
@ApiModel(description = "The Question entity.")
@Entity
@Table(name = "question")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "question")
    private String question;

    /**
     * tipologia di domanda per sapere in quale "vista" andra inserita
     */
    //@ApiModelProperty(value = "tipologia di domanda per sapere in quale "vista" andra inserita")
    @Column(name = "type")
    private String type;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_of_response", nullable = false)
    private TypeOfResponse typeOfResponse;

    @OneToMany(mappedBy = "question")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UserInformation> questions = new HashSet<>();

    @OneToMany(mappedBy = "question")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Response> ids = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public Question question(String question) {
        this.question = question;
        return this;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public Question type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public TypeOfResponse getTypeOfResponse() {
        return typeOfResponse;
    }

    public Question typeOfResponse(TypeOfResponse typeOfResponse) {
        this.typeOfResponse = typeOfResponse;
        return this;
    }

    public void setTypeOfResponse(TypeOfResponse typeOfResponse) {
        this.typeOfResponse = typeOfResponse;
    }

    public Set<UserInformation> getQuestions() {
        return questions;
    }

    public Question questions(Set<UserInformation> userInformations) {
        this.questions = userInformations;
        return this;
    }

    public Question addQuestion(UserInformation userInformation) {
        questions.add(userInformation);
        userInformation.setQuestion(this);
        return this;
    }

    public Question removeQuestion(UserInformation userInformation) {
        questions.remove(userInformation);
        userInformation.setQuestion(null);
        return this;
    }

    public void setQuestions(Set<UserInformation> userInformations) {
        this.questions = userInformations;
    }

    public Set<Response> getIds() {
        return ids;
    }

    public Question ids(Set<Response> responses) {
        this.ids = responses;
        return this;
    }

    public Question addId(Response response) {
        ids.add(response);
        response.setQuestion(this);
        return this;
    }

    public Question removeId(Response response) {
        ids.remove(response);
        response.setQuestion(null);
        return this;
    }

    public void setIds(Set<Response> responses) {
        this.ids = responses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Question question = (Question) o;
        if (question.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, question.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Question{" +
            "id=" + id +
            ", question='" + question + "'" +
            ", type='" + type + "'" +
            ", typeOfResponse='" + typeOfResponse + "'" +
            '}';
    }
}
