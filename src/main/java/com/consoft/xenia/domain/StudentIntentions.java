package com.consoft.xenia.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.consoft.xenia.domain.enumeration.CourseLevel;

import com.consoft.xenia.domain.enumeration.State;

/**
 * The StudentIntentions entity.
 */
@ApiModel(description = "The StudentIntentions entity.")
@Entity
@Table(name = "student_intentions")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StudentIntentions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "level")
    private CourseLevel level;

    @Column(name = "realization_date")
    private LocalDate realizationDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private State state;

    @ManyToOne
    private User user;

    @ManyToOne
    @NotNull
    private EducationalProposer educationalProposer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public StudentIntentions creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public StudentIntentions startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public StudentIntentions endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public CourseLevel getLevel() {
        return level;
    }

    public StudentIntentions level(CourseLevel level) {
        this.level = level;
        return this;
    }

    public void setLevel(CourseLevel level) {
        this.level = level;
    }

    public LocalDate getRealizationDate() {
        return realizationDate;
    }

    public StudentIntentions realizationDate(LocalDate realizationDate) {
        this.realizationDate = realizationDate;
        return this;
    }

    public void setRealizationDate(LocalDate realizationDate) {
        this.realizationDate = realizationDate;
    }

    public State getState() {
        return state;
    }

    public StudentIntentions state(State state) {
        this.state = state;
        return this;
    }

    public void setState(State state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public StudentIntentions user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EducationalProposer getEducationalProposer() {
        return educationalProposer;
    }

    public StudentIntentions educationalProposer(EducationalProposer educationalProposer) {
        this.educationalProposer = educationalProposer;
        return this;
    }

    public void setEducationalProposer(EducationalProposer educationalProposer) {
        this.educationalProposer = educationalProposer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StudentIntentions studentIntentions = (StudentIntentions) o;
        if (studentIntentions.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, studentIntentions.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "StudentIntentions{" +
            "id=" + id +
            ", creationDate='" + creationDate + "'" +
            ", startDate='" + startDate + "'" +
            ", endDate='" + endDate + "'" +
            ", level='" + level + "'" +
            ", realizationDate='" + realizationDate + "'" +
            ", state='" + state + "'" +
            '}';
    }
}
