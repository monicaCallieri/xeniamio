package com.consoft.xenia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * The EducationalProposer entity.
 */
@ApiModel(description = "The EducationalProposer entity.")
@Entity
@Table(name = "educational_proposer")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EducationalProposer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "name")
    private String name;

    @Column(name = "type_of_course")
    private String typeOfCourse;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "description")
    private String description;

    @Column(name = "periodicity")
    private String periodicity;

    @Column(name = "startdate")
    private LocalDate startdate;

    @Column(name = "price")
    private Double price;

    @OneToMany(mappedBy = "educationalProposer")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<StudentIntentions> studentIntentions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public EducationalProposer creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public EducationalProposer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeOfCourse() {
        return typeOfCourse;
    }

    public EducationalProposer typeOfCourse(String typeOfCourse) {
        this.typeOfCourse = typeOfCourse;
        return this;
    }

    public void setTypeOfCourse(String typeOfCourse) {
        this.typeOfCourse = typeOfCourse;
    }

    public Integer getDuration() {
        return duration;
    }

    public EducationalProposer duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public EducationalProposer description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public EducationalProposer periodicity(String periodicity) {
        this.periodicity = periodicity;
        return this;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public LocalDate getStartdate() {
        return startdate;
    }

    public EducationalProposer startdate(LocalDate startdate) {
        this.startdate = startdate;
        return this;
    }

    public void setStartdate(LocalDate startdate) {
        this.startdate = startdate;
    }

    public Double getPrice() {
        return price;
    }

    public EducationalProposer price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Set<StudentIntentions> getStudentIntentions() {
        return studentIntentions;
    }

    public EducationalProposer studentIntentions(Set<StudentIntentions> studentIntentions) {
        this.studentIntentions = studentIntentions;
        return this;
    }

    public EducationalProposer addStudentIntentions(StudentIntentions studentIntentionsNew) {
        studentIntentions.add(studentIntentionsNew);
        studentIntentionsNew.setEducationalProposer(this);
        return this;
    }

    public EducationalProposer removeStudentIntentions(StudentIntentions studentIntentionsNew) {
        studentIntentions.remove(studentIntentionsNew);
        studentIntentionsNew.setEducationalProposer(null);
        return this;
    }

    public void setStudentIntentions(Set<StudentIntentions> studentIntentions) {
        this.studentIntentions = studentIntentions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EducationalProposer educationalProposer = (EducationalProposer) o;
        if (educationalProposer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, educationalProposer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EducationalProposer{" +
            "id=" + id +
            ", creationDate='" + creationDate + "'" +
            ", name='" + name + "'" +
            ", typeOfCourse='" + typeOfCourse + "'" +
            ", duration='" + duration + "'" +
            ", description='" + description + "'" +
            ", periodicity='" + periodicity + "'" +
            ", startdate='" + startdate + "'" +
            ", price='" + price + "'" +
            '}';
    }
}
