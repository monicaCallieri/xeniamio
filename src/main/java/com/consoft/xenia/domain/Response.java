package com.consoft.xenia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * The Response entity.
 */
@ApiModel(description = "The Response entity.")
@Entity
@Table(name = "response")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Response implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "response")
    private String response;

    @Column(name = "value")
    private String value;

    @ManyToOne
    private Question question;

    @OneToMany(mappedBy = "response")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<UserInformation> responses = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResponse() {
        return response;
    }

    public Response response(String response) {
        this.response = response;
        return this;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getValue() {
        return value;
    }

    public Response value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Question getQuestion() {
        return question;
    }

    public Response question(Question question) {
        this.question = question;
        return this;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Set<UserInformation> getResponses() {
        return responses;
    }

    public Response responses(Set<UserInformation> userInformations) {
        this.responses = userInformations;
        return this;
    }

    public Response addResponse(UserInformation userInformation) {
        responses.add(userInformation);
        userInformation.setResponse(this);
        return this;
    }

    public Response removeResponse(UserInformation userInformation) {
        responses.remove(userInformation);
        userInformation.setResponse(null);
        return this;
    }

    public void setResponses(Set<UserInformation> userInformations) {
        this.responses = userInformations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Response response = (Response) o;
        if (response.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, response.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Response{" +
            "id=" + id +
            ", response='" + response + "'" +
            ", value='" + value + "'" +
            '}';
    }
}
