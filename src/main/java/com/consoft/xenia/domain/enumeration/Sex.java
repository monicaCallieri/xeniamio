package com.consoft.xenia.domain.enumeration;

/**
 * The Sex enumeration.
 */
public enum Sex {
    MALE,FEMALE
}
