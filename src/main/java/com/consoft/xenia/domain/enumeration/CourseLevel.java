package com.consoft.xenia.domain.enumeration;

/**
 * The CourseLevel enumeration.
 */
public enum CourseLevel {
    A1,A2,B1,B2,C1,C2,SPE,BA,IN
}
