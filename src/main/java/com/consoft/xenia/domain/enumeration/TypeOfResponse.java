package com.consoft.xenia.domain.enumeration;

/**
 * The TypeOfResponse enumeration.
 */
public enum TypeOfResponse {
    Single, Multiple
}
