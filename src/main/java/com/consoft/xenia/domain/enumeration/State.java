package com.consoft.xenia.domain.enumeration;

/**
 * The State enumeration.
 */
public enum State {
    ENROLLED, NOTENROLLED, NOTINTERESTED
}
