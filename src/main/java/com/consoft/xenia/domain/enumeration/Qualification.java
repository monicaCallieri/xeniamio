package com.consoft.xenia.domain.enumeration;

/**
 * The Qualification enumeration.
 */
public enum Qualification {
    PHD, MASTEROFSCIENCE, BACHELOR, HIGHSCHOOL, OTHER
}
