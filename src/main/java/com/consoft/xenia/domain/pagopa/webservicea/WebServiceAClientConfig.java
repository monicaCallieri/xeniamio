package com.consoft.xenia.domain.pagopa.webservicea;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 *
 * @author Administrator
 */

@Configuration
public class WebServiceAClientConfig {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl");
		return marshaller;
	}
	@Bean
	public WebServiceAClient webServiceAClient(Jaxb2Marshaller marshaller) {
		WebServiceAClient client = new WebServiceAClient();
                client.setDefaultUri("https://tesopen.unicredit.it/gate/gestoreposizioni");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}