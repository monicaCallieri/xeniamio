package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java per inserimentoPosizioneInputType complex type.
 *
 * <p>
 * Il seguente frammento di schema specifica il contenuto previsto contenuto in
 * questa classe.
 *
 * <pre>
 * &lt;complexType name="inserimentoPosizioneInputType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="identificativo_beneficiario"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="16"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="codice_servizio"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *               &lt;minInclusive value="1"/&gt;
 *               &lt;maxInclusive value="9999999"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tipo_riferimento_creditore"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="codice_riferimento_creditore"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="codice_identificativo_presentazione" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="identificativo_univoco_versamento" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ckey_5" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ckey_6" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="data_scadenza_pagamento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="importo"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;totalDigits value="15"/&gt;
 *               &lt;fractionDigits value="2"/&gt;
 *               &lt;minInclusive value="0.01"/&gt;
 *               &lt;maxInclusive value="9999999999.99"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="causale"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="140"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="savv" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="6"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tipo_id_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="identificativo_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="anagrafica_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="indirizzo_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="civico_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="cap_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="5"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="localita_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="provincia_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="nazione_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="email_debitore" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="70"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */

/**
 * A InserimentoPosizioneInputType.
 */
@Entity
@Table(name = "ins_pos_input")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inserimentoPosizioneInputType", propOrder = {
    "identificativo_beneficiario",
    "codice_servizio",
    "tipo_riferimento_creditore",
    "codice_riferimento_creditore",
    "codice_identificativo_presentazione",
    "identificativo_univoco_versamento",
    "ckey_5",
    "ckey_6",
    "data_scadenza_pagamento",
    "importo",
    "causale",
    "savv",
    "tipo_id_debitore",
    "identificativo_debitore",
    "anagrafica_debitore",
    "indirizzo_debitore",
    "civico_debitore",
    "cap_debitore",
    "localita_debitore",
    "provincia_debitore",
    "nazione_debitore",
    "email_debitore"
})
public class InserimentoPosizioneInputType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlTransient
    private Long id;

    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "identificativo_beneficiario", length = 16, nullable = false)
    @XmlElement(name = "identificativo_beneficiario", required = true)
    private String identificativo_beneficiario;

    @NotNull
    @Min(value = 1)
    @Max(value = 9999999)
    @Column(name = "codice_servizio", nullable = false)
    @XmlElement(name = "codice_servizio")
    private Integer codice_servizio;

    @NotNull
    @Size(max = 35)
    @Column(name = "tipo_riferimento_creditore", length = 35, nullable = false)
    @XmlElement(name = "tipo_riferimento_creditore", required = true)
    private String tipo_riferimento_creditore;

    @NotNull
    @Size(max = 35)
    @Column(name = "codice_riferimento_creditore", length = 35, nullable = false)
    @XmlElement(name = "codice_riferimento_creditore", required = true)
    private String codice_riferimento_creditore;

    @Size(max = 35)
    @Column(name = "codice_identificativo_presentazione", length = 35)
    @XmlElement(name = "codice_identificativo_presentazione")
    private String codice_identificativo_presentazione;

    @Size(max = 35)
    @Column(name = "identificativo_univoco_versamento", length = 35)
    @XmlElement(name = "identificativo_univoco_versamento")
    private String identificativo_univoco_versamento;

    @Size(max = 35)
    @Column(name = "ckey_5", length = 35)
    @XmlElement(name = "ckey_5")
    private String ckey_5;

    @Size(max = 35)
    @Column(name = "ckey_6", length = 35)
    @XmlElement(name = "ckey_6")
    private String ckey_6;

    @Column(name = "data_scadenza_pagamento")
    @XmlElement(name = "data_scadenza_pagamento")
    @XmlSchemaType(name = "date")
    private LocalDate data_scadenza_pagamento;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "9999999999.99")
    @XmlElement(required = true)
    @Column(name = "importo", nullable = false)
    private Double importo;

    @NotNull
    @Size(min = 1, max = 140)
    @XmlElement(required = true)
    @Column(name = "causale", length = 140, nullable = false)
    private String causale;

    @Size(max = 6)
    @Column(name = "savv", length = 6)
    private String savv;

    @Size(max = 2)
    @Column(name = "tipo_id_debitore", length = 2)
    @XmlElement(name = "tipo_id_debitore")
    private String tipo_id_debitore;

    @Size(max = 35)
    @Column(name = "identificativo_debitore", length = 35)
    @XmlElement(name = "identificativo_debitore")
    private String identificativo_debitore;

    @Size(max = 50)
    @Column(name = "anagrafica_debitore", length = 50)
    @XmlElement(name = "anagrafica_debitore")
    private String anagrafica_debitore;

    @Size(max = 50)
    @Column(name = "indirizzo_debitore", length = 50)
    @XmlElement(name = "indirizzo_debitore")
    private String indirizzo_debitore;

    @Size(max = 5)
    @Column(name = "cap_debitore", length = 5)
    @XmlElement(name = "cap_debitore")
    private String cap_debitore;

    @Size(max = 35)
    @Column(name = "localita_debitore", length = 35)
    @XmlElement(name = "localita_debitore")
    private String localita_debitore;

    @Size(max = 2)
    @Column(name = "nazione_debitore", length = 2)
    @XmlElement(name = "nazione_debitore")
    private String nazione_debitore;

    @Size(max = 70)
    @Column(name = "email_debitore", length = 70)
    @XmlElement(name = "email_debitore")
    private String email_debitore;

    @Size(max = 5)
    @Column(name = "civico_debitore", length = 5)
    @XmlElement(name = "civico_debitore")
    private String civico_debitore;

    @Size(max = 2)
    @Column(name = "provincia_debitore", length = 2)
    @XmlElement(name = "provincia_debitore")
    private String provincia_debitore;

    @OneToOne
    @NotNull
    @JoinColumn(unique = true)
    @XmlTransient
    private InserimentoPosizioneOutputType inserimentoPosizioneResponse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificativo_beneficiario() {
        return identificativo_beneficiario;
    }

    public InserimentoPosizioneInputType identificativo_beneficiario(String identificativo_beneficiario) {
        this.identificativo_beneficiario = identificativo_beneficiario;
        return this;
    }

    public void setIdentificativo_beneficiario(String identificativo_beneficiario) {
        this.identificativo_beneficiario = identificativo_beneficiario;
    }

    public Integer getCodice_servizio() {
        return codice_servizio;
    }

    public InserimentoPosizioneInputType codice_servizio(Integer codice_servizio) {
        this.codice_servizio = codice_servizio;
        return this;
    }

    public void setCodice_servizio(Integer codice_servizio) {
        this.codice_servizio = codice_servizio;
    }

    public String getTipo_riferimento_creditore() {
        return tipo_riferimento_creditore;
    }

    public InserimentoPosizioneInputType tipo_riferimento_creditore(String tipo_riferimento_creditore) {
        this.tipo_riferimento_creditore = tipo_riferimento_creditore;
        return this;
    }

    public void setTipo_riferimento_creditore(String tipo_riferimento_creditore) {
        this.tipo_riferimento_creditore = tipo_riferimento_creditore;
    }

    public String getCodice_riferimento_creditore() {
        return codice_riferimento_creditore;
    }

    public InserimentoPosizioneInputType codice_riferimento_creditore(String codice_riferimento_creditore) {
        this.codice_riferimento_creditore = codice_riferimento_creditore;
        return this;
    }

    public void setCodice_riferimento_creditore(String codice_riferimento_creditore) {
        this.codice_riferimento_creditore = codice_riferimento_creditore;
    }

    public String getCodice_identificativo_presentazione() {
        return codice_identificativo_presentazione;
    }

    public InserimentoPosizioneInputType codice_identificativo_presentazione(String codice_identificativo_presentazione) {
        this.codice_identificativo_presentazione = codice_identificativo_presentazione;
        return this;
    }

    public void setCodice_identificativo_presentazione(String codice_identificativo_presentazione) {
        this.codice_identificativo_presentazione = codice_identificativo_presentazione;
    }

    public String getIdentificativo_univoco_versamento() {
        return identificativo_univoco_versamento;
    }

    public InserimentoPosizioneInputType identificativo_univoco_versamento(String identificativo_univoco_versamento) {
        this.identificativo_univoco_versamento = identificativo_univoco_versamento;
        return this;
    }

    public void setIdentificativo_univoco_versamento(String identificativo_univoco_versamento) {
        this.identificativo_univoco_versamento = identificativo_univoco_versamento;
    }

    public String getCkey_5() {
        return ckey_5;
    }

    public InserimentoPosizioneInputType ckey_5(String ckey_5) {
        this.ckey_5 = ckey_5;
        return this;
    }

    public void setCkey_5(String ckey_5) {
        this.ckey_5 = ckey_5;
    }

    public String getCkey_6() {
        return ckey_6;
    }

    public InserimentoPosizioneInputType ckey_6(String ckey_6) {
        this.ckey_6 = ckey_6;
        return this;
    }

    public void setCkey_6(String ckey_6) {
        this.ckey_6 = ckey_6;
    }

    public LocalDate getData_scadenza_pagamento() {
        return data_scadenza_pagamento;
    }

    public InserimentoPosizioneInputType data_scadenza_pagamento(LocalDate data_scadenza_pagamento) {
        this.data_scadenza_pagamento = data_scadenza_pagamento;
        return this;
    }

    public void setData_scadenza_pagamento(LocalDate data_scadenza_pagamento) {
        this.data_scadenza_pagamento = data_scadenza_pagamento;
    }

    public Double getImporto() {
        return importo;
    }

    public InserimentoPosizioneInputType importo(Double importo) {
        this.importo = importo;
        return this;
    }

    public void setImporto(Double importo) {
        this.importo = importo;
    }

    public String getCausale() {
        return causale;
    }

    public InserimentoPosizioneInputType causale(String causale) {
        this.causale = causale;
        return this;
    }

    public void setCausale(String causale) {
        this.causale = causale;
    }

    public String getSavv() {
        return savv;
    }

    public InserimentoPosizioneInputType savv(String savv) {
        this.savv = savv;
        return this;
    }

    public void setSavv(String savv) {
        this.savv = savv;
    }

    public String getTipo_id_debitore() {
        return tipo_id_debitore;
    }

    public InserimentoPosizioneInputType tipo_id_debitore(String tipo_id_debitore) {
        this.tipo_id_debitore = tipo_id_debitore;
        return this;
    }

    public void setTipo_id_debitore(String tipo_id_debitore) {
        this.tipo_id_debitore = tipo_id_debitore;
    }

    public String getIdentificativo_debitore() {
        return identificativo_debitore;
    }

    public InserimentoPosizioneInputType identificativo_debitore(String identificativo_debitore) {
        this.identificativo_debitore = identificativo_debitore;
        return this;
    }

    public void setIdentificativo_debitore(String identificativo_debitore) {
        this.identificativo_debitore = identificativo_debitore;
    }

    public String getAnagrafica_debitore() {
        return anagrafica_debitore;
    }

    public InserimentoPosizioneInputType anagrafica_debitore(String anagrafica_debitore) {
        this.anagrafica_debitore = anagrafica_debitore;
        return this;
    }

    public void setAnagrafica_debitore(String anagrafica_debitore) {
        this.anagrafica_debitore = anagrafica_debitore;
    }

    public String getIndirizzo_debitore() {
        return indirizzo_debitore;
    }

    public InserimentoPosizioneInputType indirizzo_debitore(String indirizzo_debitore) {
        this.indirizzo_debitore = indirizzo_debitore;
        return this;
    }

    public void setIndirizzo_debitore(String indirizzo_debitore) {
        this.indirizzo_debitore = indirizzo_debitore;
    }

    public String getCap_debitore() {
        return cap_debitore;
    }

    public InserimentoPosizioneInputType cap_debitore(String cap_debitore) {
        this.cap_debitore = cap_debitore;
        return this;
    }

    public void setCap_debitore(String cap_debitore) {
        this.cap_debitore = cap_debitore;
    }

    public String getLocalita_debitore() {
        return localita_debitore;
    }

    public InserimentoPosizioneInputType localita_debitore(String localita_debitore) {
        this.localita_debitore = localita_debitore;
        return this;
    }

    public void setLocalita_debitore(String localita_debitore) {
        this.localita_debitore = localita_debitore;
    }

    public String getNazione_debitore() {
        return nazione_debitore;
    }

    public InserimentoPosizioneInputType nazione_debitore(String nazione_debitore) {
        this.nazione_debitore = nazione_debitore;
        return this;
    }

    public void setNazione_debitore(String nazione_debitore) {
        this.nazione_debitore = nazione_debitore;
    }

    public String getEmail_debitore() {
        return email_debitore;
    }

    public InserimentoPosizioneInputType email_debitore(String email_debitore) {
        this.email_debitore = email_debitore;
        return this;
    }

    public void setEmail_debitore(String email_debitore) {
        this.email_debitore = email_debitore;
    }

    public String getCivico_debitore() {
        return civico_debitore;
    }

    public InserimentoPosizioneInputType civico_debitore(String civico_debitore) {
        this.civico_debitore = civico_debitore;
        return this;
    }

    public void setCivico_debitore(String civico_debitore) {
        this.civico_debitore = civico_debitore;
    }

    public String getProvincia_debitore() {
        return provincia_debitore;
    }

    public InserimentoPosizioneInputType provincia_debitore(String provincia_debitore) {
        this.provincia_debitore = provincia_debitore;
        return this;
    }

    public void setProvincia_debitore(String provincia_debitore) {
        this.provincia_debitore = provincia_debitore;
    }

    public InserimentoPosizioneOutputType getInserimentoPosizioneResponse() {
        return inserimentoPosizioneResponse;
    }

    public InserimentoPosizioneInputType inserimentoPosizioneResponse(InserimentoPosizioneOutputType inserimentoPosizioneOutputType) {
        this.inserimentoPosizioneResponse = inserimentoPosizioneOutputType;
        return this;
    }

    public void setInserimentoPosizioneResponse(InserimentoPosizioneOutputType inserimentoPosizioneOutputType) {
        this.inserimentoPosizioneResponse = inserimentoPosizioneOutputType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InserimentoPosizioneInputType inserimentoPosizioneInputType = (InserimentoPosizioneInputType) o;
        if (inserimentoPosizioneInputType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, inserimentoPosizioneInputType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InserimentoPosizioneInputType{" +
            "id=" + id +
            ", identificativo_beneficiario='" + identificativo_beneficiario + "'" +
            ", codice_servizio='" + codice_servizio + "'" +
            ", tipo_riferimento_creditore='" + tipo_riferimento_creditore + "'" +
            ", codice_riferimento_creditore='" + codice_riferimento_creditore + "'" +
            ", codice_identificativo_presentazione='" + codice_identificativo_presentazione + "'" +
            ", identificativo_univoco_versamento='" + identificativo_univoco_versamento + "'" +
            ", ckey_5='" + ckey_5 + "'" +
            ", ckey_6='" + ckey_6 + "'" +
            ", data_scadenza_pagamento='" + data_scadenza_pagamento + "'" +
            ", importo='" + importo + "'" +
            ", causale='" + causale + "'" +
            ", savv='" + savv + "'" +
            ", tipo_id_debitore='" + tipo_id_debitore + "'" +
            ", identificativo_debitore='" + identificativo_debitore + "'" +
            ", anagrafica_debitore='" + anagrafica_debitore + "'" +
            ", indirizzo_debitore='" + indirizzo_debitore + "'" +
            ", cap_debitore='" + cap_debitore + "'" +
            ", localita_debitore='" + localita_debitore + "'" +
            ", nazione_debitore='" + nazione_debitore + "'" +
            ", email_debitore='" + email_debitore + "'" +
            ", civico_debitore='" + civico_debitore + "'" +
            ", provincia_debitore='" + provincia_debitore + "'" +
            '}';
    }
    
    public boolean isEmpty(){
        if(identificativo_beneficiario==null || codice_servizio==null ||tipo_riferimento_creditore==null||
                codice_riferimento_creditore==null || causale==null)
            return true;
        else
            return false;
    }
}
