/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni;


import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.qrcode.EncodeHintType;
import com.itextpdf.text.pdf.qrcode.Mode;
import com.itextpdf.text.pdf.qrcode.QRCode;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public class GenerateQrCode {
    //BarcodeQRCode qrCode = new BarcodeQRCode("Example QR Code Creation in Itext", 1, 1, null);
    private BarcodeQRCode qrCode = null;
    //private com.itextpdf.text.pdf.qrcode.QRCode myQRCode = null ;

   
    public BarcodeQRCode getQrCode() {
        return qrCode;
    }

    public GenerateQrCode(){
        //qrCode = new BarcodeQRCode("PAGOPA|002|123456789012345678|12345678901|1234567801", 10, 10, null);
        //qrCode = new BarcodeQRCode(content, 0, 0, hints)
         Map<EncodeHintType,Object> hintsMap = new HashMap<EncodeHintType, Object>();
         hintsMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
         hintsMap.put(EncodeHintType.ERROR_CORRECTION, 15);
//         qrCode = new BarcodeQRCode("PAGOPA|002|123456789012345678|12345678901|1234567801", 1, 1, null);
         StringBuilder sb = new StringBuilder(54);
         sb.append(PagoPAConstants.pagoPAString)
                 .append(PagoPAConstants.qrCodeSeparator)
                 .append(PagoPAConstants.qrCodeVersion)
                 .append(PagoPAConstants.qrCodeSeparator)
                 .append("001544628105799735") //codiceAvviso
                 .append(PagoPAConstants.qrCodeSeparator)
                 .append(PagoPAConstants.codiceFiscaleUnistrapg)//codiceFiscaleEnte
                 .append(PagoPAConstants.qrCodeSeparator)
                 .append("1300")//importo
                 ;
                         
         qrCode = new BarcodeQRCode(sb.toString(), 1, 1, null);
                                                
         
    }
    
}
