/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea;

import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public final class ISO7064 {

    private ISO7064() { // private constructor
        //String myStaticMember = "";
    }

    /*
    
    1. Calcolare il resto della divisione per 97 (modulo 97) di detto numero,
    2. Sottrarre il risultato ottenuto da 98;
    3. Se tale valore è maggiore di 10, il dato check digits è il valore ottenuto al punto 2, altrimenti
    anteporre uno zero (esempio: se il risultato è 4, il valore del check digits è 04). 
    
     */

    public static String getISO7064(BigDecimal convertedReference) {
        BigDecimal resto = convertedReference.remainder(new BigDecimal(97));
        BigDecimal bd98 = new BigDecimal(98);
        BigDecimal diff = bd98.subtract(resto);
        if(diff.compareTo(new BigDecimal(10))< 0){
            return "0"+diff.toString();
        }
        return diff.toString();
    }
}
