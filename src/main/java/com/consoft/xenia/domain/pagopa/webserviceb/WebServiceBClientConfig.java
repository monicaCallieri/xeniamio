package com.consoft.xenia.domain.pagopa.webserviceb;

import com.consoft.xenia.domain.pagopa.webservicea.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 *
 * @author Administrator
 */

@Configuration
public class WebServiceBClientConfig {
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl");
		return marshaller;
	}
	@Bean
	public WebServiceBClient webServiceAClient(Jaxb2Marshaller marshaller) {
		WebServiceBClient client = new WebServiceBClient();
                client.setDefaultUri("https://tst.pasemplice.eu/connettorenodo/services/soap/paInviaCarrelloPosizioni");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}