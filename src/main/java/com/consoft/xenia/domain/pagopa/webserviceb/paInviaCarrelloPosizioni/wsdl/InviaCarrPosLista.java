package com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * A InviaCarrPosLista.
 */
/**
 * <p>
 * Classe Java per paInviaCarrelloPosizioniListaType complex type.
 *
 * <p>
 * Il seguente frammento di schema specifica il contenuto previsto contenuto in
 * questa classe.
 *
 * <pre>
 * &lt;complexType name="paInviaCarrelloPosizioniListaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="spontaneo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="identificativoBeneficiario" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ccp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codicePa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codiceServizio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoIdPagatore" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="identificativoPagatore" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="anagraficaPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="indirizzoPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="civicoPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="capPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="localitaPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="provinciaPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codiceNazionePagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataScadenzaPagamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="importoPagamento" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="tipoFirmaRicevuta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tipoRiferimentoCreditore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codiceRiferimentoCreditore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="identificativoUnivocoVersamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="datiSingoloVersamento" type="{http://services.sia.eu/}datiSingoloVersamentoListaType" maxOccurs="5"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@Entity
@Table(name = "invia_carr_pos_lista")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paInviaCarrelloPosizioniLista", propOrder = {
    "spontaneo",
    "identificativoBeneficiario",
    "ccp",
    "codicePa",
    "codiceServizio",
    "tipoIdPagatore",
    "identificativoPagatore",
    "anagraficaPagatore",
    "indirizzoPagatore",
    "civicoPagatore",
    "capPagatore",
    "localitaPagatore",
    "provinciaPagatore",
    "codiceNazionePagatore",
    "emailPagatore",
    "dataScadenzaPagamento",
    "importoPagamento",
    "tipoFirmaRicevuta",
    "tipoRiferimentoCreditore",
    "codiceRiferimentoCreditore",
    "identificativoUnivocoVersamento",
    "datiSingoloVersamento"
})
public class InviaCarrPosLista implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlTransient
    private Long id;

    @NotNull
    @Column(name = "spontaneo", nullable = false)
    @XmlElement(name = "spontaneo", required = true)
    private String spontaneo;

    @NotNull
    @Column(name = "identificativo_beneficiario", nullable = false)
    @XmlElement(name = "identificativoBeneficiario", required = true)
    private String identificativoBeneficiario;

    @Column(name = "ccp")
    @XmlElement(name = "ccp")
    private String ccp;

    @Column(name = "codice_pa")
    @XmlElement(name = "codicePa")
    private String codicePa;

    @Column(name = "codice_servizio")
    @XmlElement(name = "codiceServizio")
    private String codiceServizio;

    @NotNull
    @Column(name = "tipo_id_pagatore", nullable = false)
    @XmlElement(name = "tipoIdPagatore", required = true)
    private String tipoIdPagatore;

    @NotNull
    @Column(name = "identificativo_pagatore", nullable = false)
    @XmlElement(name = "identificativoPagatore", required = true)
    private String identificativoPagatore;

    @Column(name = "anagrafica_pagatore")
    @XmlElement(name = "anagraficaPagatore")
    private String anagraficaPagatore;

    @Column(name = "indirizzo_pagatore")
    @XmlElement(name = "indirizzoPagatore")
    private String indirizzoPagatore;

    @Column(name = "civico_pagatore")
    @XmlElement(name = "civicoPagatore")
    private String civicoPagatore;

    @Column(name = "cap_pagatore")
    @XmlElement(name = "capPagatore")
    private String capPagatore;

    @Column(name = "localita_pagatore")
    @XmlElement(name = "localitaPagatore")
    private String localitaPagatore;

    @Column(name = "provincia_pagatore")
    @XmlElement(name = "provinciaPagatore")
    private String provinciaPagatore;

    @Column(name = "codice_nazione_pagatore")
    @XmlElement(name = "codiceNazionePagatore")
    private String codiceNazionePagatore;

    @Column(name = "email_pagatore")
    @XmlElement(name = "emailPagatore")
    private String emailPagatore;

    @Column(name = "data_scadenza_pagamento")
    @XmlElement(name = "dataScadenzaPagamento")
    private String dataScadenzaPagamento;

    @NotNull
    @Column(name = "importo_pagamento", nullable = false)
    @XmlElement(name = "importoPagamento", required = true)
    private Double importoPagamento;

    @NotNull
    @Column(name = "tipo_firma_ricevuta", nullable = false)
    @XmlElement(name = "tipoFirmaRicevuta", required = true)
    private String tipoFirmaRicevuta;

    @Column(name = "tipo_riferimento_creditore")
    @XmlElement(name = "tipoRiferimentoCreditore")
    private String tipoRiferimentoCreditore;

    @Column(name = "codice_riferimento_creditore")
    @XmlElement(name = "codiceRiferimentoCreditore")
    private String codiceRiferimentoCreditore;

    @Column(name = "identificativo_univoco_versamento")
    @XmlElement(name = "identificativoUnivocoVersamento")
    private String identificativoUnivocoVersamento;

    @OneToOne
    @NotNull
    @JoinColumn(unique = true)
    @XmlTransient
    private InviaCarrPosOutput inviaCarrPosLista;

    @OneToMany(mappedBy = "inviaCarrPosLista")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @XmlElement(name = "datiSingoloVersamento", required = true)
    private Set<DatiSingVersLista> datiSingoloVersamento = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpontaneo() {
        return spontaneo;
    }

    public InviaCarrPosLista spontaneo(String spontaneo) {
        this.spontaneo = spontaneo;
        return this;
    }

    public void setSpontaneo(String spontaneo) {
        this.spontaneo = spontaneo;
    }

    public String getIdentificativoBeneficiario() {
        return identificativoBeneficiario;
    }

    public InviaCarrPosLista identificativoBeneficiario(String identificativoBeneficiario) {
        this.identificativoBeneficiario = identificativoBeneficiario;
        return this;
    }

    public void setIdentificativoBeneficiario(String identificativoBeneficiario) {
        this.identificativoBeneficiario = identificativoBeneficiario;
    }

    public String getCcp() {
        return ccp;
    }

    public InviaCarrPosLista ccp(String ccp) {
        this.ccp = ccp;
        return this;
    }

    public void setCcp(String ccp) {
        this.ccp = ccp;
    }

    public String getCodicePa() {
        return codicePa;
    }

    public InviaCarrPosLista codicePa(String codicePa) {
        this.codicePa = codicePa;
        return this;
    }

    public void setCodicePa(String codicePa) {
        this.codicePa = codicePa;
    }

    public String getCodiceServizio() {
        return codiceServizio;
    }

    public InviaCarrPosLista codiceServizio(String codiceServizio) {
        this.codiceServizio = codiceServizio;
        return this;
    }

    public void setCodiceServizio(String codiceServizio) {
        this.codiceServizio = codiceServizio;
    }

    public String getTipoIdPagatore() {
        return tipoIdPagatore;
    }

    public InviaCarrPosLista tipoIdPagatore(String tipoIdPagatore) {
        this.tipoIdPagatore = tipoIdPagatore;
        return this;
    }

    public void setTipoIdPagatore(String tipoIdPagatore) {
        this.tipoIdPagatore = tipoIdPagatore;
    }

    public String getIdentificativoPagatore() {
        return identificativoPagatore;
    }

    public InviaCarrPosLista identificativoPagatore(String identificativoPagatore) {
        this.identificativoPagatore = identificativoPagatore;
        return this;
    }

    public void setIdentificativoPagatore(String identificativoPagatore) {
        this.identificativoPagatore = identificativoPagatore;
    }

    public String getAnagraficaPagatore() {
        return anagraficaPagatore;
    }

    public InviaCarrPosLista anagraficaPagatore(String anagraficaPagatore) {
        this.anagraficaPagatore = anagraficaPagatore;
        return this;
    }

    public void setAnagraficaPagatore(String anagraficaPagatore) {
        this.anagraficaPagatore = anagraficaPagatore;
    }

    public String getIndirizzoPagatore() {
        return indirizzoPagatore;
    }

    public InviaCarrPosLista indirizzoPagatore(String indirizzoPagatore) {
        this.indirizzoPagatore = indirizzoPagatore;
        return this;
    }

    public void setIndirizzoPagatore(String indirizzoPagatore) {
        this.indirizzoPagatore = indirizzoPagatore;
    }

    public String getCivicoPagatore() {
        return civicoPagatore;
    }

    public InviaCarrPosLista civicoPagatore(String civicoPagatore) {
        this.civicoPagatore = civicoPagatore;
        return this;
    }

    public void setCivicoPagatore(String civicoPagatore) {
        this.civicoPagatore = civicoPagatore;
    }

    public String getCapPagatore() {
        return capPagatore;
    }

    public InviaCarrPosLista capPagatore(String capPagatore) {
        this.capPagatore = capPagatore;
        return this;
    }

    public void setCapPagatore(String capPagatore) {
        this.capPagatore = capPagatore;
    }

    public String getLocalitaPagatore() {
        return localitaPagatore;
    }

    public InviaCarrPosLista localitaPagatore(String localitaPagatore) {
        this.localitaPagatore = localitaPagatore;
        return this;
    }

    public void setLocalitaPagatore(String localitaPagatore) {
        this.localitaPagatore = localitaPagatore;
    }

    public String getProvinciaPagatore() {
        return provinciaPagatore;
    }

    public InviaCarrPosLista provinciaPagatore(String provinciaPagatore) {
        this.provinciaPagatore = provinciaPagatore;
        return this;
    }

    public void setProvinciaPagatore(String provinciaPagatore) {
        this.provinciaPagatore = provinciaPagatore;
    }

    public String getCodiceNazionePagatore() {
        return codiceNazionePagatore;
    }

    public InviaCarrPosLista codiceNazionePagatore(String codiceNazionePagatore) {
        this.codiceNazionePagatore = codiceNazionePagatore;
        return this;
    }

    public void setCodiceNazionePagatore(String codiceNazionePagatore) {
        this.codiceNazionePagatore = codiceNazionePagatore;
    }

    public String getEmailPagatore() {
        return emailPagatore;
    }

    public InviaCarrPosLista emailPagatore(String emailPagatore) {
        this.emailPagatore = emailPagatore;
        return this;
    }

    public void setEmailPagatore(String emailPagatore) {
        this.emailPagatore = emailPagatore;
    }

    public String getDataScadenzaPagamento() {
        return dataScadenzaPagamento;
    }

    public InviaCarrPosLista dataScadenzaPagamento(String dataScadenzaPagamento) {
        this.dataScadenzaPagamento = dataScadenzaPagamento;
        return this;
    }

    public void setDataScadenzaPagamento(String dataScadenzaPagamento) {
        this.dataScadenzaPagamento = dataScadenzaPagamento;
    }

    public Double getImportoPagamento() {
        return importoPagamento;
    }

    public InviaCarrPosLista importoPagamento(Double importoPagamento) {
        this.importoPagamento = importoPagamento;
        return this;
    }

    public void setImportoPagamento(Double importoPagamento) {
        this.importoPagamento = importoPagamento;
    }

    public String getTipoFirmaRicevuta() {
        return tipoFirmaRicevuta;
    }

    public InviaCarrPosLista tipoFirmaRicevuta(String tipoFirmaRicevuta) {
        this.tipoFirmaRicevuta = tipoFirmaRicevuta;
        return this;
    }

    public void setTipoFirmaRicevuta(String tipoFirmaRicevuta) {
        this.tipoFirmaRicevuta = tipoFirmaRicevuta;
    }

    public String getTipoRiferimentoCreditore() {
        return tipoRiferimentoCreditore;
    }

    public InviaCarrPosLista tipoRiferimentoCreditore(String tipoRiferimentoCreditore) {
        this.tipoRiferimentoCreditore = tipoRiferimentoCreditore;
        return this;
    }

    public void setTipoRiferimentoCreditore(String tipoRiferimentoCreditore) {
        this.tipoRiferimentoCreditore = tipoRiferimentoCreditore;
    }

    public String getCodiceRiferimentoCreditore() {
        return codiceRiferimentoCreditore;
    }

    public InviaCarrPosLista codiceRiferimentoCreditore(String codiceRiferimentoCreditore) {
        this.codiceRiferimentoCreditore = codiceRiferimentoCreditore;
        return this;
    }

    public void setCodiceRiferimentoCreditore(String codiceRiferimentoCreditore) {
        this.codiceRiferimentoCreditore = codiceRiferimentoCreditore;
    }

    public String getIdentificativoUnivocoVersamento() {
        return identificativoUnivocoVersamento;
    }

    public InviaCarrPosLista identificativoUnivocoVersamento(String identificativoUnivocoVersamento) {
        this.identificativoUnivocoVersamento = identificativoUnivocoVersamento;
        return this;
    }

    public void setIdentificativoUnivocoVersamento(String identificativoUnivocoVersamento) {
        this.identificativoUnivocoVersamento = identificativoUnivocoVersamento;
    }

    public InviaCarrPosOutput getInviaCarrPosLista() {
        return inviaCarrPosLista;
    }

    public InviaCarrPosLista inviaCarrPosLista(InviaCarrPosOutput inviaCarrPosOutput) {
        this.inviaCarrPosLista = inviaCarrPosOutput;
        return this;
    }

    public void setInviaCarrPosLista(InviaCarrPosOutput inviaCarrPosOutput) {
        this.inviaCarrPosLista = inviaCarrPosOutput;
    }

    public Set<DatiSingVersLista> getDatiSingVersListas() {
        return datiSingoloVersamento;
    }

    public InviaCarrPosLista datiSingVersListas(Set<DatiSingVersLista> datiSingVersListas) {
        this.datiSingoloVersamento = datiSingVersListas;
        return this;
    }

    public InviaCarrPosLista addDatiSingVersLista(DatiSingVersLista datiSingVersLista) {
        datiSingoloVersamento.add(datiSingVersLista);
        datiSingVersLista.setInviaCarrPosLista(this);
        return this;
    }

    public InviaCarrPosLista removeDatiSingVersLista(DatiSingVersLista datiSingVersLista) {
        datiSingoloVersamento.remove(datiSingVersLista);
        datiSingVersLista.setInviaCarrPosLista(null);
        return this;
    }

    public void setDatiSingVersListas(Set<DatiSingVersLista> datiSingVersListas) {
        this.datiSingoloVersamento = datiSingVersListas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InviaCarrPosLista inviaCarrPosLista = (InviaCarrPosLista) o;
        if (inviaCarrPosLista.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, inviaCarrPosLista.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InviaCarrPosLista{"
                + "id=" + id
                + ", spontaneo='" + spontaneo + "'"
                + ", identificativoBeneficiario='" + identificativoBeneficiario + "'"
                + ", ccp='" + ccp + "'"
                + ", codicePa='" + codicePa + "'"
                + ", codiceServizio='" + codiceServizio + "'"
                + ", tipoIdPagatore='" + tipoIdPagatore + "'"
                + ", identificativoPagatore='" + identificativoPagatore + "'"
                + ", anagraficaPagatore='" + anagraficaPagatore + "'"
                + ", indirizzoPagatore='" + indirizzoPagatore + "'"
                + ", civicoPagatore='" + civicoPagatore + "'"
                + ", capPagatore='" + capPagatore + "'"
                + ", localitaPagatore='" + localitaPagatore + "'"
                + ", provinciaPagatore='" + provinciaPagatore + "'"
                + ", codiceNazionePagatore='" + codiceNazionePagatore + "'"
                + ", emailPagatore='" + emailPagatore + "'"
                + ", dataScadenzaPagamento='" + dataScadenzaPagamento + "'"
                + ", importoPagamento='" + importoPagamento + "'"
                + ", tipoFirmaRicevuta='" + tipoFirmaRicevuta + "'"
                + ", tipoRiferimentoCreditore='" + tipoRiferimentoCreditore + "'"
                + ", codiceRiferimentoCreditore='" + codiceRiferimentoCreditore + "'"
                + ", identificativoUnivocoVersamento='" + identificativoUnivocoVersamento + "'"
                + '}';
    }
}
