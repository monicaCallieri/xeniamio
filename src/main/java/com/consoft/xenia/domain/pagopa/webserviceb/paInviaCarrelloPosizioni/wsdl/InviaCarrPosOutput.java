package com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * A InviaCarrPosOutput.
 */
/**
 * <p>
 * Classe Java per paInviaCarrelloPosizioniOutputType complex type.
 *
 * <p>
 * Il seguente frammento di schema specifica il contenuto previsto contenuto in
 * questa classe.
 *
 * <pre>
 * &lt;complexType name="paInviaCarrelloPosizioniOutputType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="esito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="codiceErrore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idTransazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="iuvLista" type="{http://services.sia.eu/}iuvListaType" maxOccurs="5" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@Entity
@Table(name = "invia_carr_pos_output")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paInviaCarrelloPosizioniOutputType", propOrder = {
    "esito",
    "codiceErrore",
    "descrizione",
    "idTransazione",
    "url",
    "iuvLista"
})
public class InviaCarrPosOutput implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlTransient
    private Long id;

    @NotNull
    @Column(name = "esito", nullable = false)
    @XmlElement(name = "esito", required = true)
    private String esito;

    @Column(name = "codice_errore")
    @XmlElement(name = "codiceErrore")
    private String codiceErrore;

    @Column(name = "descrizione")
    @XmlElement(name = "descrizione")
    private String descrizione;

    @Column(name = "id_transazione")
    @XmlElement(name = "idTransazione")
    private String idTransazione;

    @Column(name = "url")
    @XmlElement(name = "url")
    private String url;

    @Column(name = "iuv_lista")
    @XmlTransient
    private String iuvListaConcat;

    @Transient
    @XmlElement(name = "iuvLista")
    protected List<IuvListaType> iuvLista;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEsito() {
        return esito;
    }

    public InviaCarrPosOutput esito(String esito) {
        this.esito = esito;
        return this;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public String getCodiceErrore() {
        return codiceErrore;
    }

    public InviaCarrPosOutput codiceErrore(String codiceErrore) {
        this.codiceErrore = codiceErrore;
        return this;
    }

    public void setCodiceErrore(String codiceErrore) {
        this.codiceErrore = codiceErrore;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public InviaCarrPosOutput descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getIdTransazione() {
        return idTransazione;
    }

    public InviaCarrPosOutput idTransazione(String idTransazione) {
        this.idTransazione = idTransazione;
        return this;
    }

    public void setIdTransazione(String idTransazione) {
        this.idTransazione = idTransazione;
    }

    public String getUrl() {
        return url;
    }

    public InviaCarrPosOutput url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InviaCarrPosOutput inviaCarrPosOutput = (InviaCarrPosOutput) o;
        if (inviaCarrPosOutput.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, inviaCarrPosOutput.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InviaCarrPosOutput{"
                + "id=" + id
                + ", esito='" + esito + "'"
                + ", codiceErrore='" + codiceErrore + "'"
                + ", descrizione='" + descrizione + "'"
                + ", idTransazione='" + idTransazione + "'"
                + ", url='" + url + "'"
                + ", iuvLista='" + iuvLista + "'"
                + '}';
    }

    /**
     * Gets the value of the iuvLista property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the iuvLista property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIuvLista().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IuvListaType }
     *
     *
     */
    public List<IuvListaType> getIuvLista() {
        if (iuvLista == null) {
            iuvLista = new ArrayList<IuvListaType>();
        }
        return this.iuvLista;
    }
    

    public String getIuvListaConcat() {
        return iuvListaConcat;
    }

    public InviaCarrPosOutput iuvListaConcat(String iuvListaConcat) {
        this.iuvListaConcat = iuvListaConcat;
        return this;
    }

    public void setIuvListaConcat(String iuvListaConcat) {
        this.iuvListaConcat = iuvListaConcat;
    }

}
