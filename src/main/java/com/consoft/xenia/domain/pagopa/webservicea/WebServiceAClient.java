package com.consoft.xenia.domain.pagopa.webservicea;

import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneInputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneResponse;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneInputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneRequest;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.ObjectFactory;
import com.consoft.xenia.repository.InserimentoPosizioneInputTypeRepository;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import javax.xml.bind.JAXBElement;
import org.springframework.format.annotation.DateTimeFormat;

public class WebServiceAClient extends WebServiceGatewaySupport {

    private InserimentoPosizioneInputType inserimentoPosizioneInputType = new InserimentoPosizioneInputType();

    public InserimentoPosizioneInputType getInserimentoPosizioneInputType() {
        return inserimentoPosizioneInputType;
    }

    public JAXBElement getInserimentoPosizioneResponse(String user, String password, String identificativo_beneficiario,
            String tipo_riferimento_creditore, int codice_servizio, String codice_riferimento_creditore, Double importo,
            String codice_identificativo_presentazione, String identificativo_univoco_versamento, String causale, String emailDebitore) {
        Date dob = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        InserimentoPosizioneRequest inserimentoPosizioneRequest = new InserimentoPosizioneRequest();
        InserimentoPosizioneResponse inserimentoPosizioneResponse = new InserimentoPosizioneResponse();

        JAXBElement jres = new ObjectFactory().createInserimentoPosizioneResponse(inserimentoPosizioneResponse);
        try {
            dob = df.parse("2099-12-31");

            GregorianCalendar cal = new GregorianCalendar();
            XMLGregorianCalendar xmlDate3 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), dob.getHours(), dob.getMinutes(), dob.getSeconds(), DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);

            //Costruisco la Richiesta InserimentoPosizioneInputType
            //inserimentoPosizioneInputType.setIdentificativoBeneficiario("12345678901");
            inserimentoPosizioneInputType.setIdentificativo_beneficiario(identificativo_beneficiario);
            inserimentoPosizioneInputType.setCodice_servizio(codice_servizio);
            inserimentoPosizioneInputType.setTipo_riferimento_creditore(tipo_riferimento_creditore);
            inserimentoPosizioneInputType.setCodice_riferimento_creditore(codice_riferimento_creditore);
            inserimentoPosizioneInputType.setCodice_identificativo_presentazione(codice_identificativo_presentazione);
            inserimentoPosizioneInputType.setIdentificativo_univoco_versamento(identificativo_univoco_versamento);
            //inserimentoPosizioneInputType.setData_scadenza_pagamento(dataScadenzaPagamento);
            inserimentoPosizioneInputType.setImporto(importo);
            inserimentoPosizioneInputType.setCausale(causale);
            inserimentoPosizioneInputType.setTipo_id_debitore("F");
            inserimentoPosizioneInputType.setIdentificativo_debitore("CPTMCC88D05L218G");
            inserimentoPosizioneInputType.setAnagrafica_debitore("12345678");
            inserimentoPosizioneInputType.setIndirizzo_debitore("indirizzo_debitore");
            inserimentoPosizioneInputType.setCivico_debitore("12345");
            inserimentoPosizioneInputType.setCap_debitore("12345");
            inserimentoPosizioneInputType.setLocalita_debitore("12345678");
            inserimentoPosizioneInputType.setProvincia_debitore("RM");
            inserimentoPosizioneInputType.setNazione_debitore("IT");
            inserimentoPosizioneInputType.setEmail_debitore(emailDebitore);
            inserimentoPosizioneInputType.setSavv("");
            inserimentoPosizioneInputType.setCkey_5("");
            inserimentoPosizioneInputType.setCkey_6("");

            inserimentoPosizioneRequest.setInserimentoPosizioneInput(inserimentoPosizioneInputType);

            /*InserimentoPosizioneInputTypeRepository inserimentoPosizioneInputTypeRepository;
            inserimentoPosizioneInputTypeRepository.save(inserimentoPosizioneInputType);*/
            //Composizione Risposta
            InserimentoPosizioneOutputType inserimentoPosizioneOutput = new InserimentoPosizioneOutputType();
            inserimentoPosizioneResponse.setInserimentoPosizioneOutput(inserimentoPosizioneOutput);

            JAXBElement jreq = new ObjectFactory().createInserimentoPosizioneRequest(inserimentoPosizioneRequest);
            jres = (JAXBElement) getWebServiceTemplate().marshalSendAndReceive(jreq, new SoapRequestHeaderModifier(user, password));
            System.out.println("risposta:" + jres.toString());

        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(WebServiceAClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DatatypeConfigurationException ex) {
            java.util.logging.Logger.getLogger(WebServiceAClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jres;
    }

    /*private String prepareSecurityHeader() {
        String result = "";
        StringWriter sw = new StringWriter();
        GestorePosizioniHeader gestorePosizioniHeader = new GestorePosizioniHeader();
        gestorePosizioniHeader.setPassword("");
        gestorePosizioniHeader.setPassword("");
        try {
            JAXBContext carContext = JAXBContext.newInstance(GestorePosizioniHeader.class);
            Marshaller carMarshaller = carContext.createMarshaller();
            carMarshaller.marshal(gestorePosizioniHeader, sw);
            result = sw.toString();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
        return result;
    }*/
}
