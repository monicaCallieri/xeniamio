package com.consoft.xenia.domain.pagopa.webservicea;

import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Administrator
 */
public class CodiceAvviso {

    private String auxDigit;
    private String applicationCode;
    private IUV iuv;
    private String iuvBase;

    public String getIuvBase() {
        return iuvBase + this.checkDigits;
    }

    public void setIuvBase(String iuvBase) {
        this.iuvBase = iuvBase;
    }
    private String checkDigits;
    private int modello;

    public CodiceAvviso(String auxDigit, String applicationCode, String iuvReference) {
        this.auxDigit = auxDigit;
        this.applicationCode = applicationCode;
        //if (iuvReference.matches("^[\\p{IsAlphabetic}\\p{IsDigit}]+$")) {
        //Pattern p = Pattern.compile("^[\\p{IsAlphabetic}\\p{IsDigit}]+$");
        Pattern p = Pattern.compile("^\\p{Digit}+$");

        Matcher m = p.matcher(iuvReference);
        boolean b = m.matches();

        if (m.matches()) {
            this.modello = 3;
            this.iuvBase = iuvReference;
            this.setCheckDigits(this.modello, this.auxDigit + this.applicationCode + this.iuvBase);
        } else {
            //MODELLO 1 -> IUV alfanumerico max 35 char
            this.modello = 1;
            this.iuv = new IUV(iuvReference,PagoPAConstants.iuvModello1);
            this.setCheckDigits(this.modello, this.auxDigit + this.applicationCode + iuv.getIso11649().getConvertedReference()); //stringa numerica composta da <aux digit>+<application code>+<base IUV>

        }

    }

    public CodiceAvviso(String auxDigit, String iuvReference) {
        this.auxDigit = auxDigit;
        //this.applicationCode = applicationCode;
        //this.iuv = new IUV(iuvReference);
        this.iuvBase = iuvReference;
        this.modello = 3;
        //this.setCheckDigits(this.auxDigit + this.applicationCode + iuv.getIso11649().getConvertedReference()); //stringa numerica composta da <aux digit>+<application code>+<base IUV>
        this.setCheckDigits(this.modello, this.auxDigit + this.iuvBase);

    }

    public String getCheckDigits() {
        return checkDigits;
    }

    /*
     * Le ultime due cifre sono il modulo 93 della stringa numerica composta da <aux digit>+<application code>+<base IUV>
     */
    public void setCheckDigits(int modello, String iuvBase) {
        BigDecimal convertedReference = new BigDecimal(0);
        switch (modello) {
            case 1:
                System.out.println(iuvBase.substring(0, iuvBase.length() - 6));
                String fixedNumericString = iuvBase.substring(0, iuvBase.length() - 6);//rimuovo il codice 271500 accodato dallo standard ISO11649
                convertedReference = new BigDecimal(fixedNumericString);
                break;
            case 3:
                convertedReference = new BigDecimal(iuvBase);
                break;
        }

        BigDecimal resto = convertedReference.remainder(new BigDecimal(93));

        if (resto.compareTo(new BigDecimal(10)) < 0) {
            this.checkDigits = "0" + resto.toString();
        } else {
            this.checkDigits = resto.toString();
        }
    }

    @Override
    public String toString() {
        return "CodiceAvviso{" + "auxDigit=" + auxDigit + ", applicationCode=" + applicationCode + ", iuv=" + iuv.getIso11649().getIso11649() + ", checkDigits=" + checkDigits + '}';
    }

    public String getAuxDigit() {
        return auxDigit;
    }

    public void setAuxDigit(String auxDigit) {
        this.auxDigit = auxDigit;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public IUV getIuv() {
        return iuv;
    }

    public void setIuv(IUV iuv) {
        this.iuv = iuv;
    }

    public String getCodiceAvviso() {
        if (this.modello == 1) {
            return this.auxDigit + this.applicationCode + this.iuv.getIso11649().getIso11649() + this.checkDigits;
        }
        if (this.modello == 3) {
            return this.auxDigit + this.applicationCode + this.iuvBase + this.checkDigits;
        }
        return "Unknown Model";
    }
}
