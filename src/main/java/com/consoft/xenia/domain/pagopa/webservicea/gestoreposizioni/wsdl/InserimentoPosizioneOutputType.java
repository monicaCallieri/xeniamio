package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Classe Java per inserimentoPosizioneOutputType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="inserimentoPosizioneOutputType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="esito" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="codiceErrore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="identificativo_univoco_versamento" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="codice_identificativo_presentazione" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="35"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */

/**
 * A InserimentoPosizioneOutputType.
 */
@Entity
@Table(name = "ins_pos_output")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inserimentoPosizioneOutputType", propOrder = {
    "esito",
    "codiceErrore",
    "descrizione",
    "identificativo_univoco_versamento",
    "codice_identificativo_presentazione"
})
public class InserimentoPosizioneOutputType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlTransient
    private Long id;

    @NotNull
    @Column(name = "esito", nullable = false)
    @XmlElement(name = "esito",required = true)
    private String esito;
    
    @Column(name = "codice_errore")
    @XmlElement(name = "codice_errore")
    private String codiceErrore;
    
    @Column(name = "descrizione")
    @XmlElement(name = "descrizione")
    private String descrizione;

    @Size(max = 35)
    @Column(name = "identificativo_univoco_versamento", length = 35)
    @XmlElement(name = "identificativo_univoco_versamento")
    private String identificativo_univoco_versamento;

    @Size(max = 35)
    @Column(name = "codice_identificativo_presentazione", length = 35)
    @XmlElement(name = "codice_identificativo_presentazione")
    private String codice_identificativo_presentazione;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEsito() {
        return esito;
    }

    public InserimentoPosizioneOutputType esito(String esito) {
        this.esito = esito;
        return this;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public String getCodiceErrore() {
        return codiceErrore;
    }

    public InserimentoPosizioneOutputType codiceErrore(String codiceErrore) {
        this.codiceErrore = codiceErrore;
        return this;
    }

    public void setCodiceErrore(String codiceErrore) {
        this.codiceErrore = codiceErrore;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public InserimentoPosizioneOutputType descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getIdentificativo_univoco_versamento() {
        return identificativo_univoco_versamento;
    }

    public InserimentoPosizioneOutputType identificativo_univoco_versamento(String identificativo_univoco_versamento) {
        this.identificativo_univoco_versamento = identificativo_univoco_versamento;
        return this;
    }

    public void setIdentificativo_univoco_versamento(String identificativo_univoco_versamento) {
        this.identificativo_univoco_versamento = identificativo_univoco_versamento;
    }

    public String getCodice_identificativo_presentazione() {
        return codice_identificativo_presentazione;
    }

    public InserimentoPosizioneOutputType codice_identificativo_presentazione(String codice_identificativo_presentazione) {
        this.codice_identificativo_presentazione = codice_identificativo_presentazione;
        return this;
    }

    public void setCodice_identificativo_presentazione(String codice_identificativo_presentazione) {
        this.codice_identificativo_presentazione = codice_identificativo_presentazione;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InserimentoPosizioneOutputType inserimentoPosizioneOutputType = (InserimentoPosizioneOutputType) o;
        if (inserimentoPosizioneOutputType.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, inserimentoPosizioneOutputType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InserimentoPosizioneOutputType{" +
            "id=" + id +
            ", esito='" + esito + "'" +
            ", codiceErrore='" + codiceErrore + "'" +
            ", descrizione='" + descrizione + "'" +
            ", identificativo_univoco_versamento='" + identificativo_univoco_versamento + "'" +
            ", codice_identificativo_presentazione='" + codice_identificativo_presentazione + "'" +
            '}';
    }
}
