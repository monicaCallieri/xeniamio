//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2017.03.21 alle 03:05:46 PM CET 
//


package com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per paInviaCarrelloPosizioniResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="paInviaCarrelloPosizioniResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="paInviaCarrelloPosizioniOutput" type="{http://services.sia.eu/}paInviaCarrelloPosizioniOutputType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paInviaCarrelloPosizioniResponse", propOrder = {
    "paInviaCarrelloPosizioniOutput"
})
public class PaInviaCarrelloPosizioniResponse {

    @XmlElement(required = true)
    protected InviaCarrPosOutput paInviaCarrelloPosizioniOutput;

    /**
     * Recupera il valore della proprietà paInviaCarrelloPosizioniOutput.
     * 
     * @return
     *     possible object is
     *     {@link PaInviaCarrelloPosizioniOutputType }
     *     
     */
    public InviaCarrPosOutput getPaInviaCarrelloPosizioniOutput() {
        return paInviaCarrelloPosizioniOutput;
    }

    /**
     * Imposta il valore della proprietà paInviaCarrelloPosizioniOutput.
     * 
     * @param value
     *     allowed object is
     *     {@link PaInviaCarrelloPosizioniOutputType }
     *     
     */
    public void setPaInviaCarrelloPosizioniOutput(InviaCarrPosOutput value) {
        this.paInviaCarrelloPosizioniOutput = value;
    }

}
