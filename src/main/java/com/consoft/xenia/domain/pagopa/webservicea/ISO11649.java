/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea;

import ch.qos.logback.core.CoreConstants;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *
 * @author Administrator
 */
public class ISO11649 {

    private String rf;
    private String checkDigits;
    private String reference;
    private String convertedReference;

    public String getConvertedReference() {
        return convertedReference;
    }

    public void setConvertedReference(String convertedReference) {
        this.convertedReference = convertedReference;
    }

    public String getRf() {
        return rf;
    }

    public void setRf(String rf) {
        this.rf = rf;
    }

    public String getCheckDigits() {
        return checkDigits;
    }

    public void setCheckDigits(String checkDigits) {
        this.checkDigits = checkDigits;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getIso11649() {
        return iso11649;
    }

    public void setIso11649(String iso11649) {
        this.iso11649 = iso11649;
    }
    private String iso11649;

    public ISO11649(String reference) {
        this.reference = reference;
        this.rf = "RF";
        //this.rf = "28";
        this.convertedReference="";
        try {
            String upperReference = reference.toUpperCase();
            System.out.println(reference + " -> " + upperReference);
            byte[] bytes = upperReference.getBytes("US-ASCII");
            //this.convertedReference = new String(bytes);
            String tmp="";
            for (byte b : bytes) {
                System.out.println("ascii: " + b);
                if (b >= 65) {//è una lettera
                    System.out.println("b: " + b + " -> " + (b - 55));
                    int v = b-55;
                    System.out.println(v);
                    this.convertedReference+=v;
                }else{//è un numero
                    System.out.println("b: "+b);
                    tmp = Character.toString ((char) b);
                    System.out.println("tmp: "+tmp);
                    this.convertedReference+=tmp;
                }
            }
            this.convertedReference+="271500";//Una volta convertito tutto il dato reference si aggiunge alla stringa numerica così ottenuta il valore 2715 (conversione di RF) ed il valore fisso 00
            System.out.println("converted: " + this.convertedReference);
            this.checkDigits=ISO7064.getISO7064(new BigDecimal(this.convertedReference));//alla stringa risultante si applica l’algoritmo di calcolo ISO/IEC 7064
            //Compongo infine il vero codice iso11649
            this.iso11649=this.rf+this.checkDigits+this.reference;
            System.out.println("iso11649 di "+this.reference+": "+this.iso11649);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ISO11649.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
