/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webserviceb;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.PaInviaCarrelloPosizioniHeader;
import java.io.IOException;
import java.io.StringWriter;
import java.security.Security;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;

import org.springframework.xml.transform.StringSource;

/**
 *
 * @author Administrator
 */
public class SoapRequestHeaderModifier implements WebServiceMessageCallback {

    private String user;
    private String password;
    private String abiCode;

    SoapRequestHeaderModifier(String user, String password, String abiCode) {
        this.user = user;
        this.password = password;
        this.abiCode = abiCode;
    }

    @Override
    public void doWithMessage(WebServiceMessage wsm) throws IOException, TransformerException {
        SoapHeader header = ((SoapMessage) wsm).getSoapHeader();

        StringSource headerSource = new StringSource(prepareSecurityHeader());

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(headerSource, header.getResult());
    }

    private String prepareSecurityHeader() {
        String result = "";
        StringWriter sw = new StringWriter();
        /*Costruisco l'HEADER*/
        PaInviaCarrelloPosizioniHeader paInviaCarrelloPosizioniHeader = new PaInviaCarrelloPosizioniHeader();
        paInviaCarrelloPosizioniHeader.setUser(this.user);
        paInviaCarrelloPosizioniHeader.setPassword(this.password);
        paInviaCarrelloPosizioniHeader.setAbiCode(this.abiCode);

        try {
            JAXBContext carContext = JAXBContext.newInstance(PaInviaCarrelloPosizioniHeader.class);
            Marshaller carMarshaller = carContext.createMarshaller();
            carMarshaller.marshal(paInviaCarrelloPosizioniHeader, sw);
            result = sw.toString();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

}
