/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni;

import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import org.krysalis.barcode4j.ChecksumMode;
import org.krysalis.barcode4j.impl.code128.EAN128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
public class GenerateBarcode {

   
    private EAN128Bean objEAN128Bean = new EAN128Bean();
    private int dpi = 400;
    private final Logger log = LoggerFactory.getLogger(GenerateBarcode.class);
    
    public  GenerateBarcode(String codiceAvviso, String importoInCentesimi) {
        if (importoInCentesimi.length() < 10) {
            //importoInCentesimi = String.format("%"+(14-importoInCentesimi.length())+"s", importoInCentesimi).replace(' ', '0');
            importoInCentesimi = String.format("%10s", importoInCentesimi).replace(' ', '0');
        }
        //Open output file
        File outputFile = new File(PagoPAConstants.pdfGeneratedLocation+codiceAvviso+".png");
        OutputStream out = null;
        try {
            out = new FileOutputStream(outputFile);
        // barcode  
        //objEAN128Bean.setTemplate("(415)n13+FNC1+(8020)n18+FNC1+(3902)n10+cd");
        objEAN128Bean.setTemplate("(415)n13+(8020)n18+(3902)n10+cd");
        //objEAN128Bean.setModuleWidth(PagoPAConstants.moduleWidthmm);
        objEAN128Bean.setModuleWidth(0.1944);
        //objEAN128Bean.setHeight(PagoPAConstants.moduleHeightmm);
        objEAN128Bean.setHeight(15.06);
        // objEAN128Bean.setWideFactor(3);
        objEAN128Bean.doQuietZone(true);
        objEAN128Bean.setQuietZone(2);
        // human-readable
        objEAN128Bean.setFontName("Helvetica");
        objEAN128Bean.setFontSize(2);
        // checksum
        objEAN128Bean.setChecksumMode(ChecksumMode.CP_ADD);
        //objEAN128Bean.calcDimensions("415808888875560380200014616781565729143902"+importoInCentesimi);
        log.info("objEAN128Bean.getCheckDigitMarker(): "+objEAN128Bean.getCheckDigitMarker());
        BitmapCanvasProvider canvas = new BitmapCanvasProvider(out,
                "image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, true, 0);
        objEAN128Bean.generateBarcode(canvas, "415"+PagoPAConstants.globalLocationNumber+"8020"+codiceAvviso+"3902"+importoInCentesimi);
        //objEAN128Bean.generateBarcode(canvas, msg);
        
            canvas.finish();
            out.close();
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(GenerateBarcode.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (IOException ex) {
            java.util.logging.Logger.getLogger(GenerateBarcode.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        
    }

}
