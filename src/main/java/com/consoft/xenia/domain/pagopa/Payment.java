/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa;

import com.consoft.xenia.domain.StudentIntentions;

/**
 *
 * @author Administrator
 */
public class Payment {
    
    private String typeOfPayment;
    private Double total;
    private StudentIntentions[] studentIntentions;

    public String getTypeOfPayment() {
        return typeOfPayment;
    }
    
    public Payment typeOfPayment(String typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
        return this;
    }

    public void setTypeOfPayment(String typeOfPayment) {
        this.typeOfPayment = typeOfPayment;
    }

    public Double getTotal() {
        return total;
    }
    
    public Payment total(Double total) {
        this.total = total;
        return this;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public StudentIntentions[] getStudentIntentions() {
        return studentIntentions;
    }
    
    public Payment studentIntentions(StudentIntentions[] studentIntentions) {
        this.studentIntentions = studentIntentions;
        return this;
    }

    public void setStudentIntentions(StudentIntentions[] studentIntentions) {
        this.studentIntentions = studentIntentions;
    }
    
}
