/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni;


import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
public class GeneratePDF {
private final static org.slf4j.Logger log = LoggerFactory.getLogger(GenerateBarcode.class);
    /**
     * Creates a PDF file: hello.pdf
     *
     * @param args no arguments needed
     */
    /**
     * Creates a PDF document.
     *
     * @param filename the path to the new PDF document
     * @throws DocumentException
     * @throws IOException
     */
    public static void createPdf(String filename) {
        // step 1
        //Document document = new Document();
        float left = 15;
        float right = 30;
        float top = 60;
        float bottom = 0;
        
        Document document = new Document(PageSize.A4, left, right, top, bottom);
        try {

            // step 2
            PdfWriter.getInstance(document, new FileOutputStream(filename));
            // step 3
            document.open();
            // step 4
            //document.add(new Paragraph("Hello World!"));
            // step 5
            //add Image
            Image img = Image.getInstance(PagoPAConstants.barcodeImageName);
            img.scaleAbsolute(new Float(212.5),new Float(42.5));
            
            log.info("img.getWidth(): "+img.getWidth());
            
            //document.add(new Paragraph("Sample 1: This is simple image demo."));
            document.add(img);
            //document.add(new Paragraph("Sample 2: This is simple image demo."));
            //document.add(img2);
            // step 6
            //close pdf
            document.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeneratePDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(GeneratePDF.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeneratePDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
