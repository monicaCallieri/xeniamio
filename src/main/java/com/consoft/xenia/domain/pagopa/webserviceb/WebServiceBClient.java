/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webserviceb;

import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.DatiSingVersLista;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosLista;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.ObjectFactory;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.PaInviaCarrelloPosizioniRequest;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.PaInviaCarrelloPosizioniResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author Administrator
 */
public class WebServiceBClient extends WebServiceGatewaySupport {

    private InviaCarrPosLista inviaCarrPosLista = new InviaCarrPosLista();

    public InviaCarrPosLista getInviaCarrPosLista() {
        return inviaCarrPosLista;
    }

    public Set<DatiSingVersLista> createDatiSingVersLista(StudentIntentions[] datiSingVersLista) {
        /**
         * Manca la parte di implementazione che verifica che il numero di
         * elementi deve essere compreso tra 1 e 5 inclusi
         */
        /*Set<Integer> test = new HashSet();
        for (int j = 0; j <= 2; j++) {
            Integer k = j;
            System.out.println("l'elemento è stato aggiunto?: " + test.add(k));
        }*/

        Set<DatiSingVersLista> datiVersLista = new HashSet<>();
        //DatiSingVersLista[] versListaArray = new DatiSingVersLista[datiSingVersLista.length];
        
        for (int i = 0; i < datiSingVersLista.length; i++) {
            DatiSingVersLista vers = new DatiSingVersLista();
            //setto i dati del singolo versamento
            vers.setImportoSingoloVersamento(datiSingVersLista[i].getEducationalProposer().getPrice());
            vers.setCommissioniCaricoPa(PagoPAConstants.commissioniCaricoPa);
            vers.setIbanAccredito(PagoPAConstants.ibanAccredito);
            vers.setBicAccredito(PagoPAConstants.bicAccredito);
            vers.setIbanAppoggio(PagoPAConstants.ibanAppoggio);
            vers.setBicAppoggio(PagoPAConstants.bicAppoggio);
            vers.setCredenzialiPagatore("");
            vers.setCaulsaleVersamento(datiSingVersLista[i].getEducationalProposer().getName());
            
            //aggiungo il singolo versmento nella lista dei versamenti
            //System.out.println("Contiene l'elemento? "+datiVersLista.contains(vers));
            //versListaArray[i]=vers;
            //System.out.println("Elemento prezzo: " + versListaArray[i].getImportoSingoloVersamento());
            //System.out.println("Elemento causale: " + versListaArray[i].getCaulsaleVersamento());
            datiVersLista.add(vers);
            //System.out.println("l'elemento è stato aggiunto?: " + datiVersLista.add(vers));
        }
       /* for(int i=0; i<versListaArray.length; i++)
        System.out.println("l'elemento è stato aggiunto?: " + datiVersLista.add(versListaArray[i]));*/
        return datiVersLista;
    }

    public JAXBElement getPaInviaCarrelloPosizioneResponse(String spontaneo, String identificativoBeneficiario, String ccp,
            String codicePa, String codiceServizio, String tipoIdPagatore, String identificativoPagatore,
            String anagraficaPagatore, String indirizzoPagatore, String civicoPagatore, String capPagatore,
            String localitaPagatore, String provinciaPagatore, String codiceNazionePagatore, String emailPagatore,
            String dataScadenzaPagamento, Double importoPagamento, String tipoFirmaRicevuta, String tipoRiferimentoCreditore,
            String codiceRiferimentoCreditore, String identificativoUnivocoVersamento, StudentIntentions[] datiSingoloVersamento) {
        Date dob = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //List<PaInviaCarrelloPosizioniRequest> paInviaCarrelloPosizioniRequestList = new ArrayList<PaInviaCarrelloPosizioniRequest>();
        PaInviaCarrelloPosizioniRequest paInviaCarrelloPosizioniRequest = new PaInviaCarrelloPosizioniRequest();
        PaInviaCarrelloPosizioniResponse paInviaCarrelloPosizioniResponse = new PaInviaCarrelloPosizioniResponse();

        JAXBElement jresB = new ObjectFactory().createPaInviaCarrelloPosizioniResponse(paInviaCarrelloPosizioniResponse);
        try {
            dob = df.parse("2099-12-31");

            GregorianCalendar cal = new GregorianCalendar();
            XMLGregorianCalendar xmlDate3 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), dob.getHours(), dob.getMinutes(), dob.getSeconds(), DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);

            LocalDate dataScadenza = datiSingoloVersamento[0].getEducationalProposer().getStartdate();//For reference
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String dataScadenzaString = dataScadenza.format(formatter);

            //Costruisco la Richiesta InviaCarrPosLista
            inviaCarrPosLista.setSpontaneo(spontaneo);
            inviaCarrPosLista.setIdentificativoBeneficiario(identificativoBeneficiario);

            //I tre campi seguenti sono mutuamente esclusivi
            inviaCarrPosLista.setCcp(ccp);
            inviaCarrPosLista.setCodicePa(codicePa);
            inviaCarrPosLista.setCodiceServizio(codiceServizio);

            inviaCarrPosLista.setTipoIdPagatore(tipoIdPagatore);
            inviaCarrPosLista.setIdentificativoPagatore(identificativoPagatore);
            inviaCarrPosLista.setAnagraficaPagatore(anagraficaPagatore);
            inviaCarrPosLista.setIndirizzoPagatore(indirizzoPagatore);
            inviaCarrPosLista.setCivicoPagatore(civicoPagatore);
            inviaCarrPosLista.setCapPagatore(capPagatore);
            inviaCarrPosLista.setLocalitaPagatore(localitaPagatore);
            inviaCarrPosLista.setProvinciaPagatore(provinciaPagatore);
            inviaCarrPosLista.setCodiceNazionePagatore(codiceNazionePagatore);
            inviaCarrPosLista.setEmailPagatore(emailPagatore);
            inviaCarrPosLista.setDataScadenzaPagamento(dataScadenzaString);
            inviaCarrPosLista.setImportoPagamento(importoPagamento);
            inviaCarrPosLista.setTipoFirmaRicevuta(tipoFirmaRicevuta);
            inviaCarrPosLista.setTipoRiferimentoCreditore(tipoRiferimentoCreditore);
            inviaCarrPosLista.setCodiceRiferimentoCreditore(codiceRiferimentoCreditore);
            inviaCarrPosLista.setIdentificativoUnivocoVersamento(identificativoUnivocoVersamento);
            inviaCarrPosLista.setDatiSingVersListas(createDatiSingVersLista(datiSingoloVersamento));//dovrà essere un array di StudentIntentions
System.out.println("Importo carrello: "+inviaCarrPosLista.getImportoPagamento());
            //inserisco i dati del carrello nella request
            //paInviaCarrelloPosizioniRequest.setInviaCarrPosLista(inviaCarrPosLista);
            //paInviaCarrelloPosizioniRequest.setInviaCarrPosLista(inviaCarrPosLista);
            paInviaCarrelloPosizioniRequest.getInviaCarrPosLista().add(inviaCarrPosLista);
            //paInviaCarrelloPosizioniRequestList.add(paInviaCarrelloPosizioniRequest);

            //Composizione Risposta
            InviaCarrPosOutput inviaCarrPosOutput = new InviaCarrPosOutput();
            paInviaCarrelloPosizioniResponse.setPaInviaCarrelloPosizioniOutput(inviaCarrPosOutput);

            JAXBElement jreqB = new ObjectFactory().createPaInviaCarrelloPosizioniRequest(paInviaCarrelloPosizioniRequest);
            jresB = (JAXBElement) getWebServiceTemplate().marshalSendAndReceive(jreqB, new SoapRequestHeaderModifier(PagoPAConstants.paInviaCarrelloPosizioniUser, PagoPAConstants.paInviaCarrelloPosizioniPassword, PagoPAConstants.paInviaCarrelloPosizioniabiCode));
            System.out.println("risposta:" + jresB.toString());

        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(WebServiceBClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DatatypeConfigurationException ex) {
            java.util.logging.Logger.getLogger(WebServiceBClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jresB;
    }
}
