/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni;

//import com.documents4j.api.DocumentType;
import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.consoft.xenia.domain.pagopa.Payment;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
public class GeneratePdfFromPdfTemplate {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GeneratePdfFromPdfTemplate.class);

    public static void generatePdf(String src, String dest, String codiceAvviso, Payment payment) {

        try {

            payment.getStudentIntentions()[0].getEducationalProposer();

            PdfReader reader = new PdfReader(src);
            System.out.println(System.getProperty("java.io.tmpdir"));
            FileOutputStream fos = new FileOutputStream(dest);
            PdfStamper stamper = new PdfStamper(reader, fos);
            PdfContentByte canvas = stamper.getOverContent(1);
            //Cognome Debitore
            ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase(payment.getStudentIntentions()[0].getUser().getLastName() + ", " + payment.getStudentIntentions()[0].getUser().getFirstName()), 382, 625, 0);
            //Nome Debitore
            //ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase("Marco"), 455, 625, 0);
            //Indirizzo Debitore
            ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase("Via Torino 127"), 382, 611, 0);
            //Cap Debitore
            ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase("10127, Torino"), 382, 597, 0);
            //Codice Fiscale
            ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase("_CodiceFiscale_??"), 417, 571, 0);

            //Codice Avviso Centrale
            ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase(codiceAvviso), 245, 529, 0);
            //Importo
            ColumnText.showTextAligned(canvas, Element.ALIGN_MIDDLE, new Phrase(payment.getTotal().toString()), 245, 512, 0);
            //DataScadenza
            SimpleDateFormat actual = new SimpleDateFormat("yyyy-MM-dd");

            ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(payment.getStudentIntentions()[0].getEducationalProposer().getStartdate().toString()), 245, 495, 0);
            //IUV Operazione
            ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(codiceAvviso.substring(3)), 402, 463, 0);

            //Tipologia Servizio
            ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(payment.getStudentIntentions()[0].getEducationalProposer().getName()), 128, 334, 0);
            //Causale Pagamento
            ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(payment.getStudentIntentions()[0].getEducationalProposer().getTypeOfCourse()), 42, 308, 0);
            //Importo
            ColumnText.showTextAligned(canvas, Element.ALIGN_RIGHT, new Phrase(payment.getTotal().toString()), 569, 308, 0);

            //Generazione Barcode EAN128 univoco per codice avviso
            Integer prezzoCent = (int) (payment.getTotal() * 100);

            GenerateBarcode ean128 = new GenerateBarcode(codiceAvviso, prezzoCent.toString());
            PdfContentByte content = stamper.getOverContent(reader.getNumberOfPages());
            Image img = Image.getInstance(PagoPAConstants.pdfGeneratedLocation + codiceAvviso + ".png");
            img.scaleAbsolute(new Float(212.5), new Float(42.5));
            //img.setAbsolutePosition(505f, 735f);
            img.setAbsolutePosition(40, 20);
            content.addImage(img);

            //Generazione QRCode
            GenerateQrCode qrCodeGenerator = new GenerateQrCode();
            Image qrcodeImage = qrCodeGenerator.getQrCode().getImage();
            qrcodeImage.setAbsolutePosition(500, 20);
            qrcodeImage.scalePercent(200);
            content.addImage(qrcodeImage);
            //fos.close();
            stamper.close();
            reader.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeneratePdfFromPdfTemplate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadElementException ex) {
            Logger.getLogger(GeneratePdfFromPdfTemplate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GeneratePdfFromPdfTemplate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(GeneratePdfFromPdfTemplate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*public static void deleteBarcodeImage(String codiceAvviso) {
        //Rimozione png da file system
        
            File barcodeEAN128Image = new File(PagoPAConstants.pdfGeneratedLocation + codiceAvviso + ".png");
            log.debug("canWrite?:" + barcodeEAN128Image.canWrite());
            if (barcodeEAN128Image.delete()) {
                log.debug(barcodeEAN128Image.getName() + " is deleted!");
            } else {
                log.debug("Delete operation of " + barcodeEAN128Image.getName() + " is failed.");
            }
        /*catch (Exception e) {
            log.debug("Delete operation is failed due to " + e.getMessage());
            e.printStackTrace();
        }
    }*/
}
