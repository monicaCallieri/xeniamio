//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2017.03.21 alle 03:05:46 PM CET 
//


package com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per paInviaCarrelloPosizioniRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="paInviaCarrelloPosizioniRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="paInviaCarrelloPosizioniLista" type="{http://services.sia.eu/}paInviaCarrelloPosizioniListaType" maxOccurs="5"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paInviaCarrelloPosizioniRequest", propOrder = {
    "paInviaCarrelloPosizioniListaType"
})
public class PaInviaCarrelloPosizioniRequest {

    @XmlElement(name="paInviaCarrelloPosizioniLista", required = true)
    protected List<InviaCarrPosLista> paInviaCarrelloPosizioniListaType;

    /**
     * Gets the value of the paInviaCarrelloPosizioniLista property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paInviaCarrelloPosizioniLista property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaInviaCarrelloPosizioniLista().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaInviaCarrelloPosizioniListaType }
     * 
     * 
     */
    public List<InviaCarrPosLista> getInviaCarrPosLista() {
        if (paInviaCarrelloPosizioniListaType == null) {
            paInviaCarrelloPosizioniListaType = new ArrayList<InviaCarrPosLista>();
        }
        return this.paInviaCarrelloPosizioniListaType;
    }
    
   /* public void setInviaCarrPosLista(InviaCarrPosLista value) {
        this.paInviaCarrelloPosizioniListaType.add(value);
    }*/
    

}
