/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa;

/**
 *
 * @author Administrator
 */
public final class PagoPAConstants {
    //Regex for acceptable logins

    public static final String gestorePosizioniUser = "WS9000777TEST###";
    public static final String gestorePosizioniPassword = "02008PWDINSPOS###TEST";
    public static final String paInviaCarrelloPosizioniUser = "WSCAR02008U";
    public static final String paInviaCarrelloPosizioniPassword = "c20d3d5fac14a42";
    public static final String globalLocationNumber = "8088888755603";
    public static final String barcodeImageName = "EAN128.png";
    public static final String identificativoBeneficiario = "80213750583";
    public static final String tipo_riferimento_creditore = "universita";
    public static final String ibanAppoggio = "0123456789";
    public static final String ibanAccredito = "0123456789";
    public static final String bicAccredito = "0123456789";
    public static final String bicAppoggio = "0123456789";
    public static final String ccp = "";
    public static final String codiceServizio = "0000001";
    public static final String codicePa = "";
    public static final String paInviaCarrelloPosizioniabiCode = "02008";
    public static final double moduleWidthmm = 75;
    public static final double moduleHeightmm = 14;
    public static final double commissioniCaricoPa = 1.50;

    public static final String iuvModello1 = "MODELLO1";
    public static final String iuvModello3 = "MODELLO3";

    public static final String barcodePayToLoc = "415";
    public static final String barcodeRefNo = "8020";
    public static final String barcodeAmount = "3902";
    public static final String auxDigit = "0";
    public static final String applicationCode = "01";
    public static final String qrCodeVersion = "002";
    public static final String qrCodeSeparator = "|";
    public static final String pagoPAString = "PAGOPA";
    public static final String codiceFiscaleUnistrapg = "80002630541";
    
    public static final String pdfTemplate = System.getProperty("user.dir") + "\\src\\main\\resources\\templateAgid.pdf";
    //public static final String pdfGeneratedLocation = System.getProperty("user.dir") + "\\src\\main\\resources\\";
    public static final String pdfGeneratedLocation = System.getProperty("java.io.tmpdir");
    

}
