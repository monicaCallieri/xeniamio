//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.11 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2017.03.21 alle 12:16:04 PM CET 
//


package com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per inserimentoPosizioneRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="inserimentoPosizioneRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="inserimentoPosizioneInput" type="{http://services.sia.eu/}inserimentoPosizioneInputType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inserimentoPosizioneRequest", propOrder = {
    "inserimentoPosizioneInput"
})
public class InserimentoPosizioneRequest {

    @XmlElement(required = true)
    protected InserimentoPosizioneInputType inserimentoPosizioneInput;

    /**
     * Recupera il valore della proprietà inserimentoPosizioneInput.
     * 
     * @return
     *     possible object is
     *     {@link InserimentoPosizioneInputType }
     *     
     */
    public InserimentoPosizioneInputType getInserimentoPosizioneInput() {
        return inserimentoPosizioneInput;
    }

    /**
     * Imposta il valore della proprietà inserimentoPosizioneInput.
     * 
     * @param value
     *     allowed object is
     *     {@link InserimentoPosizioneInputType }
     *     
     */
    public void setInserimentoPosizioneInput(InserimentoPosizioneInputType value) {
        this.inserimentoPosizioneInput = value;
    }

}
