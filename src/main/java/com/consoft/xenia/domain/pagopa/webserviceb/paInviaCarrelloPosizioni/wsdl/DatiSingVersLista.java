package com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * A DatiSingVersLista.
 */

/**
 * <p>Classe Java per datiSingoloVersamentoListaType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="datiSingoloVersamentoListaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="importoSingoloVersamento" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="commissioniCaricoPa" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="ibanAccredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bicAccredito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ibanAppoggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bicAppoggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="credenzialiPagatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="causaleVersamento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */

@Entity
@Table(name = "dati_sing_vers_lista")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datiSingoloVersamentoListaType", propOrder = {
    "importoSingoloVersamento",
    "commissioniCaricoPa",
    "ibanAccredito",
    "bicAccredito",
    "ibanAppoggio",
    "bicAppoggio",
    "credenzialiPagatore",
    "causaleVersamento"
})
public class DatiSingVersLista implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlTransient
    private Long id;

    @NotNull
    @Column(name = "importo_singolo_versamento", nullable = false)
    @XmlElement(name = "importoSingoloVersamento" , required = true)
    private Double importoSingoloVersamento;

    @NotNull
    @Column(name = "commissioni_carico_pa", nullable = false)
    @XmlElement(name = "commissioniCaricoPa" ,required = true)
    private Double commissioniCaricoPa;

    @Column(name = "iban_accredito")
    @XmlElement(name = "ibanAccredito")
    private String ibanAccredito;

    @Column(name = "bic_accredito")
    @XmlElement(name = "bicAccredito")
    private String bicAccredito;

    @Column(name = "iban_appoggio")
    @XmlElement(name = "ibanAppoggio")
    private String ibanAppoggio;

    @Column(name = "bic_appoggio")
    @XmlElement(name = "bicAppoggio")
    private String bicAppoggio;

    @Column(name = "credenziali_pagatore")
    @XmlElement(name = "credenzialiPagatore")
    private String credenzialiPagatore;

    @NotNull
    @Column(name = "caulsale_versamento", nullable = false)
    @XmlElement(name = "causaleVersamento" ,required = true)
    private String causaleVersamento;

    @ManyToOne
    @XmlTransient
    private InviaCarrPosLista inviaCarrPosLista;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getImportoSingoloVersamento() {
        return importoSingoloVersamento;
    }

    public DatiSingVersLista importoSingoloVersamento(Double importoSingoloVersamento) {
        this.importoSingoloVersamento = importoSingoloVersamento;
        return this;
    }

    public void setImportoSingoloVersamento(Double importoSingoloVersamento) {
        this.importoSingoloVersamento = importoSingoloVersamento;
    }

    public Double getCommissioniCaricoPa() {
        return commissioniCaricoPa;
    }

    public DatiSingVersLista commissioniCaricoPa(Double commissioniCaricoPa) {
        this.commissioniCaricoPa = commissioniCaricoPa;
        return this;
    }

    public void setCommissioniCaricoPa(Double commissioniCaricoPa) {
        this.commissioniCaricoPa = commissioniCaricoPa;
    }

    public String getIbanAccredito() {
        return ibanAccredito;
    }

    public DatiSingVersLista ibanAccredito(String ibanAccredito) {
        this.ibanAccredito = ibanAccredito;
        return this;
    }

    public void setIbanAccredito(String ibanAccredito) {
        this.ibanAccredito = ibanAccredito;
    }

    public String getBicAccredito() {
        return bicAccredito;
    }

    public DatiSingVersLista bicAccredito(String bicAccredito) {
        this.bicAccredito = bicAccredito;
        return this;
    }

    public void setBicAccredito(String bicAccredito) {
        this.bicAccredito = bicAccredito;
    }

    public String getIbanAppoggio() {
        return ibanAppoggio;
    }

    public DatiSingVersLista ibanAppoggio(String ibanAppoggio) {
        this.ibanAppoggio = ibanAppoggio;
        return this;
    }

    public void setIbanAppoggio(String ibanAppoggio) {
        this.ibanAppoggio = ibanAppoggio;
    }

    public String getBicAppoggio() {
        return bicAppoggio;
    }

    public DatiSingVersLista bicAppoggio(String bicAppoggio) {
        this.bicAppoggio = bicAppoggio;
        return this;
    }

    public void setBicAppoggio(String bicAppoggio) {
        this.bicAppoggio = bicAppoggio;
    }

    public String getCredenzialiPagatore() {
        return credenzialiPagatore;
    }

    public DatiSingVersLista credenzialiPagatore(String credenzialiPagatore) {
        this.credenzialiPagatore = credenzialiPagatore;
        return this;
    }

    public void setCredenzialiPagatore(String credenzialiPagatore) {
        this.credenzialiPagatore = credenzialiPagatore;
    }

    public String getCaulsaleVersamento() {
        return causaleVersamento;
    }

    public DatiSingVersLista caulsaleVersamento(String caulsaleVersamento) {
        this.causaleVersamento = caulsaleVersamento;
        return this;
    }

    public void setCaulsaleVersamento(String caulsaleVersamento) {
        this.causaleVersamento = caulsaleVersamento;
    }

    public InviaCarrPosLista getInviaCarrPosLista() {
        return inviaCarrPosLista;
    }

    public DatiSingVersLista inviaCarrPosLista(InviaCarrPosLista inviaCarrPosLista) {
        this.inviaCarrPosLista = inviaCarrPosLista;
        return this;
    }

    public void setInviaCarrPosLista(InviaCarrPosLista inviaCarrPosLista) {
        this.inviaCarrPosLista = inviaCarrPosLista;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatiSingVersLista datiSingVersLista = (DatiSingVersLista) o;
        if (datiSingVersLista.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, datiSingVersLista.id);
    }

    @Override
    public int hashCode() {
        if(id==null){
            String value = importoSingoloVersamento.toString();//.concat(causaleVersamento);
            return Objects.hashCode(value);
        }else
            return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DatiSingVersLista{" +
            "id=" + id +
            ", importoSingoloVersamento='" + importoSingoloVersamento + "'" +
            ", commissioniCaricoPa='" + commissioniCaricoPa + "'" +
            ", ibanAccredito='" + ibanAccredito + "'" +
            ", bicAccredito='" + bicAccredito + "'" +
            ", ibanAppoggio='" + ibanAppoggio + "'" +
            ", bicAppoggio='" + bicAppoggio + "'" +
            ", credenzialiPagatore='" + credenzialiPagatore + "'" +
            ", causaleVersamento='" + causaleVersamento + "'" +
            '}';
    }
    
    /*@Override
    public int hashCode() {
        return Integer.valueOf(this.toString());
    }*/
}
