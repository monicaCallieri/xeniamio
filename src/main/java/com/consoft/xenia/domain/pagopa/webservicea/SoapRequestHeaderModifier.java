/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consoft.xenia.domain.pagopa.webservicea;


import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.GestorePosizioniHeader;
import java.io.IOException;
import java.io.StringWriter;
import java.security.Security;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;

import org.springframework.xml.transform.StringSource;

/**
 *
 * @author Administrator
 */
public class SoapRequestHeaderModifier implements WebServiceMessageCallback{
    
    private String user;
    private String password;
    SoapRequestHeaderModifier(String user, String password) {
        this.user=user;
        this.password=password;
    }

    @Override
    public void doWithMessage(WebServiceMessage wsm) throws IOException, TransformerException {
        SoapHeader header = ((SoapMessage)wsm).getSoapHeader();
		
		StringSource headerSource = new StringSource(prepareSecurityHeader());
        
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(headerSource, header.getResult());
    }
    
    private String prepareSecurityHeader() {
		String result = "";
		StringWriter sw = new StringWriter();
                /*Costruisco l'HEADER*/
            GestorePosizioniHeader gestorePosizioniHeader = new GestorePosizioniHeader();
            gestorePosizioniHeader.setUser(this.user);
            gestorePosizioniHeader.setPassword(this.password);
        try {
            JAXBContext carContext = JAXBContext.newInstance(GestorePosizioniHeader.class);
            Marshaller carMarshaller = carContext.createMarshaller();
            carMarshaller.marshal(gestorePosizioniHeader, sw);
            result = sw.toString();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
		return result;
	}

}
