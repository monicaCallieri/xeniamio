/*
 * IUV: composto da Base (max 13n) + check digit (2n). 
 * Le prime 13 cifre sono univoche per singoloincasso e non possono replicarsi mai 
 * a livello di tutti gli incassi del Politecnico. 
 * 
 * 
 */
package com.consoft.xenia.domain.pagopa.webservicea;

import com.consoft.xenia.domain.pagopa.PagoPAConstants;

/**
 *
 * @author Administrator
 */
public class IUV {

    private ISO11649 iso11649;

    public ISO11649 getIso11649() {
        return iso11649;
    }
    //private String iuvBase;
    private String iuvCode;

    

    public IUV(String reference, String modelType) {
        switch (modelType) {
            case PagoPAConstants.iuvModello1:
                iso11649 = new ISO11649(reference); //MODELLO1 identifica lo IUV ALFANUMERICO
                iuvCode = iso11649.getIso11649();
                break;
            case PagoPAConstants.iuvModello3:
                iuvCode = reference;
                break;
        }

    }
    
    public String getIuvCode() {
        return iuvCode;
    }
    
}
