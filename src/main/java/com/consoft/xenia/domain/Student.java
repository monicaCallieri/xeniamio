package com.consoft.xenia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.consoft.xenia.domain.enumeration.Sex;

import com.consoft.xenia.domain.enumeration.Qualification;

import com.consoft.xenia.domain.enumeration.Level;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The Student entity.
 */
@ApiModel(description = "The Student entity.")
@Entity
@Table(name = "student")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex")
    private Sex sex;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "city_of_birth")
    private String cityOfBirth;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "address")
    private String address;

    @Column(name = "country")
    private String country;

    @Column(name = "state_or_province")
    private String stateOrProvince;

    @Column(name = "city")
    private String city;

    @Column(name = "profession")
    private String profession;

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(name = "qualification")
    private Qualification qualification;

    @Enumerated(EnumType.STRING)
    @Column(name = "knowledge_of_italian")
    private Level knowledgeOfItalian;

    @Column(name = "already_student")
    private Boolean alreadyStudent;

    @Column(name = "previous_attend_year")
    private Integer previousAttendYear;

    @Column(name = "scholarship")
    private Boolean scholarship;

    @Column(name = "scholarship_start_date")
    private LocalDate scholarshipStartDate;

    @Column(name = "scholarship_end_date")
    private LocalDate scholarshipEndDate;

    @Column(name = "scholarship_awarded_by")
    private String scholarshipAwardedBy;

    @Column(name = "privacy_authorize")
    private Boolean privacyAuthorize;

    @Column(name = "registration_date")
    private LocalDate registrationDate;

    @Column(name = "student_number")
    private Integer studentNumber;

    @Column(name = "postal_code")
    private String postal_code;

    @Column(name = "mobile_phone")
    private String mobile_phone;

    @Column(name = "mother_tongue")
    private String mother_tongue;

    @Column(name = "other_languages")
    private String other_languages;

    @Column(name = "italian_studied_in")
    private String italian_studied_in;

    @Column(name = "italian_studied_for")
    private String italian_studied_for;

    @Column(name = "italian_studied_hour_week")
    private String italian_studied_hour_week;

    @Column(name = "italian_certification")
    private String italian_certification;

    @Column(name = "italian_level")
    private String italian_level;

    @Column(name = "how_long_student_at_unistrapg")
    private Integer how_long_student_at_unistrapg;

    @Column(name = "accomodation")
    private Boolean accomodation;

    @Column(name = "learning_disability")
    private String learning_disability;

    @Column(name = "physical_disability")
    private String physical_disability;

    @Column(name = "other_special_requests")
    private String other_special_requests;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "student")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<StudentCourse> ids = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "student_placement_test",
               joinColumns = @JoinColumn(name="students_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="placement_tests_id", referencedColumnName="ID"))
    private Set<PlacementTest> placementTests = new HashSet<>();

    @Transient
   // @JsonInclude
    private Response[] iois;
    
    //@Transient
    //@JsonIgnoreProperties(ignoreUnknown = true)
    //private String test;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Student name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public Student surname(String surname) {
        this.surname = surname;
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Sex getSex() {
        return sex;
    }

    public Student sex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public Student email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Student phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCityOfBirth() {
        return cityOfBirth;
    }

    public Student cityOfBirth(String cityOfBirth) {
        this.cityOfBirth = cityOfBirth;
        return this;
    }

    public void setCityOfBirth(String cityOfBirth) {
        this.cityOfBirth = cityOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public Student nationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public Student address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public Student country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public Student stateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
        return this;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public String getCity() {
        return city;
    }

    public Student city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfession() {
        return profession;
    }

    public Student profession(String profession) {
        this.profession = profession;
        return this;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public Student dateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public Student qualification(Qualification qualification) {
        this.qualification = qualification;
        return this;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public Level getKnowledgeOfItalian() {
        return knowledgeOfItalian;
    }

    public Student knowledgeOfItalian(Level knowledgeOfItalian) {
        this.knowledgeOfItalian = knowledgeOfItalian;
        return this;
    }

    public void setKnowledgeOfItalian(Level knowledgeOfItalian) {
        this.knowledgeOfItalian = knowledgeOfItalian;
    }

    public Boolean isAlreadyStudent() {
        return alreadyStudent;
    }

    public Student alreadyStudent(Boolean alreadyStudent) {
        this.alreadyStudent = alreadyStudent;
        return this;
    }

    public void setAlreadyStudent(Boolean alreadyStudent) {
        this.alreadyStudent = alreadyStudent;
    }

    public Integer getPreviousAttendYear() {
        return previousAttendYear;
    }

    public Student previousAttendYear(Integer previousAttendYear) {
        this.previousAttendYear = previousAttendYear;
        return this;
    }

    public void setPreviousAttendYear(Integer previousAttendYear) {
        this.previousAttendYear = previousAttendYear;
    }

    public Boolean isScholarship() {
        return scholarship;
    }

    public Student scholarship(Boolean scholarship) {
        this.scholarship = scholarship;
        return this;
    }

    public void setScholarship(Boolean scholarship) {
        this.scholarship = scholarship;
    }

    public LocalDate getScholarshipStartDate() {
        return scholarshipStartDate;
    }

    public Student scholarshipStartDate(LocalDate scholarshipStartDate) {
        this.scholarshipStartDate = scholarshipStartDate;
        return this;
    }

    public void setScholarshipStartDate(LocalDate scholarshipStartDate) {
        this.scholarshipStartDate = scholarshipStartDate;
    }

    public LocalDate getScholarshipEndDate() {
        return scholarshipEndDate;
    }

    public Student scholarshipEndDate(LocalDate scholarshipEndDate) {
        this.scholarshipEndDate = scholarshipEndDate;
        return this;
    }

    public void setScholarshipEndDate(LocalDate scholarshipEndDate) {
        this.scholarshipEndDate = scholarshipEndDate;
    }

    public String getScholarshipAwardedBy() {
        return scholarshipAwardedBy;
    }

    public Student scholarshipAwardedBy(String scholarshipAwardedBy) {
        this.scholarshipAwardedBy = scholarshipAwardedBy;
        return this;
    }

    public void setScholarshipAwardedBy(String scholarshipAwardedBy) {
        this.scholarshipAwardedBy = scholarshipAwardedBy;
    }

    public Boolean isPrivacyAuthorize() {
        return privacyAuthorize;
    }

    public Student privacyAuthorize(Boolean privacyAuthorize) {
        this.privacyAuthorize = privacyAuthorize;
        return this;
    }

    public void setPrivacyAuthorize(Boolean privacyAuthorize) {
        this.privacyAuthorize = privacyAuthorize;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public Student registrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getStudentNumber() {
        return studentNumber;
    }

    public Student studentNumber(Integer studentNumber) {
        this.studentNumber = studentNumber;
        return this;
    }

    public void setStudentNumber(Integer studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public Student postal_code(String postal_code) {
        this.postal_code = postal_code;
        return this;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public Student mobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
        return this;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getMother_tongue() {
        return mother_tongue;
    }

    public Student mother_tongue(String mother_tongue) {
        this.mother_tongue = mother_tongue;
        return this;
    }

    public void setMother_tongue(String mother_tongue) {
        this.mother_tongue = mother_tongue;
    }

    public String getOther_languages() {
        return other_languages;
    }

    public Student other_languages(String other_languages) {
        this.other_languages = other_languages;
        return this;
    }

    public void setOther_languages(String other_languages) {
        this.other_languages = other_languages;
    }

    public String getItalian_studied_in() {
        return italian_studied_in;
    }

    public Student italian_studied_in(String italian_studied_in) {
        this.italian_studied_in = italian_studied_in;
        return this;
    }

    public void setItalian_studied_in(String italian_studied_in) {
        this.italian_studied_in = italian_studied_in;
    }

    public String getItalian_studied_for() {
        return italian_studied_for;
    }

    public Student italian_studied_for(String italian_studied_for) {
        this.italian_studied_for = italian_studied_for;
        return this;
    }

    public void setItalian_studied_for(String italian_studied_for) {
        this.italian_studied_for = italian_studied_for;
    }

    public String getItalian_studied_hour_week() {
        return italian_studied_hour_week;
    }

    public Student italian_studied_hour_week(String italian_studied_hour_week) {
        this.italian_studied_hour_week = italian_studied_hour_week;
        return this;
    }

    public void setItalian_studied_hour_week(String italian_studied_hour_week) {
        this.italian_studied_hour_week = italian_studied_hour_week;
    }

    public String getItalian_certification() {
        return italian_certification;
    }

    public Student italian_certification(String italian_certification) {
        this.italian_certification = italian_certification;
        return this;
    }

    public void setItalian_certification(String italian_certification) {
        this.italian_certification = italian_certification;
    }

    public String getItalian_level() {
        return italian_level;
    }

    public Student italian_level(String italian_level) {
        this.italian_level = italian_level;
        return this;
    }

    public void setItalian_level(String italian_level) {
        this.italian_level = italian_level;
    }

    public Integer getHow_long_student_at_unistrapg() {
        return how_long_student_at_unistrapg;
    }

    public Student how_long_student_at_unistrapg(Integer how_long_student_at_unistrapg) {
        this.how_long_student_at_unistrapg = how_long_student_at_unistrapg;
        return this;
    }

    public void setHow_long_student_at_unistrapg(Integer how_long_student_at_unistrapg) {
        this.how_long_student_at_unistrapg = how_long_student_at_unistrapg;
    }

    public Boolean isAccomodation() {
        return accomodation;
    }

    public Student accomodation(Boolean accomodation) {
        this.accomodation = accomodation;
        return this;
    }

    public void setAccomodation(Boolean accomodation) {
        this.accomodation = accomodation;
    }

    public String getLearning_disability() {
        return learning_disability;
    }

    public Student learning_disability(String learning_disability) {
        this.learning_disability = learning_disability;
        return this;
    }

    public void setLearning_disability(String learning_disability) {
        this.learning_disability = learning_disability;
    }

    public String getPhysical_disability() {
        return physical_disability;
    }

    public Student physical_disability(String physical_disability) {
        this.physical_disability = physical_disability;
        return this;
    }

    public void setPhysical_disability(String physical_disability) {
        this.physical_disability = physical_disability;
    }

    public String getOther_special_requests() {
        return other_special_requests;
    }

    public Student other_special_requests(String other_special_requests) {
        this.other_special_requests = other_special_requests;
        return this;
    }

    public void setOther_special_requests(String other_special_requests) {
        this.other_special_requests = other_special_requests;
    }

    public User getUser() {
        return user;
    }

    public Student user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<StudentCourse> getIds() {
        return ids;
    }

    public Student ids(Set<StudentCourse> studentCourses) {
        this.ids = studentCourses;
        return this;
    }

    public Student addId(StudentCourse studentCourse) {
        ids.add(studentCourse);
        studentCourse.setStudent(this);
        return this;
    }

    public Student removeId(StudentCourse studentCourse) {
        ids.remove(studentCourse);
        studentCourse.setStudent(null);
        return this;
    }

    public void setIds(Set<StudentCourse> studentCourses) {
        this.ids = studentCourses;
    }

    public Set<PlacementTest> getPlacementTests() {
        return placementTests;
    }

    public Student placementTests(Set<PlacementTest> placementTests) {
        this.placementTests = placementTests;
        return this;
    }

    public Student addPlacementTest(PlacementTest placementTest) {
        placementTests.add(placementTest);
        placementTest.getStudents().add(this);
        return this;
    }

    public Student removePlacementTest(PlacementTest placementTest) {
        placementTests.remove(placementTest);
        placementTest.getStudents().remove(this);
        return this;
    }

    public void setPlacementTests(Set<PlacementTest> placementTests) {
        this.placementTests = placementTests;
    }

    //Response[] interestsOfItalianStudy
    public Response[] getIois() {
        return iois;
    }

    public Student iois(Response[] iois) {
        this.iois = iois;
        return this;
    }

    public void setIois(Response[] iois) {
        this.iois = iois;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        if (student.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, student.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Student{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", surname='" + surname + "'" +
            ", sex='" + sex + "'" +
            ", email='" + email + "'" +
            ", phoneNumber='" + phoneNumber + "'" +
            ", cityOfBirth='" + cityOfBirth + "'" +
            ", nationality='" + nationality + "'" +
            ", address='" + address + "'" +
            ", country='" + country + "'" +
            ", stateOrProvince='" + stateOrProvince + "'" +
            ", city='" + city + "'" +
            ", profession='" + profession + "'" +
            ", dateOfBirth='" + dateOfBirth + "'" +
            ", qualification='" + qualification + "'" +
            ", knowledgeOfItalian='" + knowledgeOfItalian + "'" +
            ", alreadyStudent='" + alreadyStudent + "'" +
            ", previousAttendYear='" + previousAttendYear + "'" +
            ", scholarship='" + scholarship + "'" +
            ", scholarshipStartDate='" + scholarshipStartDate + "'" +
            ", scholarshipEndDate='" + scholarshipEndDate + "'" +
            ", scholarshipAwardedBy='" + scholarshipAwardedBy + "'" +
            ", privacyAuthorize='" + privacyAuthorize + "'" +
            ", registrationDate='" + registrationDate + "'" +
            ", studentNumber='" + studentNumber + "'" +
            ", postal_code='" + postal_code + "'" +
            ", mobile_phone='" + mobile_phone + "'" +
            ", mother_tongue='" + mother_tongue + "'" +
            ", other_languages='" + other_languages + "'" +
            ", italian_studied_in='" + italian_studied_in + "'" +
            ", italian_studied_for='" + italian_studied_for + "'" +
            ", italian_studied_hour_week='" + italian_studied_hour_week + "'" +
            ", italian_certification='" + italian_certification + "'" +
            ", italian_level='" + italian_level + "'" +
            ", how_long_student_at_unistrapg='" + how_long_student_at_unistrapg + "'" +
            ", accomodation='" + accomodation + "'" +
            ", learning_disability='" + learning_disability + "'" +
            ", physical_disability='" + physical_disability + "'" +
            ", other_special_requests='" + other_special_requests + "'" +
            '}';
    }
}
