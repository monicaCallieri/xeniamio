package com.consoft.xenia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.consoft.xenia.domain.enumeration.CourseLevel;

/**
 * The Course entity.
 */
@ApiModel(description = "The Course entity.")
@Entity
@Table(name = "course")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "duration")
    private Integer duration;

    /**
     * ricavata da endDate - startDate
     */
    @ApiModelProperty(value = "ricavata da endDate - startDate")
    @Column(name = "price")
    private Double price;

    /**
     * prezzo per il corso
     */
    @ApiModelProperty(value = "prezzo per il corso")
    @Column(name = "intensive")
    private Boolean intensive;

    @Column(name = "marco_polo")
    private Boolean marcoPolo;

    @Column(name = "turandot")
    private Boolean turandot;

    @Enumerated(EnumType.STRING)
    @Column(name = "level")
    private CourseLevel level;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    /**
     * MMM-AAAA Giorno della creazione
     */
    @ApiModelProperty(value = "MMM-AAAA Giorno della creazione")
    @Column(name = "max_stud_num")
    private Integer maxStudNum;

    /**
     * numero massimo di studenti che possono essere iscritti
     */
    @ApiModelProperty(value = "numero massimo di studenti che possono essere iscritti")
    @Column(name = "start_date")
    private LocalDate startDate;

    /**
     * giorno effettivo di inizio del corso che non puo essere un festivo
     */
    @ApiModelProperty(value = "giorno effettivo di inizio del corso che non puo essere un festivo")
    @Column(name = "end_date")
    private LocalDate endDate;

    @OneToMany(mappedBy = "course")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<StudentCourse> ids = new HashSet<>();

    @ManyToOne
    private Lecturer lecturer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Course code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Course description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuration() {
        return duration;
    }

    public Course duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Double getPrice() {
        return price;
    }

    public Course price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean isIntensive() {
        return intensive;
    }

    public Course intensive(Boolean intensive) {
        this.intensive = intensive;
        return this;
    }

    public void setIntensive(Boolean intensive) {
        this.intensive = intensive;
    }

    public Boolean isMarcoPolo() {
        return marcoPolo;
    }

    public Course marcoPolo(Boolean marcoPolo) {
        this.marcoPolo = marcoPolo;
        return this;
    }

    public void setMarcoPolo(Boolean marcoPolo) {
        this.marcoPolo = marcoPolo;
    }

    public Boolean isTurandot() {
        return turandot;
    }

    public Course turandot(Boolean turandot) {
        this.turandot = turandot;
        return this;
    }

    public void setTurandot(Boolean turandot) {
        this.turandot = turandot;
    }

    public CourseLevel getLevel() {
        return level;
    }

    public Course level(CourseLevel level) {
        this.level = level;
        return this;
    }

    public void setLevel(CourseLevel level) {
        this.level = level;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Course creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getMaxStudNum() {
        return maxStudNum;
    }

    public Course maxStudNum(Integer maxStudNum) {
        this.maxStudNum = maxStudNum;
        return this;
    }

    public void setMaxStudNum(Integer maxStudNum) {
        this.maxStudNum = maxStudNum;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Course startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Course endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Set<StudentCourse> getIds() {
        return ids;
    }

    public Course ids(Set<StudentCourse> studentCourses) {
        this.ids = studentCourses;
        return this;
    }

    public Course addId(StudentCourse studentCourse) {
        ids.add(studentCourse);
        studentCourse.setCourse(this);
        return this;
    }

    public Course removeId(StudentCourse studentCourse) {
        ids.remove(studentCourse);
        studentCourse.setCourse(null);
        return this;
    }

    public void setIds(Set<StudentCourse> studentCourses) {
        this.ids = studentCourses;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public Course lecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
        return this;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Course course = (Course) o;
        if (course.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, course.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Course{" +
            "id=" + id +
            ", code='" + code + "'" +
            ", description='" + description + "'" +
            ", duration='" + duration + "'" +
            ", price='" + price + "'" +
            ", intensive='" + intensive + "'" +
            ", marcoPolo='" + marcoPolo + "'" +
            ", turandot='" + turandot + "'" +
            ", level='" + level + "'" +
            ", creationDate='" + creationDate + "'" +
            ", maxStudNum='" + maxStudNum + "'" +
            ", startDate='" + startDate + "'" +
            ", endDate='" + endDate + "'" +
            '}';
    }
}
