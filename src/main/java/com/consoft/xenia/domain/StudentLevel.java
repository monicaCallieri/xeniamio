package com.consoft.xenia.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.consoft.xenia.domain.enumeration.CourseLevel;

/**
 * The StudentLevel entity.
 */
@ApiModel(description = "The StudentLevel entity.")
@Entity
@Table(name = "student_level")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StudentLevel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "level")
    private CourseLevel level;

    @Column(name = "type")
    private String type;

    @Column(name = "date")
    private LocalDate date;

    @ManyToOne
    private Student student;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CourseLevel getLevel() {
        return level;
    }

    public StudentLevel level(CourseLevel level) {
        this.level = level;
        return this;
    }

    public void setLevel(CourseLevel level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public StudentLevel type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getDate() {
        return date;
    }

    public StudentLevel date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Student getStudent() {
        return student;
    }

    public StudentLevel student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StudentLevel studentLevel = (StudentLevel) o;
        if (studentLevel.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, studentLevel.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "StudentLevel{" +
            "id=" + id +
            ", level='" + level + "'" +
            ", type='" + type + "'" +
            ", date='" + date + "'" +
            '}';
    }
}
