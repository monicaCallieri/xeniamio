package com.consoft.xenia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A PlacementTest.
 */
@Entity
@Table(name = "placement_test")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlacementTest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "schedule_time")
    private ZonedDateTime scheduleTime;

    @Column(name = "location")
    private String location;

    @Column(name = "max_num_stud")
    private Integer maxNumStud;

    @ManyToMany(mappedBy = "placementTests")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Student> students = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public PlacementTest creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getScheduleTime() {
        return scheduleTime;
    }

    public PlacementTest scheduleTime(ZonedDateTime scheduleTime) {
        this.scheduleTime = scheduleTime;
        return this;
    }

    public void setScheduleTime(ZonedDateTime scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getLocation() {
        return location;
    }

    public PlacementTest location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getMaxNumStud() {
        return maxNumStud;
    }

    public PlacementTest maxNumStud(Integer maxNumStud) {
        this.maxNumStud = maxNumStud;
        return this;
    }

    public void setMaxNumStud(Integer maxNumStud) {
        this.maxNumStud = maxNumStud;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public PlacementTest students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public PlacementTest addStudent(Student student) {
        students.add(student);
        student.getPlacementTests().add(this);
        return this;
    }

    public PlacementTest removeStudent(Student student) {
        students.remove(student);
        student.getPlacementTests().remove(this);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlacementTest placementTest = (PlacementTest) o;
        if (placementTest.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, placementTest.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PlacementTest{" +
            "id=" + id +
            ", creationDate='" + creationDate + "'" +
            ", scheduleTime='" + scheduleTime + "'" +
            ", location='" + location + "'" +
            ", maxNumStud='" + maxNumStud + "'" +
            '}';
    }
}
