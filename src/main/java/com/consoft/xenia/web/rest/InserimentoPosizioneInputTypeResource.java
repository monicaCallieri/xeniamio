package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.consoft.xenia.domain.pagopa.Payment;
import com.consoft.xenia.domain.pagopa.webservicea.CodiceAvviso;
import com.consoft.xenia.domain.pagopa.webservicea.WebServiceAClient;
import com.consoft.xenia.domain.pagopa.webservicea.WebServiceAClientConfig;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneInputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneResponse;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;
import com.consoft.xenia.domain.pagopa.webserviceb.WebServiceBClient;
import com.consoft.xenia.domain.pagopa.webserviceb.WebServiceBClientConfig;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosLista;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.PaInviaCarrelloPosizioniResponse;

import com.consoft.xenia.repository.InserimentoPosizioneInputTypeRepository;
import com.consoft.xenia.repository.InserimentoPosizioneOutputTypeRepository;
import com.consoft.xenia.repository.InviaCarrPosListaRepository;
import com.consoft.xenia.repository.InviaCarrPosOutputRepository;
import com.consoft.xenia.service.PagoPAService;
import com.consoft.xenia.web.rest.util.HeaderUtil;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import javax.xml.bind.JAXBElement;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * REST controller for managing InserimentoPosizioneInputType.
 */
@RestController
@RequestMapping("/api")
public class InserimentoPosizioneInputTypeResource {

    private final Logger log = LoggerFactory.getLogger(InserimentoPosizioneInputTypeResource.class);

    @Inject
    private InserimentoPosizioneInputTypeRepository inserimentoPosizioneInputTypeRepository;

    @Inject
    private InserimentoPosizioneOutputTypeRepository inserimentoPosizioneOutputTypeRepository;

    @Inject
    private InviaCarrPosListaRepository inviaCarrPosListaRepository;

    @Inject
    private PagoPAService pagoPAService;

    @Inject
    private InviaCarrPosOutputRepository inviaCarrPosOutputRepository;

    /**
     * POST /inserimento-posizione-input-types : Create a new
     * inserimentoPosizioneInputType.
     *
     * @param payOnLine
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new inserimentoPosizioneInputType, or with status 400 (Bad Request) if
     * the inserimentoPosizioneInputType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inserimento-posizione-input-types")
    @Timed
    public ResponseEntity<InserimentoPosizioneInputType> createInserimentoPosizioneInputType(@Valid @RequestBody Payment payment) throws MalformedURLException, IOException, URISyntaxException {
        InserimentoPosizioneInputType ipit = pagoPAService.createPay(payment);

        if (ipit.isEmpty()) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createEntityNotPayedAlert("studentIntentions", "toolong",
                            "Il carrello non può contenere più di 5 corsi da pagare."))
                    .body(null);
        } else {
            return ResponseEntity.created(new URI("/api/student-intentions/" + ipit.getId()))
                    .headers(HeaderUtil.createEntityPayedAlert("studentIntentions", ipit.getId().toString()))
                    .body(ipit);
        }

        /*if (inserimentoPosizioneInputType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("inserimentoPosizioneInputType", "idexists", "A new inserimentoPosizioneInputType cannot already have an ID")).body(null);
        }*/
        //InserimentoPosizioneInputType result = inserimentoPosizioneInputTypeRepository.save(inserimentoPosizioneInputType);
        //return null;
    }

    /**
     * PUT /inserimento-posizione-input-types : Updates an existing
     * inserimentoPosizioneInputType.
     *
     * @param inserimentoPosizioneInputType the inserimentoPosizioneInputType to
     * update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     * inserimentoPosizioneInputType, or with status 400 (Bad Request) if the
     * inserimentoPosizioneInputType is not valid, or with status 500 (Internal
     * Server Error) if the inserimentoPosizioneInputType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/inserimento-posizione-input-types")
    @Timed
    public ResponseEntity<InserimentoPosizioneInputType> updateInserimentoPosizioneInputType(@Valid
            @RequestBody InserimentoPosizioneInputType inserimentoPosizioneInputType) throws URISyntaxException, IOException {
        log.debug("REST request to update InserimentoPosizioneInputType : {}", inserimentoPosizioneInputType);
        //if (inserimentoPosizioneInputType.getId() == null) {
        //return createInserimentoPosizioneInputType(inserimentoPosizioneInputType);
        //return createInserimentoPosizioneInputType(null);
        //}
        InserimentoPosizioneInputType result = inserimentoPosizioneInputTypeRepository.save(inserimentoPosizioneInputType);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("inserimentoPosizioneInputType", inserimentoPosizioneInputType.getId().toString()))
                .body(result);
    }

    /**
     * GET /inserimento-posizione-input-types : get all the
     * inserimentoPosizioneInputTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of
     * inserimentoPosizioneInputTypes in body
     */
    @GetMapping("/inserimento-posizione-input-types")
    @Timed
    public List<InserimentoPosizioneInputType> getAllInserimentoPosizioneInputTypes() {
        log.debug("REST request to get all InserimentoPosizioneInputTypes");
        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypes = inserimentoPosizioneInputTypeRepository.findAll();
        return inserimentoPosizioneInputTypes;
    }

    /**
     * GET /inserimento-posizione-input-types/:id : get the "id"
     * inserimentoPosizioneInputType.
     *
     * @param id the id of the inserimentoPosizioneInputType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * inserimentoPosizioneInputType, or with status 404 (Not Found)
     */
    @GetMapping("/inserimento-posizione-input-types/{id}")
    @Timed
    public ResponseEntity<InserimentoPosizioneInputType> getInserimentoPosizioneInputType(@PathVariable Long id) {
        log.debug("REST request to get InserimentoPosizioneInputType : {}", id);
        InserimentoPosizioneInputType inserimentoPosizioneInputType = inserimentoPosizioneInputTypeRepository.findOne(id);
        return Optional.ofNullable(inserimentoPosizioneInputType)
                .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /inserimento-posizione-input-types/:id : delete the "id"
     * inserimentoPosizioneInputType.
     *
     * @param id the id of the inserimentoPosizioneInputType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/inserimento-posizione-input-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteInserimentoPosizioneInputType(@PathVariable Long id) {
        log.debug("REST request to delete InserimentoPosizioneInputType : {}", id);
        inserimentoPosizioneInputTypeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("inserimentoPosizioneInputType", id.toString())).build();
    }

}
