package com.consoft.xenia.web.rest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

/**
 * Utility class for HTTP headers creation.
 */
public final class HeaderUtil {

    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    private HeaderUtil() {
    }

    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-xeniaApp-alert", message);
        headers.add("X-xeniaApp-params", param);
        return headers;
    }

    public static HttpHeaders createEntityCreationAlert(String entityName, String param) {
        return createAlert("xeniaApp." + entityName + ".created", param);
    }
    
    public static HttpHeaders createEntityPayedAlert(String entityName, String param) {
        return createAlert("xeniaApp." + entityName + ".payed", param);
    }
    
    public static HttpHeaders createEntityNotPayedAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Payment not executed, {}", defaultMessage);
        //return createAlert("xeniaApp." + entityName + ".notpayed", param);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-xeniaApp-error", "error." + errorKey);
        headers.add("X-xeniaApp-params", entityName);
        return headers;
    }

    public static HttpHeaders createEntityUpdateAlert(String entityName, String param) {
        return createAlert("xeniaApp." + entityName + ".updated", param);
    }

    public static HttpHeaders createEntityDeletionAlert(String entityName, String param) {
        return createAlert("xeniaApp." + entityName + ".deleted", param);
    }

    public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Entity creation failed, {}", defaultMessage);
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-xeniaApp-error", "error." + errorKey);
        headers.add("X-xeniaApp-params", entityName);
        return headers;
    }
}
