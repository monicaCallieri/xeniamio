package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.Authority;
import com.consoft.xenia.domain.Response;
import com.consoft.xenia.domain.Student;
import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.User;
import com.consoft.xenia.domain.UserInformation;
import com.consoft.xenia.domain.enumeration.CourseLevel;
import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.consoft.xenia.domain.pagopa.webservicea.CodiceAvviso;
import com.consoft.xenia.domain.pagopa.webservicea.WebServiceAClient;
import com.consoft.xenia.domain.pagopa.webservicea.WebServiceAClientConfig;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneResponse;
import com.consoft.xenia.repository.StudentIntentionsRepository;

import com.consoft.xenia.repository.StudentRepository;
import com.consoft.xenia.repository.UserInformationRepository;
import com.consoft.xenia.security.AuthoritiesConstants;
import com.consoft.xenia.service.UserService;
import com.consoft.xenia.web.rest.util.HeaderUtil;
import com.fasterxml.jackson.annotation.JsonView;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.xml.bind.JAXBElement;
import org.krysalis.barcode4j.ChecksumMode;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code128.EAN128;
import org.krysalis.barcode4j.impl.code128.EAN128Bean;
import org.krysalis.barcode4j.impl.code128.EAN128LogicImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.Transactional;

/**
 * REST controller for managing Student.
 */
@RestController
@RequestMapping("/api")
public class StudentResource {

    private final Logger log = LoggerFactory.getLogger(StudentResource.class);

    @Inject
    private StudentRepository studentRepository;

    @Inject
    private StudentIntentionsResource studentIntentionsResource;

    @Inject
    private UserInformationRepository userInformationRepository;

    @Inject
    private UserService userService;

    /**
     * POST /students : Create a new student.
     *
     * @param student the student to create
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new student, or with status 400 (Bad Request) if the student has already
     * an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/students")
    @Timed
    @Transactional
    public ResponseEntity<Student> createStudent(@RequestBody Student student) throws URISyntaxException {
        log.debug("REST request to save Student : {}", student);
        if (student.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("student", "idexists", "A new student cannot already have an ID")).body(null);
        }

        /*L'utente associato allo studente creato dovrà cambiare il suo ruolo da USER->STUDENT */
        User user = userService.getUserWithAuthorities();
        Set<String> authorities = new HashSet<>();
        authorities.add(AuthoritiesConstants.STUDENT);
        log.debug("User id: " + user.getId());

        /*Inserimento del livello della lingua nella student intentions nel caso in cui lo studente
        non ha un certificato che ne attesta con certezza il livello*/
        if (student.getItalian_certification() == "") {
            String level = student.getItalian_level();
            student.setItalian_level("");
            List<StudentIntentions> studentIntentions = studentIntentionsResource.getAllStudentIntentions("NOTENROLLED");
            for (StudentIntentions studentIntention : studentIntentions) {
                studentIntention.setUser(user);
                studentIntention.setLevel(CourseLevel.valueOf(level));
                studentIntentionsResource.updateStudentIntentions(studentIntention);
            }
        }
        userService.updateUser(user.getId(), user.getLogin(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getActivated(), user.getLangKey(), authorities);

        /*Inserimento degli interessi per quanto riguarda la conoscenza della lingua italiana dello studente*/
        if (student.getIois() != null) {
            Response[] iois = student.getIois();

            /*Codice che si occupa del salvataggio delle risposte date dall'utente all'atto della creazione dello studente*/
            for (int i = 0; i < iois.length; i++) {
                UserInformation userInformation = new UserInformation();
                userInformation.setUser(user);
                userInformation.setResponse(iois[i]);
                userInformation.setQuestion(iois[i].getQuestion());
                userInformationRepository.save(userInformation);
            }
        }

        Student result = studentRepository.save(student);

        //Richiamo il webServiceA per inserire la posizione sulla base della student intention che l'utente ha manifestato sul portale di ateneo
       /* AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(WebServiceAClientConfig.class);
        ctx.refresh();
        WebServiceAClient webServiceAClient = ctx.getBean(WebServiceAClient.class);
        //WebServiceAClient webServiceAClient = new WebServiceAClient();
        CodiceAvviso caModello3_1 = new CodiceAvviso("0", "01", "4616785165729");//da generare (il codice avviso (e quindi anche lo IUV))
        //opportunamente collegato al tipo_riferimento_creditore ma sopratutto al codice_riferimento_creditore
        JAXBElement response = webServiceAClient.getInserimentoPosizioneResponse(
                PagoPAConstants.gestorePosizioniUser,//user Da rendere una costante
                PagoPAConstants.gestorePosizioniPassword,//password Da rendere una costante
                new BigDecimal(22),
                caModello3_1.getCodiceAvviso(),
                user.getEmail(),
                "36311285236299909031"
        //student.getUser().getActivationKey()//codice_riferimento_creditore: si riferisce alla banca, 
        //non allo studente. Ho messo l'activation key perchè cambia per ogni studente 
        //in quanto ogni posizione inserita deve avere il tipo_riferimento e il
        //codice_riferimento CREDITORE diversi
        );
        InserimentoPosizioneResponse ipr = (InserimentoPosizioneResponse) response.getValue();
        MARCO_OLD_InserimentoPosizioneOutputType ipo = ipr.getInserimentoPosizioneOutput();
        log.info("Esito: " + ipo.getEsito());
        log.info("Codice Errore: " + ipo.getCodiceErrore());
        log.info("Descrizione: " + ipo.getDescrizione());
        
        //EAN128LogicImpl objEAN128 = new EAN128LogicImpl(ChecksumMode.CP_ADD, template)
        EAN128 objEAN128 = new EAN128();
        EAN128Bean objEAN128Bean= (EAN128Bean) objEAN128.getBean();*/
        
        /*objEAN128.setModuleWidth(PagoPAConstants.moduleWidthmm);
        objEAN128.setHeight(PagoPAConstants.moduleHeightmm);
        // objEAN128Bean.setWideFactor(3);
        objEAN128.doQuietZone(true);
        objEAN128.setQuietZone(2);
        // human-readable
        objEAN128.setFontName("Helvetica");
        objEAN128.setFontSize(3);
        // checksum
        
        objEAN128.setChecksumMode(ChecksumMode.CP_ADD);
        */
        return ResponseEntity.created(new URI("/api/students/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("student", result.getId().toString()))
                .body(result);
        //return null;
    }

    /**
     * PUT /students : Updates an existing student.
     *
     * @param student the student to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     * student, or with status 400 (Bad Request) if the student is not valid, or
     * with status 500 (Internal Server Error) if the student couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/students")
    @Timed
    public ResponseEntity<Student> updateStudent(@RequestBody Student student) throws URISyntaxException {
        log.debug("REST request to update Student : {}", student);
        if (student.getId() == null) {
            return createStudent(student);
        }
        Student result = studentRepository.save(student);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("student", student.getId().toString()))
                .body(result);
    }

    /**
     * GET /students : get all the students.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of students
     * in body
     */
    @GetMapping("/students")
    @Timed
    public List<Student> getAllStudents() {
        log.debug("REST request to get all Students");

        /*User myUser = userService.getUserWithAuthorities();
        Set<Authority> userAuthorities = myUser.getAuthorities();
        for (Authority a : userAuthorities) {
            if (a.getName().equals(AuthoritiesConstants.STUDENT)) {
                List<Student> students = studentRepository.findByUserIsCurrentUser();
        return students;
            }
        }*/
        List<Student> students = studentRepository.findAllWithEagerRelationships();
        return students;
    }

    /**
     * GET /students/:id : get the "id" student.
     *
     * @param id the id of the student to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * student, or with status 404 (Not Found)
     */
    @GetMapping("/students/{id}")
    @Timed
    public ResponseEntity<Student> getStudent(@PathVariable Long id) {
        log.debug("REST request to get Student : {}", id);
        Student student = studentRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(student)
                .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /students/:id : delete the "id" student.
     *
     * @param id the id of the student to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/students/{id}")
    @Timed
    public ResponseEntity<Void> deleteStudent(@PathVariable Long id) {
        log.debug("REST request to delete Student : {}", id);
        studentRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("student", id.toString())).build();
    }

}
