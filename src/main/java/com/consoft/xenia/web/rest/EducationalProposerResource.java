package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.EducationalProposer;

import com.consoft.xenia.repository.EducationalProposerRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EducationalProposer.
 */
@RestController
@RequestMapping("/api")
public class EducationalProposerResource {

    private final Logger log = LoggerFactory.getLogger(EducationalProposerResource.class);

    @Inject
    private EducationalProposerRepository educationalProposerRepository;

    /**
     * POST /educational-proposers : Create a new educationalProposer.
     *
     * @param educationalProposer the educationalProposer to create
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new educationalProposer, or with status 400 (Bad Request) if the
     * educationalProposer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/educational-proposers")
    @Timed
    public ResponseEntity<EducationalProposer> createEducationalProposer(@RequestBody EducationalProposer educationalProposer) throws URISyntaxException {
        log.debug("REST request to save EducationalProposer : {}", educationalProposer);
        if (educationalProposer.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("educationalProposer", "idexists", "A new educationalProposer cannot already have an ID")).body(null);
        }
        EducationalProposer result = educationalProposerRepository.save(educationalProposer);
        return ResponseEntity.created(new URI("/api/educational-proposers/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("educationalProposer", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /educational-proposers : Updates an existing educationalProposer.
     *
     * @param educationalProposer the educationalProposer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     * educationalProposer, or with status 400 (Bad Request) if the
     * educationalProposer is not valid, or with status 500 (Internal Server
     * Error) if the educationalProposer couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/educational-proposers")
    @Timed
    public ResponseEntity<EducationalProposer> updateEducationalProposer(@RequestBody EducationalProposer educationalProposer) throws URISyntaxException {
        log.debug("REST request to update EducationalProposer : {}", educationalProposer);
        if (educationalProposer.getId() == null) {
            return createEducationalProposer(educationalProposer);
        }
        EducationalProposer result = educationalProposerRepository.save(educationalProposer);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("educationalProposer", educationalProposer.getId().toString()))
                .body(result);
    }

    /**
     * GET /educational-proposers : get all the educationalProposers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of
     * educationalProposers in body
     */
    @GetMapping("/educational-proposers")
    @Timed
    public List<EducationalProposer> getAllEducationalProposers(@RequestParam(name = "startdate", required = false) LocalDate startdate) {
        log.debug("REST request to get all EducationalProposers");
        if (startdate != null) {
            List<EducationalProposer> educationalProposers = educationalProposerRepository.findByStartdateGreaterThanEqual(startdate);
            return educationalProposers;
        } else {
            List<EducationalProposer> educationalProposers = educationalProposerRepository.findAll();
            return educationalProposers;
        }
    }

    /**
     * GET /educational-proposers/:id : get the "id" educationalProposer.
     *
     * @param id the id of the educationalProposer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * educationalProposer, or with status 404 (Not Found)
     */
    @GetMapping("/educational-proposers/{id}")
    @Timed
    public ResponseEntity<EducationalProposer> getEducationalProposer(@PathVariable Long id) {
        log.debug("REST request to get EducationalProposer : {}", id);
        EducationalProposer educationalProposer = educationalProposerRepository.findOne(id);
        return Optional.ofNullable(educationalProposer)
                .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /educational-proposers/:id : delete the "id" educationalProposer.
     *
     * @param id the id of the educationalProposer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/educational-proposers/{id}")
    @Timed
    public ResponseEntity<Void> deleteEducationalProposer(@PathVariable Long id) {
        log.debug("REST request to delete EducationalProposer : {}", id);
        educationalProposerRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("educationalProposer", id.toString())).build();
    }

}
