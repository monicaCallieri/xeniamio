package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.Authority;
import com.consoft.xenia.domain.Course;
import com.consoft.xenia.domain.User;
import com.consoft.xenia.security.AuthoritiesConstants;
import com.consoft.xenia.repository.CourseRepository;
import com.consoft.xenia.service.UserService;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * REST controller for managing Course.
 */
@RestController
@RequestMapping("/api")
public class CourseResource {

    private final Logger log = LoggerFactory.getLogger(CourseResource.class);
        
    @Inject
    private CourseRepository courseRepository;
    
    @Inject
    private UserService userService;
    
    /**
     * POST  /courses : Create a new course.
     *
     * @param course the course to create
     * @return the ResponseEntity with status 201 (Created) and with body the new course, or with status 400 (Bad Request) if the course has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/courses")
    @Timed
    public ResponseEntity<Course> createCourse(@RequestBody Course course) throws URISyntaxException {
        log.debug("REST request to save Course : {}", course);
        if (course.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("course", "idexists", "A new course cannot already have an ID")).body(null);
        }
        if (course.getStartDate().isAfter(course.getEndDate())) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("course", "idexists", "A new course cannot have a endDate before a startDate")).body(null);
        }
        if (course.getCode().length()< 15) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("course", "idexists", "A new course cannot have a courseCode with least 15 characters")).body(null);
        }
        Course result = courseRepository.save(course);
        return ResponseEntity.created(new URI("/api/courses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("course", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /courses : Updates an existing course.
     *
     * @param course the course to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated course,
     * or with status 400 (Bad Request) if the course is not valid,
     * or with status 500 (Internal Server Error) if the course couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/courses")
    @Timed
    public ResponseEntity<Course> updateCourse(@RequestBody Course course) throws URISyntaxException {
        log.debug("REST request to update Course : {}", course);
        if (course.getId() == null) {
            return createCourse(course);
        }
        Course result = courseRepository.save(course);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("course", course.getId().toString()))
            .body(result);
    }

    /**
     * GET  /courses : get all the courses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of courses in body
     */
    @GetMapping("/courses")
    @Timed
    public List<Course> getAllCourses(@RequestParam(name="AllCourses", required = false) Boolean AllCourses) {
        log.debug("REST request to get all Courses");
        List<Course> courses = null;
        //List<Course> courses = courseRepository.findAll();
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User myUser = userService.getUserWithAuthorities();
        Set<Authority> userAuthorities = myUser.getAuthorities();
        for (Authority a : userAuthorities) {
            if (a.getName().equals(AuthoritiesConstants.ADMIN)) {
                log.debug("I'm logged as Admin -> I'll load EVERY Course");
                courses = courseRepository.findAll();
                return courses;
            }
             if (a.getName().equals(AuthoritiesConstants.LECTURER)) {
                log.debug("I'm logged as Lecturer -> I'll load ONLY my Courses");
                courses = courseRepository.findByLecturerIsCurrentUser();
                return courses;
            }
            if (a.getName().equals(AuthoritiesConstants.USER) && AllCourses!=null && AllCourses) {
                log.debug("I'm logged as Student but i but All the Course Available");
                courses = courseRepository.findAll();
                return courses;
            }
            
        }
        /*
        Da gestire il caso in cui sono student MA devo caricare l'elenco dei corsi disponibili a cui potermi iscrivere
        */
        log.debug("I'm logged as Student -> I'll load ONLY my Courses");
        courses = courseRepository.findByUserIsCurrentUser();
        return courses;
    }

    /**
     * GET  /courses/:id : get the "id" course.
     *
     * @param id the id of the course to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the course, or with status 404 (Not Found)
     */
    @GetMapping("/courses/{id}")
    @Timed
    public ResponseEntity<Course> getCourse(@PathVariable Long id) {
        log.debug("REST request to get Course : {}", id);
        Course course = courseRepository.findOne(id);
        return Optional.ofNullable(course)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /courses/:id : delete the "id" course.
     *
     * @param id the id of the course to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/courses/{id}")
    @Timed
    public ResponseEntity<Void> deleteCourse(@PathVariable Long id) {
        log.debug("REST request to delete Course : {}", id);
        courseRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("course", id.toString())).build();
    }

}
