package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.Authority;
import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.User;
import com.consoft.xenia.domain.enumeration.State;

import com.consoft.xenia.repository.StudentIntentionsRepository;
import com.consoft.xenia.security.AuthoritiesConstants;
import com.consoft.xenia.service.UserService;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing StudentIntentions.
 */
@RestController
@RequestMapping("/api")
public class StudentIntentionsResource {

    private final Logger log = LoggerFactory.getLogger(StudentIntentionsResource.class);

    @Inject
    private StudentIntentionsRepository studentIntentionsRepository;

    @Inject
    private UserService userService;

    /**
     * POST /student-intentions : Create a new studentIntentions.
     *
     * @param studentIntentions the studentIntentions to create
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new studentIntentions, or with status 400 (Bad Request) if the
     * studentIntentions has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/student-intentions")
    @Timed
    public ResponseEntity<StudentIntentions> createStudentIntentions(@Valid @RequestBody StudentIntentions studentIntentions) throws URISyntaxException {
        log.debug("REST request to save StudentIntentions : {}", studentIntentions);
        if (studentIntentions.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("studentIntentions", "idexists", "A new studentIntentions cannot already have an ID")).body(null);
        }
        StudentIntentions result = studentIntentionsRepository.save(studentIntentions);
        return ResponseEntity.created(new URI("/api/student-intentions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("studentIntentions", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /student-intentions : Updates an existing studentIntentions.
     *
     * @param studentIntentions the studentIntentions to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     * studentIntentions, or with status 400 (Bad Request) if the
     * studentIntentions is not valid, or with status 500 (Internal Server
     * Error) if the studentIntentions couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/student-intentions")
    @Timed
    public ResponseEntity<StudentIntentions> updateStudentIntentions(@Valid @RequestBody StudentIntentions studentIntentions) throws URISyntaxException {
        log.debug("REST request to update StudentIntentions : {}", studentIntentions);
        if (studentIntentions.getId() == null) {
            return createStudentIntentions(studentIntentions);
        }
        StudentIntentions result = studentIntentionsRepository.save(studentIntentions);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("studentIntentions", studentIntentions.getId().toString()))
                .body(result);
    }

    /**
     * GET /student-intentions : get all the studentIntentions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of
     * studentIntentions in body
     */
    @GetMapping("/student-intentions")
    @Timed
    public List<StudentIntentions> getAllStudentIntentions(@RequestParam(name = "state", required = false) String state) {
        log.debug("REST request to get all StudentIntentions");
        List<StudentIntentions> studentIntentions = new ArrayList<StudentIntentions>();
        User myUser = userService.getUserWithAuthorities();

        if (myUser != null) {
            log.debug("Id user corrente: " + myUser.getId());
            log.debug("State of StudentINtentions: " + state);
            Set<Authority> userAuthorities = myUser.getAuthorities();
            if (state != null && state.equals("NOTENROLLED")) {
                log.debug("Lo state è valorizzato nella chiamata!");
                //studentIntentions = studentIntentionsRepository.findByUserIsCurrentUser();
                studentIntentions = studentIntentionsRepository.findByUserIsCurrentUserState(State.NOTENROLLED);
                return studentIntentions;
            }
            for (Authority a : userAuthorities) {
                if (a.getName().equals(AuthoritiesConstants.STUDENT)) {
                    studentIntentions = studentIntentionsRepository.findByUserIsCurrentUser();
                    //return studentIntentions;
                } else if (a.getName().equals(AuthoritiesConstants.USER)) {
                    studentIntentions = studentIntentionsRepository.findByUserIsCurrentUser();
                    //return studentIntentions;
                }else if (a.getName().equals(AuthoritiesConstants.ADMIN)) {
                    studentIntentions = studentIntentionsRepository.findAllWithEagerRelationships();
                    //return studentIntentions;
                }else{
                    studentIntentions = studentIntentionsRepository.findAllWithEagerRelationships();
                }
            }
            
            return studentIntentions;
            
        }
        return studentIntentions;

    }

    /**
     * GET /student-intentions/:id : get the "id" studentIntentions.
     *
     * @param id the id of the studentIntentions to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * studentIntentions, or with status 404 (Not Found)
     */
    @GetMapping("/student-intentions/{id}")
    @Timed
    public ResponseEntity<StudentIntentions> getStudentIntentions(@PathVariable Long id) {
        log.debug("REST request to get StudentIntentions : {}", id);
        StudentIntentions studentIntentions = studentIntentionsRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(studentIntentions)
                .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /student-intentions/:id : delete the "id" studentIntentions.
     *
     * @param id the id of the studentIntentions to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/student-intentions/{id}")
    @Timed
    public ResponseEntity<Void> deleteStudentIntentions(@PathVariable Long id) {
        log.debug("REST request to delete StudentIntentions : {}", id);
        studentIntentionsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("studentIntentions", id.toString())).build();
    }

}
