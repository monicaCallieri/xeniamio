package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.DatiSingVersLista;

import com.consoft.xenia.repository.DatiSingVersListaRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DatiSingVersLista.
 */
@RestController
@RequestMapping("/api")
public class DatiSingVersListaResource {

    private final Logger log = LoggerFactory.getLogger(DatiSingVersListaResource.class);
        
    @Inject
    private DatiSingVersListaRepository datiSingVersListaRepository;

    /**
     * POST  /dati-sing-vers-listas : Create a new datiSingVersLista.
     *
     * @param datiSingVersLista the datiSingVersLista to create
     * @return the ResponseEntity with status 201 (Created) and with body the new datiSingVersLista, or with status 400 (Bad Request) if the datiSingVersLista has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dati-sing-vers-listas")
    @Timed
    public ResponseEntity<DatiSingVersLista> createDatiSingVersLista(@Valid @RequestBody DatiSingVersLista datiSingVersLista) throws URISyntaxException {
        log.debug("REST request to save DatiSingVersLista : {}", datiSingVersLista);
        if (datiSingVersLista.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("datiSingVersLista", "idexists", "A new datiSingVersLista cannot already have an ID")).body(null);
        }
        DatiSingVersLista result = datiSingVersListaRepository.save(datiSingVersLista);
        return ResponseEntity.created(new URI("/api/dati-sing-vers-listas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("datiSingVersLista", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dati-sing-vers-listas : Updates an existing datiSingVersLista.
     *
     * @param datiSingVersLista the datiSingVersLista to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated datiSingVersLista,
     * or with status 400 (Bad Request) if the datiSingVersLista is not valid,
     * or with status 500 (Internal Server Error) if the datiSingVersLista couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dati-sing-vers-listas")
    @Timed
    public ResponseEntity<DatiSingVersLista> updateDatiSingVersLista(@Valid @RequestBody DatiSingVersLista datiSingVersLista) throws URISyntaxException {
        log.debug("REST request to update DatiSingVersLista : {}", datiSingVersLista);
        if (datiSingVersLista.getId() == null) {
            return createDatiSingVersLista(datiSingVersLista);
        }
        DatiSingVersLista result = datiSingVersListaRepository.save(datiSingVersLista);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("datiSingVersLista", datiSingVersLista.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dati-sing-vers-listas : get all the datiSingVersListas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of datiSingVersListas in body
     */
    @GetMapping("/dati-sing-vers-listas")
    @Timed
    public List<DatiSingVersLista> getAllDatiSingVersListas() {
        log.debug("REST request to get all DatiSingVersListas");
        List<DatiSingVersLista> datiSingVersListas = datiSingVersListaRepository.findAll();
        return datiSingVersListas;
    }

    /**
     * GET  /dati-sing-vers-listas/:id : get the "id" datiSingVersLista.
     *
     * @param id the id of the datiSingVersLista to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the datiSingVersLista, or with status 404 (Not Found)
     */
    @GetMapping("/dati-sing-vers-listas/{id}")
    @Timed
    public ResponseEntity<DatiSingVersLista> getDatiSingVersLista(@PathVariable Long id) {
        log.debug("REST request to get DatiSingVersLista : {}", id);
        DatiSingVersLista datiSingVersLista = datiSingVersListaRepository.findOne(id);
        return Optional.ofNullable(datiSingVersLista)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /dati-sing-vers-listas/:id : delete the "id" datiSingVersLista.
     *
     * @param id the id of the datiSingVersLista to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dati-sing-vers-listas/{id}")
    @Timed
    public ResponseEntity<Void> deleteDatiSingVersLista(@PathVariable Long id) {
        log.debug("REST request to delete DatiSingVersLista : {}", id);
        datiSingVersListaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("datiSingVersLista", id.toString())).build();
    }

}
