package com.consoft.xenia.web.rest.vm;

import com.consoft.xenia.domain.Question;
import com.consoft.xenia.domain.Response;
import com.consoft.xenia.domain.StudentIntentions;
import java.time.ZonedDateTime;

import java.util.Set;

import com.consoft.xenia.domain.User;
import com.consoft.xenia.service.dto.UserDTO;
import java.time.LocalDate;
import java.util.Map;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Transient;

/**
 * View Model extending the UserDTO, which is meant to be used in the user
 * management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 100;

    private Long id;

    private String createdBy;

    private ZonedDateTime createdDate;

    private String lastModifiedBy;

    private ZonedDateTime lastModifiedDate;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;
    
    //@Transient
    private Response[] responseUser;
    
    //@Transient
    private LocalDate [] studentIntentionsDate;
    //private Set<StudentIntentions> studentIntentions;
    
   
    //@Transient
    private StudentIntentions studentIntentions;

    public ManagedUserVM() {
    }

    public ManagedUserVM(User user) {
        super(user);
        this.id = user.getId();
        this.createdBy = user.getCreatedBy();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedBy = user.getLastModifiedBy();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.password = null;
        this.responseUser = user.getResponseUser();
        this.studentIntentions = user.getStudentIntentions();
    }

    public ManagedUserVM(Long id, String login, String password, String firstName, String lastName,
            String email, boolean activated, String langKey, Set<String> authorities,
            String createdBy, ZonedDateTime createdDate, String lastModifiedBy, ZonedDateTime lastModifiedDate,
            Response[] responseUser, StudentIntentions studentIntentions) {
        super(login, firstName, lastName, email, activated, langKey, authorities,responseUser, studentIntentions);
        this.id = id;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.password = password;
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public ZonedDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "ManagedUserVM{"
                + "id=" + id
                + ", createdBy=" + createdBy
                + ", createdDate=" + createdDate
                + ", lastModifiedBy='" + lastModifiedBy + '\''
                + ", lastModifiedDate=" + lastModifiedDate
                + "} " + super.toString();
    }

    /**
     * @return the responseUser
     */
    @Override
    public Response[] getResponseUser() {
        return responseUser;
    }

    /**
     * @param responseUser the responseUser to set
     */
    @Override
    public void setResponseUser(Response[] responseUser) {
        this.responseUser = responseUser;
    }
    
    /**
     * @return the studentIntentionsDate
     */
    @Override
    public StudentIntentions getStudentIntentions() {
        return studentIntentions;
    }

    /**
     * @param studentIntentionsDate the responseUser to set
     */
    @Override
    public void setStudentIntentions(StudentIntentions studentIntentions) {
        this.studentIntentions = studentIntentions;
    }
}
    
   
