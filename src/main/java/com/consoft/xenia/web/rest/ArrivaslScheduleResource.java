package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.PlacementTest;
import com.consoft.xenia.domain.Student;
import com.consoft.xenia.repository.PlacementTestRepository;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Inject;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing PlacementTest.
 */
@RestController
@RequestMapping("/api")
public class ArrivaslScheduleResource {

    @Inject
    private PlacementTestRepository placementTestRepository;

    private final Logger log = LoggerFactory.getLogger(ArrivaslScheduleResource.class);

    /**
     * GET /arrivals-schedule : get all the arrivals-schedule.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of
     * arrivals-schedule in body FUNZIONANTE, SENZA ALCUNA SELEZIONE
     */
    /*  @GetMapping("/arrivals-schedule")
    @Timed
    public List<Student> getArrivalsSchedule() {
        log.debug("REST request to get all arrivals-schedule");
        List<Student> students = placementTestRepository.findAllWithEagerRelationships();
        return students;
    }*/
    /**
     * GET /arrivals-schedule/:date : get the "date" arrivals-schedule.
     *
     * @param date the date of the arrivals-schedule to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * student, or with status 404 (Not Found)
     */
    /* @GetMapping("/arrivals-schedule/{date}")
    @Timed
    public List<Student> getArrivalsScheduleByDate(@DateTimeFormat(pattern="yyyy-MM-dd") @PathVariable Date date) {
        log.debug("REST request to get all arrivals-schedule");
        List<Student> students = placementTestRepository.findAllByDate(date);
        return students;
    }*/
    /**
     * GET /students/:id : get the "id" student.
     *
     * @param id the id of the student to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * student, or with status 404 (Not Found)
     */
    /* @GetMapping("/arrivals-schedule/{id}")
    @Timed
    public List<Student> getStudent(@PathVariable Long id) {
        log.debug("REST request to get all arrivals-schedule");
        List<Student> students = placementTestRepository.findOneWithEagerRelationships(id);
        return students;
    }*/
    /**
     * GET /students/:id : get the "id" student.
     *
     * @param date the date of the student to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * student, or with status 404 (Not Found)
     */
    @GetMapping("/arrivals-schedule")
    @Timed
    public List<Student> getStudent(@RequestParam(name="date", required = false) ZonedDateTime date) {
        log.debug("REST request to get all arrivals-schedule by date");
       // String formattedDate = date.toString("yyyy-MM-DD");
        //formattedDate = formattedDate+" 01:00:00";
        //ZonedDateTime zdt = date.atStartOfDay(ZoneOffset.UTC);
        //log.debug("zoneddatetime: "+zdt);
        //final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //System.out.println("dateFormat.format(date): "+dateFormat.format(date));
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        //formatter = formatter.withLocale(Locale.US);  // Locale specifies human language for translating, and cultural norms for lowercase/uppercase and abbreviations and such. Example: Locale.US or Locale.CANADA_FRENCH
       // LocalDate myLocalDate = LocalDate.parse(date, formatter);
        List<Student> students = placementTestRepository.findAllByDate(date);
        return students;
    }

}
