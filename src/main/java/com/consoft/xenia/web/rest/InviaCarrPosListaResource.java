package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosLista;

import com.consoft.xenia.repository.InviaCarrPosListaRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InviaCarrPosLista.
 */
@RestController
@RequestMapping("/api")
public class InviaCarrPosListaResource {

    private final Logger log = LoggerFactory.getLogger(InviaCarrPosListaResource.class);
        
    @Inject
    private InviaCarrPosListaRepository inviaCarrPosListaRepository;

    /**
     * POST  /invia-carr-pos-listas : Create a new inviaCarrPosLista.
     *
     * @param inviaCarrPosLista the inviaCarrPosLista to create
     * @return the ResponseEntity with status 201 (Created) and with body the new inviaCarrPosLista, or with status 400 (Bad Request) if the inviaCarrPosLista has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invia-carr-pos-listas")
    @Timed
    public ResponseEntity<InviaCarrPosLista> createInviaCarrPosLista(@Valid @RequestBody InviaCarrPosLista inviaCarrPosLista) throws URISyntaxException {
        log.debug("REST request to save InviaCarrPosLista : {}", inviaCarrPosLista);
        if (inviaCarrPosLista.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("inviaCarrPosLista", "idexists", "A new inviaCarrPosLista cannot already have an ID")).body(null);
        }
        InviaCarrPosLista result = inviaCarrPosListaRepository.save(inviaCarrPosLista);
        return ResponseEntity.created(new URI("/api/invia-carr-pos-listas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("inviaCarrPosLista", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invia-carr-pos-listas : Updates an existing inviaCarrPosLista.
     *
     * @param inviaCarrPosLista the inviaCarrPosLista to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated inviaCarrPosLista,
     * or with status 400 (Bad Request) if the inviaCarrPosLista is not valid,
     * or with status 500 (Internal Server Error) if the inviaCarrPosLista couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invia-carr-pos-listas")
    @Timed
    public ResponseEntity<InviaCarrPosLista> updateInviaCarrPosLista(@Valid @RequestBody InviaCarrPosLista inviaCarrPosLista) throws URISyntaxException {
        log.debug("REST request to update InviaCarrPosLista : {}", inviaCarrPosLista);
        if (inviaCarrPosLista.getId() == null) {
            return createInviaCarrPosLista(inviaCarrPosLista);
        }
        InviaCarrPosLista result = inviaCarrPosListaRepository.save(inviaCarrPosLista);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("inviaCarrPosLista", inviaCarrPosLista.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invia-carr-pos-listas : get all the inviaCarrPosListas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of inviaCarrPosListas in body
     */
    @GetMapping("/invia-carr-pos-listas")
    @Timed
    public List<InviaCarrPosLista> getAllInviaCarrPosListas() {
        log.debug("REST request to get all InviaCarrPosListas");
        List<InviaCarrPosLista> inviaCarrPosListas = inviaCarrPosListaRepository.findAll();
        return inviaCarrPosListas;
    }

    /**
     * GET  /invia-carr-pos-listas/:id : get the "id" inviaCarrPosLista.
     *
     * @param id the id of the inviaCarrPosLista to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the inviaCarrPosLista, or with status 404 (Not Found)
     */
    @GetMapping("/invia-carr-pos-listas/{id}")
    @Timed
    public ResponseEntity<InviaCarrPosLista> getInviaCarrPosLista(@PathVariable Long id) {
        log.debug("REST request to get InviaCarrPosLista : {}", id);
        InviaCarrPosLista inviaCarrPosLista = inviaCarrPosListaRepository.findOne(id);
        return Optional.ofNullable(inviaCarrPosLista)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /invia-carr-pos-listas/:id : delete the "id" inviaCarrPosLista.
     *
     * @param id the id of the inviaCarrPosLista to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invia-carr-pos-listas/{id}")
    @Timed
    public ResponseEntity<Void> deleteInviaCarrPosLista(@PathVariable Long id) {
        log.debug("REST request to delete InviaCarrPosLista : {}", id);
        inviaCarrPosListaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("inviaCarrPosLista", id.toString())).build();
    }

}
