package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.PlacementTest;

import com.consoft.xenia.repository.PlacementTestRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PlacementTest.
 */
@RestController
@RequestMapping("/api")
public class PlacementTestResource {

    private final Logger log = LoggerFactory.getLogger(PlacementTestResource.class);
        
    @Inject
    private PlacementTestRepository placementTestRepository;

    /**
     * POST /pretests : Create a new pretest.
     *
     * @param pretest the pretest to create
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new pretest, or with status 400 (Bad Request) if the pretest has already
     * an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pretests")
    @Timed
    public ResponseEntity<PlacementTest> createPretest(@RequestBody PlacementTest pretest) throws URISyntaxException {
        log.debug("REST request to save Pretest : {}", pretest);
        if (pretest.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("pretest", "idexists", "A new pretest cannot already have an ID")).body(null);
        }
        PlacementTest result = placementTestRepository.save(pretest);
        return ResponseEntity.created(new URI("/api/pretests/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("pretest", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT /pretests : Updates an existing pretest.
     *
     * @param pretest the pretest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     * pretest, or with status 400 (Bad Request) if the pretest is not valid, or
     * with status 500 (Internal Server Error) if the pretest couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pretests")
    @Timed
    public ResponseEntity<PlacementTest> updatePretest(@RequestBody PlacementTest pretest) throws URISyntaxException {
        log.debug("REST request to update Pretest : {}", pretest);
        if (pretest.getId() == null) {
            return createPretest(pretest);
        }
        PlacementTest result = placementTestRepository.save(pretest);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("pretest", pretest.getId().toString()))
                .body(result);
    }

    /**
     * GET /pretests : get all the pretests.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pretests
     * in body
     */
    @GetMapping("/pretests")
    @Timed
    public List<PlacementTest> getAllPretests(@RequestParam(name="date", required = false) Optional<LocalDate> optionalDate) {
        log.debug("REST request to get all Pretests");
        List<PlacementTest> pretests = optionalDate.map(date -> {
            //System.out.println("while opt is present...");
            
            LocalDate startDate= date.withDayOfMonth(1);
            ZonedDateTime zdtStartDate = startDate.atStartOfDay(ZoneOffset.UTC);
        log.debug("zoneddatetime: "+zdtStartDate);
            LocalDate endDate= date.withDayOfMonth(date.lengthOfMonth());
            ZonedDateTime zdtEndDate = endDate.atStartOfDay(ZoneOffset.UTC);
        log.debug("zoneddatetime: "+zdtEndDate);
            return placementTestRepository.findAllByScheduleTimeBetween(zdtStartDate, zdtEndDate);
            //return pretestRepository.findPretestsByDate(date);
        })
                .orElseGet(() ->{
                   return placementTestRepository.findAll();
                }
        );
        
        
        //List<Pretest> pretests= pretestRepository.findAllByDateBetween(startDate, endDate);
       
        return pretests;
        
    }

    /**
     * GET /pretests/:id : get the "id" pretest.
     *
     * @param id the id of the pretest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     * pretest, or with status 404 (Not Found)
     */
    @GetMapping("/pretests/{id}")
    @Timed
    public ResponseEntity<PlacementTest> getPretest(@PathVariable Long id) {
        log.debug("REST request to get Pretest : {}", id);
        PlacementTest pretest = placementTestRepository.findOne(id);
        return Optional.ofNullable(pretest)
                .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE /pretests/:id : delete the "id" pretest.
     *
     * @param id the id of the pretest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pretests/{id}")
    @Timed
    public ResponseEntity<Void> deletePretest(@PathVariable Long id) {
        log.debug("REST request to delete Pretest : {}", id);
        placementTestRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pretest", id.toString())).build();
    }

}
