/**
 * View Models used by Spring MVC REST controllers.
 */
package com.consoft.xenia.web.rest.vm;
