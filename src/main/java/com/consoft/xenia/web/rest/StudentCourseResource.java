package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.StudentCourse;

import com.consoft.xenia.repository.StudentCourseRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StudentCourse.
 */
@RestController
@RequestMapping("/api")
public class StudentCourseResource {

    private final Logger log = LoggerFactory.getLogger(StudentCourseResource.class);
        
    @Inject
    private StudentCourseRepository studentCourseRepository;

    /**
     * POST  /student-courses : Create a new studentCourse.
     *
     * @param studentCourse the studentCourse to create
     * @return the ResponseEntity with status 201 (Created) and with body the new studentCourse, or with status 400 (Bad Request) if the studentCourse has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/student-courses")
    @Timed
    public ResponseEntity<StudentCourse> createStudentCourse(@RequestBody StudentCourse studentCourse) throws URISyntaxException {
        log.debug("REST request to save StudentCourse : {}", studentCourse);
        if (studentCourse.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("studentCourse", "idexists", "A new studentCourse cannot already have an ID")).body(null);
        }
        StudentCourse result = studentCourseRepository.save(studentCourse);
        return ResponseEntity.created(new URI("/api/student-courses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("studentCourse", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /student-courses : Updates an existing studentCourse.
     *
     * @param studentCourse the studentCourse to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated studentCourse,
     * or with status 400 (Bad Request) if the studentCourse is not valid,
     * or with status 500 (Internal Server Error) if the studentCourse couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/student-courses")
    @Timed
    public ResponseEntity<StudentCourse> updateStudentCourse(@RequestBody StudentCourse studentCourse) throws URISyntaxException {
        log.debug("REST request to update StudentCourse : {}", studentCourse);
        if (studentCourse.getId() == null) {
            return createStudentCourse(studentCourse);
        }
        StudentCourse result = studentCourseRepository.save(studentCourse);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("studentCourse", studentCourse.getId().toString()))
            .body(result);
    }

    /**
     * GET  /student-courses : get all the studentCourses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of studentCourses in body
     */
    @GetMapping("/student-courses")
    @Timed
    public List<StudentCourse> getAllStudentCourses(@RequestParam(name = "courseCode", required = false) String courseCode) {
        List<StudentCourse> studentCourses =new ArrayList<StudentCourse>();
        if(courseCode!=null){
            log.debug("REST request to get all StudentCourses for "+courseCode+" course");
            studentCourses = studentCourseRepository.findAllByCourseCode(courseCode);
            return studentCourses;
        }
        log.debug("REST request to get all StudentCourses");
        studentCourses = studentCourseRepository.findAll();
        return studentCourses;
    }

    /**
     * GET  /student-courses/:id : get the "id" studentCourse.
     *
     * @param id the id of the studentCourse to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the studentCourse, or with status 404 (Not Found)
     */
    @GetMapping("/student-courses/{id}")
    @Timed
    public ResponseEntity<StudentCourse> getStudentCourse(@PathVariable Long id) {
        log.debug("REST request to get StudentCourse : {}", id);
        StudentCourse studentCourse = studentCourseRepository.findOne(id);
        return Optional.ofNullable(studentCourse)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /student-courses/:id : delete the "id" studentCourse.
     *
     * @param id the id of the studentCourse to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/student-courses/{id}")
    @Timed
    public ResponseEntity<Void> deleteStudentCourse(@PathVariable Long id) {
        log.debug("REST request to delete StudentCourse : {}", id);
        studentCourseRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("studentCourse", id.toString())).build();
    }

}
