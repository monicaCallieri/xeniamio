package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;

import com.consoft.xenia.repository.InviaCarrPosOutputRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InviaCarrPosOutput.
 */
@RestController
@RequestMapping("/api")
public class InviaCarrPosOutputResource {

    private final Logger log = LoggerFactory.getLogger(InviaCarrPosOutputResource.class);
        
    @Inject
    private InviaCarrPosOutputRepository inviaCarrPosOutputRepository;

    /**
     * POST  /invia-carr-pos-outputs : Create a new inviaCarrPosOutput.
     *
     * @param inviaCarrPosOutput the inviaCarrPosOutput to create
     * @return the ResponseEntity with status 201 (Created) and with body the new inviaCarrPosOutput, or with status 400 (Bad Request) if the inviaCarrPosOutput has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invia-carr-pos-outputs")
    @Timed
    public ResponseEntity<InviaCarrPosOutput> createInviaCarrPosOutput(@Valid @RequestBody InviaCarrPosOutput inviaCarrPosOutput) throws URISyntaxException {
        log.debug("REST request to save InviaCarrPosOutput : {}", inviaCarrPosOutput);
        if (inviaCarrPosOutput.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("inviaCarrPosOutput", "idexists", "A new inviaCarrPosOutput cannot already have an ID")).body(null);
        }
        InviaCarrPosOutput result = inviaCarrPosOutputRepository.save(inviaCarrPosOutput);
        return ResponseEntity.created(new URI("/api/invia-carr-pos-outputs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("inviaCarrPosOutput", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invia-carr-pos-outputs : Updates an existing inviaCarrPosOutput.
     *
     * @param inviaCarrPosOutput the inviaCarrPosOutput to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated inviaCarrPosOutput,
     * or with status 400 (Bad Request) if the inviaCarrPosOutput is not valid,
     * or with status 500 (Internal Server Error) if the inviaCarrPosOutput couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invia-carr-pos-outputs")
    @Timed
    public ResponseEntity<InviaCarrPosOutput> updateInviaCarrPosOutput(@Valid @RequestBody InviaCarrPosOutput inviaCarrPosOutput) throws URISyntaxException {
        log.debug("REST request to update InviaCarrPosOutput : {}", inviaCarrPosOutput);
        if (inviaCarrPosOutput.getId() == null) {
            return createInviaCarrPosOutput(inviaCarrPosOutput);
        }
        InviaCarrPosOutput result = inviaCarrPosOutputRepository.save(inviaCarrPosOutput);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("inviaCarrPosOutput", inviaCarrPosOutput.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invia-carr-pos-outputs : get all the inviaCarrPosOutputs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of inviaCarrPosOutputs in body
     */
    @GetMapping("/invia-carr-pos-outputs")
    @Timed
    public List<InviaCarrPosOutput> getAllInviaCarrPosOutputs() {
        log.debug("REST request to get all InviaCarrPosOutputs");
        List<InviaCarrPosOutput> inviaCarrPosOutputs = inviaCarrPosOutputRepository.findAll();
        return inviaCarrPosOutputs;
    }

    /**
     * GET  /invia-carr-pos-outputs/:id : get the "id" inviaCarrPosOutput.
     *
     * @param id the id of the inviaCarrPosOutput to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the inviaCarrPosOutput, or with status 404 (Not Found)
     */
    @GetMapping("/invia-carr-pos-outputs/{id}")
    @Timed
    public ResponseEntity<InviaCarrPosOutput> getInviaCarrPosOutput(@PathVariable Long id) {
        log.debug("REST request to get InviaCarrPosOutput : {}", id);
        InviaCarrPosOutput inviaCarrPosOutput = inviaCarrPosOutputRepository.findOne(id);
        return Optional.ofNullable(inviaCarrPosOutput)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /invia-carr-pos-outputs/:id : delete the "id" inviaCarrPosOutput.
     *
     * @param id the id of the inviaCarrPosOutput to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invia-carr-pos-outputs/{id}")
    @Timed
    public ResponseEntity<Void> deleteInviaCarrPosOutput(@PathVariable Long id) {
        log.debug("REST request to delete InviaCarrPosOutput : {}", id);
        inviaCarrPosOutputRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("inviaCarrPosOutput", id.toString())).build();
    }

}
