package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.User;
import com.consoft.xenia.domain.UserInformation;

import com.consoft.xenia.repository.UserInformationRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserInformation.
 */
@RestController
@RequestMapping("/api")
public class UserInformationResource {

    private final Logger log = LoggerFactory.getLogger(UserInformationResource.class);
        
    @Inject
    private UserInformationRepository userInformationRepository;

    /**
     * POST  /user-informations : Create a new userInformation.
     *
     * @param userInformation the userInformation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userInformation, or with status 400 (Bad Request) if the userInformation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-informations")
    @Timed
    public ResponseEntity<UserInformation> createUserInformation(@RequestBody UserInformation userInformation) throws URISyntaxException {
        log.debug("REST request to save UserInformation : {}", userInformation);
        if (userInformation.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userInformation", "idexists", "A new userInformation cannot already have an ID")).body(null);
        }
        UserInformation result = userInformationRepository.save(userInformation);
        return ResponseEntity.created(new URI("/api/user-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userInformation", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-informations : Updates an existing userInformation.
     *
     * @param userInformation the userInformation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userInformation,
     * or with status 400 (Bad Request) if the userInformation is not valid,
     * or with status 500 (Internal Server Error) if the userInformation couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-informations")
    @Timed
    public ResponseEntity<UserInformation> updateUserInformation(@RequestBody UserInformation userInformation) throws URISyntaxException {
        log.debug("REST request to update UserInformation : {}", userInformation);
        if (userInformation.getId() == null) {
            return createUserInformation(userInformation);
        }
        UserInformation result = userInformationRepository.save(userInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userInformation", userInformation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-informations : get all the userInformations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userInformations in body
     */
    @GetMapping("/user-informations")
    @Timed
    public List<UserInformation> getAllUserInformations() {
        log.debug("REST request to get all UserInformations");
        List<UserInformation> userInformations = userInformationRepository.findAll();
        return userInformations;
    }

    /**
     * GET  /user-informations/:id : get the "id" userInformation.
     *
     * @param id the id of the userInformation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userInformation, or with status 404 (Not Found)
     */
    @GetMapping("/user-informations/{id}")
    @Timed
    public ResponseEntity<UserInformation> getUserInformation(@PathVariable Long id) {
        log.debug("REST request to get UserInformation : {}", id);
        UserInformation userInformation = userInformationRepository.findOne(id);
        return Optional.ofNullable(userInformation)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    /**
     * GET  /user-informations/:id : get the "id" userInformation.
     *
     * @param id the id of the userInformation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userInformation, or with status 404 (Not Found)
     */
    @GetMapping("/user-informations-byUser")
    @Timed
    public UserInformation getUserInformationByUserId() {
        log.debug("REST request to get UserInformation for current User");
        Long id = 1L;
        //UserInformation userInformation = userInformationRepository.findByUser(id);
        UserInformation userInformation = userInformationRepository.findByUser(id);
        
        //if(userInformation == null)
            //return null;
        return userInformation;
    }

    /**
     * DELETE  /user-informations/:id : delete the "id" userInformation.
     *
     * @param id the id of the userInformation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-informations/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserInformation(@PathVariable Long id) {
        log.debug("REST request to delete UserInformation : {}", id);
        userInformationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userInformation", id.toString())).build();
    }

}
