package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;

import com.consoft.xenia.repository.InserimentoPosizioneOutputTypeRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InserimentoPosizioneOutputType.
 */
@RestController
@RequestMapping("/api")
public class InserimentoPosizioneOutputTypeResource {

    private final Logger log = LoggerFactory.getLogger(InserimentoPosizioneOutputTypeResource.class);
        
    @Inject
    private InserimentoPosizioneOutputTypeRepository inserimentoPosizioneOutputTypeRepository;

    /**
     * POST  /inserimento-posizione-output-types : Create a new inserimentoPosizioneOutputType.
     *
     * @param inserimentoPosizioneOutputType the inserimentoPosizioneOutputType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new inserimentoPosizioneOutputType, or with status 400 (Bad Request) if the inserimentoPosizioneOutputType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inserimento-posizione-output-types")
    @Timed
    public ResponseEntity<InserimentoPosizioneOutputType> createInserimentoPosizioneOutputType(@Valid @RequestBody InserimentoPosizioneOutputType inserimentoPosizioneOutputType) throws URISyntaxException {
        log.debug("REST request to save InserimentoPosizioneOutputType : {}", inserimentoPosizioneOutputType);
        if (inserimentoPosizioneOutputType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("inserimentoPosizioneOutputType", "idexists", "A new inserimentoPosizioneOutputType cannot already have an ID")).body(null);
        }
        InserimentoPosizioneOutputType result = inserimentoPosizioneOutputTypeRepository.save(inserimentoPosizioneOutputType);
        return ResponseEntity.created(new URI("/api/inserimento-posizione-output-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("inserimentoPosizioneOutputType", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /inserimento-posizione-output-types : Updates an existing inserimentoPosizioneOutputType.
     *
     * @param inserimentoPosizioneOutputType the inserimentoPosizioneOutputType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated inserimentoPosizioneOutputType,
     * or with status 400 (Bad Request) if the inserimentoPosizioneOutputType is not valid,
     * or with status 500 (Internal Server Error) if the inserimentoPosizioneOutputType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/inserimento-posizione-output-types")
    @Timed
    public ResponseEntity<InserimentoPosizioneOutputType> updateInserimentoPosizioneOutputType(@Valid @RequestBody InserimentoPosizioneOutputType inserimentoPosizioneOutputType) throws URISyntaxException {
        log.debug("REST request to update InserimentoPosizioneOutputType : {}", inserimentoPosizioneOutputType);
        if (inserimentoPosizioneOutputType.getId() == null) {
            return createInserimentoPosizioneOutputType(inserimentoPosizioneOutputType);
        }
        InserimentoPosizioneOutputType result = inserimentoPosizioneOutputTypeRepository.save(inserimentoPosizioneOutputType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("inserimentoPosizioneOutputType", inserimentoPosizioneOutputType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /inserimento-posizione-output-types : get all the inserimentoPosizioneOutputTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of inserimentoPosizioneOutputTypes in body
     */
    @GetMapping("/inserimento-posizione-output-types")
    @Timed
    public List<InserimentoPosizioneOutputType> getAllInserimentoPosizioneOutputTypes() {
        log.debug("REST request to get all InserimentoPosizioneOutputTypes");
        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypes = inserimentoPosizioneOutputTypeRepository.findAll();
        return inserimentoPosizioneOutputTypes;
    }

    /**
     * GET  /inserimento-posizione-output-types/:id : get the "id" inserimentoPosizioneOutputType.
     *
     * @param id the id of the inserimentoPosizioneOutputType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the inserimentoPosizioneOutputType, or with status 404 (Not Found)
     */
    @GetMapping("/inserimento-posizione-output-types/{id}")
    @Timed
    public ResponseEntity<InserimentoPosizioneOutputType> getInserimentoPosizioneOutputType(@PathVariable Long id) {
        log.debug("REST request to get InserimentoPosizioneOutputType : {}", id);
        InserimentoPosizioneOutputType inserimentoPosizioneOutputType = inserimentoPosizioneOutputTypeRepository.findOne(id);
        return Optional.ofNullable(inserimentoPosizioneOutputType)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /inserimento-posizione-output-types/:id : delete the "id" inserimentoPosizioneOutputType.
     *
     * @param id the id of the inserimentoPosizioneOutputType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/inserimento-posizione-output-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteInserimentoPosizioneOutputType(@PathVariable Long id) {
        log.debug("REST request to delete InserimentoPosizioneOutputType : {}", id);
        inserimentoPosizioneOutputTypeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("inserimentoPosizioneOutputType", id.toString())).build();
    }

}
