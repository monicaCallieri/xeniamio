package com.consoft.xenia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.consoft.xenia.domain.StudentLevel;

import com.consoft.xenia.repository.StudentLevelRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StudentLevel.
 */
@RestController
@RequestMapping("/api")
public class StudentLevelResource {

    private final Logger log = LoggerFactory.getLogger(StudentLevelResource.class);
        
    @Inject
    private StudentLevelRepository studentLevelRepository;

    /**
     * POST  /student-levels : Create a new studentLevel.
     *
     * @param studentLevel the studentLevel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new studentLevel, or with status 400 (Bad Request) if the studentLevel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/student-levels")
    @Timed
    public ResponseEntity<StudentLevel> createStudentLevel(@RequestBody StudentLevel studentLevel) throws URISyntaxException {
        log.debug("REST request to save StudentLevel : {}", studentLevel);
        if (studentLevel.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("studentLevel", "idexists", "A new studentLevel cannot already have an ID")).body(null);
        }
        StudentLevel result = studentLevelRepository.save(studentLevel);
        return ResponseEntity.created(new URI("/api/student-levels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("studentLevel", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /student-levels : Updates an existing studentLevel.
     *
     * @param studentLevel the studentLevel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated studentLevel,
     * or with status 400 (Bad Request) if the studentLevel is not valid,
     * or with status 500 (Internal Server Error) if the studentLevel couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/student-levels")
    @Timed
    public ResponseEntity<StudentLevel> updateStudentLevel(@RequestBody StudentLevel studentLevel) throws URISyntaxException {
        log.debug("REST request to update StudentLevel : {}", studentLevel);
        if (studentLevel.getId() == null) {
            return createStudentLevel(studentLevel);
        }
        StudentLevel result = studentLevelRepository.save(studentLevel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("studentLevel", studentLevel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /student-levels : get all the studentLevels.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of studentLevels in body
     */
    @GetMapping("/student-levels")
    @Timed
    public List<StudentLevel> getAllStudentLevels() {
        log.debug("REST request to get all StudentLevels");
        List<StudentLevel> studentLevels = studentLevelRepository.findAll();
        return studentLevels;
    }

    /**
     * GET  /student-levels/:id : get the "id" studentLevel.
     *
     * @param id the id of the studentLevel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the studentLevel, or with status 404 (Not Found)
     */
    @GetMapping("/student-levels/{id}")
    @Timed
    public ResponseEntity<StudentLevel> getStudentLevel(@PathVariable Long id) {
        log.debug("REST request to get StudentLevel : {}", id);
        StudentLevel studentLevel = studentLevelRepository.findOne(id);
        return Optional.ofNullable(studentLevel)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /student-levels/:id : delete the "id" studentLevel.
     *
     * @param id the id of the studentLevel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/student-levels/{id}")
    @Timed
    public ResponseEntity<Void> deleteStudentLevel(@PathVariable Long id) {
        log.debug("REST request to delete StudentLevel : {}", id);
        studentLevelRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("studentLevel", id.toString())).build();
    }

}
