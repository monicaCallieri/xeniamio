package com.consoft.xenia.service;

import com.consoft.xenia.config.JHipsterProperties;
import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.enumeration.State;
import com.consoft.xenia.domain.pagopa.PagoPAConstants;
import com.consoft.xenia.domain.pagopa.Payment;
import com.consoft.xenia.domain.pagopa.webservicea.CodiceAvviso;
import com.consoft.xenia.domain.pagopa.webservicea.WebServiceAClient;
import com.consoft.xenia.domain.pagopa.webservicea.WebServiceAClientConfig;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.GeneratePdfFromPdfTemplate;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneInputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneResponse;
import com.consoft.xenia.domain.pagopa.webserviceb.WebServiceBClient;
import com.consoft.xenia.domain.pagopa.webserviceb.WebServiceBClientConfig;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosLista;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.PaInviaCarrelloPosizioniResponse;
import com.consoft.xenia.repository.InserimentoPosizioneInputTypeRepository;
import com.consoft.xenia.repository.InserimentoPosizioneOutputTypeRepository;
import com.consoft.xenia.repository.InviaCarrPosListaRepository;
import com.consoft.xenia.repository.InviaCarrPosOutputRepository;
import com.consoft.xenia.repository.StudentIntentionsRepository;
import com.consoft.xenia.web.rest.util.HeaderUtil;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import javax.inject.Inject;
import javax.xml.bind.JAXBElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PagoPAService {

    private final Logger log = LoggerFactory.getLogger(PagoPAService.class);

    @Inject
    private InserimentoPosizioneInputTypeRepository inserimentoPosizioneInputTypeRepository;

    @Inject
    private InserimentoPosizioneOutputTypeRepository inserimentoPosizioneOutputTypeRepository;

    @Inject
    private InviaCarrPosListaRepository inviaCarrPosListaRepository;

    @Inject
    private InviaCarrPosOutputRepository inviaCarrPosOutputRepository;

    @Inject
    private StudentIntentionsRepository studentIntentionsRepository;
    @Inject
    private MailService mailService;

    InserimentoPosizioneInputType ipit = new InserimentoPosizioneInputType();

    public PagoPAService() {

    }

    private CodiceAvviso createCodiceAvviso() {
        Integer randomNumP1 = 100000 + (int) (Math.random() * 999999);
        Integer randomNumP2 = 1000000 + (int) (Math.random() * 9999999);
        CodiceAvviso codiceAvviso = new CodiceAvviso(PagoPAConstants.auxDigit, PagoPAConstants.applicationCode, new String(randomNumP1.toString() + randomNumP2.toString()).substring(0, 13));
        return codiceAvviso;
    }

    private InserimentoPosizioneOutputType callWSAClient(CodiceAvviso codiceAvviso, Payment payment) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(WebServiceAClientConfig.class);
        ctx.refresh();

        WebServiceAClient webServiceAClient = ctx.getBean(WebServiceAClient.class);
        JAXBElement response = webServiceAClient.getInserimentoPosizioneResponse(
                PagoPAConstants.gestorePosizioniUser,//user Da rendere una costante
                PagoPAConstants.gestorePosizioniPassword,//password Da rendere una costante
                PagoPAConstants.identificativoBeneficiario,
                PagoPAConstants.tipo_riferimento_creditore,
                1, //codiceServizio
                codiceAvviso.getCodiceAvviso(), //CodiceRiferimentoCreditore
                payment.getTotal(),//importo
                codiceAvviso.getCodiceAvviso(), //Codice_identificativo_presentazione
                codiceAvviso.getIuvBase(),//Identificativo_univoco_versamento
                payment.getStudentIntentions()[0].getEducationalProposer().getName(), //causale
                payment.getStudentIntentions()[0].getUser().getLogin()//user.getEmail()
        );

        InserimentoPosizioneResponse ipr = (InserimentoPosizioneResponse) response.getValue();
        InserimentoPosizioneOutputType ipo = ipr.getInserimentoPosizioneOutput();
        log.debug("Esito: " + ipo.getEsito());
        log.debug("Codice Errore: " + ipo.getCodiceErrore());
        log.debug("Descrizione: " + ipo.getDescrizione());
        inserimentoPosizioneOutputTypeRepository.save(ipo);
        ipit = webServiceAClient.getInserimentoPosizioneInputType();
        ipit.setData_scadenza_pagamento(payment.getStudentIntentions()[0].getEducationalProposer().getStartdate());
        ipit.setInserimentoPosizioneResponse(ipo);
        inserimentoPosizioneInputTypeRepository.save(ipit);
        //Generazione PDF 
        GeneratePdfFromPdfTemplate.generatePdf(PagoPAConstants.pdfTemplate, PagoPAConstants.pdfGeneratedLocation + "Avviso_Pagamento_" + codiceAvviso.getCodiceAvviso() + ".pdf", codiceAvviso.getCodiceAvviso(), payment);
        //Spedizione via email del pdf generato (ci servirà l'oggetto USER per ricavare l'email
        mailService.sendMailWithFileAttachment(payment.getStudentIntentions()[0].getUser().getLogin(), codiceAvviso.getCodiceAvviso());
        
        

        return ipo;
    }

    private InviaCarrPosOutput callWSBClient(Payment payment, InserimentoPosizioneOutputType ipo) {
        AnnotationConfigApplicationContext ctxB = new AnnotationConfigApplicationContext();
        ctxB.register(WebServiceBClientConfig.class);
        ctxB.refresh();
        WebServiceBClient webServiceBClient = ctxB.getBean(WebServiceBClient.class);
        JAXBElement responseB = webServiceBClient.getPaInviaCarrelloPosizioneResponse(
                "N",//String spontaneo, 
                PagoPAConstants.identificativoBeneficiario,//String identificativoBeneficiario, 
                PagoPAConstants.ccp,//String ccp,
                PagoPAConstants.codicePa,//String codicePa,
                PagoPAConstants.codiceServizio,// String codiceServizio,
                "F",//String tipoIdPagatore,
                "ANONIMO",//    String identificativoPagatore,
                "ANONIMO",// String anagraficaPagatore,
                "",// String indirizzoPagatore,
                "",//  String civicoPagatore,
                "",//  String capPagatore,
                "",//   String localitaPagatore, 
                "",//String provinciaPagatore, 
                "",// String codiceNazionePagatore,
                payment.getStudentIntentions()[0].getUser().getLogin(),//String emailPagatore,
                "",// String dataScadenzaPagamento,
                payment.getTotal(),//Double importoPagamento,
                "0",// String tipoFirmaRicevuta,
                PagoPAConstants.tipo_riferimento_creditore,//  String tipoRiferimentoCreditore,
                PagoPAConstants.tipo_riferimento_creditore,//String codiceRiferimentoCreditore,
                ipo.getIdentificativo_univoco_versamento(),//String identificativoUnivocoVersamento,
                payment.getStudentIntentions()//String datiSingoloVersamento
        );
        PaInviaCarrelloPosizioniResponse picpr = (PaInviaCarrelloPosizioniResponse) responseB.getValue();
        InviaCarrPosOutput icpo = picpr.getPaInviaCarrelloPosizioniOutput();
        log.debug("Esito: " + icpo.getEsito());
        log.debug("Codice Errore: " + icpo.getCodiceErrore());
        log.debug("Descrizione: " + icpo.getDescrizione());
        //inserimentoPosizioneOutputTypeRepository.save(ipo);
        if (icpo.getIuvLista().size() > 0) {
            //da testare
            String iuv = "";
            for (int i = 0; i < icpo.getIuvLista().size(); i++) {
                iuv += icpo.getIuvLista().get(i).getIdentificativoUnivocoVersamento();
                if (i + 1 != icpo.getIuvLista().size()) {
                    iuv += "|";
                }
            }
            icpo.setIuvListaConcat(iuv);
        } else {
            icpo.setIuvListaConcat(icpo.getIuvLista().get(0).getIdentificativoUnivocoVersamento());
        }
        inviaCarrPosOutputRepository.save(icpo);
        InviaCarrPosLista icpl = webServiceBClient.getInviaCarrPosLista();
        icpl.setInviaCarrPosLista(icpo);
        inviaCarrPosListaRepository.save(icpl);
        return icpo;
    }

    private void callHttpRequest() throws MalformedURLException, IOException {
        /*POST INIZIO*/
        String nome = "pippo";
        //L'URL a cui fare la POST
        URL url = new URL("https://posttestserver.com/post.php");
        HttpURLConnection connection = (HttpURLConnection) url
                .openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type",
                "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "UTF-8");
        connection.setRequestProperty("Content-Length", Integer
                .toString(nome.getBytes().length));
        connection.setUseCaches(false);

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes("nome=" + nome + "&test=prova");
        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();
        if (connection.getResponseCode() == 200) {

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String inputLine;
            StringBuffer responsePost = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                responsePost.append(inputLine);
            }
            in.close();

            // print result
            log.debug("Body" + responsePost.toString());
        }

        log.debug("Messaggio di risposta " + connection.getResponseMessage());
        log.debug("Codice di risposta " + responseCode);
    }

    private void clearCart(StudentIntentions[] studentIntentions) {
        for (int i = 0; i < studentIntentions.length; i++) {
            studentIntentions[i].setState(State.ENROLLED);
            studentIntentions[i].setRealizationDate(LocalDate.now());
            studentIntentionsRepository.save(studentIntentions[i]);
        }
    }

    public InserimentoPosizioneInputType createPay(Payment payment) throws IOException, URISyntaxException {

        if (payment.getStudentIntentions().length < 6 && payment.getStudentIntentions().length > 0
                && payment.getStudentIntentions()[0].getEducationalProposer().getStartdate().compareTo(LocalDate.now()) >= 0) {

            CodiceAvviso codiceAvviso = createCodiceAvviso();

            //Richiamo il webServiceA per inserire la posizione sulla base della student intention che l'utente ha manifestato sul portale di ateneo
            InserimentoPosizioneOutputType ipo = callWSAClient(codiceAvviso, payment);
            
            //WSB
            InviaCarrPosOutput icpo = new InviaCarrPosOutput();
            if (ipo.getEsito().equals("OK")) {

                icpo = callWSBClient(payment, ipo);
                /*Inserire il controllo che verifica se l'esito del WSB è OK allora effettuo la chiamata HTTP*/
                if (icpo.getEsito().equals("OK")) {
                    //callHttpRequest();
                }
                clearCart(payment.getStudentIntentions());
            }

            //InserimentoPosizioneInputType result = new InserimentoPosizioneInputType();
            return ipit;
        }
        return ipit;
        /*POST FINE*/
    }

}
