package com.consoft.xenia.service.dto;

import com.consoft.xenia.config.Constants;

import com.consoft.xenia.domain.Authority;
import com.consoft.xenia.domain.Question;
import com.consoft.xenia.domain.Response;
import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.User;
import java.time.LocalDate;
import java.util.Map;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.*;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Pattern(regexp = "\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})")
    @Size(min = 5, max = 100)
    private String email;

    private boolean activated = false;

    @Size(min = 2, max = 5)
    private String langKey;

    private Set<String> authorities;
    
    private Response[] responseUser;
    
    //private LocalDate [] studentIntentionsDate;
    
    private StudentIntentions studentIntentions;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this(user.getLogin(), user.getFirstName(), user.getLastName(),
            user.getEmail(), user.getActivated(), user.getLangKey(),
            user.getAuthorities().stream().map(Authority::getName)
                .collect(Collectors.toSet()),
            user.getResponseUser(), user.getStudentIntentions());
    }

    public UserDTO(String login, String firstName, String lastName,
        String email, boolean activated, String langKey, Set<String> authorities, Response[] responseUser, StudentIntentions studentIntentions) {

        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;
        this.langKey = langKey;
        this.authorities = authorities;
        this.responseUser = responseUser;
        this.studentIntentions = studentIntentions;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActivated() {
        return activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", authorities=" + authorities +
            "}";
    }

    /**
     * @return the responseUser
     */
    public Response[] getResponseUser() {
        return responseUser;
    }

    /**
     * @param responseUser the responseUser to set
     */
    public void setResponseUser(Response[] responseUser) {
        this.responseUser = responseUser;
    }
    
    /**
     * @return the studentIntentionsDate
     */
    public StudentIntentions getStudentIntentions() {
        return studentIntentions;
    }

    /**
     * @param studentIntentions the responseUser to set
     */
    public void setStudentIntentions(StudentIntentions studentIntentions) {
        this.studentIntentions = studentIntentions;
    }
}
