/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function () {
    'use strict';

    angular.module('xeniaApp')
            .controller('CarouselController', CarouselController);

    CarouselController.$inject=['$scope'];
    function CarouselController($scope) {
        var vm = this;
        vm.myInterval = 5200;
        vm.noWrapSlides = false;
        vm.active = 1;
        vm.carouselTitle = "study with Us because. . .";
        var slides = vm.slides = [];
        var currIndex = 0;
        var host = window.location.origin+window.location.pathname;
        var index_image =0;
        vm.miaFunc = miaFunc;
        vm.addSlide = function () {
            var newWidth = 600 + slides.length + 1;
            index_image = index_image + 1;
            //var newWidth = 'vJqSAasmCEY';
            slides.push({
                //image: '//unsplash.it/' + newWidth + '/300',
                //image: 'http://web-faventia.s3-eu-west-1.amazonaws.com/wp-content/uploads/2016/10/03085826/Musei-gratis-a-Perugia-e-in-Umbria-con-domenicalmuseo.jpg',
                //image: 'http://localhost:9000/content/images/perugia.png',
                image: host+'content/images/image_carousel_'+index_image+'.png',
                //image: '//unsplash.com/collections/87821/write-read-note',
                text: ['. . . we are in Italy', '. . . we are culture', '. . . you feel home', '. . . we are not just food'][slides.length % 4],
                id: currIndex++
                
            });
        };

        vm.randomize = function () {
            var indexes = generateIndexesArray();
            assignNewIndexesToSlides(indexes);
        };

        for (var i = 0; i < 4; i++) {
            vm.addSlide();
        }

        // Randomize logic below

        function assignNewIndexesToSlides(indexes) {
            for (var i = 0, l = slides.length; i < l; i++) {
                slides[i].id = indexes.pop();
            }
        }

        function generateIndexesArray() {
            var indexes = [];
            for (var i = 0; i < currIndex; ++i) {
                indexes[i] = i;
            }
            return shuffle(indexes);
        }

        // http://stackoverflow.com/questions/962802#962890
        function shuffle(array) {
            var tmp, current, top = array.length;

            if (top) {
                while (--top) {
                    current = Math.floor(Math.random() * (top + 1));
                    tmp = array[current];
                    array[current] = array[top];
                    array[top] = tmp;
                }
            }

            return array;
        }
        
        
        function miaFunc(){
            console.log("Sono nel controller della navbar in miaFunc");
        }
    }
})();

