(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('UserInformationDetailController', UserInformationDetailController);

    UserInformationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserInformation', 'Question', 'User', 'Response'];

    function UserInformationDetailController($scope, $rootScope, $stateParams, previousState, entity, UserInformation, Question, User, Response) {
        var vm = this;

        vm.userInformation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:userInformationUpdate', function(event, result) {
            vm.userInformation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
