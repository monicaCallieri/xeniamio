(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('UserInformationCurrentUser', UserInformationCurrentUser);

    UserInformationCurrentUser.$inject = ['$resource'];

    function UserInformationCurrentUser ($resource) {
        var resourceUrl =  'api/user-informations-byUser';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
