(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('UserInformationController', UserInformationController);

    UserInformationController.$inject = ['$scope', '$state', 'UserInformation'];

    function UserInformationController ($scope, $state, UserInformation) {
        var vm = this;

        vm.userInformations = [];

        loadAll();

        function loadAll() {
            UserInformation.query(function(result) {
                vm.userInformations = result;
                vm.searchQuery = null;
            });
        }
    }
})();
