(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('UserInformation', UserInformation);

    UserInformation.$inject = ['$resource'];

    function UserInformation ($resource) {
        var resourceUrl =  'api/user-informations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
