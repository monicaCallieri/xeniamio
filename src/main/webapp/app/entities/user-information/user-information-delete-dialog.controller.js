(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('UserInformationDeleteController',UserInformationDeleteController);

    UserInformationDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserInformation'];

    function UserInformationDeleteController($uibModalInstance, entity, UserInformation) {
        var vm = this;

        vm.userInformation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserInformation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
