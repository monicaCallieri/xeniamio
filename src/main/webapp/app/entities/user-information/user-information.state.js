(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-information', {
            parent: 'entity',
            url: '/user-information',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.userInformation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-information/user-informations.html',
                    controller: 'UserInformationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userInformation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-information-detail', {
            parent: 'entity',
            url: '/user-information/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.userInformation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-information/user-information-detail.html',
                    controller: 'UserInformationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userInformation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserInformation', function($stateParams, UserInformation) {
                    return UserInformation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-information',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-information-detail.edit', {
            parent: 'user-information-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-information/user-information-dialog.html',
                    controller: 'UserInformationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserInformation', function(UserInformation) {
                            return UserInformation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-information.new', {
            parent: 'user-information',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-information/user-information-dialog.html',
                    controller: 'UserInformationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-information', null, { reload: 'user-information' });
                }, function() {
                    $state.go('user-information');
                });
            }]
        })
        .state('user-information.edit', {
            parent: 'user-information',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-information/user-information-dialog.html',
                    controller: 'UserInformationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserInformation', function(UserInformation) {
                            return UserInformation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-information', null, { reload: 'user-information' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-information.delete', {
            parent: 'user-information',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-information/user-information-delete-dialog.html',
                    controller: 'UserInformationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserInformation', function(UserInformation) {
                            return UserInformation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-information', null, { reload: 'user-information' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
