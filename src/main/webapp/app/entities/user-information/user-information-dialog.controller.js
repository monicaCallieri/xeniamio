(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('UserInformationDialogController', UserInformationDialogController);

    UserInformationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserInformation', 'Question', 'User', 'Response'];

    function UserInformationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserInformation, Question, User, Response) {
        var vm = this;

        vm.userInformation = entity;
        vm.clear = clear;
        vm.save = save;
        vm.questions = Question.query();
        vm.users = User.query();
        vm.responses = Response.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userInformation.id !== null) {
                UserInformation.update(vm.userInformation, onSaveSuccess, onSaveError);
            } else {
                UserInformation.save(vm.userInformation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:userInformationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
