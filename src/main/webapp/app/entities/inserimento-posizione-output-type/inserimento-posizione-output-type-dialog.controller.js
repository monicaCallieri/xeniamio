(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneOutputTypeDialogController', InserimentoPosizioneOutputTypeDialogController);

    InserimentoPosizioneOutputTypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InserimentoPosizioneOutputType'];

    function InserimentoPosizioneOutputTypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InserimentoPosizioneOutputType) {
        var vm = this;

        vm.inserimentoPosizioneOutputType = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.inserimentoPosizioneOutputType.id !== null) {
                InserimentoPosizioneOutputType.update(vm.inserimentoPosizioneOutputType, onSaveSuccess, onSaveError);
            } else {
                InserimentoPosizioneOutputType.save(vm.inserimentoPosizioneOutputType, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:inserimentoPosizioneOutputTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
