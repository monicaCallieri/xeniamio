(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('inserimento-posizione-output-type', {
            parent: 'entity',
            url: '/inserimento-posizione-output-type',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inserimentoPosizioneOutputType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/inserimento-posizione-output-type/inserimento-posizione-output-types.html',
                    controller: 'InserimentoPosizioneOutputTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inserimentoPosizioneOutputType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('inserimento-posizione-output-type-detail', {
            parent: 'entity',
            url: '/inserimento-posizione-output-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inserimentoPosizioneOutputType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/inserimento-posizione-output-type/inserimento-posizione-output-type-detail.html',
                    controller: 'InserimentoPosizioneOutputTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inserimentoPosizioneOutputType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InserimentoPosizioneOutputType', function($stateParams, InserimentoPosizioneOutputType) {
                    return InserimentoPosizioneOutputType.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'inserimento-posizione-output-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('inserimento-posizione-output-type-detail.edit', {
            parent: 'inserimento-posizione-output-type-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-output-type/inserimento-posizione-output-type-dialog.html',
                    controller: 'InserimentoPosizioneOutputTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InserimentoPosizioneOutputType', function(InserimentoPosizioneOutputType) {
                            return InserimentoPosizioneOutputType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('inserimento-posizione-output-type.new', {
            parent: 'inserimento-posizione-output-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-output-type/inserimento-posizione-output-type-dialog.html',
                    controller: 'InserimentoPosizioneOutputTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                esito: null,
                                codiceErrore: null,
                                descrizione: null,
                                identificativo_univoco_versamento: null,
                                codice_identificativo_presentazione: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('inserimento-posizione-output-type', null, { reload: 'inserimento-posizione-output-type' });
                }, function() {
                    $state.go('inserimento-posizione-output-type');
                });
            }]
        })
        .state('inserimento-posizione-output-type.edit', {
            parent: 'inserimento-posizione-output-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-output-type/inserimento-posizione-output-type-dialog.html',
                    controller: 'InserimentoPosizioneOutputTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InserimentoPosizioneOutputType', function(InserimentoPosizioneOutputType) {
                            return InserimentoPosizioneOutputType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('inserimento-posizione-output-type', null, { reload: 'inserimento-posizione-output-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('inserimento-posizione-output-type.delete', {
            parent: 'inserimento-posizione-output-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-output-type/inserimento-posizione-output-type-delete-dialog.html',
                    controller: 'InserimentoPosizioneOutputTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InserimentoPosizioneOutputType', function(InserimentoPosizioneOutputType) {
                            return InserimentoPosizioneOutputType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('inserimento-posizione-output-type', null, { reload: 'inserimento-posizione-output-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
