(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneOutputTypeDeleteController',InserimentoPosizioneOutputTypeDeleteController);

    InserimentoPosizioneOutputTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'InserimentoPosizioneOutputType'];

    function InserimentoPosizioneOutputTypeDeleteController($uibModalInstance, entity, InserimentoPosizioneOutputType) {
        var vm = this;

        vm.inserimentoPosizioneOutputType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InserimentoPosizioneOutputType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
