(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('InserimentoPosizioneOutputType', InserimentoPosizioneOutputType);

    InserimentoPosizioneOutputType.$inject = ['$resource'];

    function InserimentoPosizioneOutputType ($resource) {
        var resourceUrl =  'api/inserimento-posizione-output-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
