(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneOutputTypeDetailController', InserimentoPosizioneOutputTypeDetailController);

    InserimentoPosizioneOutputTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InserimentoPosizioneOutputType'];

    function InserimentoPosizioneOutputTypeDetailController($scope, $rootScope, $stateParams, previousState, entity, InserimentoPosizioneOutputType) {
        var vm = this;

        vm.inserimentoPosizioneOutputType = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:inserimentoPosizioneOutputTypeUpdate', function(event, result) {
            vm.inserimentoPosizioneOutputType = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
