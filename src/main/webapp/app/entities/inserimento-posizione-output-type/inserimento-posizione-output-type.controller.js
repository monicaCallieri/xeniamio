(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneOutputTypeController', InserimentoPosizioneOutputTypeController);

    InserimentoPosizioneOutputTypeController.$inject = ['$scope', '$state', 'InserimentoPosizioneOutputType'];

    function InserimentoPosizioneOutputTypeController ($scope, $state, InserimentoPosizioneOutputType) {
        var vm = this;

        vm.inserimentoPosizioneOutputTypes = [];

        loadAll();

        function loadAll() {
            InserimentoPosizioneOutputType.query(function(result) {
                vm.inserimentoPosizioneOutputTypes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
