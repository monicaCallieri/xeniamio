(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('PlacementTestDetailController', PlacementTestDetailController);

    PlacementTestDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PlacementTest', 'Student'];

    function PlacementTestDetailController($scope, $rootScope, $stateParams, previousState, entity, PlacementTest, Student) {
        var vm = this;

        vm.placementTest = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:placementTestUpdate', function(event, result) {
            vm.placementTest = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
