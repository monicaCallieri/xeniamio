(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('PlacementTestDeleteController',PlacementTestDeleteController);

    PlacementTestDeleteController.$inject = ['$uibModalInstance', 'entity', 'PlacementTest'];

    function PlacementTestDeleteController($uibModalInstance, entity, PlacementTest) {
        var vm = this;

        vm.placementTest = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PlacementTest.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
