(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('PlacementTestDialogController', PlacementTestDialogController);

    PlacementTestDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PlacementTest', 'Student'];

    function PlacementTestDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PlacementTest, Student) {
        var vm = this;

        vm.placementTest = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.students = Student.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.placementTest.id !== null) {
                PlacementTest.update(vm.placementTest, onSaveSuccess, onSaveError);
            } else {
                PlacementTest.save(vm.placementTest, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:placementTestUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.scheduleTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
