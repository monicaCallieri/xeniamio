(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('PlacementTestController', PlacementTestController);

    PlacementTestController.$inject = ['$scope', '$state', 'PlacementTest'];

    function PlacementTestController ($scope, $state, PlacementTest) {
        var vm = this;

        vm.placementTests = [];

        loadAll();

        function loadAll() {
            PlacementTest.query(function(result) {
                vm.placementTests = result;
                vm.searchQuery = null;
            });
        }
    }
})();
