(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('placement-test', {
            parent: 'entity',
            url: '/placement-test',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.placementTest.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/placement-test/placement-tests.html',
                    controller: 'PlacementTestController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('placementTest');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('placement-test-detail', {
            parent: 'entity',
            url: '/placement-test/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.placementTest.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/placement-test/placement-test-detail.html',
                    controller: 'PlacementTestDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('placementTest');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PlacementTest', function($stateParams, PlacementTest) {
                    return PlacementTest.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'placement-test',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('placement-test-detail.edit', {
            parent: 'placement-test-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/placement-test/placement-test-dialog.html',
                    controller: 'PlacementTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PlacementTest', function(PlacementTest) {
                            return PlacementTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('placement-test.new', {
            parent: 'placement-test',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/placement-test/placement-test-dialog.html',
                    controller: 'PlacementTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                creationDate: null,
                                scheduleTime: null,
                                location: null,
                                maxNumStud: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('placement-test', null, { reload: 'placement-test' });
                }, function() {
                    $state.go('placement-test');
                });
            }]
        })
        .state('placement-test.edit', {
            parent: 'placement-test',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/placement-test/placement-test-dialog.html',
                    controller: 'PlacementTestDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PlacementTest', function(PlacementTest) {
                            return PlacementTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('placement-test', null, { reload: 'placement-test' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('placement-test.delete', {
            parent: 'placement-test',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/placement-test/placement-test-delete-dialog.html',
                    controller: 'PlacementTestDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PlacementTest', function(PlacementTest) {
                            return PlacementTest.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('placement-test', null, { reload: 'placement-test' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
