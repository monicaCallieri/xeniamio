(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
                .state('payments', {
                    parent: 'entity',
                    url: '/payments',
                    data: {
                        authorities: ['ROLE_USER', 'ROLE_STUDENT'],
                        pageTitle: 'xeniaApp.payments.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/entities/payments/payments.html',
                            controller: 'PaymentsController',
                            controllerAs: 'pc'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                                $translatePartialLoader.addPart('studentIntentions');
                                $translatePartialLoader.addPart('courseLevel');
                                $translatePartialLoader.addPart('state');
                                $translatePartialLoader.addPart('global');
                                return $translate.refresh();
                            }]/*,
                             entity: ['$stateParams', 'StudentIntentions', function($stateParams, StudentIntentions) {
                             return StudentIntentions.query({state : 'NOTENROLLED'}).$promise;
                             }]*/
                    }
                })
    }

})();
