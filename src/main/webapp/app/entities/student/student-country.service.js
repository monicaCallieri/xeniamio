(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('StudentCountry', StudentCountry);

    StudentCountry.$inject = ['$resource'];

    function StudentCountry ($resource) {
        var resourceUrl =  'https://restcountries.eu/rest/v2/all';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',isArray: true,
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        var result = data.map(function(country) {
                            var myFilteredCountry={};
                            myFilteredCountry.translatedEnglishName=country.name;
                            myFilteredCountry.demonym=country.demonym;
                            myFilteredCountry.nativeName=country.nativeName;
                            myFilteredCountry.translatedItalianName=country.translations['it'];
                            myFilteredCountry.languageName=country.languages[0].name;
                            /*Posso aggiungere qualsiasi campo mi interessi dal servizio di restcountries*/
                            return myFilteredCountry;});
                    }
                    return result;
                }
            }
        });
    }
})();
