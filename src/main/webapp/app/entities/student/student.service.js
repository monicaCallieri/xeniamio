(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('Student', Student);

    Student.$inject = ['$resource', 'DateUtils'];

    function Student ($resource, DateUtils) {
        var resourceUrl =  'api/students/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    /*if(data instanceof Array){
                        console.log("ho un array");
                        data = data[0];
                    }*/
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateOfBirth = DateUtils.convertLocalDateFromServer(data.dateOfBirth);
                        data.scholarshipStartDate = DateUtils.convertLocalDateFromServer(data.scholarshipStartDate);
                        data.scholarshipEndDate = DateUtils.convertLocalDateFromServer(data.scholarshipEndDate);
                        data.registrationDate = DateUtils.convertLocalDateFromServer(data.registrationDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateOfBirth = DateUtils.convertLocalDateToServer(copy.dateOfBirth);
                    copy.scholarshipStartDate = DateUtils.convertLocalDateToServer(copy.scholarshipStartDate);
                    copy.scholarshipEndDate = DateUtils.convertLocalDateToServer(copy.scholarshipEndDate);
                    copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateOfBirth = DateUtils.convertLocalDateToServer(copy.dateOfBirth);
                    copy.scholarshipStartDate = DateUtils.convertLocalDateToServer(copy.scholarshipStartDate);
                    copy.scholarshipEndDate = DateUtils.convertLocalDateToServer(copy.scholarshipEndDate);
                    copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
