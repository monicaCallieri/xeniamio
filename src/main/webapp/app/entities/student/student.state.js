(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('student', {
            parent: 'entity',
            url: '/student',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT'],
                pageTitle: 'xeniaApp.student.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student/students.html',
                    controller: 'StudentController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('student');
                    $translatePartialLoader.addPart('sex');
                    $translatePartialLoader.addPart('qualification');
                    $translatePartialLoader.addPart('level');
                    $translatePartialLoader.addPart('global');
                     $translatePartialLoader.addPart('question_response');
                    return $translate.refresh();
                }]
            }
        })
        .state('student-detail', {
            parent: 'entity',
            url: '/student/{id}',
            data: {
                authorities: ['ROLE_USER','ROLE_STUDENT','ROLE_LECTURER'],
                pageTitle: 'xeniaApp.student.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student/student-detail.html',
                    controller: 'StudentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('student');
                    $translatePartialLoader.addPart('sex');
                    $translatePartialLoader.addPart('qualification');
                    $translatePartialLoader.addPart('level');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Student', function($stateParams, Student) {
                    //return Student.get({id : $stateParams.id}).$promise;
                    return Student.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'student',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('student-detail.edit', {
            parent: 'student-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_STUDENT','ROLE_LECTURER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student/student-dialog.html',
                    controller: 'StudentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Student', function(Student) {
                            return Student.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student.new', {
            parent: 'student',
            url: '/new',
            data: {
                authorities: ['ROLE_USER','ROLE_STUDENT','ROLE_LECTURER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student/student-dialog.html',
                    controller: 'StudentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                surname: null,
                                sex: null,
                                email: null,
                                phoneNumber: null,
                                cityOfBirth: null,
                                nationality: null,
                                address: null,
                                country: null,
                                stateOrProvince: null,
                                city: null,
                                profession: null,
                                dateOfBirth: null,
                                qualification: null,
                                knowledgeOfItalian: null,
                                alreadyStudent: null,
                                previousAttendYear: null,
                                scholarship: null,
                                scholarshipStartDate: null,
                                scholarshipEndDate: null,
                                scholarshipAwardedBy: null,
                                privacyAuthorize: null,
                                registrationDate: null,
                                studentNumber: null,
                                postal_code: null,
                                mobile_phone: null,
                                mother_tongue: null,
                                other_languages: null,
                                italian_studied_in: null,
                                italian_studied_for: null,
                                italian_studied_hour_week: null,
                                italian_studied_interests: null,
                                italian_certification: null,
                                italian_level: null,
                                how_long_student_at_unistrapg: null,
                                accomodation: null,
                                learning_disability: null,
                                physical_disability: null,
                                other_special_requests: null,
                                id: null
                            };
                        },
                    response: ['Response', function(Response) {
                            return Response.query({type : 'student'}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-intentions', null, { reload: true });
                }, function() {
                    $state.go('student');
                });
            }]
        })
        .state('student.edit', {
            parent: 'student',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER','ROLE_STUDENT','ROLE_LECTURER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student/student-dialog.html',
                    controller: 'StudentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Student', function(Student) {
                            return Student.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student', null, { reload: 'student' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student.delete', {
            parent: 'student',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER','ROLE_LECTURER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student/student-delete-dialog.html',
                    controller: 'StudentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Student', function(Student) {
                            return Student.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student', null, { reload: 'student' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
