(function () {
    'use strict';

    angular
            .module("xeniaApp")
            .filter("countryFilter", countryFilter);

    countryFilter.$inject = ['$filter'];

    function countryFilter($filter) {

        return function (input) {
            var myString = '';
            if (input!=undefined && input.translatedEnglishName != undefined) {
                console.log("sono nel filtro ed input.name vale: " + input.translatedEnglishName);
                var myString = input.translatedEnglishName + ' ciao ';
            }
            
            return myString;
        };
    }
})();
