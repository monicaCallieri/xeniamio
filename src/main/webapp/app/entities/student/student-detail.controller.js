(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentDetailController', StudentDetailController);

    StudentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Student', 'User', 'StudentCourse', 'PlacementTest'];

    function StudentDetailController($scope, $rootScope, $stateParams, previousState, entity, Student, User, StudentCourse, PlacementTest) {
        var vm = this;

        vm.student = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:studentUpdate', function(event, result) {
            vm.student = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
