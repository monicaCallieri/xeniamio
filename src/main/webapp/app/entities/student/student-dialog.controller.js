(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .controller('StudentDialogController', StudentDialogController);

    StudentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Student', 'User', 'StudentCourse', 'PlacementTest', 'Principal', 'UserInformationCurrentUser', 'StudentCountry', '$filter', 'response'];

    function StudentDialogController($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Student, User, StudentCourse, PlacementTest, Principal, UserInformationCurrentUser, StudentCountry, $filter, response) {
        var vm = this;

        vm.student = entity;
        //vm.student = saveStudent();
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.studentcourses = StudentCourse.query();
        vm.placementTests = PlacementTest.query();
        vm.login = getAccount();
        vm.student.alreadyStudent = "false";
        vm.user = {};
        vm.alert = false;
        vm.closeAlert = closeAlert;
        vm.responseKnow = {};
        vm.setNationality = setNationality;
        vm.otherLanguages = [
            'Francese', 'Spagnolo', 'Portoghese', 'Arabo', 'Inglese', 'Tedesco', 'Russo', 'Greco', 'Cinese', 'Giapponese'
        ];
        vm.professions = ['Accompagnatore Turistico', 'Agente di Commercio', 'Artigiano', 'Casalinga', 'Commerciante', 'Consulente', 'Designer', 'Dirigente/Funzionario', 'Ecclesiastico', 'Impiegato', 'Imprenditore', 'Insegnante', 'Libero professionista', 'Musicista', 'Medico', 'Operaio', 'Forze Armate', 'Pensionato', 'Studente', 'Stilista', 'In cerca di Occupazione', 'Altro'];
        vm.response = response;
        vm.someSelected = someSelected;

        StudentCountry.get(function (result) {
            vm.countries = result;
        });
        vm.student.registrationDate = new Date();
        //vm.student.country=$filter('countryFilter')(vm.countries);

        function setNationality() {
            //console.log("sono nella setNationality()");
            //console.log("vm.student.country: " + vm.student.country);
            vm.countries.find(function (value, index) {
                if (value.translatedEnglishName === vm.student.country) {
                    //console.log('Visited index ' + index + ' with value ' + value.demonym);
                    vm.student.nationality = value.demonym;
                    vm.student.mother_tongue = value.languageName;
                }

            });

            //
        }
        
        function someSelected(idQues) {
            for (var i = 0; i < vm.response.length; i++) {
                if (vm.response[i].question.id === idQues && vm.response[i].selected) {
                    return true;
                }
            }
            return false;
        }

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.login = account.login;
                load(vm.login);
            });
        }

        function load(login) {
            User.get({login: login}, function (result) {
                vm.user = result;
                vm.student.email = vm.user.email;
                UserInformationCurrentUser.get(function (result) {
                    vm.responseKnow = result;
                    if (vm.responseKnow.response != undefined) {
                        vm.student.knowledgeOfItalian = vm.responseKnow.response.response.toUpperCase();
                        //console.log("Informazioni prese: " + vm.student.knowledgeOfItalian);
                        if (vm.student.knowledgeOfItalian === 'NO')
                            vm.student.italian_certification_ownership = 'NO';
                    }
                });
            });

        }

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            //console.log("nella save, il country vale: " + vm.student.country);

            if (vm.student.previousAttendYear !== null && vm.student.how_long_student_at_unistrapg != null) {
                vm.student.alreadyStudent = "true";
            } else {
                if (vm.student.previousAttendYear === null && vm.student.how_long_student_at_unistrapg === null) {
                    vm.student.alreadyStudent = "false";
                } else {
                    //console.log("Devo segnalare di completare meglio la sezione di Already Student facendogli inserire tutti e due i campi")
                    document.getElementById('messageAlreadyStudent').style.display = "block";
                    vm.alert = true;
                }
            }

            if (vm.student.scholarshipAwardedBy !== null && vm.student.scholarshipEndDate !== null && vm.student.scholarshipStartDate !== null) {
                vm.student.scholarship = "true";
                //document.getElementById('message').style.display = "block";
            } else {
                //console.log("Devo segnalare di completare meglio la sezione di Scholarship facendogli inserire tutti e tre i campi")
                document.getElementById('messageScholarship').style.display = "block";
                vm.alert = true;
            }
            /*if (vm.student.scholarshipAwardedBy === null && vm.student.scholarshipEndDate === null && vm.student.scholarshipStartDate === null) {
             vm.student.scholarship = "false";
             document.getElementById('message').style.display = "block";
             }*/
//            if (vm.student.scholarship === "true") {

            /*Validazione dei dati in base al valore inserito in knowledgeOfItalian*/
            if (vm.knowledgeOfItalian === 'NO') {
                vm.student.italian_studied_in = '';
                vm.student.italian_studied_for = '';
                vm.student.italian_studied_hour_week = '';
            }

            /*Validazione dei dati in base al valore inserito in italian_certification_ownership*/
            if (vm.student.italian_certification_ownership === 'NO') {
                vm.student.italian_certification = '';
            }

            /*Creazione risposta multipla per il campo: vm.field_italian_studied_interests.write*/
            vm.student.iois = [];
            vm.response.forEach(function (entry) {
                if (entry.selected) {
                    vm.student.iois.push(entry);
                }
            });

            vm.student.email = vm.user.email;
            vm.student.test = "prova";
            vm.student.user = vm.user;
            vm.isSaving = true;
            if (vm.student.id !== null) {
                Student.update(vm.student, onSaveSuccess, onSaveError);
            } else {
                Student.save(vm.student, onSaveSuccess, onSaveError);
            }

        }

        function onSaveSuccess(result) {
            $scope.$emit('xeniaApp:studentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            //document.location.href = "/#/student-intentions";
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateOfBirth = false;
        vm.datePickerOpenStatus.scholarshipStartDate = false;
        vm.datePickerOpenStatus.scholarshipEndDate = false;
        vm.datePickerOpenStatus.registrationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }

        function closeAlert() {
            console.log("Devo chiudere l'alert");
            vm.alert = false;
        }
    }
})();
