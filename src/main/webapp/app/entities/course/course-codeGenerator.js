(function () {
    'use strict';

    angular
            .module("xeniaApp")
            .filter("codeGenerator", codeGenerator);

    codeGenerator.$inject = ['$filter'];

    function codeGenerator($filter) {

        /*Vettore per la traduzione dei mesi*/
        var monthTrad = ["GEN", "FEB", "MAR", "APR", "MAG", "GIU", "LUG", "AGO", "SET", "OTT", "NOV", "DIC"];

        return function (course) {
            /*setto il valore iniziale della tipologia del corso*/
            course.intensive = 0;
            course.marcoPolo = 0;
            course.turandot = 0;
            
            /*Inserimento del livello all'interno del codice del corso*/
            var code = course.level;
            var date = new Date(course.startDate);
            
            /*verifica della tipologia del corso per la corretta crezione del codice */
            if (course.level !== 'SPE') {
                if (course.typeOfCourse === "MAPO") {
                    course.marcoPolo = 1;
                    code = code + "" + course.typeOfCourse;
                } else if (course.typeOfCourse === "TURA") {
                    course.turandot = 1;
                    code = code + "" + course.typeOfCourse;
                } else if (course.typeOfCourse === "I") {
                    course.intensive = 1;
                    code = code + "" + course.typeOfCourse;
                } else if (course.typeOfCourse === "O") {
                    code = code + "" + course.typeOfCourse;
                }
            }
            if (course.typeOfCourse === "I" || course.typeOfCourse === "O" || course.level === "SPE") {
                var month = date.getMonth();//0 based, gen = 0
                code = code + monthTrad[month];
            }
            
            /*Inserimento della parte del codice del corso relativa all'anno*/
            var year = date.getFullYear();
            code = code + year;
            
            /*Inserimento della durata nel codice del corso
             * la durata può essere MENSILE (MEN) TRIMESTRALE (TRI) ANNUALE (ANN) o G00,
             * indica corsi con durata inferiore al mese*/
            var duration = (((course.endDate - course.startDate) / (1000 * 24 * 60 * 60))+1);
            var durationString;
            if(duration > 28 && duration < 35)
                durationString = 'MEN';
            else if(duration > 85 && duration < 100)
                durationString = 'TRI';
            else if(duration > 350 && duration < 370)
                durationString = 'ANN';
            else if(duration < 31)
                if(duration <10)
                    durationString = 'G0'+duration;
                else
                    durationString = 'G'+duration;
            else if(duration > 370)
                durationString = duration;
            code = code + durationString;
            
            /*Inserimento della parte del codice del corso relativa al numero del corso,
             * attualmente settata manualmente a 00*/
            code = code + '00';
            
            course.code = code;
            return code;
        }

    }

}
)();
