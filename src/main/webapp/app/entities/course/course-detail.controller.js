(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('CourseDetailController', CourseDetailController);

    CourseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Course', 'StudentCourse', 'Lecturer'];

    function CourseDetailController($scope, $rootScope, $stateParams, previousState, entity, Course, StudentCourse, Lecturer) {
        var vm = this;

        vm.course = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:courseUpdate', function(event, result) {
            vm.course = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
