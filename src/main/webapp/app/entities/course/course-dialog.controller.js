(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .controller('CourseDialogController', CourseDialogController);

    CourseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$filter', 'entity', 'Course', 'StudentCourse', 'Lecturer'];

    function CourseDialogController($timeout, $scope, $stateParams, $uibModalInstance, $filter, entity, Course, StudentCourse, Lecturer) {
        var vm = this;
        vm.course = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendarStart = openCalendarStart;
        vm.openCalendarEnd = openCalendarEnd;
        vm.save = save;
        vm.studentcourses = StudentCourse.query();
        vm.lecturers = Lecturer.query();
        vm.course.code = vm.course.typeOfCourse;

        vm.startDay = getDate();

        function getDate() {
            var day = new Date();
            day.setUTCDate(day.getUTCDate() + 1);
            if (day == null)
                return null;
            return day;
        }

        vm.datepickerStartDateOptions = {
            minDate: vm.startDay,
            dateDisabled: disabled
        };
        vm.datepickerEndDateOptions = {
            minDate: vm.startDay,
            dateDisabled: disabled
        };

        /*metodo che si occupa di settare le date che devono essere disabilitate
         * perchè sono considerate festività*/
        function disabled(data) {
            var date = data.date,
                    mode = data.mode;
            //il metodo getMonth() è 0-based, quindi Gennaio è 0 e così via
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6|| 
                    (date.getMonth()===0&&date.getDate()===1)||     /*primo giorno dell'anno*/
                    (date.getMonth()===3&&date.getDate()===25)||    /*giorno della liberazione*/
                    (date.getMonth()===4&&date.getDate()===1)||     /*festa del lavoro*/
                    (date.getMonth()===5&&date.getDate()===2)||     /*festa della repubblica*/
                    (date.getMonth()===7&&date.getDate()===15)||    /*ferragosto*/
                    (date.getMonth()===10&&date.getDate()===1)||    /*festa dei santi*/
                    (date.getMonth()===11&&date.getDate()===8)||    /*festa dell'immacolata*/
                    (date.getMonth()===11&&date.getDate()===25)     /*Natale*/
                    );
        }

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            /*INIZIO parte di codice che verifica che il giorno settato come data di 
             * inizio del corso non sia un giorno festivo, cioè un sabato o una domenica*/
            if (vm.course.startDate.getDay() == 0)//il giorno 0 corrisponde a domenica
                vm.course.startDate.setUTCDate(vm.course.startDate.getUTCDate() + 1);
            if (vm.course.startDate.getDay() == 6)//il giorno 6 corrisponde a sabato
                vm.course.startDate.setUTCDate(vm.course.startDate.getUTCDate() + 2);
            if (vm.course.endDate.getDay() == 0)
                vm.course.endDate.setUTCDate(vm.course.endDate.getUTCDate() + 1);
            if (vm.course.endDate.getDay() == 6)
                vm.course.endDate.setUTCDate(vm.course.endDate.getUTCDate() + 2);
            /*FINE*/
            
            vm.error = false;
            vm.course.code = $filter('codeGenerator')(vm.course);
            vm.course.duration = ((vm.course.endDate - vm.course.startDate) / (1000 * 24 * 60 * 60)) + 1;
            vm.isSaving = true;
            vm.course.creationDate = new Date();
            if (vm.course.id !== null) {
                Course.update(vm.course, onSaveSuccess, onSaveError);
            } else {
                Course.save(vm.course, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('xeniaApp:courseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.startDate = false;
        vm.datePickerOpenStatus.endDate = false;

        function openCalendarStart(date) {

            /*if (vm.course.endDate != null) {
                console.log("End date: " + vm.course.endDate);
                vm.datepickerStartDateOptions.maxDate = vm.course.endDate;
            } else {
                vm.datepickerStartDateOptions.maxDate = null;
            }*/
            vm.datePickerOpenStatus[date] = true;
        }

        function openCalendarEnd(date) {
            /*vm.course.endDate = null;
            if (vm.course.startDate != null) {
                vm.datepickerEndDateOptions.minDate = vm.course.startDate;
                vm.course.endDate = vm.course.startDate;
            }*/
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
