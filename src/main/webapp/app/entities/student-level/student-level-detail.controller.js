(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentLevelDetailController', StudentLevelDetailController);

    StudentLevelDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'StudentLevel', 'Student'];

    function StudentLevelDetailController($scope, $rootScope, $stateParams, previousState, entity, StudentLevel, Student) {
        var vm = this;

        vm.studentLevel = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:studentLevelUpdate', function(event, result) {
            vm.studentLevel = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
