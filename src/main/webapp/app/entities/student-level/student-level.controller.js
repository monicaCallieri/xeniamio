(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentLevelController', StudentLevelController);

    StudentLevelController.$inject = ['$scope', '$state', 'StudentLevel'];

    function StudentLevelController ($scope, $state, StudentLevel) {
        var vm = this;

        vm.studentLevels = [];

        loadAll();

        function loadAll() {
            StudentLevel.query(function(result) {
                vm.studentLevels = result;
                vm.searchQuery = null;
            });
        }
    }
})();
