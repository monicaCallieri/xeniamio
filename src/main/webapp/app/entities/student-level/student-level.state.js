(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('student-level', {
            parent: 'entity',
            url: '/student-level',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.studentLevel.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student-level/student-levels.html',
                    controller: 'StudentLevelController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('studentLevel');
                    $translatePartialLoader.addPart('courseLevel');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('student-level-detail', {
            parent: 'entity',
            url: '/student-level/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.studentLevel.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student-level/student-level-detail.html',
                    controller: 'StudentLevelDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('studentLevel');
                    $translatePartialLoader.addPart('courseLevel');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'StudentLevel', function($stateParams, StudentLevel) {
                    return StudentLevel.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'student-level',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('student-level-detail.edit', {
            parent: 'student-level-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-level/student-level-dialog.html',
                    controller: 'StudentLevelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['StudentLevel', function(StudentLevel) {
                            return StudentLevel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student-level.new', {
            parent: 'student-level',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-level/student-level-dialog.html',
                    controller: 'StudentLevelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                level: null,
                                type: null,
                                date: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('student-level', null, { reload: 'student-level' });
                }, function() {
                    $state.go('student-level');
                });
            }]
        })
        .state('student-level.edit', {
            parent: 'student-level',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-level/student-level-dialog.html',
                    controller: 'StudentLevelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['StudentLevel', function(StudentLevel) {
                            return StudentLevel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-level', null, { reload: 'student-level' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student-level.delete', {
            parent: 'student-level',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-level/student-level-delete-dialog.html',
                    controller: 'StudentLevelDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['StudentLevel', function(StudentLevel) {
                            return StudentLevel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-level', null, { reload: 'student-level' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
