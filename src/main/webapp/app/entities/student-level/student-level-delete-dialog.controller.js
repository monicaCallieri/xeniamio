(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentLevelDeleteController',StudentLevelDeleteController);

    StudentLevelDeleteController.$inject = ['$uibModalInstance', 'entity', 'StudentLevel'];

    function StudentLevelDeleteController($uibModalInstance, entity, StudentLevel) {
        var vm = this;

        vm.studentLevel = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            StudentLevel.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
