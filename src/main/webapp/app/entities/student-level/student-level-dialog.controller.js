(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentLevelDialogController', StudentLevelDialogController);

    StudentLevelDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'StudentLevel', 'Student'];

    function StudentLevelDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, StudentLevel, Student) {
        var vm = this;

        vm.studentLevel = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.students = Student.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.studentLevel.id !== null) {
                StudentLevel.update(vm.studentLevel, onSaveSuccess, onSaveError);
            } else {
                StudentLevel.save(vm.studentLevel, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:studentLevelUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
