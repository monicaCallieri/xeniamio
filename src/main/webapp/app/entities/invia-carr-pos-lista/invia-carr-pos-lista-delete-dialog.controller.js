(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosListaDeleteController',InviaCarrPosListaDeleteController);

    InviaCarrPosListaDeleteController.$inject = ['$uibModalInstance', 'entity', 'InviaCarrPosLista'];

    function InviaCarrPosListaDeleteController($uibModalInstance, entity, InviaCarrPosLista) {
        var vm = this;

        vm.inviaCarrPosLista = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InviaCarrPosLista.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
