(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosListaController', InviaCarrPosListaController);

    InviaCarrPosListaController.$inject = ['$scope', '$state', 'InviaCarrPosLista'];

    function InviaCarrPosListaController ($scope, $state, InviaCarrPosLista) {
        var vm = this;

        vm.inviaCarrPosListas = [];

        loadAll();

        function loadAll() {
            InviaCarrPosLista.query(function(result) {
                vm.inviaCarrPosListas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
