(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('invia-carr-pos-lista', {
            parent: 'entity',
            url: '/invia-carr-pos-lista',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inviaCarrPosLista.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/invia-carr-pos-lista/invia-carr-pos-listas.html',
                    controller: 'InviaCarrPosListaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inviaCarrPosLista');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('invia-carr-pos-lista-detail', {
            parent: 'entity',
            url: '/invia-carr-pos-lista/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inviaCarrPosLista.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/invia-carr-pos-lista/invia-carr-pos-lista-detail.html',
                    controller: 'InviaCarrPosListaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inviaCarrPosLista');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InviaCarrPosLista', function($stateParams, InviaCarrPosLista) {
                    return InviaCarrPosLista.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'invia-carr-pos-lista',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('invia-carr-pos-lista-detail.edit', {
            parent: 'invia-carr-pos-lista-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-lista/invia-carr-pos-lista-dialog.html',
                    controller: 'InviaCarrPosListaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InviaCarrPosLista', function(InviaCarrPosLista) {
                            return InviaCarrPosLista.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('invia-carr-pos-lista.new', {
            parent: 'invia-carr-pos-lista',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-lista/invia-carr-pos-lista-dialog.html',
                    controller: 'InviaCarrPosListaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                spontaneo: null,
                                identificativoBeneficiario: null,
                                ccp: null,
                                codicePa: null,
                                codiceServizio: null,
                                tipoIdPagatore: null,
                                identificativoPagatore: null,
                                anagraficaPagatore: null,
                                indirizzoPagatore: null,
                                civicoPagatore: null,
                                capPagatore: null,
                                localitaPagatore: null,
                                provinciaPagatore: null,
                                codiceNazionePagatore: null,
                                emailPagatore: null,
                                dataScadenzaPagamento: null,
                                importoPagamento: null,
                                tipoFirmaRicevuta: null,
                                tipoRiferimentoCreditore: null,
                                codiceRiferimentoCreditore: null,
                                identificativoUnivocoVersamento: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('invia-carr-pos-lista', null, { reload: 'invia-carr-pos-lista' });
                }, function() {
                    $state.go('invia-carr-pos-lista');
                });
            }]
        })
        .state('invia-carr-pos-lista.edit', {
            parent: 'invia-carr-pos-lista',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-lista/invia-carr-pos-lista-dialog.html',
                    controller: 'InviaCarrPosListaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InviaCarrPosLista', function(InviaCarrPosLista) {
                            return InviaCarrPosLista.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('invia-carr-pos-lista', null, { reload: 'invia-carr-pos-lista' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('invia-carr-pos-lista.delete', {
            parent: 'invia-carr-pos-lista',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-lista/invia-carr-pos-lista-delete-dialog.html',
                    controller: 'InviaCarrPosListaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InviaCarrPosLista', function(InviaCarrPosLista) {
                            return InviaCarrPosLista.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('invia-carr-pos-lista', null, { reload: 'invia-carr-pos-lista' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
