(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosListaDetailController', InviaCarrPosListaDetailController);

    InviaCarrPosListaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InviaCarrPosLista', 'InviaCarrPosOutput', 'DatiSingVersLista'];

    function InviaCarrPosListaDetailController($scope, $rootScope, $stateParams, previousState, entity, InviaCarrPosLista, InviaCarrPosOutput, DatiSingVersLista) {
        var vm = this;

        vm.inviaCarrPosLista = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:inviaCarrPosListaUpdate', function(event, result) {
            vm.inviaCarrPosLista = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
