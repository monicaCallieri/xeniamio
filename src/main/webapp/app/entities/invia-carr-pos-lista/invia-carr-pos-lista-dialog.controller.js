(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosListaDialogController', InviaCarrPosListaDialogController);

    InviaCarrPosListaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'InviaCarrPosLista', 'InviaCarrPosOutput', 'DatiSingVersLista'];

    function InviaCarrPosListaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, InviaCarrPosLista, InviaCarrPosOutput, DatiSingVersLista) {
        var vm = this;

        vm.inviaCarrPosLista = entity;
        vm.clear = clear;
        vm.save = save;
        vm.inviacarrposlistas = InviaCarrPosOutput.query({filter: 'requst-is-null'});
        $q.all([vm.inviaCarrPosLista.$promise, vm.inviacarrposlistas.$promise]).then(function() {
            if (!vm.inviaCarrPosLista.inviaCarrPosLista || !vm.inviaCarrPosLista.inviaCarrPosLista.id) {
                return $q.reject();
            }
            return InviaCarrPosOutput.get({id : vm.inviaCarrPosLista.inviaCarrPosLista.id}).$promise;
        }).then(function(inviaCarrPosLista) {
            vm.inviacarrposlistas.push(inviaCarrPosLista);
        });
        vm.datisingverslistas = DatiSingVersLista.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.inviaCarrPosLista.id !== null) {
                InviaCarrPosLista.update(vm.inviaCarrPosLista, onSaveSuccess, onSaveError);
            } else {
                InviaCarrPosLista.save(vm.inviaCarrPosLista, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:inviaCarrPosListaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
