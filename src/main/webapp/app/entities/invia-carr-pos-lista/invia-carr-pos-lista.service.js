(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('InviaCarrPosLista', InviaCarrPosLista);

    InviaCarrPosLista.$inject = ['$resource'];

    function InviaCarrPosLista ($resource) {
        var resourceUrl =  'api/invia-carr-pos-listas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
