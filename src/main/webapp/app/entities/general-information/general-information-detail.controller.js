(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('GeneralInformationDetailController', GeneralInformationDetailController);

    GeneralInformationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'GeneralInformation', 'UserInformation', 'Response'];

    function GeneralInformationDetailController($scope, $rootScope, $stateParams, previousState, entity, GeneralInformation, UserInformation, Response) {
        var vm = this;

        vm.generalInformation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:generalInformationUpdate', function(event, result) {
            vm.generalInformation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
