(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('GeneralInformationController', GeneralInformationController);

    GeneralInformationController.$inject = ['$scope', '$state', 'GeneralInformation'];

    function GeneralInformationController ($scope, $state, GeneralInformation) {
        var vm = this;

        vm.generalInformations = [];

        loadAll();

        function loadAll() {
            GeneralInformation.query(function(result) {
                vm.generalInformations = result;
                vm.searchQuery = null;
            });
        }
    }
})();
