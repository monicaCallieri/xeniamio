(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('GeneralInformationDeleteController',GeneralInformationDeleteController);

    GeneralInformationDeleteController.$inject = ['$uibModalInstance', 'entity', 'GeneralInformation'];

    function GeneralInformationDeleteController($uibModalInstance, entity, GeneralInformation) {
        var vm = this;

        vm.generalInformation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            GeneralInformation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
