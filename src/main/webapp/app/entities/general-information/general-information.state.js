(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('general-information', {
            parent: 'entity',
            url: '/general-information',
            data: {
                authorities: ['ROLE_USER'],
                //roles: [],
                pageTitle: 'xeniaApp.generalInformation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/general-information/general-informations.html',
                    controller: 'GeneralInformationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('generalInformation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('general-information-detail', {
            parent: 'entity',
            url: '/general-information/{id}',
            data: {
                authorities: ['ROLE_USER'],
                //roles: [],
                pageTitle: 'xeniaApp.generalInformation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/general-information/general-information-detail.html',
                    controller: 'GeneralInformationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('generalInformation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'GeneralInformation', function($stateParams, GeneralInformation) {
                    return GeneralInformation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'general-information',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('general-information-detail.edit', {
            parent: 'general-information-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
                //roles: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/general-information/general-information-dialog.html',
                    controller: 'GeneralInformationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GeneralInformation', function(GeneralInformation) {
                            return GeneralInformation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('general-information.new', {
            parent: 'general-information',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
                //roles: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/general-information/general-information-dialog.html',
                    controller: 'GeneralInformationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                question: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('general-information', null, { reload: 'general-information' });
                }, function() {
                    $state.go('general-information');
                });
            }]
        })
        .state('general-information.edit', {
            parent: 'general-information',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
                //roles: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/general-information/general-information-dialog.html',
                    controller: 'GeneralInformationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['GeneralInformation', function(GeneralInformation) {
                            return GeneralInformation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('general-information', null, { reload: 'general-information' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('general-information.delete', {
            parent: 'general-information',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
                //roles: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/general-information/general-information-delete-dialog.html',
                    controller: 'GeneralInformationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['GeneralInformation', function(GeneralInformation) {
                            return GeneralInformation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('general-information', null, { reload: 'general-information' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
