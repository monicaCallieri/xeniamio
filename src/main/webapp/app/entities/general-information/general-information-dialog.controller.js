(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('GeneralInformationDialogController', GeneralInformationDialogController);

    GeneralInformationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'GeneralInformation', 'UserInformation', 'Response'];

    function GeneralInformationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, GeneralInformation, UserInformation, Response) {
        var vm = this;

        vm.generalInformation = entity;
        vm.clear = clear;
        vm.save = save;
        vm.userinformations = UserInformation.query();
        vm.responses = Response.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.generalInformation.id !== null) {
                GeneralInformation.update(vm.generalInformation, onSaveSuccess, onSaveError);
            } else {
                GeneralInformation.save(vm.generalInformation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:generalInformationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
