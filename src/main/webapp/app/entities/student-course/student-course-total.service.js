(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('StudentCourseTotal', StudentCourseTotal);

    StudentCourseTotal.$inject = ['$resource', 'DateUtils'];

    function StudentCourseTotal ($resource, DateUtils) {
        var resourceUrl =  'api/student-courses-total';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: false}
            /*'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.registrationDate = DateUtils.convertLocalDateFromServer(data.registrationDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            }*/
        });
    }
})();
