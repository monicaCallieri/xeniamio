(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentCourseController', StudentCourseController);

    StudentCourseController.$inject = ['$scope', '$state', 'StudentCourse'];

    function StudentCourseController ($scope, $state, StudentCourse) {
        var vm = this;

        vm.studentCourses = [];

        loadAll();

        function loadAll() {
            StudentCourse.query(function(result) {
                vm.studentCourses = result;
                vm.searchQuery = null;
            });
        }
    }
})();
