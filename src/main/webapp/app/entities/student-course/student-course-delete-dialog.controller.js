(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentCourseDeleteController',StudentCourseDeleteController);

    StudentCourseDeleteController.$inject = ['$uibModalInstance', 'entity', 'StudentCourse'];

    function StudentCourseDeleteController($uibModalInstance, entity, StudentCourse) {
        var vm = this;

        vm.studentCourse = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            StudentCourse.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
