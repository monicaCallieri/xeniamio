(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentCourseDialogController', StudentCourseDialogController);

    StudentCourseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'StudentCourse', 'Student', 'Course'];

    function StudentCourseDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, StudentCourse, Student, Course) {
        var vm = this;

        vm.studentCourse = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.students = Student.query();
        vm.courses = Course.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.studentCourse.id !== null) {
                StudentCourse.update(vm.studentCourse, onSaveSuccess, onSaveError);
            } else {
                StudentCourse.save(vm.studentCourse, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:studentCourseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.registrationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
