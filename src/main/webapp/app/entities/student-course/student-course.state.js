(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('student-course', {
            parent: 'entity',
            url: '/student-course?courseCode',
            data: {
                authorities: ['ROLE_USER', 'ROLE_LECTURER'],
                pageTitle: 'xeniaApp.studentCourse.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student-course/student-courses.html',
                    controller: 'StudentCourseController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('studentCourse');
                                $translatePartialLoader.addPart('student');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
                    }, params: {
                        courseCode: null
            }
        })
        .state('student-course-detail', {
            parent: 'entity',
            url: '/student-course/{id}',
            data: {
                        authorities: ['ROLE_USER', 'ROLE_LECTURER'],
                pageTitle: 'xeniaApp.studentCourse.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student-course/student-course-detail.html',
                    controller: 'StudentCourseDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('studentCourse');
                    return $translate.refresh();
                }],
                        entity: ['$stateParams', 'StudentCourse', function ($stateParams, StudentCourse) {
                                return StudentCourse.get({id: $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'student-course',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('student-course-detail.edit', {
            parent: 'student-course-detail',
            url: '/detail/edit',
            data: {
                        authorities: ['ROLE_USER', 'ROLE_LECTURER']
            },
                    onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-course/student-course-dialog.html',
                    controller: 'StudentCourseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                                    entity: ['StudentCourse', function (StudentCourse) {
                                            return StudentCourse.get({id: $stateParams.id}).$promise;
                        }]
                    }
                            }).result.then(function () {
                                $state.go('^', {}, {reload: false});
                            }, function () {
                    $state.go('^');
                });
            }]
        })
        .state('student-course.new', {
            parent: 'student-course',
            url: '/new',
            data: {
                        authorities: ['ROLE_USER', 'ROLE_LECTURER']
            },
                    onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-course/student-course-dialog.html',
                    controller: 'StudentCourseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                registrationDate: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('student-course', null, { reload: 'student-course' });
                }, function() {
                    $state.go('student-course');
                });
            }]
        })
        .state('student-course.edit', {
            parent: 'student-course',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-course/student-course-dialog.html',
                    controller: 'StudentCourseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['StudentCourse', function(StudentCourse) {
                            return StudentCourse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-course', null, { reload: 'student-course' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student-course.delete', {
            parent: 'student-course',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-course/student-course-delete-dialog.html',
                    controller: 'StudentCourseDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['StudentCourse', function(StudentCourse) {
                            return StudentCourse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-course', null, { reload: 'student-course' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
