(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentCourseDetailController', StudentCourseDetailController);

    StudentCourseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'StudentCourse', 'Student', 'Course'];

    function StudentCourseDetailController($scope, $rootScope, $stateParams, previousState, entity, StudentCourse, Student, Course) {
        var vm = this;

        vm.studentCourse = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:studentCourseUpdate', function(event, result) {
            vm.studentCourse = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
