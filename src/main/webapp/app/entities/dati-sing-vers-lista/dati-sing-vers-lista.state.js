(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dati-sing-vers-lista', {
            parent: 'entity',
            url: '/dati-sing-vers-lista',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.datiSingVersLista.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dati-sing-vers-lista/dati-sing-vers-listas.html',
                    controller: 'DatiSingVersListaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('datiSingVersLista');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('dati-sing-vers-lista-detail', {
            parent: 'entity',
            url: '/dati-sing-vers-lista/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.datiSingVersLista.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dati-sing-vers-lista/dati-sing-vers-lista-detail.html',
                    controller: 'DatiSingVersListaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('datiSingVersLista');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DatiSingVersLista', function($stateParams, DatiSingVersLista) {
                    return DatiSingVersLista.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dati-sing-vers-lista',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dati-sing-vers-lista-detail.edit', {
            parent: 'dati-sing-vers-lista-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dati-sing-vers-lista/dati-sing-vers-lista-dialog.html',
                    controller: 'DatiSingVersListaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DatiSingVersLista', function(DatiSingVersLista) {
                            return DatiSingVersLista.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dati-sing-vers-lista.new', {
            parent: 'dati-sing-vers-lista',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dati-sing-vers-lista/dati-sing-vers-lista-dialog.html',
                    controller: 'DatiSingVersListaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                importoSingoloVersamento: null,
                                commissioniCaricoPa: null,
                                ibanAccredito: null,
                                bicAccredito: null,
                                ibanAppoggio: null,
                                bicAppoggio: null,
                                credenzialiPagatore: null,
                                caulsaleVersamento: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dati-sing-vers-lista', null, { reload: 'dati-sing-vers-lista' });
                }, function() {
                    $state.go('dati-sing-vers-lista');
                });
            }]
        })
        .state('dati-sing-vers-lista.edit', {
            parent: 'dati-sing-vers-lista',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dati-sing-vers-lista/dati-sing-vers-lista-dialog.html',
                    controller: 'DatiSingVersListaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DatiSingVersLista', function(DatiSingVersLista) {
                            return DatiSingVersLista.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dati-sing-vers-lista', null, { reload: 'dati-sing-vers-lista' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dati-sing-vers-lista.delete', {
            parent: 'dati-sing-vers-lista',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dati-sing-vers-lista/dati-sing-vers-lista-delete-dialog.html',
                    controller: 'DatiSingVersListaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DatiSingVersLista', function(DatiSingVersLista) {
                            return DatiSingVersLista.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dati-sing-vers-lista', null, { reload: 'dati-sing-vers-lista' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
