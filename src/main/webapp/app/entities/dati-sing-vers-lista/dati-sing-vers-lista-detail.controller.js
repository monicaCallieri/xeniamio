(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('DatiSingVersListaDetailController', DatiSingVersListaDetailController);

    DatiSingVersListaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DatiSingVersLista', 'InviaCarrPosLista'];

    function DatiSingVersListaDetailController($scope, $rootScope, $stateParams, previousState, entity, DatiSingVersLista, InviaCarrPosLista) {
        var vm = this;

        vm.datiSingVersLista = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:datiSingVersListaUpdate', function(event, result) {
            vm.datiSingVersLista = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
