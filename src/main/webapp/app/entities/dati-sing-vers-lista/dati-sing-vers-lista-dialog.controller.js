(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('DatiSingVersListaDialogController', DatiSingVersListaDialogController);

    DatiSingVersListaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DatiSingVersLista', 'InviaCarrPosLista'];

    function DatiSingVersListaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DatiSingVersLista, InviaCarrPosLista) {
        var vm = this;

        vm.datiSingVersLista = entity;
        vm.clear = clear;
        vm.save = save;
        vm.inviacarrposlistas = InviaCarrPosLista.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.datiSingVersLista.id !== null) {
                DatiSingVersLista.update(vm.datiSingVersLista, onSaveSuccess, onSaveError);
            } else {
                DatiSingVersLista.save(vm.datiSingVersLista, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:datiSingVersListaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
