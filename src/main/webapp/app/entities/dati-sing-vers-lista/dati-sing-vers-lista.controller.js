(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('DatiSingVersListaController', DatiSingVersListaController);

    DatiSingVersListaController.$inject = ['$scope', '$state', 'DatiSingVersLista'];

    function DatiSingVersListaController ($scope, $state, DatiSingVersLista) {
        var vm = this;

        vm.datiSingVersListas = [];

        loadAll();

        function loadAll() {
            DatiSingVersLista.query(function(result) {
                vm.datiSingVersListas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
