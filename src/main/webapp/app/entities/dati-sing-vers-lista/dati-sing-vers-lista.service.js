(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('DatiSingVersLista', DatiSingVersLista);

    DatiSingVersLista.$inject = ['$resource'];

    function DatiSingVersLista ($resource) {
        var resourceUrl =  'api/dati-sing-vers-listas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
