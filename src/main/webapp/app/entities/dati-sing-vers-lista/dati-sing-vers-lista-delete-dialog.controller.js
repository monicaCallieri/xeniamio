(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('DatiSingVersListaDeleteController',DatiSingVersListaDeleteController);

    DatiSingVersListaDeleteController.$inject = ['$uibModalInstance', 'entity', 'DatiSingVersLista'];

    function DatiSingVersListaDeleteController($uibModalInstance, entity, DatiSingVersLista) {
        var vm = this;

        vm.datiSingVersLista = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DatiSingVersLista.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
