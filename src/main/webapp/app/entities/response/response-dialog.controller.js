(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('ResponseDialogController', ResponseDialogController);

    ResponseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Response', 'Question', 'UserInformation'];

    function ResponseDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Response, Question, UserInformation) {
        var vm = this;

        vm.response = entity;
        vm.clear = clear;
        vm.save = save;
        vm.questions = Question.query();
        vm.userinformations = UserInformation.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.response.id !== null) {
                Response.update(vm.response, onSaveSuccess, onSaveError);
            } else {
                Response.save(vm.response, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:responseUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
