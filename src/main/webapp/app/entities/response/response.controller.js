(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('ResponseController', ResponseController);

    ResponseController.$inject = ['$scope', '$state', 'Response'];

    function ResponseController ($scope, $state, Response) {
        var vm = this;

        vm.responses = [];

        loadAll();

        function loadAll() {
            Response.query(function(result) {
                vm.responses = result;
                vm.searchQuery = null;
            });
        }
    }
})();
