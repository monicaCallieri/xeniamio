(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('response', {
            parent: 'entity',
            url: '/response',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.response.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/response/responses.html',
                    controller: 'ResponseController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('response');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('response-detail', {
            parent: 'entity',
            url: '/response/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.response.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/response/response-detail.html',
                    controller: 'ResponseDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('response');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Response', function($stateParams, Response) {
                    return Response.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'response',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('response-detail.edit', {
            parent: 'response-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/response/response-dialog.html',
                    controller: 'ResponseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Response', function(Response) {
                            return Response.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('response.new', {
            parent: 'response',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/response/response-dialog.html',
                    controller: 'ResponseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                response: null,
                                value: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('response', null, { reload: 'response' });
                }, function() {
                    $state.go('response');
                });
            }]
        })
        .state('response.edit', {
            parent: 'response',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/response/response-dialog.html',
                    controller: 'ResponseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Response', function(Response) {
                            return Response.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('response', null, { reload: 'response' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('response.delete', {
            parent: 'response',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/response/response-delete-dialog.html',
                    controller: 'ResponseDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Response', function(Response) {
                            return Response.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('response', null, { reload: 'response' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
