(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('ResponseDetailController', ResponseDetailController);

    ResponseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Response', 'Question', 'UserInformation'];

    function ResponseDetailController($scope, $rootScope, $stateParams, previousState, entity, Response, Question, UserInformation) {
        var vm = this;

        vm.response = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:responseUpdate', function(event, result) {
            vm.response = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
