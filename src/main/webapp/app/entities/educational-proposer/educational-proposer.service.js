(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('EducationalProposer', EducationalProposer);

    EducationalProposer.$inject = ['$resource', 'DateUtils'];

    function EducationalProposer ($resource, DateUtils) {
        var resourceUrl =  'api/educational-proposers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertLocalDateFromServer(data.creationDate);
                        data.startdate = DateUtils.convertLocalDateFromServer(data.startdate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    copy.startdate = DateUtils.convertLocalDateToServer(copy.startdate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    copy.startdate = DateUtils.convertLocalDateToServer(copy.startdate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
