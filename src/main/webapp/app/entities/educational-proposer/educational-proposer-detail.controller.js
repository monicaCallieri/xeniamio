(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('EducationalProposerDetailController', EducationalProposerDetailController);

    EducationalProposerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'EducationalProposer', 'StudentIntentions'];

    function EducationalProposerDetailController($scope, $rootScope, $stateParams, previousState, entity, EducationalProposer, StudentIntentions) {
        var vm = this;

        vm.educationalProposer = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:educationalProposerUpdate', function(event, result) {
            vm.educationalProposer = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
