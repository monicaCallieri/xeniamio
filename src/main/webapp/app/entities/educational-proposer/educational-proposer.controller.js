(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('EducationalProposerController', EducationalProposerController);

    EducationalProposerController.$inject = ['$scope', '$state', 'EducationalProposer'];

    function EducationalProposerController ($scope, $state, EducationalProposer) {
        var vm = this;

        vm.educationalProposers = [];

        loadAll();

        function loadAll() {
            EducationalProposer.query(function(result) {
                vm.educationalProposers = result;
                vm.searchQuery = null;
            });
        }
    }
})();
