(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('EducationalProposerDialogController', EducationalProposerDialogController);

    EducationalProposerDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'EducationalProposer', 'StudentIntentions'];

    function EducationalProposerDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, EducationalProposer, StudentIntentions) {
        var vm = this;

        vm.educationalProposer = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.studentintentions = StudentIntentions.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.educationalProposer.id !== null) {
                EducationalProposer.update(vm.educationalProposer, onSaveSuccess, onSaveError);
            } else {
                EducationalProposer.save(vm.educationalProposer, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:educationalProposerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.startdate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
