(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('educational-proposer', {
            parent: 'entity',
            url: '/educational-proposer',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.educationalProposer.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/educational-proposer/educational-proposers.html',
                    controller: 'EducationalProposerController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('educationalProposer');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('educational-proposer-detail', {
            parent: 'entity',
            url: '/educational-proposer/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.educationalProposer.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/educational-proposer/educational-proposer-detail.html',
                    controller: 'EducationalProposerDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('educationalProposer');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'EducationalProposer', function($stateParams, EducationalProposer) {
                    return EducationalProposer.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'educational-proposer',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('educational-proposer-detail.edit', {
            parent: 'educational-proposer-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/educational-proposer/educational-proposer-dialog.html',
                    controller: 'EducationalProposerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EducationalProposer', function(EducationalProposer) {
                            return EducationalProposer.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('educational-proposer.new', {
            parent: 'educational-proposer',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/educational-proposer/educational-proposer-dialog.html',
                    controller: 'EducationalProposerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                creationDate: null,
                                name: null,
                                typeOfCourse: null,
                                duration: null,
                                description: null,
                                periodicity: null,
                                startdate: null,
                                price: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('educational-proposer', null, { reload: 'educational-proposer' });
                }, function() {
                    $state.go('educational-proposer');
                });
            }]
        })
        .state('educational-proposer.edit', {
            parent: 'educational-proposer',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/educational-proposer/educational-proposer-dialog.html',
                    controller: 'EducationalProposerDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['EducationalProposer', function(EducationalProposer) {
                            return EducationalProposer.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('educational-proposer', null, { reload: 'educational-proposer' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('educational-proposer.delete', {
            parent: 'educational-proposer',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/educational-proposer/educational-proposer-delete-dialog.html',
                    controller: 'EducationalProposerDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['EducationalProposer', function(EducationalProposer) {
                            return EducationalProposer.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('educational-proposer', null, { reload: 'educational-proposer' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
