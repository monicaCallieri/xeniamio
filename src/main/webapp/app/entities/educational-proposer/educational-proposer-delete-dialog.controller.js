(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('EducationalProposerDeleteController',EducationalProposerDeleteController);

    EducationalProposerDeleteController.$inject = ['$uibModalInstance', 'entity', 'EducationalProposer'];

    function EducationalProposerDeleteController($uibModalInstance, entity, EducationalProposer) {
        var vm = this;

        vm.educationalProposer = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            EducationalProposer.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
