(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('inserimento-posizione-input-type', {
            parent: 'entity',
            url: '/inserimento-posizione-input-type',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inserimentoPosizioneInputType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/inserimento-posizione-input-type/inserimento-posizione-input-types.html',
                    controller: 'InserimentoPosizioneInputTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inserimentoPosizioneInputType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('inserimento-posizione-input-type-detail', {
            parent: 'entity',
            url: '/inserimento-posizione-input-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inserimentoPosizioneInputType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/inserimento-posizione-input-type/inserimento-posizione-input-type-detail.html',
                    controller: 'InserimentoPosizioneInputTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inserimentoPosizioneInputType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InserimentoPosizioneInputType', function($stateParams, InserimentoPosizioneInputType) {
                    return InserimentoPosizioneInputType.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'inserimento-posizione-input-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('inserimento-posizione-input-type-detail.edit', {
            parent: 'inserimento-posizione-input-type-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-input-type/inserimento-posizione-input-type-dialog.html',
                    controller: 'InserimentoPosizioneInputTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InserimentoPosizioneInputType', function(InserimentoPosizioneInputType) {
                            return InserimentoPosizioneInputType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('inserimento-posizione-input-type.new', {
            parent: 'inserimento-posizione-input-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-input-type/inserimento-posizione-input-type-dialog.html',
                    controller: 'InserimentoPosizioneInputTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                identificativo_beneficiario: null,
                                codice_servizio: null,
                                tipo_riferimento_creditore: null,
                                codice_riferimento_creditore: null,
                                codice_identificativo_presentazione: null,
                                identificativo_univoco_versamento: null,
                                ckey_5: null,
                                ckey_6: null,
                                data_scadenza_pagamento: null,
                                importo: null,
                                causale: null,
                                savv: null,
                                tipo_id_debitore: null,
                                identificativo_debitore: null,
                                anagrafica_debitore: null,
                                indirizzo_debitore: null,
                                cap_debitore: null,
                                localita_debitore: null,
                                nazione_debitore: null,
                                email_debitore: null,
                                civico_debitore: null,
                                provincia_debitore: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('inserimento-posizione-input-type', null, { reload: 'inserimento-posizione-input-type' });
                }, function() {
                    $state.go('inserimento-posizione-input-type');
                });
            }]
        })
        .state('inserimento-posizione-input-type.edit', {
            parent: 'inserimento-posizione-input-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-input-type/inserimento-posizione-input-type-dialog.html',
                    controller: 'InserimentoPosizioneInputTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InserimentoPosizioneInputType', function(InserimentoPosizioneInputType) {
                            return InserimentoPosizioneInputType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('inserimento-posizione-input-type', null, { reload: 'inserimento-posizione-input-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('inserimento-posizione-input-type.delete', {
            parent: 'inserimento-posizione-input-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/inserimento-posizione-input-type/inserimento-posizione-input-type-delete-dialog.html',
                    controller: 'InserimentoPosizioneInputTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InserimentoPosizioneInputType', function(InserimentoPosizioneInputType) {
                            return InserimentoPosizioneInputType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('inserimento-posizione-input-type', null, { reload: 'inserimento-posizione-input-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
