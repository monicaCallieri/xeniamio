(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneInputTypeDialogController', InserimentoPosizioneInputTypeDialogController);

    InserimentoPosizioneInputTypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'InserimentoPosizioneInputType', 'InserimentoPosizioneOutputType'];

    function InserimentoPosizioneInputTypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, InserimentoPosizioneInputType, InserimentoPosizioneOutputType) {
        var vm = this;

        vm.inserimentoPosizioneInputType = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.inserimentoposizioneresponses = InserimentoPosizioneOutputType.query({filter: 'inserimentoposizionerequest-is-null'});
        $q.all([vm.inserimentoPosizioneInputType.$promise, vm.inserimentoposizioneresponses.$promise]).then(function() {
            if (!vm.inserimentoPosizioneInputType.inserimentoPosizioneResponse || !vm.inserimentoPosizioneInputType.inserimentoPosizioneResponse.id) {
                return $q.reject();
            }
            return InserimentoPosizioneOutputType.get({id : vm.inserimentoPosizioneInputType.inserimentoPosizioneResponse.id}).$promise;
        }).then(function(inserimentoPosizioneResponse) {
            vm.inserimentoposizioneresponses.push(inserimentoPosizioneResponse);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.inserimentoPosizioneInputType.id !== null) {
                InserimentoPosizioneInputType.update(vm.inserimentoPosizioneInputType, onSaveSuccess, onSaveError);
            } else {
                InserimentoPosizioneInputType.save(vm.inserimentoPosizioneInputType, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:inserimentoPosizioneInputTypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.data_scadenza_pagamento = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
