(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneInputTypeController', InserimentoPosizioneInputTypeController);

    InserimentoPosizioneInputTypeController.$inject = ['$scope', '$state', 'InserimentoPosizioneInputType'];

    function InserimentoPosizioneInputTypeController ($scope, $state, InserimentoPosizioneInputType) {
        var vm = this;

        vm.inserimentoPosizioneInputTypes = [];

        loadAll();

        function loadAll() {
            InserimentoPosizioneInputType.query(function(result) {
                vm.inserimentoPosizioneInputTypes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
