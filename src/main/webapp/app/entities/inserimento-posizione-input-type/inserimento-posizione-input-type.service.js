(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('InserimentoPosizioneInputType', InserimentoPosizioneInputType);

    InserimentoPosizioneInputType.$inject = ['$resource', 'DateUtils'];

    function InserimentoPosizioneInputType ($resource, DateUtils) {
        var resourceUrl =  'api/inserimento-posizione-input-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data_scadenza_pagamento = DateUtils.convertLocalDateFromServer(data.data_scadenza_pagamento);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_scadenza_pagamento = DateUtils.convertLocalDateToServer(copy.data_scadenza_pagamento);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    //copy.data_scadenza_pagamento = DateUtils.convertLocalDateToServer(copy.data_scadenza_pagamento);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
