(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneInputTypeDetailController', InserimentoPosizioneInputTypeDetailController);

    InserimentoPosizioneInputTypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InserimentoPosizioneInputType', 'InserimentoPosizioneOutputType'];

    function InserimentoPosizioneInputTypeDetailController($scope, $rootScope, $stateParams, previousState, entity, InserimentoPosizioneInputType, InserimentoPosizioneOutputType) {
        var vm = this;

        vm.inserimentoPosizioneInputType = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:inserimentoPosizioneInputTypeUpdate', function(event, result) {
            vm.inserimentoPosizioneInputType = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
