(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InserimentoPosizioneInputTypeDeleteController',InserimentoPosizioneInputTypeDeleteController);

    InserimentoPosizioneInputTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'InserimentoPosizioneInputType'];

    function InserimentoPosizioneInputTypeDeleteController($uibModalInstance, entity, InserimentoPosizioneInputType) {
        var vm = this;

        vm.inserimentoPosizioneInputType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InserimentoPosizioneInputType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
