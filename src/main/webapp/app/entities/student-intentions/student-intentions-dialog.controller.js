(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentIntentionsDialogController', StudentIntentionsDialogController);

    StudentIntentionsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'StudentIntentions', 'User', 'EducationalProposer', 'Principal'];

    function StudentIntentionsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, StudentIntentions, User, EducationalProposer, Principal) {
        var vm = this;

        vm.studentIntentions = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.date = new Date().toISOString().substring(0,10);
        //vm.users = User.query();
        vm.user = getAccount();
        vm.educationalproposers = EducationalProposer.query({startdate : vm.date});

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });
        
        function getAccount() {
            Principal.identity().then(function (account) {
                vm.login = account.login;
                load(vm.login);
            });
        }

        function load(login) {
            User.get({login: login}, function (result) {
                vm.user = result;
            });

        }

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            vm.studentIntentions.creationDate = new Date();
            vm.studentIntentions.state = "NOTENROLLED";
            vm.studentIntentions.user = vm.user;
            if (vm.studentIntentions.id !== null) {
                StudentIntentions.update(vm.studentIntentions, onSaveSuccess, onSaveError);
            } else {
                StudentIntentions.save(vm.studentIntentions, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:studentIntentionsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.startDate = false;
        vm.datePickerOpenStatus.endDate = false;
        vm.datePickerOpenStatus.realizationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
