(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentIntentionsPayController', StudentIntentionsPayController);

    StudentIntentionsPayController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'StudentIntentions', 'User', 'EducationalProposer'];

    function StudentIntentionsPayController ($timeout, $scope, $stateParams, $uibModalInstance,  StudentIntentions, User, EducationalProposer) {
        var vm = this;

        //vm.studentIntentions = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.educationalproposers = EducationalProposer.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.studentIntentions.id !== null) {
                StudentIntentions.update(vm.studentIntentions, onSaveSuccess, onSaveError);
            } else {
                StudentIntentions.save(vm.studentIntentions, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:studentIntentionsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.startDate = false;
        vm.datePickerOpenStatus.endDate = false;
        vm.datePickerOpenStatus.realizationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
