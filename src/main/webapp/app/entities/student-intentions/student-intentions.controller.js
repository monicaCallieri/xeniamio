(function () {
    'use strict';
    angular
            .module('xeniaApp')
            .controller('StudentIntentionsController', StudentIntentionsController);
    StudentIntentionsController.$inject = ['$scope', '$state', 'StudentIntentions', 'InserimentoPosizioneInputType', 'InserimentoPosizioneOutputType'];
    function StudentIntentionsController($scope, $state, StudentIntentions, InserimentoPosizioneInputType, InserimentoPosizioneOutputType) {
        var vm = this;
        vm.totalPay = 0;
        vm.studentIntentions = [];
        vm.studentIntentionsToPay = [];
        vm.PayOnLine;
        vm.pay = pay;
        vm.inserimentoPosizioneInputType = {};
        loadAll();
        loadPay();
        //console.log("caricamento student-intentions.controller");
        function loadAll() {
            StudentIntentions.query(function (result) {
                vm.studentIntentions = result;
                vm.searchQuery = null;
            });
        }

        function loadPay() {
            StudentIntentions.query({state: 'NOTENROLLED'}, function (result) {
                vm.studentIntentionsToPay = result;
                showTotal();
                vm.searchQuery = null;
            });
        }

        function showTotal() {
            for (var i = 0; i < vm.studentIntentionsToPay.length; i++) {
                vm.totalPay = vm.totalPay + vm.studentIntentionsToPay[i].educationalProposer.price;
            }
        }

        function pay() {
            document.getElementById('mydiv').style.display = "block";
            //console.log("Hai premuto il bottone pay");
            //console.log("Hai selezionato la modalità di pagamento onLine: " + vm.PayOnLine);
            vm.isSaving = true;
            //if (vm.PayOnLine === 'true') {
            /*if (vm.inserimentoPosizioneInputType.id !== null) {
             InserimentoPosizioneInputType.update(vm.inserimentoPosizioneInputType, onSaveSuccess, onSaveError);
             } else {*/
            console.log("chiamo il lato Server");
            /*vm.payment = [];
             vm.payment[0] = vm.PayOnLine;
             vm.payment[1] = vm.totalPay;
             if (vm.studentIntentionsToPay.length <= 1)
             vm.payment[2] = vm.studentIntentionsToPay[0].educationalProposer.name;
             vm.payment[3] = vm.studentIntentionsToPay[0].id;*/
            vm.payTestObj = {};
            vm.payTestObj.total = vm.totalPay;
            vm.payTestObj.typeOfPayment = vm.PayOnLine;
            vm.payTestObj.studentIntentions = vm.studentIntentionsToPay;
            if (vm.payTestObj.typeOfPayment === "true") {
                if (vm.payTestObj.studentIntentions.length > 0) {
                    if (vm.payTestObj.studentIntentions.length < 6) {
                        /*var oldCourses = "false";
                        for (var i = 0; i < vm.payTestObj.studentIntentions.length; i++) {
                            var dayStudentIntentions = new Date(vm.studentIntentionsToPay[0].educationalProposer.startdate);
                            dayStudentIntentions.setHours(0);
                            dayStudentIntentions.setSeconds(0);
                            dayStudentIntentions.setMinutes(0);
                            console.log("data di inizio del corso da pagare: "+dayStudentIntentions);
                            var today = new Date();
                            today.setHours(0);
                            today.setSeconds(0);
                            today.setMinutes(0);
                            console.log("data odierna: "+today);
                            if (+dayStudentIntentions<+today){
                                oldCourses = "true";
                            }
                            if (+dayStudentIntentions>=+today){
                                oldCourses = "false";
                            }
                        }
                        if (oldCourses==="true") {
                            document.getElementById('mydiv').style.display = "none";
                            alert("Ci sono corsi che sono già iniziati nel carrello e non possono essere pagati. Eliminali e procedi al pagamento");
                        } else {*/
                            InserimentoPosizioneInputType.save(vm.payTestObj, onSaveSuccess, onSaveError);
                        //}
                    } else {
                        alert("Non puoi pagare più di 5 elementi insieme e al momento sono: " + vm.payTestObj.studentIntentions.length);
                        document.getElementById('mydiv').style.display = "none";
                    }
                } else {
                    alert("Devi avere almeno un corso nel carrello per effettuare il pagamento: " + vm.payTestObj.studentIntentions.length);
                    document.getElementById('mydiv').style.display = "none";
                }
            } else {
                alert("hai scelto il pagamento off-line");
                document.getElementById('mydiv').style.display = "none";
            }
            //}
            //}
        }

        function onSaveSuccess(result) {
            document.getElementById('mydiv').style.display = "none";
            $scope.$emit('xeniaApp:inserimentoPosizioneInputTypeUpdate', result);
            $state.go('student-intentions', null, {reload: true});
            vm.isSaving = false;
        }

        function onSaveError() {
            document.getElementById('mydiv').style.display = "none";
            vm.isSaving = false;
        }

    }
})();