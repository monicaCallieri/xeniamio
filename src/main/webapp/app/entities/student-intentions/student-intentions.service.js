(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('StudentIntentions', StudentIntentions);

    StudentIntentions.$inject = ['$resource', 'DateUtils'];

    function StudentIntentions ($resource, DateUtils) {
        var resourceUrl =  'api/student-intentions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertLocalDateFromServer(data.creationDate);
                        data.startDate = DateUtils.convertLocalDateFromServer(data.startDate);
                        data.endDate = DateUtils.convertLocalDateFromServer(data.endDate);
                        data.realizationDate = DateUtils.convertLocalDateFromServer(data.realizationDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    copy.startDate = DateUtils.convertLocalDateToServer(copy.startDate);
                    copy.endDate = DateUtils.convertLocalDateToServer(copy.endDate);
                    copy.realizationDate = DateUtils.convertLocalDateToServer(copy.realizationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.creationDate = DateUtils.convertLocalDateToServer(copy.creationDate);
                    copy.startDate = DateUtils.convertLocalDateToServer(copy.startDate);
                    copy.endDate = DateUtils.convertLocalDateToServer(copy.endDate);
                    copy.realizationDate = DateUtils.convertLocalDateToServer(copy.realizationDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
