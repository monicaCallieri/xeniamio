(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentIntentionsDetailController', StudentIntentionsDetailController);

    StudentIntentionsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'StudentIntentions', 'User', 'EducationalProposer'];

    function StudentIntentionsDetailController($scope, $rootScope, $stateParams, previousState, entity, StudentIntentions, User, EducationalProposer) {
        var vm = this;

        vm.studentIntentions = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:studentIntentionsUpdate', function(event, result) {
            vm.studentIntentions = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
