(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('student-intentions', {
            parent: 'entity',
            url: '/student-intentions',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT'],
                pageTitle: 'xeniaApp.studentIntentions.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student-intentions/student-intentions.html',
                    controller: 'StudentIntentionsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('studentIntentions');
                    $translatePartialLoader.addPart('courseLevel');
                    $translatePartialLoader.addPart('state');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }],
            entity: ['$stateParams', 'StudentIntentions', function($stateParams, StudentIntentions) {
                    return StudentIntentions.query({state : 'NOTENROLLED'}).$promise;
                }]
            }
        })
        .state('student-intentions-detail', {
            parent: 'entity',
            url: '/student-intentions/{id}',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT'],
                pageTitle: 'xeniaApp.studentIntentions.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/student-intentions/student-intentions-detail.html',
                    controller: 'StudentIntentionsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('studentIntentions');
                    $translatePartialLoader.addPart('courseLevel');
                    $translatePartialLoader.addPart('state');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'StudentIntentions', function($stateParams, StudentIntentions) {
                    return StudentIntentions.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'student-intentions',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('student-intentions-detail.edit', {
            parent: 'student-intentions-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-intentions/student-intentions-dialog.html',
                    controller: 'StudentIntentionsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['StudentIntentions', function(StudentIntentions) {
                            return StudentIntentions.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student-intentions.new', {
            parent: 'student-intentions',
            url: '/new',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-intentions/student-intentions-dialog.html',
                    controller: 'StudentIntentionsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                creationDate: null,
                                startDate: null,
                                endDate: null,
                                level: null,
                                realizationDate: null,
                                state: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('student-intentions', null, { reload: true });
                }, function() {
                    $state.go('student-intentions');
                });
            }]
        })
        .state('student-intentions.edit', {
            parent: 'student-intentions',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-intentions/student-intentions-dialog.html',
                    controller: 'StudentIntentionsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['StudentIntentions', function(StudentIntentions) {
                            return StudentIntentions.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-intentions', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('student-intentions.delete', {
            parent: 'student-intentions',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER', 'ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-intentions/student-intentions-delete-dialog.html',
                    controller: 'StudentIntentionsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['StudentIntentions', function(StudentIntentions) {
                            return StudentIntentions.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('student-intentions', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        /*.state('student-intentions.pay', {
            parent: 'student-intentions',
            url: '/pay',*/
           /* views:{
                 'content@': {
                 templateUrl: 'app/entities/student-intentions/student-intentions-pay.html',
                    controller: 'StudentIntentionsPayController',
                    controllerAs: 'vm'
                }
            },*/
           /*
            
            data: {
                authorities: ['ROLE_STUDENT']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/student-intentions/student-intentions-pay.html',
                    controller: 'StudentIntentionsPayController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',*/
                    /*resolve: {
                        entity: ['StudentIntentions', function(StudentIntentions) {
                            return StudentIntentions.get({id : $stateParams.id}).$promise;
                        }]
                    }*/
            /*    }).result.then(function() {
                    $state.go('student-intentions', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        ;
    }

})();
