(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('StudentIntentionsDeleteController',StudentIntentionsDeleteController);

    StudentIntentionsDeleteController.$inject = ['$uibModalInstance', 'entity', 'StudentIntentions'];

    function StudentIntentionsDeleteController($uibModalInstance, entity, StudentIntentions) {
        var vm = this;

        vm.studentIntentions = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            /*StudentIntentions.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });*/
            vm.studentIntentions.state = "NOTINTERESTED";
            StudentIntentions.update(vm.studentIntentions, onSaveSuccess);
        }
        
        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
        }
    }
})();
