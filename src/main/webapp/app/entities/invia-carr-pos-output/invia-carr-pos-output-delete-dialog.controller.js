(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosOutputDeleteController',InviaCarrPosOutputDeleteController);

    InviaCarrPosOutputDeleteController.$inject = ['$uibModalInstance', 'entity', 'InviaCarrPosOutput'];

    function InviaCarrPosOutputDeleteController($uibModalInstance, entity, InviaCarrPosOutput) {
        var vm = this;

        vm.inviaCarrPosOutput = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InviaCarrPosOutput.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
