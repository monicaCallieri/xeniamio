(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosOutputController', InviaCarrPosOutputController);

    InviaCarrPosOutputController.$inject = ['$scope', '$state', 'InviaCarrPosOutput'];

    function InviaCarrPosOutputController ($scope, $state, InviaCarrPosOutput) {
        var vm = this;

        vm.inviaCarrPosOutputs = [];

        loadAll();

        function loadAll() {
            InviaCarrPosOutput.query(function(result) {
                vm.inviaCarrPosOutputs = result;
                vm.searchQuery = null;
            });
        }
    }
})();
