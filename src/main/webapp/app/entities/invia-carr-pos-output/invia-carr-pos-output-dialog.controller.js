(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosOutputDialogController', InviaCarrPosOutputDialogController);

    InviaCarrPosOutputDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InviaCarrPosOutput'];

    function InviaCarrPosOutputDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InviaCarrPosOutput) {
        var vm = this;

        vm.inviaCarrPosOutput = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.inviaCarrPosOutput.id !== null) {
                InviaCarrPosOutput.update(vm.inviaCarrPosOutput, onSaveSuccess, onSaveError);
            } else {
                InviaCarrPosOutput.save(vm.inviaCarrPosOutput, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('xeniaApp:inviaCarrPosOutputUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
