(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .controller('InviaCarrPosOutputDetailController', InviaCarrPosOutputDetailController);

    InviaCarrPosOutputDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InviaCarrPosOutput'];

    function InviaCarrPosOutputDetailController($scope, $rootScope, $stateParams, previousState, entity, InviaCarrPosOutput) {
        var vm = this;

        vm.inviaCarrPosOutput = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('xeniaApp:inviaCarrPosOutputUpdate', function(event, result) {
            vm.inviaCarrPosOutput = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
