(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('invia-carr-pos-output', {
            parent: 'entity',
            url: '/invia-carr-pos-output',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inviaCarrPosOutput.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/invia-carr-pos-output/invia-carr-pos-outputs.html',
                    controller: 'InviaCarrPosOutputController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inviaCarrPosOutput');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('invia-carr-pos-output-detail', {
            parent: 'entity',
            url: '/invia-carr-pos-output/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'xeniaApp.inviaCarrPosOutput.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/invia-carr-pos-output/invia-carr-pos-output-detail.html',
                    controller: 'InviaCarrPosOutputDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('inviaCarrPosOutput');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InviaCarrPosOutput', function($stateParams, InviaCarrPosOutput) {
                    return InviaCarrPosOutput.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'invia-carr-pos-output',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('invia-carr-pos-output-detail.edit', {
            parent: 'invia-carr-pos-output-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-output/invia-carr-pos-output-dialog.html',
                    controller: 'InviaCarrPosOutputDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InviaCarrPosOutput', function(InviaCarrPosOutput) {
                            return InviaCarrPosOutput.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('invia-carr-pos-output.new', {
            parent: 'invia-carr-pos-output',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-output/invia-carr-pos-output-dialog.html',
                    controller: 'InviaCarrPosOutputDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                esito: null,
                                codiceErrore: null,
                                descrizione: null,
                                idTransazione: null,
                                url: null,
                                iuvLista: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('invia-carr-pos-output', null, { reload: 'invia-carr-pos-output' });
                }, function() {
                    $state.go('invia-carr-pos-output');
                });
            }]
        })
        .state('invia-carr-pos-output.edit', {
            parent: 'invia-carr-pos-output',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-output/invia-carr-pos-output-dialog.html',
                    controller: 'InviaCarrPosOutputDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InviaCarrPosOutput', function(InviaCarrPosOutput) {
                            return InviaCarrPosOutput.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('invia-carr-pos-output', null, { reload: 'invia-carr-pos-output' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('invia-carr-pos-output.delete', {
            parent: 'invia-carr-pos-output',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/invia-carr-pos-output/invia-carr-pos-output-delete-dialog.html',
                    controller: 'InviaCarrPosOutputDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InviaCarrPosOutput', function(InviaCarrPosOutput) {
                            return InviaCarrPosOutput.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('invia-carr-pos-output', null, { reload: 'invia-carr-pos-output' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
