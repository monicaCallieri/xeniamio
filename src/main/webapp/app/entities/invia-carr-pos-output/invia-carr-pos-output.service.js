(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('InviaCarrPosOutput', InviaCarrPosOutput);

    InviaCarrPosOutput.$inject = ['$resource'];

    function InviaCarrPosOutput ($resource) {
        var resourceUrl =  'api/invia-carr-pos-outputs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
