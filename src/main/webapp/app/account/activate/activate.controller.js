(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .controller('ActivationController', ActivationController);

    ActivationController.$inject = ['$stateParams', 'Auth', 'LoginService'];

    function ActivationController($stateParams, Auth, LoginService) {
        var vm = this;

        angular.element(document).ready(function () { 
           window.scrollTo(0,document.body.scrollHeight);
        });

        Auth.activateAccount({key: $stateParams.key}).then(function () {
            vm.error = null;
            vm.success = 'OK';
        }).catch(function () {
            vm.success = null;
            vm.error = 'ERROR';
        });

        vm.login = LoginService.open;
    }
})();
