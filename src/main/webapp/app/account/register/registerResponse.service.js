(function () {
    'use strict';

    angular
        .module('xeniaApp')
        .factory('RegisterResponse', RegisterResponse);

    RegisterResponse.$inject = ['$resource'];

    function RegisterResponse ($resource) {
        //var resourceUrl =  'api/registerResponse/:general_information_id';
        var resourceUrl =  'api/registerResponse';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();