(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .factory('RegisterInformationFactory', RegisterInformationFactory);


    RegisterInformationFactory.$inject = ['$translate', '$timeout', 'Auth', 'LoginService'];

    function RegisterInformationFactory($translate, $timeout, Auth, LoginService) {
        var information = {};
        
        function insert(information){
            information.knowledgeOfItalian = 'NONE';
            information.howDoUKnow = 'NONE';
            information.whyUStudy = 'NONE';
            information.hopeFuture = 'NONE';
            information.extraInterest = 'NONE';
            
            return information;
        }
        
        return information;
    }
})();
