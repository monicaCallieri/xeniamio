(function () {
    'use strict';
    angular
            .module('xeniaApp')
            .controller('RegisterController', RegisterController);
    RegisterController.$inject = ['$translate', '$timeout', 'Auth', 'LoginService', 'Principal', 'response', 'questions', '$stateParams'];
    function RegisterController($translate, $timeout, Auth, LoginService, Principal, response, questions, $stateParams) {
        var vm = this;
        vm.startDate = $stateParams.startDate;
        vm.endDate = $stateParams.endDate;
        console.log("startDate = " + vm.startDate);
        console.log("endDate = " + vm.endDate);
        vm.doNotMatch = null;
        vm.error = null;
        vm.errorUserExists = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.registerAccount = {};
        vm.studentIntentions = {};
        vm.educationalProposer = {};
        vm.language = $translate.use();
        vm.isAuthenticated = Principal.isAuthenticated;
        vm.questions = questions;
        vm.idQuestion;
        vm.response = response;
        vm.responseUser = [];
        vm.userId = {};
        vm.startDate = $stateParams.startDate;
        vm.endDate = $stateParams.endDate;
        vm.educationalProposer.id = $stateParams.educationalProposer;
        console.log("Inserimento educationalProposer dal sito Unistrapg con valore: "+$stateParams.educationalProposer);
        vm.userResponses = new Map();
        vm.responseUser = [];
        vm.changeRadio = changeRadio;
        vm.someSelected = someSelected;
        
        function changeRadio(idRes, idQues) {
            console.log("Siamo nella change e l'id della risposta selezionata è: " + idRes + " per la domanda di id: " + idQues);
            for (var i = 0; i < vm.response.length; i++) {
                if (vm.response[i].id !== idRes && vm.response[i].question.id === idQues) {
                    if (vm.response[i].selected !== null) {
                        vm.response[i].selected = false;
                    }
                }
            }
        }

        function someSelected(idQues) {
            for (var i = 0; i < vm.response.length; i++) {
                if (vm.response[i].question.id === idQues && vm.response[i].selected) {
                    return true;
                }
            }
            return false;
        }

        /* function loadQuestion() {
         RegisterQuestion.query(function (result) {
         vm.questions = result;
         vm.searchQuery = null;
         //vm.idQuestion = vm.generalInformations[0].id;
         
         });
         }*/

        /*function loadResponse() {
         RegisterResponse.query(function (result) {
         vm.response = result;
         vm.searchQuery = null;
         });
         }*/

        $timeout(function () {
            angular.element('#login').focus();
        });

        function register() {
            if (vm.registerAccount.password !== vm.confirmPassword) {
                vm.doNotMatch = 'ERROR';
            } else {
                vm.registerAccount.langKey = $translate.use();
                vm.doNotMatch = null;
                vm.error = null;
                vm.errorUserExists = null;
                vm.errorEmailExists = null;
                vm.registerAccount.login = vm.registerAccount.email; /*impongo uguali username e password*/

                /*Salvo i valori delle risposte alle domande che sono state poste al nuovo utente*/
                vm.registerAccount.responseUser = vm.responseUser;
                vm.response.forEach(function (entry) {
                    if (entry.selected) {
                        vm.registerAccount.responseUser.push(entry);
                    }
                });

                /*Setto i parametri per la student Intentions, con startDate ed endDate.
                 * Nel caso in cui non sono presenti nella url li inizializzo ad oggi*/
                vm.studentIntentions.creationDate = new Date();
                //console.log("data di creazione della student intentions: " + vm.studentIntentionsDate[0]);
                if (vm.startDate != null) {
                    vm.studentIntentions.startDate = new Date(vm.startDate);
                    //console.log("startDate della student intentions: " + vm.studentIntentionsDate[1]);
                } else {
                    vm.studentIntentions.startDate = new Date();
                    //console.log("startDate non settato dai parametri: " + vm.studentIntentionsDate[1]);
                }
                if (vm.endDate != null) {
                    vm.studentIntentions.endDate = new Date(vm.endDate);
                    //console.log("endDate della student intentions: " + vm.studentIntentionsDate[2]);
                } else {
                    vm.studentIntentions.endDate = new Date();
                    //console.log("endDate non settato dai parametri: " + vm.studentIntentions[2]);
                }
                if(vm.educationalProposer.id != null){
                    vm.studentIntentions.educationalProposer = vm.educationalProposer;
                }else{
                    vm.educationalProposer.id = 2;
                    vm.studentIntentions.educationalProposer = vm.educationalProposer;
                }
                vm.registerAccount.studentIntentions = vm.studentIntentions;

                /*Registrazione del nuovo account*/
                Auth.createAccount(vm.registerAccount).then(function () {
                    vm.success = 'OK';
                }).catch(function (response) {
                    vm.success = null;
                    if (response.status === 400 && response.data === 'login already in use') {
                        vm.errorUserExists = 'ERROR';
                    } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                        vm.errorEmailExists = 'ERROR';
                    } else {
                        vm.error = 'ERROR';
                    }
                });
            }

        }
    }
})();
