(function () {
    'use strict';

    angular
        .module('xeniaApp')
        .factory('RegisterQuestion', RegisterQuestion);

    RegisterQuestion.$inject = ['$resource'];

    function RegisterQuestion ($resource) {
        var resourceUrl =  'api/registerQuestion';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();