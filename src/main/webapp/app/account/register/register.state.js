(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('register', {
            parent: 'account',
            url: '/register?startDate&endDate&educationalProposer',
            data: {
                authorities: [],
                pageTitle: 'register.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/account/register/register.html',
                    controller: 'RegisterController',
                    controllerAs: 'vm'
                },
                'userInformation@register': {
                    templateUrl: 'app/account/register/information.html',
                    controller: 'RegisterInformationController',
                    controllerAs: 'ric'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('register');
                        $translatePartialLoader.addPart('level');
                        $translatePartialLoader.addPart('question_response');
                        return $translate.refresh();
                    }],
                response: ['$stateParams', 'RegisterResponse', function ($stateParams, RegisterResponse) {
                        return RegisterResponse.query().$promise;
                    }],
                questions: ['$stateParams', 'RegisterQuestion', function ($stateParams, RegisterQuestion) {
                        return RegisterQuestion.query().$promise;
                    }]
            }
        })

                ;
    }
})();
