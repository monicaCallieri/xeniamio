(function () {
    'use strict';

    angular
            .module('xeniaApp')
            .controller('SideNavbarController', SideNavbarController);

    SideNavbarController.$inject = ['$scope', 'Principal', 'LoginService', '$state', '$rootScope', 'entity'];

    function SideNavbarController($scope, Principal, LoginService, $state, $rootScope, entity) {
        var vm = this;
        vm.isAuthenticated = Principal.isAuthenticated;
        $rootScope.isAuthenticated = vm.isAuthenticated;
        if(vm.isAuthenticated()===true){
            document.getElementById('side-navbar').style.display = "block";
        }
        vm.badgeValue = 1;
        vm.setExpanded = function () {
            $rootScope.expanded = true;
        };

        vm.removeExpanded = function () {
            $rootScope.expanded = false;
        };
        
        vm.futureCourses = 1;
        
        vm.numStudIntentionsPending = entity.length;
        //console.log("Lunghezza array: "+vm.numStudIntentionsPending);
    }
})();
