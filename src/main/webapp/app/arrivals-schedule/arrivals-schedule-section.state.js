(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('arrivals-schedule-section', {
            parent: 'app',
            url: '/arrivals-schedule-section',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/arrivals-schedule/arrivals-schedule-section.html',
                    controller: 'arrivalsScheduleSectionController',
                    css: 'app/arrivals-schedule/arrivals-schedule-section.css',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('arrivals-schedule-section');
                    $translatePartialLoader.addPart('student');
                    return $translate.refresh();
                }]/*,
                entity: ['$stateParams', 'ArrivalSchedule', function($stateParams, ArrivalSchedule) {
                    return ArrivalSchedule.get({date : $stateParams.date}).$promise;
                }]*/
            }
        });
    }
})();
