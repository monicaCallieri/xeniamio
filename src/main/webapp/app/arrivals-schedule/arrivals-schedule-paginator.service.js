(function () {
    'use strict';
    angular
            .module('xeniaApp')
            .factory('ArrivalSchedulePaginator', ArrivalSchedulePaginator);

    function ArrivalSchedulePaginator() {
        var myData = {
            year:'',
            month: '',
            /*Metodi getter e setter, non sono veri e propri Metodi, ma sono delle properties richiamabili come parametri dell'oggetto*/
            set setYear(year) {
                this.year = year;
            },
           
            set setMonth(month) {
                this.month = month;
            },
            get getYear(){
                return this.year;
            },
            get getMonth(){
                return this.month;
            }
        };
        var count = 1;

        return {
            getYear: function () {
                return myData.getYear;
            },
            setYear: function (year) {
                myData.setYear=year;
            },
            getMonth: function () {
                return myData.getMonth;
            },
            setMonth: function (month) {
                myData.setMonth=month; /*richiamo il setter del mese*/
            },
            incrementCount: function () {
                count++;
                return count;
            },
            getCount: function () {
                return count;
            }

        };
    }
})();