(function () {
    'use strict';
    angular
            .module('xeniaApp')
            .factory('ArrivalSchedulePretests', ArrivalSchedulePretests);

    ArrivalSchedulePretests.$inject = ['$resource', 'DateUtils'];

    function ArrivalSchedulePretests($resource, DateUtils) {
        var resourceUrl = 'api/pretests';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true,
                params: {
                    date: "@date"
                }
            },
            'get': {
                method: 'GET', isArray: true,
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertLocalDateFromServer(data.date);
                        data.time = DateUtils.convertDateTimeFromServer(data.time);
                    }
                    return data;
                }
            }/*,
             'update': {
             method: 'PUT',
             transformRequest: function (data) {
             var copy = angular.copy(data);
             copy.dateOfBirth = DateUtils.convertLocalDateToServer(copy.dateOfBirth);
             copy.scholarshipStartDate = DateUtils.convertLocalDateToServer(copy.scholarshipStartDate);
             copy.scholarshipEndDate = DateUtils.convertLocalDateToServer(copy.scholarshipEndDate);
             copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
             return angular.toJson(copy);
             }
             },
             'save': {
             method: 'POST',
             transformRequest: function (data) {
             var copy = angular.copy(data);
             copy.dateOfBirth = DateUtils.convertLocalDateToServer(copy.dateOfBirth);
             copy.scholarshipStartDate = DateUtils.convertLocalDateToServer(copy.scholarshipStartDate);
             copy.scholarshipEndDate = DateUtils.convertLocalDateToServer(copy.scholarshipEndDate);
             copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
             return angular.toJson(copy);
             }
             }
             */});
    }
})();
