(function () {
    'use strict';
    angular
            .module('xeniaApp')
            .controller('arrivalsScheduleSectionController', arrivalsScheduleSectionController);

    arrivalsScheduleSectionController.$inject = ['$scope', '$translate', 'ArrivalSchedulePretests', 'ArrivalSchedule', 'ArrivalSchedulePaginator'];

    function arrivalsScheduleSectionController($scope, $translate, ArrivalSchedulePretests, ArrivalSchedule, ArrivalSchedulePaginator) {
        var vm = this;
        console.log("count: " + ArrivalSchedulePaginator.getCount());
        ArrivalSchedulePaginator.incrementCount();
        console.log("count: " + ArrivalSchedulePaginator.getCount());
        console.log("year: " + ArrivalSchedulePaginator.getYear());
        console.log("month: " + ArrivalSchedulePaginator.getMonth());
        if (ArrivalSchedulePaginator.getYear() !== undefined && ArrivalSchedulePaginator.getYear()!="" ) {
            /*Se sto ritornando nella pagina, il servizio si ricorderà dell'anno settato nella visita precedente*/
            vm.yearCurrent = ArrivalSchedulePaginator.getYear();/*Pertanto valorizzo il modello yearCurrent a quell'anno*/
        } else {
            /*è la prima volta che entro nella pagina, pertanto devo genereare un anno (quello corrente) e lo memorizzo nel servizio*/
            vm.yearCurrent = new Date().getFullYear();
            ArrivalSchedulePaginator.setYear(vm.yearCurrent);/*Questo è un metodo esposto dal service che, a sua volta, richiama il vero e proprio setter dell'oggetto 
             * A differenza di Java, il setter non è un metodo, ma è una property*/
        }
        console.log("year after set: " + ArrivalSchedulePaginator.getYear());
        if (ArrivalSchedulePaginator.getMonth() !== undefined && ArrivalSchedulePaginator.getMonth()!= "") {
            /*Stesso discorso di sopra applicato al mese*/
            vm.monthCurrent = ArrivalSchedulePaginator.getMonth();
        }else{
            vm.monthCurrent = ("0" + (new Date().getMonth() + 1)).slice(-2);
        ArrivalSchedulePaginator.setMonth(vm.monthCurrent);
        }
        vm.yearPrev = yearPrev;
        vm.yearNext = yearNext;
        vm.monthChange = monthChange;
        vm.yearChange = yearChange;
        vm.loadPretests = loadPretests;
        vm.loadStudents = loadStudents;
        vm.pretests = [];
        vm.students = [];
        vm.months = [
            {
                value: '01',
                label: 'Jen'
            },
            {
                value: '02',
                label: 'Feb'
            },
            {
                value: '03',
                label: 'Mar'
            },
            {
                value: '04',
                label: 'Apr'
            },
            {
                value: '05',
                label: 'May'
            },
            {
                value: '06',
                label: 'Jun'
            },
            {
                value: '07',
                label: 'Jul'
            },
            {
                value: '08',
                label: 'Aug'
            },
            {
                value: '09',
                label: 'Sep'
            },
            {
                value: '10',
                label: 'Oct'
            },
            {
                value: '11',
                label: 'Nov'
            },
            {
                value: '12',
                label: 'Dec'
            }
        ];

        /*vm.status = {
         isOpen: false
         };*/

        loadPretests();

        function loadPretests() {
            ArrivalSchedulePretests.query({date: vm.yearCurrent + "-" + vm.monthCurrent + "-01"}, function (result) {
                vm.pretests = result;
                vm.searchQuery = null;
            });
        }

        function loadStudents(pretest) {
            //if (vm.status.isOpen == true) {
            ArrivalSchedule.query({date: pretest}, function (result) {
                vm.students = result;
                vm.searchQuery = null;
            });
            /*}
             else{
             alert("chiuso");
             }*/
        }

        function yearPrev() {
            vm.yearCurrent = vm.yearCurrent - 1;
            ArrivalSchedulePaginator.setYear(vm.yearCurrent);
            loadPretests();

        }

        function yearNext() {
            vm.yearCurrent = vm.yearCurrent + 1;
            ArrivalSchedulePaginator.setYear(vm.yearCurrent);
            loadPretests();

        }

        function monthChange(value) {
            vm.monthCurrent = value;
            ArrivalSchedulePaginator.setMonth(vm.monthCurrent);
            loadPretests();
        }

        function yearChange(value) {
            if (vm.yearCurrent.length === 4) {
                ArrivalSchedulePaginator.setYear(vm.yearCurrent);
                loadPretests();
            }
        }
    }
})();
