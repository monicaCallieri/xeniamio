(function() {
    'use strict';
    angular
        .module('xeniaApp')
        .factory('ArrivalSchedule', ArrivalSchedule);

    ArrivalSchedule.$inject = ['$resource', 'DateUtils'];

    function ArrivalSchedule ($resource, DateUtils) {
        var resourceUrl =  'api/arrivals-schedule';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',isArray: true/*,
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateOfBirth = DateUtils.convertLocalDateFromServer(data.dateOfBirth);
                        data.scholarshipStartDate = DateUtils.convertLocalDateFromServer(data.scholarshipStartDate);
                        data.scholarshipEndDate = DateUtils.convertLocalDateFromServer(data.scholarshipEndDate);
                        data.registrationDate = DateUtils.convertLocalDateFromServer(data.registrationDate);
                    }
                    return data;
                }*/
            }/*,
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateOfBirth = DateUtils.convertLocalDateToServer(copy.dateOfBirth);
                    copy.scholarshipStartDate = DateUtils.convertLocalDateToServer(copy.scholarshipStartDate);
                    copy.scholarshipEndDate = DateUtils.convertLocalDateToServer(copy.scholarshipEndDate);
                    copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.dateOfBirth = DateUtils.convertLocalDateToServer(copy.dateOfBirth);
                    copy.scholarshipStartDate = DateUtils.convertLocalDateToServer(copy.scholarshipStartDate);
                    copy.scholarshipEndDate = DateUtils.convertLocalDateToServer(copy.scholarshipEndDate);
                    copy.registrationDate = DateUtils.convertLocalDateToServer(copy.registrationDate);
                    return angular.toJson(copy);
                }
            }
        */});
    }
})();
