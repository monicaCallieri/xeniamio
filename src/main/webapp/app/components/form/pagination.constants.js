(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
