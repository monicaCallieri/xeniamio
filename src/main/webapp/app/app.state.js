(function() {
    'use strict';

    angular
        .module('xeniaApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('app', {
            abstract: true,
            /*data: {
                authorities: ['ROLE_USER','ROLE_STUDENT','ROLE_LECTURER']
            },*/
            views: {
                'navbar@': {
                    templateUrl: 'app/layouts/navbar/navbar.html',
                    controller: 'NavbarController',
                    controllerAs: 'vm'
                },
                'side-navbar@': {
                    templateUrl: 'app/layouts/side-navbar/side-navbar.html',
                    controller: 'SideNavbarController',
                    controllerAs: 'snc'
                },
                'carousel@':{
                    templateUrl: 'app/home/carousel.html',
                    controller: 'CarouselController',
                    controllerAs: 'cc',
                    css: 'content/css/full-slider.css'
                }
            },
            resolve: {
                authorize: ['Auth',
                    function (Auth) {
                        return Auth.authorize();
                    }
                ],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('side-navbar');
                }],
            entity: ['$stateParams', 'StudentIntentions', function($stateParams, StudentIntentions) {
                    return StudentIntentions.query({state : 'NOTENROLLED'}).$promise;
                }]
            }
        });
    }
})();
