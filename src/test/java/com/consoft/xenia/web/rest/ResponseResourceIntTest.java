package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.Response;
import com.consoft.xenia.repository.ResponseRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResponseResource REST controller.
 *
 * @see ResponseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class ResponseResourceIntTest {

    private static final String DEFAULT_RESPONSE = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Inject
    private ResponseRepository responseRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restResponseMockMvc;

    private Response response;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ResponseResource responseResource = new ResponseResource();
        ReflectionTestUtils.setField(responseResource, "responseRepository", responseRepository);
        this.restResponseMockMvc = MockMvcBuilders.standaloneSetup(responseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Response createEntity(EntityManager em) {
        Response response = new Response()
                .response(DEFAULT_RESPONSE)
                .value(DEFAULT_VALUE);
        return response;
    }

    @Before
    public void initTest() {
        response = createEntity(em);
    }

    @Test
    @Transactional
    public void createResponse() throws Exception {
        int databaseSizeBeforeCreate = responseRepository.findAll().size();

        // Create the Response

        restResponseMockMvc.perform(post("/api/responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(response)))
            .andExpect(status().isCreated());

        // Validate the Response in the database
        List<Response> responseList = responseRepository.findAll();
        assertThat(responseList).hasSize(databaseSizeBeforeCreate + 1);
        Response testResponse = responseList.get(responseList.size() - 1);
        assertThat(testResponse.getResponse()).isEqualTo(DEFAULT_RESPONSE);
        assertThat(testResponse.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = responseRepository.findAll().size();

        // Create the Response with an existing ID
        Response existingResponse = new Response();
        existingResponse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResponseMockMvc.perform(post("/api/responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingResponse)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Response> responseList = responseRepository.findAll();
        assertThat(responseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllResponses() throws Exception {
        // Initialize the database
        responseRepository.saveAndFlush(response);

        // Get all the responseList
        restResponseMockMvc.perform(get("/api/responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(response.getId().intValue())))
            .andExpect(jsonPath("$.[*].response").value(hasItem(DEFAULT_RESPONSE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getResponse() throws Exception {
        // Initialize the database
        responseRepository.saveAndFlush(response);

        // Get the response
        restResponseMockMvc.perform(get("/api/responses/{id}", response.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(response.getId().intValue()))
            .andExpect(jsonPath("$.response").value(DEFAULT_RESPONSE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResponse() throws Exception {
        // Get the response
        restResponseMockMvc.perform(get("/api/responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResponse() throws Exception {
        // Initialize the database
        responseRepository.saveAndFlush(response);
        int databaseSizeBeforeUpdate = responseRepository.findAll().size();

        // Update the response
        Response updatedResponse = responseRepository.findOne(response.getId());
        updatedResponse
                .response(UPDATED_RESPONSE)
                .value(UPDATED_VALUE);

        restResponseMockMvc.perform(put("/api/responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResponse)))
            .andExpect(status().isOk());

        // Validate the Response in the database
        List<Response> responseList = responseRepository.findAll();
        assertThat(responseList).hasSize(databaseSizeBeforeUpdate);
        Response testResponse = responseList.get(responseList.size() - 1);
        assertThat(testResponse.getResponse()).isEqualTo(UPDATED_RESPONSE);
        assertThat(testResponse.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingResponse() throws Exception {
        int databaseSizeBeforeUpdate = responseRepository.findAll().size();

        // Create the Response

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResponseMockMvc.perform(put("/api/responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(response)))
            .andExpect(status().isCreated());

        // Validate the Response in the database
        List<Response> responseList = responseRepository.findAll();
        assertThat(responseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteResponse() throws Exception {
        // Initialize the database
        responseRepository.saveAndFlush(response);
        int databaseSizeBeforeDelete = responseRepository.findAll().size();

        // Get the response
        restResponseMockMvc.perform(delete("/api/responses/{id}", response.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Response> responseList = responseRepository.findAll();
        assertThat(responseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
