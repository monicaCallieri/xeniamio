package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.DatiSingVersLista;
import com.consoft.xenia.repository.DatiSingVersListaRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DatiSingVersListaResource REST controller.
 *
 * @see DatiSingVersListaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class DatiSingVersListaResourceIntTest {

    private static final Double DEFAULT_IMPORTO_SINGOLO_VERSAMENTO = 1D;
    private static final Double UPDATED_IMPORTO_SINGOLO_VERSAMENTO = 2D;

    private static final Double DEFAULT_COMMISSIONI_CARICO_PA = 1D;
    private static final Double UPDATED_COMMISSIONI_CARICO_PA = 2D;

    private static final String DEFAULT_IBAN_ACCREDITO = "AAAAAAAAAA";
    private static final String UPDATED_IBAN_ACCREDITO = "BBBBBBBBBB";

    private static final String DEFAULT_BIC_ACCREDITO = "AAAAAAAAAA";
    private static final String UPDATED_BIC_ACCREDITO = "BBBBBBBBBB";

    private static final String DEFAULT_IBAN_APPOGGIO = "AAAAAAAAAA";
    private static final String UPDATED_IBAN_APPOGGIO = "BBBBBBBBBB";

    private static final String DEFAULT_BIC_APPOGGIO = "AAAAAAAAAA";
    private static final String UPDATED_BIC_APPOGGIO = "BBBBBBBBBB";

    private static final String DEFAULT_CREDENZIALI_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_CREDENZIALI_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_CAULSALE_VERSAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_CAULSALE_VERSAMENTO = "BBBBBBBBBB";

    @Inject
    private DatiSingVersListaRepository datiSingVersListaRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restDatiSingVersListaMockMvc;

    private DatiSingVersLista datiSingVersLista;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DatiSingVersListaResource datiSingVersListaResource = new DatiSingVersListaResource();
        ReflectionTestUtils.setField(datiSingVersListaResource, "datiSingVersListaRepository", datiSingVersListaRepository);
        this.restDatiSingVersListaMockMvc = MockMvcBuilders.standaloneSetup(datiSingVersListaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DatiSingVersLista createEntity(EntityManager em) {
        DatiSingVersLista datiSingVersLista = new DatiSingVersLista()
                .importoSingoloVersamento(DEFAULT_IMPORTO_SINGOLO_VERSAMENTO)
                .commissioniCaricoPa(DEFAULT_COMMISSIONI_CARICO_PA)
                .ibanAccredito(DEFAULT_IBAN_ACCREDITO)
                .bicAccredito(DEFAULT_BIC_ACCREDITO)
                .ibanAppoggio(DEFAULT_IBAN_APPOGGIO)
                .bicAppoggio(DEFAULT_BIC_APPOGGIO)
                .credenzialiPagatore(DEFAULT_CREDENZIALI_PAGATORE)
                .caulsaleVersamento(DEFAULT_CAULSALE_VERSAMENTO);
        return datiSingVersLista;
    }

    @Before
    public void initTest() {
        datiSingVersLista = createEntity(em);
    }

    @Test
    @Transactional
    public void createDatiSingVersLista() throws Exception {
        int databaseSizeBeforeCreate = datiSingVersListaRepository.findAll().size();

        // Create the DatiSingVersLista

        restDatiSingVersListaMockMvc.perform(post("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datiSingVersLista)))
            .andExpect(status().isCreated());

        // Validate the DatiSingVersLista in the database
        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeCreate + 1);
        DatiSingVersLista testDatiSingVersLista = datiSingVersListaList.get(datiSingVersListaList.size() - 1);
        assertThat(testDatiSingVersLista.getImportoSingoloVersamento()).isEqualTo(DEFAULT_IMPORTO_SINGOLO_VERSAMENTO);
        assertThat(testDatiSingVersLista.getCommissioniCaricoPa()).isEqualTo(DEFAULT_COMMISSIONI_CARICO_PA);
        assertThat(testDatiSingVersLista.getIbanAccredito()).isEqualTo(DEFAULT_IBAN_ACCREDITO);
        assertThat(testDatiSingVersLista.getBicAccredito()).isEqualTo(DEFAULT_BIC_ACCREDITO);
        assertThat(testDatiSingVersLista.getIbanAppoggio()).isEqualTo(DEFAULT_IBAN_APPOGGIO);
        assertThat(testDatiSingVersLista.getBicAppoggio()).isEqualTo(DEFAULT_BIC_APPOGGIO);
        assertThat(testDatiSingVersLista.getCredenzialiPagatore()).isEqualTo(DEFAULT_CREDENZIALI_PAGATORE);
        assertThat(testDatiSingVersLista.getCaulsaleVersamento()).isEqualTo(DEFAULT_CAULSALE_VERSAMENTO);
    }

    @Test
    @Transactional
    public void createDatiSingVersListaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = datiSingVersListaRepository.findAll().size();

        // Create the DatiSingVersLista with an existing ID
        DatiSingVersLista existingDatiSingVersLista = new DatiSingVersLista();
        existingDatiSingVersLista.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDatiSingVersListaMockMvc.perform(post("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingDatiSingVersLista)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeCreate);
    }

    //@Test
    @Transactional
    public void checkImportoSingoloVersamentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = datiSingVersListaRepository.findAll().size();
        // set the field null
        datiSingVersLista.setImportoSingoloVersamento(null);

        // Create the DatiSingVersLista, which fails.

        restDatiSingVersListaMockMvc.perform(post("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datiSingVersLista)))
            .andExpect(status().isBadRequest());

        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommissioniCaricoPaIsRequired() throws Exception {
        int databaseSizeBeforeTest = datiSingVersListaRepository.findAll().size();
        // set the field null
        datiSingVersLista.setCommissioniCaricoPa(null);

        // Create the DatiSingVersLista, which fails.

        restDatiSingVersListaMockMvc.perform(post("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datiSingVersLista)))
            .andExpect(status().isBadRequest());

        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkCaulsaleVersamentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = datiSingVersListaRepository.findAll().size();
        // set the field null
        datiSingVersLista.setCaulsaleVersamento(null);

        // Create the DatiSingVersLista, which fails.

        restDatiSingVersListaMockMvc.perform(post("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datiSingVersLista)))
            .andExpect(status().isBadRequest());

        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDatiSingVersListas() throws Exception {
        // Initialize the database
        datiSingVersListaRepository.saveAndFlush(datiSingVersLista);

        // Get all the datiSingVersListaList
        restDatiSingVersListaMockMvc.perform(get("/api/dati-sing-vers-listas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(datiSingVersLista.getId().intValue())))
            .andExpect(jsonPath("$.[*].importoSingoloVersamento").value(hasItem(DEFAULT_IMPORTO_SINGOLO_VERSAMENTO.doubleValue())))
            .andExpect(jsonPath("$.[*].commissioniCaricoPa").value(hasItem(DEFAULT_COMMISSIONI_CARICO_PA.doubleValue())))
            .andExpect(jsonPath("$.[*].ibanAccredito").value(hasItem(DEFAULT_IBAN_ACCREDITO.toString())))
            .andExpect(jsonPath("$.[*].bicAccredito").value(hasItem(DEFAULT_BIC_ACCREDITO.toString())))
            .andExpect(jsonPath("$.[*].ibanAppoggio").value(hasItem(DEFAULT_IBAN_APPOGGIO.toString())))
            .andExpect(jsonPath("$.[*].bicAppoggio").value(hasItem(DEFAULT_BIC_APPOGGIO.toString())))
            .andExpect(jsonPath("$.[*].credenzialiPagatore").value(hasItem(DEFAULT_CREDENZIALI_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].caulsaleVersamento").value(hasItem(DEFAULT_CAULSALE_VERSAMENTO.toString())));
    }

    @Test
    @Transactional
    public void getDatiSingVersLista() throws Exception {
        // Initialize the database
        datiSingVersListaRepository.saveAndFlush(datiSingVersLista);

        // Get the datiSingVersLista
        restDatiSingVersListaMockMvc.perform(get("/api/dati-sing-vers-listas/{id}", datiSingVersLista.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(datiSingVersLista.getId().intValue()))
            .andExpect(jsonPath("$.importoSingoloVersamento").value(DEFAULT_IMPORTO_SINGOLO_VERSAMENTO.doubleValue()))
            .andExpect(jsonPath("$.commissioniCaricoPa").value(DEFAULT_COMMISSIONI_CARICO_PA.doubleValue()))
            .andExpect(jsonPath("$.ibanAccredito").value(DEFAULT_IBAN_ACCREDITO.toString()))
            .andExpect(jsonPath("$.bicAccredito").value(DEFAULT_BIC_ACCREDITO.toString()))
            .andExpect(jsonPath("$.ibanAppoggio").value(DEFAULT_IBAN_APPOGGIO.toString()))
            .andExpect(jsonPath("$.bicAppoggio").value(DEFAULT_BIC_APPOGGIO.toString()))
            .andExpect(jsonPath("$.credenzialiPagatore").value(DEFAULT_CREDENZIALI_PAGATORE.toString()))
            .andExpect(jsonPath("$.caulsaleVersamento").value(DEFAULT_CAULSALE_VERSAMENTO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDatiSingVersLista() throws Exception {
        // Get the datiSingVersLista
        restDatiSingVersListaMockMvc.perform(get("/api/dati-sing-vers-listas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDatiSingVersLista() throws Exception {
        // Initialize the database
        datiSingVersListaRepository.saveAndFlush(datiSingVersLista);
        int databaseSizeBeforeUpdate = datiSingVersListaRepository.findAll().size();

        // Update the datiSingVersLista
        DatiSingVersLista updatedDatiSingVersLista = datiSingVersListaRepository.findOne(datiSingVersLista.getId());
        updatedDatiSingVersLista
                .importoSingoloVersamento(UPDATED_IMPORTO_SINGOLO_VERSAMENTO)
                .commissioniCaricoPa(UPDATED_COMMISSIONI_CARICO_PA)
                .ibanAccredito(UPDATED_IBAN_ACCREDITO)
                .bicAccredito(UPDATED_BIC_ACCREDITO)
                .ibanAppoggio(UPDATED_IBAN_APPOGGIO)
                .bicAppoggio(UPDATED_BIC_APPOGGIO)
                .credenzialiPagatore(UPDATED_CREDENZIALI_PAGATORE)
                .caulsaleVersamento(UPDATED_CAULSALE_VERSAMENTO);

        restDatiSingVersListaMockMvc.perform(put("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDatiSingVersLista)))
            .andExpect(status().isOk());

        // Validate the DatiSingVersLista in the database
        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeUpdate);
        DatiSingVersLista testDatiSingVersLista = datiSingVersListaList.get(datiSingVersListaList.size() - 1);
        assertThat(testDatiSingVersLista.getImportoSingoloVersamento()).isEqualTo(UPDATED_IMPORTO_SINGOLO_VERSAMENTO);
        assertThat(testDatiSingVersLista.getCommissioniCaricoPa()).isEqualTo(UPDATED_COMMISSIONI_CARICO_PA);
        assertThat(testDatiSingVersLista.getIbanAccredito()).isEqualTo(UPDATED_IBAN_ACCREDITO);
        assertThat(testDatiSingVersLista.getBicAccredito()).isEqualTo(UPDATED_BIC_ACCREDITO);
        assertThat(testDatiSingVersLista.getIbanAppoggio()).isEqualTo(UPDATED_IBAN_APPOGGIO);
        assertThat(testDatiSingVersLista.getBicAppoggio()).isEqualTo(UPDATED_BIC_APPOGGIO);
        assertThat(testDatiSingVersLista.getCredenzialiPagatore()).isEqualTo(UPDATED_CREDENZIALI_PAGATORE);
        assertThat(testDatiSingVersLista.getCaulsaleVersamento()).isEqualTo(UPDATED_CAULSALE_VERSAMENTO);
    }

    @Test
    @Transactional
    public void updateNonExistingDatiSingVersLista() throws Exception {
        int databaseSizeBeforeUpdate = datiSingVersListaRepository.findAll().size();

        // Create the DatiSingVersLista

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDatiSingVersListaMockMvc.perform(put("/api/dati-sing-vers-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(datiSingVersLista)))
            .andExpect(status().isCreated());

        // Validate the DatiSingVersLista in the database
        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDatiSingVersLista() throws Exception {
        // Initialize the database
        datiSingVersListaRepository.saveAndFlush(datiSingVersLista);
        int databaseSizeBeforeDelete = datiSingVersListaRepository.findAll().size();

        // Get the datiSingVersLista
        restDatiSingVersListaMockMvc.perform(delete("/api/dati-sing-vers-listas/{id}", datiSingVersLista.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DatiSingVersLista> datiSingVersListaList = datiSingVersListaRepository.findAll();
        assertThat(datiSingVersListaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
