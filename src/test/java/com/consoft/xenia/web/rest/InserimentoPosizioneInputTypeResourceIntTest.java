package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;
import com.consoft.xenia.domain.EducationalProposer;
import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.User;
import com.consoft.xenia.domain.pagopa.Payment;

import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneInputType;
import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;
import com.consoft.xenia.repository.EducationalProposerRepository;
import com.consoft.xenia.repository.InserimentoPosizioneInputTypeRepository;
import com.consoft.xenia.repository.InserimentoPosizioneOutputTypeRepository;
import com.consoft.xenia.repository.InviaCarrPosListaRepository;
import com.consoft.xenia.repository.InviaCarrPosOutputRepository;
import com.consoft.xenia.service.PagoPAService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InserimentoPosizioneInputTypeResource REST controller.
 *
 * @see InserimentoPosizioneInputTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class InserimentoPosizioneInputTypeResourceIntTest {

    private static final String DEFAULT_IDENTIFICATIVO_BENEFICIARIO = "80213750583";
    private static final String UPDATED_IDENTIFICATIVO_BENEFICIARIO = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODICE_SERVIZIO = 1;
    private static final Integer UPDATED_CODICE_SERVIZIO = 2;

    private static final String DEFAULT_TIPO_RIFERIMENTO_CREDITORE = "universita";
    private static final String UPDATED_TIPO_RIFERIMENTO_CREDITORE = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_RIFERIMENTO_CREDITORE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_RIFERIMENTO_CREDITORE = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_IDENTIFICATIVO_PRESENTAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_CKEY_5 = "AAAAAAAAAA";
    private static final String UPDATED_CKEY_5 = "BBBBBBBBBB";

    private static final String DEFAULT_CKEY_6 = "AAAAAAAAAA";
    private static final String UPDATED_CKEY_6 = "BBBBBBBBBB";

    //private static final LocalDate DEFAULT_DATA_SCADENZA_PAGAMENTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate DEFAULT_DATA_SCADENZA_PAGAMENTO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate UPDATED_DATA_SCADENZA_PAGAMENTO = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_IMPORTO = 0.01D;
    private static final Double UPDATED_IMPORTO = 1D;

    private static final String DEFAULT_CAUSALE = "AAAAAAAAAA";
    private static final String UPDATED_CAUSALE = "BBBBBBBBBB";

    private static final String DEFAULT_SAVV = "AAAAAA";
    private static final String UPDATED_SAVV = "BBBBBB";

    private static final String DEFAULT_TIPO_ID_DEBITORE = "AA";
    private static final String UPDATED_TIPO_ID_DEBITORE = "BB";

    private static final String DEFAULT_IDENTIFICATIVO_DEBITORE = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO_DEBITORE = "BBBBBBBBBB";

    private static final String DEFAULT_ANAGRAFICA_DEBITORE = "AAAAAAAAAA";
    private static final String UPDATED_ANAGRAFICA_DEBITORE = "BBBBBBBBBB";

    private static final String DEFAULT_INDIRIZZO_DEBITORE = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO_DEBITORE = "BBBBBBBBBB";

    private static final String DEFAULT_CAP_DEBITORE = "AAAAA";
    private static final String UPDATED_CAP_DEBITORE = "BBBBB";

    private static final String DEFAULT_LOCALITA_DEBITORE = "AAAAAAAAAA";
    private static final String UPDATED_LOCALITA_DEBITORE = "BBBBBBBBBB";

    private static final String DEFAULT_NAZIONE_DEBITORE = "AA";
    private static final String UPDATED_NAZIONE_DEBITORE = "BB";

    private static final String DEFAULT_EMAIL_DEBITORE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_DEBITORE = "BBBBBBBBBB";

    private static final String DEFAULT_CIVICO_DEBITORE = "AAAAA";
    private static final String UPDATED_CIVICO_DEBITORE = "BBBBB";

    private static final String DEFAULT_PROVINCIA_DEBITORE = "AA";
    private static final String UPDATED_PROVINCIA_DEBITORE = "BB";

    @Inject
    private InserimentoPosizioneInputTypeRepository inserimentoPosizioneInputTypeRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    @Inject
    private PagoPAService pagoPAService;

    private MockMvc restInserimentoPosizioneInputTypeMockMvc;

    private InserimentoPosizioneInputType inserimentoPosizioneInputType;

    private Payment payment;

    @Inject
    private InserimentoPosizioneOutputTypeRepository inserimentoPosizioneOutputTypeRepository;

    @Inject
    private InviaCarrPosOutputRepository inviaCarrPosOutputRepository;

    @Inject
    private InviaCarrPosListaRepository inviaCarrPosListaRepository;

    @Inject
    private EducationalProposerRepository educationalProposerRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InserimentoPosizioneInputTypeResource inserimentoPosizioneInputTypeResource = new InserimentoPosizioneInputTypeResource();
        ReflectionTestUtils.setField(inserimentoPosizioneInputTypeResource, "inserimentoPosizioneInputTypeRepository", inserimentoPosizioneInputTypeRepository);
        ReflectionTestUtils.setField(inserimentoPosizioneInputTypeResource, "inserimentoPosizioneOutputTypeRepository", inserimentoPosizioneOutputTypeRepository);
        ReflectionTestUtils.setField(inserimentoPosizioneInputTypeResource, "inviaCarrPosOutputRepository", inviaCarrPosOutputRepository);
        ReflectionTestUtils.setField(inserimentoPosizioneInputTypeResource, "inviaCarrPosListaRepository", inviaCarrPosListaRepository);
        ReflectionTestUtils.setField(inserimentoPosizioneInputTypeResource, "pagoPAService", pagoPAService);
        ReflectionTestUtils.setField(inserimentoPosizioneInputTypeResource, "educationalProposerRepository", educationalProposerRepository);
        this.restInserimentoPosizioneInputTypeMockMvc = MockMvcBuilders.standaloneSetup(inserimentoPosizioneInputTypeResource)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InserimentoPosizioneInputType createEntity(EntityManager em) {
        InserimentoPosizioneInputType inserimentoPosizioneInputType = new InserimentoPosizioneInputType()
                .identificativo_beneficiario(DEFAULT_IDENTIFICATIVO_BENEFICIARIO)
                .codice_servizio(DEFAULT_CODICE_SERVIZIO)
                .tipo_riferimento_creditore(DEFAULT_TIPO_RIFERIMENTO_CREDITORE)
                .codice_riferimento_creditore(DEFAULT_CODICE_RIFERIMENTO_CREDITORE)
                .codice_identificativo_presentazione(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE)
                .identificativo_univoco_versamento(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO)
                .ckey_5(DEFAULT_CKEY_5)
                .ckey_6(DEFAULT_CKEY_6)
                .data_scadenza_pagamento(DEFAULT_DATA_SCADENZA_PAGAMENTO)
                .importo(DEFAULT_IMPORTO)
                .causale(DEFAULT_CAUSALE)
                .savv(DEFAULT_SAVV)
                .tipo_id_debitore(DEFAULT_TIPO_ID_DEBITORE)
                .identificativo_debitore(DEFAULT_IDENTIFICATIVO_DEBITORE)
                .anagrafica_debitore(DEFAULT_ANAGRAFICA_DEBITORE)
                .indirizzo_debitore(DEFAULT_INDIRIZZO_DEBITORE)
                .cap_debitore(DEFAULT_CAP_DEBITORE)
                .localita_debitore(DEFAULT_LOCALITA_DEBITORE)
                .nazione_debitore(DEFAULT_NAZIONE_DEBITORE)
                .email_debitore(DEFAULT_EMAIL_DEBITORE)
                .civico_debitore(DEFAULT_CIVICO_DEBITORE)
                .provincia_debitore(DEFAULT_PROVINCIA_DEBITORE);
        // Add required entity
        InserimentoPosizioneOutputType inserimentoPosizioneResponse = InserimentoPosizioneOutputTypeResourceIntTest.createEntity(em);
        em.persist(inserimentoPosizioneResponse);
        em.flush();
        inserimentoPosizioneInputType.setInserimentoPosizioneResponse(inserimentoPosizioneResponse);
        return inserimentoPosizioneInputType;
    }

    public static Payment createEntityPayment(EntityManager em) {
        User user = new User();
        user.setEmail(DEFAULT_EMAIL_DEBITORE);
        EducationalProposer educationalProposer = new EducationalProposer();
        educationalProposer.setName(DEFAULT_CAUSALE);
        educationalProposer.setPrice(DEFAULT_IMPORTO);
        educationalProposer.setStartdate(DEFAULT_DATA_SCADENZA_PAGAMENTO);

        StudentIntentions studentIntentions = new StudentIntentions();
        studentIntentions.setEducationalProposer(educationalProposer);
        StudentIntentions[] studentIntentionsArray = new StudentIntentions[1];
        studentIntentionsArray[0] = studentIntentions;
        studentIntentions.setUser(user);
        Payment inserimentoPosizioneInputType = new Payment();
        inserimentoPosizioneInputType.setStudentIntentions(studentIntentionsArray);
        inserimentoPosizioneInputType.setTotal(DEFAULT_IMPORTO);
        inserimentoPosizioneInputType.setTypeOfPayment("true");
        return inserimentoPosizioneInputType;
    }

    @Before
    public void initTest() {
        inserimentoPosizioneInputType = createEntity(em);
        payment = createEntityPayment(em);
    }

    @Test
    @Transactional
    public void createInserimentoPosizioneInputType() throws Exception {
        int databaseSizeBeforeCreate = inserimentoPosizioneInputTypeRepository.findAll().size();
        EducationalProposer educationalProposer = new EducationalProposer();
        educationalProposer.setName(DEFAULT_CAUSALE);
        educationalProposer.setPrice(DEFAULT_IMPORTO);
        educationalProposer.setStartdate(DEFAULT_DATA_SCADENZA_PAGAMENTO);
        educationalProposerRepository.save(educationalProposer);
        // Create the InserimentoPosizioneInputType

        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                //.content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .content(TestUtil.convertObjectToJsonBytes(payment)))
                .andExpect(status().isCreated());

        // Validate the InserimentoPosizioneInputType in the database
        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeCreate + 1);
        InserimentoPosizioneInputType testInserimentoPosizioneInputType = inserimentoPosizioneInputTypeList.get(inserimentoPosizioneInputTypeList.size() - 1);
        assertThat(testInserimentoPosizioneInputType.getIdentificativo_beneficiario()).isEqualTo(DEFAULT_IDENTIFICATIVO_BENEFICIARIO);
        /*assertThat(testInserimentoPosizioneInputType.getCodice_servizio()).isEqualTo(DEFAULT_CODICE_SERVIZIO);
        assertThat(testInserimentoPosizioneInputType.getTipo_riferimento_creditore()).isEqualTo(DEFAULT_TIPO_RIFERIMENTO_CREDITORE);
        assertThat(testInserimentoPosizioneInputType.getCodice_riferimento_creditore()).isEqualTo(DEFAULT_CODICE_RIFERIMENTO_CREDITORE);
        assertThat(testInserimentoPosizioneInputType.getCodice_identificativo_presentazione()).isEqualTo(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE);
        assertThat(testInserimentoPosizioneInputType.getIdentificativo_univoco_versamento()).isEqualTo(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
        assertThat(testInserimentoPosizioneInputType.getCkey_5()).isEqualTo(DEFAULT_CKEY_5);
        assertThat(testInserimentoPosizioneInputType.getCkey_6()).isEqualTo(DEFAULT_CKEY_6);
        assertThat(testInserimentoPosizioneInputType.getData_scadenza_pagamento()).isEqualTo(DEFAULT_DATA_SCADENZA_PAGAMENTO);
        assertThat(testInserimentoPosizioneInputType.getImporto()).isEqualTo(DEFAULT_IMPORTO);
        assertThat(testInserimentoPosizioneInputType.getCausale()).isEqualTo(DEFAULT_CAUSALE);
        assertThat(testInserimentoPosizioneInputType.getSavv()).isEqualTo(DEFAULT_SAVV);
        assertThat(testInserimentoPosizioneInputType.getTipo_id_debitore()).isEqualTo(DEFAULT_TIPO_ID_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getIdentificativo_debitore()).isEqualTo(DEFAULT_IDENTIFICATIVO_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getAnagrafica_debitore()).isEqualTo(DEFAULT_ANAGRAFICA_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getIndirizzo_debitore()).isEqualTo(DEFAULT_INDIRIZZO_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getCap_debitore()).isEqualTo(DEFAULT_CAP_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getLocalita_debitore()).isEqualTo(DEFAULT_LOCALITA_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getNazione_debitore()).isEqualTo(DEFAULT_NAZIONE_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getEmail_debitore()).isEqualTo(DEFAULT_EMAIL_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getCivico_debitore()).isEqualTo(DEFAULT_CIVICO_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getProvincia_debitore()).isEqualTo(DEFAULT_PROVINCIA_DEBITORE);*/
    }

    //@Test
    @Transactional
    public void createInserimentoPosizioneInputTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inserimentoPosizioneInputTypeRepository.findAll().size();

        // Create the InserimentoPosizioneInputType with an existing ID
        InserimentoPosizioneInputType existingInserimentoPosizioneInputType = new InserimentoPosizioneInputType();
        existingInserimentoPosizioneInputType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(payment)))
                .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeCreate);
    }

    //@Test
    @Transactional
    public void checkIdentificativo_beneficiarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneInputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneInputType.setIdentificativo_beneficiario(null);

        // Create the InserimentoPosizioneInputType, which fails.
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .andExpect(status().isBadRequest());

        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkCodice_servizioIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneInputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneInputType.setCodice_servizio(null);

        // Create the InserimentoPosizioneInputType, which fails.
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .andExpect(status().isBadRequest());

        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkTipo_riferimento_creditoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneInputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneInputType.setTipo_riferimento_creditore(null);

        // Create the InserimentoPosizioneInputType, which fails.
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .andExpect(status().isBadRequest());

        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void checkCodice_riferimento_creditoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneInputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneInputType.setCodice_riferimento_creditore(null);

        // Create the InserimentoPosizioneInputType, which fails.
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(payment)))
                .andExpect(status().isCreated());

        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeTest + 1);
    }

    //@Test
    @Transactional
    public void checkImportoIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneInputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneInputType.setImporto(null);

        // Create the InserimentoPosizioneInputType, which fails.
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .andExpect(status().isBadRequest());

        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeTest);
    }

    // @Test
    @Transactional
    public void checkCausaleIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneInputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneInputType.setCausale(null);

        // Create the InserimentoPosizioneInputType, which fails.
        restInserimentoPosizioneInputTypeMockMvc.perform(post("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .andExpect(status().isBadRequest());

        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInserimentoPosizioneInputTypes() throws Exception {
        // Initialize the database
        inserimentoPosizioneInputTypeRepository.saveAndFlush(inserimentoPosizioneInputType);

        // Get all the inserimentoPosizioneInputTypeList
        restInserimentoPosizioneInputTypeMockMvc.perform(get("/api/inserimento-posizione-input-types?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(inserimentoPosizioneInputType.getId().intValue())))
                .andExpect(jsonPath("$.[*].identificativo_beneficiario").value(hasItem(DEFAULT_IDENTIFICATIVO_BENEFICIARIO.toString())))
                .andExpect(jsonPath("$.[*].codice_servizio").value(hasItem(DEFAULT_CODICE_SERVIZIO)))
                .andExpect(jsonPath("$.[*].tipo_riferimento_creditore").value(hasItem(DEFAULT_TIPO_RIFERIMENTO_CREDITORE.toString())))
                .andExpect(jsonPath("$.[*].codice_riferimento_creditore").value(hasItem(DEFAULT_CODICE_RIFERIMENTO_CREDITORE.toString())))
                .andExpect(jsonPath("$.[*].codice_identificativo_presentazione").value(hasItem(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE.toString())))
                .andExpect(jsonPath("$.[*].identificativo_univoco_versamento").value(hasItem(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO.toString())))
                .andExpect(jsonPath("$.[*].ckey_5").value(hasItem(DEFAULT_CKEY_5.toString())))
                .andExpect(jsonPath("$.[*].ckey_6").value(hasItem(DEFAULT_CKEY_6.toString())))
                .andExpect(jsonPath("$.[*].data_scadenza_pagamento").value(hasItem(DEFAULT_DATA_SCADENZA_PAGAMENTO.toString())))
                .andExpect(jsonPath("$.[*].importo").value(hasItem(DEFAULT_IMPORTO.doubleValue())))
                .andExpect(jsonPath("$.[*].causale").value(hasItem(DEFAULT_CAUSALE.toString())))
                .andExpect(jsonPath("$.[*].savv").value(hasItem(DEFAULT_SAVV.toString())))
                .andExpect(jsonPath("$.[*].tipo_id_debitore").value(hasItem(DEFAULT_TIPO_ID_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].identificativo_debitore").value(hasItem(DEFAULT_IDENTIFICATIVO_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].anagrafica_debitore").value(hasItem(DEFAULT_ANAGRAFICA_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].indirizzo_debitore").value(hasItem(DEFAULT_INDIRIZZO_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].cap_debitore").value(hasItem(DEFAULT_CAP_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].localita_debitore").value(hasItem(DEFAULT_LOCALITA_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].nazione_debitore").value(hasItem(DEFAULT_NAZIONE_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].email_debitore").value(hasItem(DEFAULT_EMAIL_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].civico_debitore").value(hasItem(DEFAULT_CIVICO_DEBITORE.toString())))
                .andExpect(jsonPath("$.[*].provincia_debitore").value(hasItem(DEFAULT_PROVINCIA_DEBITORE.toString())));
    }

    @Test
    @Transactional
    public void getInserimentoPosizioneInputType() throws Exception {
        // Initialize the database
        inserimentoPosizioneInputTypeRepository.saveAndFlush(inserimentoPosizioneInputType);

        // Get the inserimentoPosizioneInputType
        restInserimentoPosizioneInputTypeMockMvc.perform(get("/api/inserimento-posizione-input-types/{id}", inserimentoPosizioneInputType.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(inserimentoPosizioneInputType.getId().intValue()))
                .andExpect(jsonPath("$.identificativo_beneficiario").value(DEFAULT_IDENTIFICATIVO_BENEFICIARIO.toString()))
                .andExpect(jsonPath("$.codice_servizio").value(DEFAULT_CODICE_SERVIZIO))
                .andExpect(jsonPath("$.tipo_riferimento_creditore").value(DEFAULT_TIPO_RIFERIMENTO_CREDITORE.toString()))
                .andExpect(jsonPath("$.codice_riferimento_creditore").value(DEFAULT_CODICE_RIFERIMENTO_CREDITORE.toString()))
                .andExpect(jsonPath("$.codice_identificativo_presentazione").value(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE.toString()))
                .andExpect(jsonPath("$.identificativo_univoco_versamento").value(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO.toString()))
                .andExpect(jsonPath("$.ckey_5").value(DEFAULT_CKEY_5.toString()))
                .andExpect(jsonPath("$.ckey_6").value(DEFAULT_CKEY_6.toString()))
                .andExpect(jsonPath("$.data_scadenza_pagamento").value(DEFAULT_DATA_SCADENZA_PAGAMENTO.toString()))
                .andExpect(jsonPath("$.importo").value(DEFAULT_IMPORTO.doubleValue()))
                .andExpect(jsonPath("$.causale").value(DEFAULT_CAUSALE.toString()))
                .andExpect(jsonPath("$.savv").value(DEFAULT_SAVV.toString()))
                .andExpect(jsonPath("$.tipo_id_debitore").value(DEFAULT_TIPO_ID_DEBITORE.toString()))
                .andExpect(jsonPath("$.identificativo_debitore").value(DEFAULT_IDENTIFICATIVO_DEBITORE.toString()))
                .andExpect(jsonPath("$.anagrafica_debitore").value(DEFAULT_ANAGRAFICA_DEBITORE.toString()))
                .andExpect(jsonPath("$.indirizzo_debitore").value(DEFAULT_INDIRIZZO_DEBITORE.toString()))
                .andExpect(jsonPath("$.cap_debitore").value(DEFAULT_CAP_DEBITORE.toString()))
                .andExpect(jsonPath("$.localita_debitore").value(DEFAULT_LOCALITA_DEBITORE.toString()))
                .andExpect(jsonPath("$.nazione_debitore").value(DEFAULT_NAZIONE_DEBITORE.toString()))
                .andExpect(jsonPath("$.email_debitore").value(DEFAULT_EMAIL_DEBITORE.toString()))
                .andExpect(jsonPath("$.civico_debitore").value(DEFAULT_CIVICO_DEBITORE.toString()))
                .andExpect(jsonPath("$.provincia_debitore").value(DEFAULT_PROVINCIA_DEBITORE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInserimentoPosizioneInputType() throws Exception {
        // Get the inserimentoPosizioneInputType
        restInserimentoPosizioneInputTypeMockMvc.perform(get("/api/inserimento-posizione-input-types/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInserimentoPosizioneInputType() throws Exception {
        // Initialize the database
        inserimentoPosizioneInputTypeRepository.saveAndFlush(inserimentoPosizioneInputType);
        int databaseSizeBeforeUpdate = inserimentoPosizioneInputTypeRepository.findAll().size();

        // Update the inserimentoPosizioneInputType
        InserimentoPosizioneInputType updatedInserimentoPosizioneInputType = inserimentoPosizioneInputTypeRepository.findOne(inserimentoPosizioneInputType.getId());
        updatedInserimentoPosizioneInputType
                .identificativo_beneficiario(UPDATED_IDENTIFICATIVO_BENEFICIARIO)
                .codice_servizio(UPDATED_CODICE_SERVIZIO)
                .tipo_riferimento_creditore(UPDATED_TIPO_RIFERIMENTO_CREDITORE)
                .codice_riferimento_creditore(UPDATED_CODICE_RIFERIMENTO_CREDITORE)
                .codice_identificativo_presentazione(UPDATED_CODICE_IDENTIFICATIVO_PRESENTAZIONE)
                .identificativo_univoco_versamento(UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO)
                .ckey_5(UPDATED_CKEY_5)
                .ckey_6(UPDATED_CKEY_6)
                .data_scadenza_pagamento(UPDATED_DATA_SCADENZA_PAGAMENTO)
                .importo(UPDATED_IMPORTO)
                .causale(UPDATED_CAUSALE)
                .savv(UPDATED_SAVV)
                .tipo_id_debitore(UPDATED_TIPO_ID_DEBITORE)
                .identificativo_debitore(UPDATED_IDENTIFICATIVO_DEBITORE)
                .anagrafica_debitore(UPDATED_ANAGRAFICA_DEBITORE)
                .indirizzo_debitore(UPDATED_INDIRIZZO_DEBITORE)
                .cap_debitore(UPDATED_CAP_DEBITORE)
                .localita_debitore(UPDATED_LOCALITA_DEBITORE)
                .nazione_debitore(UPDATED_NAZIONE_DEBITORE)
                .email_debitore(UPDATED_EMAIL_DEBITORE)
                .civico_debitore(UPDATED_CIVICO_DEBITORE)
                .provincia_debitore(UPDATED_PROVINCIA_DEBITORE);

        restInserimentoPosizioneInputTypeMockMvc.perform(put("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedInserimentoPosizioneInputType)))
                .andExpect(status().isOk());

        // Validate the InserimentoPosizioneInputType in the database
        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeUpdate);
        InserimentoPosizioneInputType testInserimentoPosizioneInputType = inserimentoPosizioneInputTypeList.get(inserimentoPosizioneInputTypeList.size() - 1);
        assertThat(testInserimentoPosizioneInputType.getIdentificativo_beneficiario()).isEqualTo(UPDATED_IDENTIFICATIVO_BENEFICIARIO);
        assertThat(testInserimentoPosizioneInputType.getCodice_servizio()).isEqualTo(UPDATED_CODICE_SERVIZIO);
        assertThat(testInserimentoPosizioneInputType.getTipo_riferimento_creditore()).isEqualTo(UPDATED_TIPO_RIFERIMENTO_CREDITORE);
        assertThat(testInserimentoPosizioneInputType.getCodice_riferimento_creditore()).isEqualTo(UPDATED_CODICE_RIFERIMENTO_CREDITORE);
        assertThat(testInserimentoPosizioneInputType.getCodice_identificativo_presentazione()).isEqualTo(UPDATED_CODICE_IDENTIFICATIVO_PRESENTAZIONE);
        assertThat(testInserimentoPosizioneInputType.getIdentificativo_univoco_versamento()).isEqualTo(UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
        assertThat(testInserimentoPosizioneInputType.getCkey_5()).isEqualTo(UPDATED_CKEY_5);
        assertThat(testInserimentoPosizioneInputType.getCkey_6()).isEqualTo(UPDATED_CKEY_6);
        assertThat(testInserimentoPosizioneInputType.getData_scadenza_pagamento()).isEqualTo(UPDATED_DATA_SCADENZA_PAGAMENTO);
        assertThat(testInserimentoPosizioneInputType.getImporto()).isEqualTo(UPDATED_IMPORTO);
        assertThat(testInserimentoPosizioneInputType.getCausale()).isEqualTo(UPDATED_CAUSALE);
        assertThat(testInserimentoPosizioneInputType.getSavv()).isEqualTo(UPDATED_SAVV);
        assertThat(testInserimentoPosizioneInputType.getTipo_id_debitore()).isEqualTo(UPDATED_TIPO_ID_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getIdentificativo_debitore()).isEqualTo(UPDATED_IDENTIFICATIVO_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getAnagrafica_debitore()).isEqualTo(UPDATED_ANAGRAFICA_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getIndirizzo_debitore()).isEqualTo(UPDATED_INDIRIZZO_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getCap_debitore()).isEqualTo(UPDATED_CAP_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getLocalita_debitore()).isEqualTo(UPDATED_LOCALITA_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getNazione_debitore()).isEqualTo(UPDATED_NAZIONE_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getEmail_debitore()).isEqualTo(UPDATED_EMAIL_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getCivico_debitore()).isEqualTo(UPDATED_CIVICO_DEBITORE);
        assertThat(testInserimentoPosizioneInputType.getProvincia_debitore()).isEqualTo(UPDATED_PROVINCIA_DEBITORE);
    }

    //@Test
    @Transactional
    public void updateNonExistingInserimentoPosizioneInputType() throws Exception {
        int databaseSizeBeforeUpdate = inserimentoPosizioneInputTypeRepository.findAll().size();

        // Create the InserimentoPosizioneInputType
        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInserimentoPosizioneInputTypeMockMvc.perform(put("/api/inserimento-posizione-input-types")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneInputType)))
                .andExpect(status().isCreated());

        // Validate the InserimentoPosizioneInputType in the database
        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInserimentoPosizioneInputType() throws Exception {
        // Initialize the database
        inserimentoPosizioneInputTypeRepository.saveAndFlush(inserimentoPosizioneInputType);
        int databaseSizeBeforeDelete = inserimentoPosizioneInputTypeRepository.findAll().size();

        // Get the inserimentoPosizioneInputType
        restInserimentoPosizioneInputTypeMockMvc.perform(delete("/api/inserimento-posizione-input-types/{id}", inserimentoPosizioneInputType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InserimentoPosizioneInputType> inserimentoPosizioneInputTypeList = inserimentoPosizioneInputTypeRepository.findAll();
        assertThat(inserimentoPosizioneInputTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
