package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosLista;
import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;
import com.consoft.xenia.repository.InviaCarrPosListaRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InviaCarrPosListaResource REST controller.
 *
 * @see InviaCarrPosListaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class InviaCarrPosListaResourceIntTest {

    private static final String DEFAULT_SPONTANEO = "AAAAAAAAAA";
    private static final String UPDATED_SPONTANEO = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICATIVO_BENEFICIARIO = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO_BENEFICIARIO = "BBBBBBBBBB";

    private static final String DEFAULT_CCP = "AAAAAAAAAA";
    private static final String UPDATED_CCP = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_PA = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_PA = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_SERVIZIO = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_SERVIZIO = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_ID_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_ID_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICATIVO_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_ANAGRAFICA_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_ANAGRAFICA_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_INDIRIZZO_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_INDIRIZZO_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_CIVICO_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_CIVICO_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_CAP_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_CAP_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCALITA_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_LOCALITA_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_NAZIONE_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_NAZIONE_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_PAGATORE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_PAGATORE = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_SCADENZA_PAGAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_DATA_SCADENZA_PAGAMENTO = "BBBBBBBBBB";

    private static final Double DEFAULT_IMPORTO_PAGAMENTO = 1D;
    private static final Double UPDATED_IMPORTO_PAGAMENTO = 2D;

    private static final String DEFAULT_TIPO_FIRMA_RICEVUTA = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_FIRMA_RICEVUTA = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_RIFERIMENTO_CREDITORE = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_RIFERIMENTO_CREDITORE = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_RIFERIMENTO_CREDITORE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_RIFERIMENTO_CREDITORE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO = "BBBBBBBBBB";

    @Inject
    private InviaCarrPosListaRepository inviaCarrPosListaRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restInviaCarrPosListaMockMvc;

    private InviaCarrPosLista inviaCarrPosLista;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InviaCarrPosListaResource inviaCarrPosListaResource = new InviaCarrPosListaResource();
        ReflectionTestUtils.setField(inviaCarrPosListaResource, "inviaCarrPosListaRepository", inviaCarrPosListaRepository);
        this.restInviaCarrPosListaMockMvc = MockMvcBuilders.standaloneSetup(inviaCarrPosListaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InviaCarrPosLista createEntity(EntityManager em) {
        InviaCarrPosLista inviaCarrPosLista = new InviaCarrPosLista()
                .spontaneo(DEFAULT_SPONTANEO)
                .identificativoBeneficiario(DEFAULT_IDENTIFICATIVO_BENEFICIARIO)
                .ccp(DEFAULT_CCP)
                .codicePa(DEFAULT_CODICE_PA)
                .codiceServizio(DEFAULT_CODICE_SERVIZIO)
                .tipoIdPagatore(DEFAULT_TIPO_ID_PAGATORE)
                .identificativoPagatore(DEFAULT_IDENTIFICATIVO_PAGATORE)
                .anagraficaPagatore(DEFAULT_ANAGRAFICA_PAGATORE)
                .indirizzoPagatore(DEFAULT_INDIRIZZO_PAGATORE)
                .civicoPagatore(DEFAULT_CIVICO_PAGATORE)
                .capPagatore(DEFAULT_CAP_PAGATORE)
                .localitaPagatore(DEFAULT_LOCALITA_PAGATORE)
                .provinciaPagatore(DEFAULT_PROVINCIA_PAGATORE)
                .codiceNazionePagatore(DEFAULT_CODICE_NAZIONE_PAGATORE)
                .emailPagatore(DEFAULT_EMAIL_PAGATORE)
                .dataScadenzaPagamento(DEFAULT_DATA_SCADENZA_PAGAMENTO)
                .importoPagamento(DEFAULT_IMPORTO_PAGAMENTO)
                .tipoFirmaRicevuta(DEFAULT_TIPO_FIRMA_RICEVUTA)
                .tipoRiferimentoCreditore(DEFAULT_TIPO_RIFERIMENTO_CREDITORE)
                .codiceRiferimentoCreditore(DEFAULT_CODICE_RIFERIMENTO_CREDITORE)
                .identificativoUnivocoVersamento(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
        // Add required entity
        InviaCarrPosOutput inviaCarrPosListaOut = InviaCarrPosOutputResourceIntTest.createEntity(em);
        em.persist(inviaCarrPosListaOut);
        em.flush();
        inviaCarrPosLista.setInviaCarrPosLista(inviaCarrPosListaOut);
        return inviaCarrPosLista;
    }

    @Before
    public void initTest() {
        inviaCarrPosLista = createEntity(em);
    }

    @Test
    @Transactional
    public void createInviaCarrPosLista() throws Exception {
        int databaseSizeBeforeCreate = inviaCarrPosListaRepository.findAll().size();

        // Create the InviaCarrPosLista

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isCreated());

        // Validate the InviaCarrPosLista in the database
        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeCreate + 1);
        InviaCarrPosLista testInviaCarrPosLista = inviaCarrPosListaList.get(inviaCarrPosListaList.size() - 1);
        assertThat(testInviaCarrPosLista.getSpontaneo()).isEqualTo(DEFAULT_SPONTANEO);
        assertThat(testInviaCarrPosLista.getIdentificativoBeneficiario()).isEqualTo(DEFAULT_IDENTIFICATIVO_BENEFICIARIO);
        assertThat(testInviaCarrPosLista.getCcp()).isEqualTo(DEFAULT_CCP);
        assertThat(testInviaCarrPosLista.getCodicePa()).isEqualTo(DEFAULT_CODICE_PA);
        assertThat(testInviaCarrPosLista.getCodiceServizio()).isEqualTo(DEFAULT_CODICE_SERVIZIO);
        assertThat(testInviaCarrPosLista.getTipoIdPagatore()).isEqualTo(DEFAULT_TIPO_ID_PAGATORE);
        assertThat(testInviaCarrPosLista.getIdentificativoPagatore()).isEqualTo(DEFAULT_IDENTIFICATIVO_PAGATORE);
        assertThat(testInviaCarrPosLista.getAnagraficaPagatore()).isEqualTo(DEFAULT_ANAGRAFICA_PAGATORE);
        assertThat(testInviaCarrPosLista.getIndirizzoPagatore()).isEqualTo(DEFAULT_INDIRIZZO_PAGATORE);
        assertThat(testInviaCarrPosLista.getCivicoPagatore()).isEqualTo(DEFAULT_CIVICO_PAGATORE);
        assertThat(testInviaCarrPosLista.getCapPagatore()).isEqualTo(DEFAULT_CAP_PAGATORE);
        assertThat(testInviaCarrPosLista.getLocalitaPagatore()).isEqualTo(DEFAULT_LOCALITA_PAGATORE);
        assertThat(testInviaCarrPosLista.getProvinciaPagatore()).isEqualTo(DEFAULT_PROVINCIA_PAGATORE);
        assertThat(testInviaCarrPosLista.getCodiceNazionePagatore()).isEqualTo(DEFAULT_CODICE_NAZIONE_PAGATORE);
        assertThat(testInviaCarrPosLista.getEmailPagatore()).isEqualTo(DEFAULT_EMAIL_PAGATORE);
        assertThat(testInviaCarrPosLista.getDataScadenzaPagamento()).isEqualTo(DEFAULT_DATA_SCADENZA_PAGAMENTO);
        assertThat(testInviaCarrPosLista.getImportoPagamento()).isEqualTo(DEFAULT_IMPORTO_PAGAMENTO);
        assertThat(testInviaCarrPosLista.getTipoFirmaRicevuta()).isEqualTo(DEFAULT_TIPO_FIRMA_RICEVUTA);
        assertThat(testInviaCarrPosLista.getTipoRiferimentoCreditore()).isEqualTo(DEFAULT_TIPO_RIFERIMENTO_CREDITORE);
        assertThat(testInviaCarrPosLista.getCodiceRiferimentoCreditore()).isEqualTo(DEFAULT_CODICE_RIFERIMENTO_CREDITORE);
        assertThat(testInviaCarrPosLista.getIdentificativoUnivocoVersamento()).isEqualTo(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
    }

    @Test
    @Transactional
    public void createInviaCarrPosListaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inviaCarrPosListaRepository.findAll().size();

        // Create the InviaCarrPosLista with an existing ID
        InviaCarrPosLista existingInviaCarrPosLista = new InviaCarrPosLista();
        existingInviaCarrPosLista.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingInviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkSpontaneoIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosListaRepository.findAll().size();
        // set the field null
        inviaCarrPosLista.setSpontaneo(null);

        // Create the InviaCarrPosLista, which fails.

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdentificativoBeneficiarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosListaRepository.findAll().size();
        // set the field null
        inviaCarrPosLista.setIdentificativoBeneficiario(null);

        // Create the InviaCarrPosLista, which fails.

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoIdPagatoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosListaRepository.findAll().size();
        // set the field null
        inviaCarrPosLista.setTipoIdPagatore(null);

        // Create the InviaCarrPosLista, which fails.

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIdentificativoPagatoreIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosListaRepository.findAll().size();
        // set the field null
        inviaCarrPosLista.setIdentificativoPagatore(null);

        // Create the InviaCarrPosLista, which fails.

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkImportoPagamentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosListaRepository.findAll().size();
        // set the field null
        inviaCarrPosLista.setImportoPagamento(null);

        // Create the InviaCarrPosLista, which fails.

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoFirmaRicevutaIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosListaRepository.findAll().size();
        // set the field null
        inviaCarrPosLista.setTipoFirmaRicevuta(null);

        // Create the InviaCarrPosLista, which fails.

        restInviaCarrPosListaMockMvc.perform(post("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInviaCarrPosListas() throws Exception {
        // Initialize the database
        inviaCarrPosListaRepository.saveAndFlush(inviaCarrPosLista);

        // Get all the inviaCarrPosListaList
        restInviaCarrPosListaMockMvc.perform(get("/api/invia-carr-pos-listas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inviaCarrPosLista.getId().intValue())))
            .andExpect(jsonPath("$.[*].spontaneo").value(hasItem(DEFAULT_SPONTANEO.toString())))
            .andExpect(jsonPath("$.[*].identificativoBeneficiario").value(hasItem(DEFAULT_IDENTIFICATIVO_BENEFICIARIO.toString())))
            .andExpect(jsonPath("$.[*].ccp").value(hasItem(DEFAULT_CCP.toString())))
            .andExpect(jsonPath("$.[*].codicePa").value(hasItem(DEFAULT_CODICE_PA.toString())))
            .andExpect(jsonPath("$.[*].codiceServizio").value(hasItem(DEFAULT_CODICE_SERVIZIO.toString())))
            .andExpect(jsonPath("$.[*].tipoIdPagatore").value(hasItem(DEFAULT_TIPO_ID_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].identificativoPagatore").value(hasItem(DEFAULT_IDENTIFICATIVO_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].anagraficaPagatore").value(hasItem(DEFAULT_ANAGRAFICA_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].indirizzoPagatore").value(hasItem(DEFAULT_INDIRIZZO_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].civicoPagatore").value(hasItem(DEFAULT_CIVICO_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].capPagatore").value(hasItem(DEFAULT_CAP_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].localitaPagatore").value(hasItem(DEFAULT_LOCALITA_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].provinciaPagatore").value(hasItem(DEFAULT_PROVINCIA_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].codiceNazionePagatore").value(hasItem(DEFAULT_CODICE_NAZIONE_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].emailPagatore").value(hasItem(DEFAULT_EMAIL_PAGATORE.toString())))
            .andExpect(jsonPath("$.[*].dataScadenzaPagamento").value(hasItem(DEFAULT_DATA_SCADENZA_PAGAMENTO.toString())))
            .andExpect(jsonPath("$.[*].importoPagamento").value(hasItem(DEFAULT_IMPORTO_PAGAMENTO.doubleValue())))
            .andExpect(jsonPath("$.[*].tipoFirmaRicevuta").value(hasItem(DEFAULT_TIPO_FIRMA_RICEVUTA.toString())))
            .andExpect(jsonPath("$.[*].tipoRiferimentoCreditore").value(hasItem(DEFAULT_TIPO_RIFERIMENTO_CREDITORE.toString())))
            .andExpect(jsonPath("$.[*].codiceRiferimentoCreditore").value(hasItem(DEFAULT_CODICE_RIFERIMENTO_CREDITORE.toString())))
            .andExpect(jsonPath("$.[*].identificativoUnivocoVersamento").value(hasItem(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO.toString())));
    }

    @Test
    @Transactional
    public void getInviaCarrPosLista() throws Exception {
        // Initialize the database
        inviaCarrPosListaRepository.saveAndFlush(inviaCarrPosLista);

        // Get the inviaCarrPosLista
        restInviaCarrPosListaMockMvc.perform(get("/api/invia-carr-pos-listas/{id}", inviaCarrPosLista.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inviaCarrPosLista.getId().intValue()))
            .andExpect(jsonPath("$.spontaneo").value(DEFAULT_SPONTANEO.toString()))
            .andExpect(jsonPath("$.identificativoBeneficiario").value(DEFAULT_IDENTIFICATIVO_BENEFICIARIO.toString()))
            .andExpect(jsonPath("$.ccp").value(DEFAULT_CCP.toString()))
            .andExpect(jsonPath("$.codicePa").value(DEFAULT_CODICE_PA.toString()))
            .andExpect(jsonPath("$.codiceServizio").value(DEFAULT_CODICE_SERVIZIO.toString()))
            .andExpect(jsonPath("$.tipoIdPagatore").value(DEFAULT_TIPO_ID_PAGATORE.toString()))
            .andExpect(jsonPath("$.identificativoPagatore").value(DEFAULT_IDENTIFICATIVO_PAGATORE.toString()))
            .andExpect(jsonPath("$.anagraficaPagatore").value(DEFAULT_ANAGRAFICA_PAGATORE.toString()))
            .andExpect(jsonPath("$.indirizzoPagatore").value(DEFAULT_INDIRIZZO_PAGATORE.toString()))
            .andExpect(jsonPath("$.civicoPagatore").value(DEFAULT_CIVICO_PAGATORE.toString()))
            .andExpect(jsonPath("$.capPagatore").value(DEFAULT_CAP_PAGATORE.toString()))
            .andExpect(jsonPath("$.localitaPagatore").value(DEFAULT_LOCALITA_PAGATORE.toString()))
            .andExpect(jsonPath("$.provinciaPagatore").value(DEFAULT_PROVINCIA_PAGATORE.toString()))
            .andExpect(jsonPath("$.codiceNazionePagatore").value(DEFAULT_CODICE_NAZIONE_PAGATORE.toString()))
            .andExpect(jsonPath("$.emailPagatore").value(DEFAULT_EMAIL_PAGATORE.toString()))
            .andExpect(jsonPath("$.dataScadenzaPagamento").value(DEFAULT_DATA_SCADENZA_PAGAMENTO.toString()))
            .andExpect(jsonPath("$.importoPagamento").value(DEFAULT_IMPORTO_PAGAMENTO.doubleValue()))
            .andExpect(jsonPath("$.tipoFirmaRicevuta").value(DEFAULT_TIPO_FIRMA_RICEVUTA.toString()))
            .andExpect(jsonPath("$.tipoRiferimentoCreditore").value(DEFAULT_TIPO_RIFERIMENTO_CREDITORE.toString()))
            .andExpect(jsonPath("$.codiceRiferimentoCreditore").value(DEFAULT_CODICE_RIFERIMENTO_CREDITORE.toString()))
            .andExpect(jsonPath("$.identificativoUnivocoVersamento").value(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInviaCarrPosLista() throws Exception {
        // Get the inviaCarrPosLista
        restInviaCarrPosListaMockMvc.perform(get("/api/invia-carr-pos-listas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInviaCarrPosLista() throws Exception {
        // Initialize the database
        inviaCarrPosListaRepository.saveAndFlush(inviaCarrPosLista);
        int databaseSizeBeforeUpdate = inviaCarrPosListaRepository.findAll().size();

        // Update the inviaCarrPosLista
        InviaCarrPosLista updatedInviaCarrPosLista = inviaCarrPosListaRepository.findOne(inviaCarrPosLista.getId());
        updatedInviaCarrPosLista
                .spontaneo(UPDATED_SPONTANEO)
                .identificativoBeneficiario(UPDATED_IDENTIFICATIVO_BENEFICIARIO)
                .ccp(UPDATED_CCP)
                .codicePa(UPDATED_CODICE_PA)
                .codiceServizio(UPDATED_CODICE_SERVIZIO)
                .tipoIdPagatore(UPDATED_TIPO_ID_PAGATORE)
                .identificativoPagatore(UPDATED_IDENTIFICATIVO_PAGATORE)
                .anagraficaPagatore(UPDATED_ANAGRAFICA_PAGATORE)
                .indirizzoPagatore(UPDATED_INDIRIZZO_PAGATORE)
                .civicoPagatore(UPDATED_CIVICO_PAGATORE)
                .capPagatore(UPDATED_CAP_PAGATORE)
                .localitaPagatore(UPDATED_LOCALITA_PAGATORE)
                .provinciaPagatore(UPDATED_PROVINCIA_PAGATORE)
                .codiceNazionePagatore(UPDATED_CODICE_NAZIONE_PAGATORE)
                .emailPagatore(UPDATED_EMAIL_PAGATORE)
                .dataScadenzaPagamento(UPDATED_DATA_SCADENZA_PAGAMENTO)
                .importoPagamento(UPDATED_IMPORTO_PAGAMENTO)
                .tipoFirmaRicevuta(UPDATED_TIPO_FIRMA_RICEVUTA)
                .tipoRiferimentoCreditore(UPDATED_TIPO_RIFERIMENTO_CREDITORE)
                .codiceRiferimentoCreditore(UPDATED_CODICE_RIFERIMENTO_CREDITORE)
                .identificativoUnivocoVersamento(UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);

        restInviaCarrPosListaMockMvc.perform(put("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInviaCarrPosLista)))
            .andExpect(status().isOk());

        // Validate the InviaCarrPosLista in the database
        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeUpdate);
        InviaCarrPosLista testInviaCarrPosLista = inviaCarrPosListaList.get(inviaCarrPosListaList.size() - 1);
        assertThat(testInviaCarrPosLista.getSpontaneo()).isEqualTo(UPDATED_SPONTANEO);
        assertThat(testInviaCarrPosLista.getIdentificativoBeneficiario()).isEqualTo(UPDATED_IDENTIFICATIVO_BENEFICIARIO);
        assertThat(testInviaCarrPosLista.getCcp()).isEqualTo(UPDATED_CCP);
        assertThat(testInviaCarrPosLista.getCodicePa()).isEqualTo(UPDATED_CODICE_PA);
        assertThat(testInviaCarrPosLista.getCodiceServizio()).isEqualTo(UPDATED_CODICE_SERVIZIO);
        assertThat(testInviaCarrPosLista.getTipoIdPagatore()).isEqualTo(UPDATED_TIPO_ID_PAGATORE);
        assertThat(testInviaCarrPosLista.getIdentificativoPagatore()).isEqualTo(UPDATED_IDENTIFICATIVO_PAGATORE);
        assertThat(testInviaCarrPosLista.getAnagraficaPagatore()).isEqualTo(UPDATED_ANAGRAFICA_PAGATORE);
        assertThat(testInviaCarrPosLista.getIndirizzoPagatore()).isEqualTo(UPDATED_INDIRIZZO_PAGATORE);
        assertThat(testInviaCarrPosLista.getCivicoPagatore()).isEqualTo(UPDATED_CIVICO_PAGATORE);
        assertThat(testInviaCarrPosLista.getCapPagatore()).isEqualTo(UPDATED_CAP_PAGATORE);
        assertThat(testInviaCarrPosLista.getLocalitaPagatore()).isEqualTo(UPDATED_LOCALITA_PAGATORE);
        assertThat(testInviaCarrPosLista.getProvinciaPagatore()).isEqualTo(UPDATED_PROVINCIA_PAGATORE);
        assertThat(testInviaCarrPosLista.getCodiceNazionePagatore()).isEqualTo(UPDATED_CODICE_NAZIONE_PAGATORE);
        assertThat(testInviaCarrPosLista.getEmailPagatore()).isEqualTo(UPDATED_EMAIL_PAGATORE);
        assertThat(testInviaCarrPosLista.getDataScadenzaPagamento()).isEqualTo(UPDATED_DATA_SCADENZA_PAGAMENTO);
        assertThat(testInviaCarrPosLista.getImportoPagamento()).isEqualTo(UPDATED_IMPORTO_PAGAMENTO);
        assertThat(testInviaCarrPosLista.getTipoFirmaRicevuta()).isEqualTo(UPDATED_TIPO_FIRMA_RICEVUTA);
        assertThat(testInviaCarrPosLista.getTipoRiferimentoCreditore()).isEqualTo(UPDATED_TIPO_RIFERIMENTO_CREDITORE);
        assertThat(testInviaCarrPosLista.getCodiceRiferimentoCreditore()).isEqualTo(UPDATED_CODICE_RIFERIMENTO_CREDITORE);
        assertThat(testInviaCarrPosLista.getIdentificativoUnivocoVersamento()).isEqualTo(UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
    }

    @Test
    @Transactional
    public void updateNonExistingInviaCarrPosLista() throws Exception {
        int databaseSizeBeforeUpdate = inviaCarrPosListaRepository.findAll().size();

        // Create the InviaCarrPosLista

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInviaCarrPosListaMockMvc.perform(put("/api/invia-carr-pos-listas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosLista)))
            .andExpect(status().isCreated());

        // Validate the InviaCarrPosLista in the database
        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInviaCarrPosLista() throws Exception {
        // Initialize the database
        inviaCarrPosListaRepository.saveAndFlush(inviaCarrPosLista);
        int databaseSizeBeforeDelete = inviaCarrPosListaRepository.findAll().size();

        // Get the inviaCarrPosLista
        restInviaCarrPosListaMockMvc.perform(delete("/api/invia-carr-pos-listas/{id}", inviaCarrPosLista.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InviaCarrPosLista> inviaCarrPosListaList = inviaCarrPosListaRepository.findAll();
        assertThat(inviaCarrPosListaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
