package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.PlacementTest;
import com.consoft.xenia.repository.PlacementTestRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.consoft.xenia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlacementTestResource REST controller.
 *
 * @see PlacementTestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class PlacementTestResourceIntTest {

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_SCHEDULE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SCHEDULE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_MAX_NUM_STUD = 1;
    private static final Integer UPDATED_MAX_NUM_STUD = 2;

    @Inject
    private PlacementTestRepository placementTestRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPlacementTestMockMvc;

    private PlacementTest placementTest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlacementTestResource placementTestResource = new PlacementTestResource();
        ReflectionTestUtils.setField(placementTestResource, "placementTestRepository", placementTestRepository);
        this.restPlacementTestMockMvc = MockMvcBuilders.standaloneSetup(placementTestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlacementTest createEntity(EntityManager em) {
        PlacementTest placementTest = new PlacementTest()
                .creationDate(DEFAULT_CREATION_DATE)
                .scheduleTime(DEFAULT_SCHEDULE_TIME)
                .location(DEFAULT_LOCATION)
                .maxNumStud(DEFAULT_MAX_NUM_STUD);
        return placementTest;
    }

    @Before
    public void initTest() {
        placementTest = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlacementTest() throws Exception {
        int databaseSizeBeforeCreate = placementTestRepository.findAll().size();

        // Create the PlacementTest

        restPlacementTestMockMvc.perform(post("/api/pretests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(placementTest)))
            .andExpect(status().isCreated());

        // Validate the PlacementTest in the database
        List<PlacementTest> placementTestList = placementTestRepository.findAll();
        assertThat(placementTestList).hasSize(databaseSizeBeforeCreate + 1);
        PlacementTest testPlacementTest = placementTestList.get(placementTestList.size() - 1);
        assertThat(testPlacementTest.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testPlacementTest.getScheduleTime()).isEqualTo(DEFAULT_SCHEDULE_TIME);
        assertThat(testPlacementTest.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testPlacementTest.getMaxNumStud()).isEqualTo(DEFAULT_MAX_NUM_STUD);
    }

    @Test
    @Transactional
    public void createPlacementTestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = placementTestRepository.findAll().size();

        // Create the PlacementTest with an existing ID
        PlacementTest existingPlacementTest = new PlacementTest();
        existingPlacementTest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlacementTestMockMvc.perform(post("/api/pretests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingPlacementTest)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PlacementTest> placementTestList = placementTestRepository.findAll();
        assertThat(placementTestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlacementTests() throws Exception {
        // Initialize the database
        placementTestRepository.saveAndFlush(placementTest);

        // Get all the placementTestList
        restPlacementTestMockMvc.perform(get("/api/pretests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(placementTest.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].scheduleTime").value(hasItem(sameInstant(DEFAULT_SCHEDULE_TIME))))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].maxNumStud").value(hasItem(DEFAULT_MAX_NUM_STUD)));
    }

    @Test
    @Transactional
    public void getPlacementTest() throws Exception {
        // Initialize the database
        placementTestRepository.saveAndFlush(placementTest);

        // Get the placementTest
        restPlacementTestMockMvc.perform(get("/api/pretests/{id}", placementTest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(placementTest.getId().intValue()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.scheduleTime").value(sameInstant(DEFAULT_SCHEDULE_TIME)))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.maxNumStud").value(DEFAULT_MAX_NUM_STUD));
    }

    @Test
    @Transactional
    public void getNonExistingPlacementTest() throws Exception {
        // Get the placementTest
        restPlacementTestMockMvc.perform(get("/api/pretests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlacementTest() throws Exception {
        // Initialize the database
        placementTestRepository.saveAndFlush(placementTest);
        int databaseSizeBeforeUpdate = placementTestRepository.findAll().size();

        // Update the placementTest
        PlacementTest updatedPlacementTest = placementTestRepository.findOne(placementTest.getId());
        updatedPlacementTest
                .creationDate(UPDATED_CREATION_DATE)
                .scheduleTime(UPDATED_SCHEDULE_TIME)
                .location(UPDATED_LOCATION)
                .maxNumStud(UPDATED_MAX_NUM_STUD);

        restPlacementTestMockMvc.perform(put("/api/pretests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlacementTest)))
            .andExpect(status().isOk());

        // Validate the PlacementTest in the database
        List<PlacementTest> placementTestList = placementTestRepository.findAll();
        assertThat(placementTestList).hasSize(databaseSizeBeforeUpdate);
        PlacementTest testPlacementTest = placementTestList.get(placementTestList.size() - 1);
        assertThat(testPlacementTest.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testPlacementTest.getScheduleTime()).isEqualTo(UPDATED_SCHEDULE_TIME);
        assertThat(testPlacementTest.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testPlacementTest.getMaxNumStud()).isEqualTo(UPDATED_MAX_NUM_STUD);
    }

    @Test
    @Transactional
    public void updateNonExistingPlacementTest() throws Exception {
        int databaseSizeBeforeUpdate = placementTestRepository.findAll().size();

        // Create the PlacementTest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlacementTestMockMvc.perform(put("/api/pretests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(placementTest)))
            .andExpect(status().isCreated());

        // Validate the PlacementTest in the database
        List<PlacementTest> placementTestList = placementTestRepository.findAll();
        assertThat(placementTestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlacementTest() throws Exception {
        // Initialize the database
        placementTestRepository.saveAndFlush(placementTest);
        int databaseSizeBeforeDelete = placementTestRepository.findAll().size();

        // Get the placementTest
        restPlacementTestMockMvc.perform(delete("/api/pretests/{id}", placementTest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlacementTest> placementTestList = placementTestRepository.findAll();
        assertThat(placementTestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
