package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.StudentLevel;
import com.consoft.xenia.repository.StudentLevelRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.consoft.xenia.domain.enumeration.CourseLevel;
/**
 * Test class for the StudentLevelResource REST controller.
 *
 * @see StudentLevelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class StudentLevelResourceIntTest {

    private static final CourseLevel DEFAULT_LEVEL = CourseLevel.A1;
    private static final CourseLevel UPDATED_LEVEL = CourseLevel.A2;

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private StudentLevelRepository studentLevelRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restStudentLevelMockMvc;

    private StudentLevel studentLevel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StudentLevelResource studentLevelResource = new StudentLevelResource();
        ReflectionTestUtils.setField(studentLevelResource, "studentLevelRepository", studentLevelRepository);
        this.restStudentLevelMockMvc = MockMvcBuilders.standaloneSetup(studentLevelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StudentLevel createEntity(EntityManager em) {
        StudentLevel studentLevel = new StudentLevel()
                .level(DEFAULT_LEVEL)
                .type(DEFAULT_TYPE)
                .date(DEFAULT_DATE);
        return studentLevel;
    }

    @Before
    public void initTest() {
        studentLevel = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudentLevel() throws Exception {
        int databaseSizeBeforeCreate = studentLevelRepository.findAll().size();

        // Create the StudentLevel

        restStudentLevelMockMvc.perform(post("/api/student-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentLevel)))
            .andExpect(status().isCreated());

        // Validate the StudentLevel in the database
        List<StudentLevel> studentLevelList = studentLevelRepository.findAll();
        assertThat(studentLevelList).hasSize(databaseSizeBeforeCreate + 1);
        StudentLevel testStudentLevel = studentLevelList.get(studentLevelList.size() - 1);
        assertThat(testStudentLevel.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testStudentLevel.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testStudentLevel.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    @Transactional
    public void createStudentLevelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentLevelRepository.findAll().size();

        // Create the StudentLevel with an existing ID
        StudentLevel existingStudentLevel = new StudentLevel();
        existingStudentLevel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentLevelMockMvc.perform(post("/api/student-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingStudentLevel)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<StudentLevel> studentLevelList = studentLevelRepository.findAll();
        assertThat(studentLevelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStudentLevels() throws Exception {
        // Initialize the database
        studentLevelRepository.saveAndFlush(studentLevel);

        // Get all the studentLevelList
        restStudentLevelMockMvc.perform(get("/api/student-levels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(studentLevel.getId().intValue())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }

    @Test
    @Transactional
    public void getStudentLevel() throws Exception {
        // Initialize the database
        studentLevelRepository.saveAndFlush(studentLevel);

        // Get the studentLevel
        restStudentLevelMockMvc.perform(get("/api/student-levels/{id}", studentLevel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(studentLevel.getId().intValue()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStudentLevel() throws Exception {
        // Get the studentLevel
        restStudentLevelMockMvc.perform(get("/api/student-levels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudentLevel() throws Exception {
        // Initialize the database
        studentLevelRepository.saveAndFlush(studentLevel);
        int databaseSizeBeforeUpdate = studentLevelRepository.findAll().size();

        // Update the studentLevel
        StudentLevel updatedStudentLevel = studentLevelRepository.findOne(studentLevel.getId());
        updatedStudentLevel
                .level(UPDATED_LEVEL)
                .type(UPDATED_TYPE)
                .date(UPDATED_DATE);

        restStudentLevelMockMvc.perform(put("/api/student-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStudentLevel)))
            .andExpect(status().isOk());

        // Validate the StudentLevel in the database
        List<StudentLevel> studentLevelList = studentLevelRepository.findAll();
        assertThat(studentLevelList).hasSize(databaseSizeBeforeUpdate);
        StudentLevel testStudentLevel = studentLevelList.get(studentLevelList.size() - 1);
        assertThat(testStudentLevel.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testStudentLevel.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testStudentLevel.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingStudentLevel() throws Exception {
        int databaseSizeBeforeUpdate = studentLevelRepository.findAll().size();

        // Create the StudentLevel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStudentLevelMockMvc.perform(put("/api/student-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentLevel)))
            .andExpect(status().isCreated());

        // Validate the StudentLevel in the database
        List<StudentLevel> studentLevelList = studentLevelRepository.findAll();
        assertThat(studentLevelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStudentLevel() throws Exception {
        // Initialize the database
        studentLevelRepository.saveAndFlush(studentLevel);
        int databaseSizeBeforeDelete = studentLevelRepository.findAll().size();

        // Get the studentLevel
        restStudentLevelMockMvc.perform(delete("/api/student-levels/{id}", studentLevel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<StudentLevel> studentLevelList = studentLevelRepository.findAll();
        assertThat(studentLevelList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
