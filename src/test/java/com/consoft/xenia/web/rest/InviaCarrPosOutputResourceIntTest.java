package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.pagopa.webserviceb.paInviaCarrelloPosizioni.wsdl.InviaCarrPosOutput;
import com.consoft.xenia.repository.InviaCarrPosOutputRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InviaCarrPosOutputResource REST controller.
 *
 * @see InviaCarrPosOutputResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class InviaCarrPosOutputResourceIntTest {

    private static final String DEFAULT_ESITO = "AAAAAAAAAA";
    private static final String UPDATED_ESITO = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_ERRORE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_ERRORE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_ID_TRANSAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_ID_TRANSAZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_IUV_LISTA = "AAAAAAAAAA";
    private static final String UPDATED_IUV_LISTA = "BBBBBBBBBB";

    @Inject
    private InviaCarrPosOutputRepository inviaCarrPosOutputRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restInviaCarrPosOutputMockMvc;

    private InviaCarrPosOutput inviaCarrPosOutput;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InviaCarrPosOutputResource inviaCarrPosOutputResource = new InviaCarrPosOutputResource();
        ReflectionTestUtils.setField(inviaCarrPosOutputResource, "inviaCarrPosOutputRepository", inviaCarrPosOutputRepository);
        this.restInviaCarrPosOutputMockMvc = MockMvcBuilders.standaloneSetup(inviaCarrPosOutputResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InviaCarrPosOutput createEntity(EntityManager em) {
        InviaCarrPosOutput inviaCarrPosOutput = new InviaCarrPosOutput()
                .esito(DEFAULT_ESITO)
                .codiceErrore(DEFAULT_CODICE_ERRORE)
                .descrizione(DEFAULT_DESCRIZIONE)
                .idTransazione(DEFAULT_ID_TRANSAZIONE)
                .url(DEFAULT_URL)
                .iuvListaConcat(DEFAULT_IUV_LISTA);
        return inviaCarrPosOutput;
    }

    @Before
    public void initTest() {
        inviaCarrPosOutput = createEntity(em);
    }

    @Test
    @Transactional
    public void createInviaCarrPosOutput() throws Exception {
        int databaseSizeBeforeCreate = inviaCarrPosOutputRepository.findAll().size();

        // Create the InviaCarrPosOutput

        restInviaCarrPosOutputMockMvc.perform(post("/api/invia-carr-pos-outputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosOutput)))
            .andExpect(status().isCreated());

        // Validate the InviaCarrPosOutput in the database
        List<InviaCarrPosOutput> inviaCarrPosOutputList = inviaCarrPosOutputRepository.findAll();
        assertThat(inviaCarrPosOutputList).hasSize(databaseSizeBeforeCreate + 1);
        InviaCarrPosOutput testInviaCarrPosOutput = inviaCarrPosOutputList.get(inviaCarrPosOutputList.size() - 1);
        assertThat(testInviaCarrPosOutput.getEsito()).isEqualTo(DEFAULT_ESITO);
        assertThat(testInviaCarrPosOutput.getCodiceErrore()).isEqualTo(DEFAULT_CODICE_ERRORE);
        assertThat(testInviaCarrPosOutput.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testInviaCarrPosOutput.getIdTransazione()).isEqualTo(DEFAULT_ID_TRANSAZIONE);
        assertThat(testInviaCarrPosOutput.getUrl()).isEqualTo(DEFAULT_URL);
        //assertThat(testInviaCarrPosOutput.getIuvLista()).isEqualTo(DEFAULT_IUV_LISTA);
    }

    @Test
    @Transactional
    public void createInviaCarrPosOutputWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inviaCarrPosOutputRepository.findAll().size();

        // Create the InviaCarrPosOutput with an existing ID
        InviaCarrPosOutput existingInviaCarrPosOutput = new InviaCarrPosOutput();
        existingInviaCarrPosOutput.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInviaCarrPosOutputMockMvc.perform(post("/api/invia-carr-pos-outputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingInviaCarrPosOutput)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InviaCarrPosOutput> inviaCarrPosOutputList = inviaCarrPosOutputRepository.findAll();
        assertThat(inviaCarrPosOutputList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEsitoIsRequired() throws Exception {
        int databaseSizeBeforeTest = inviaCarrPosOutputRepository.findAll().size();
        // set the field null
        inviaCarrPosOutput.setEsito(null);

        // Create the InviaCarrPosOutput, which fails.

        restInviaCarrPosOutputMockMvc.perform(post("/api/invia-carr-pos-outputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosOutput)))
            .andExpect(status().isBadRequest());

        List<InviaCarrPosOutput> inviaCarrPosOutputList = inviaCarrPosOutputRepository.findAll();
        assertThat(inviaCarrPosOutputList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInviaCarrPosOutputs() throws Exception {
        // Initialize the database
        inviaCarrPosOutputRepository.saveAndFlush(inviaCarrPosOutput);

        // Get all the inviaCarrPosOutputList
        restInviaCarrPosOutputMockMvc.perform(get("/api/invia-carr-pos-outputs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inviaCarrPosOutput.getId().intValue())))
            .andExpect(jsonPath("$.[*].esito").value(hasItem(DEFAULT_ESITO.toString())))
            .andExpect(jsonPath("$.[*].codiceErrore").value(hasItem(DEFAULT_CODICE_ERRORE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].idTransazione").value(hasItem(DEFAULT_ID_TRANSAZIONE.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())));
            //.andExpect(jsonPath("$.[*].iuvLista").value(hasItem(DEFAULT_IUV_LISTA.toString())));
    }

    @Test
    @Transactional
    public void getInviaCarrPosOutput() throws Exception {
        // Initialize the database
        inviaCarrPosOutputRepository.saveAndFlush(inviaCarrPosOutput);

        // Get the inviaCarrPosOutput
        restInviaCarrPosOutputMockMvc.perform(get("/api/invia-carr-pos-outputs/{id}", inviaCarrPosOutput.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inviaCarrPosOutput.getId().intValue()))
            .andExpect(jsonPath("$.esito").value(DEFAULT_ESITO.toString()))
            .andExpect(jsonPath("$.codiceErrore").value(DEFAULT_CODICE_ERRORE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.idTransazione").value(DEFAULT_ID_TRANSAZIONE.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()));
            //.andExpect(jsonPath("$.iuvLista").value(DEFAULT_IUV_LISTA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInviaCarrPosOutput() throws Exception {
        // Get the inviaCarrPosOutput
        restInviaCarrPosOutputMockMvc.perform(get("/api/invia-carr-pos-outputs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInviaCarrPosOutput() throws Exception {
        // Initialize the database
        inviaCarrPosOutputRepository.saveAndFlush(inviaCarrPosOutput);
        int databaseSizeBeforeUpdate = inviaCarrPosOutputRepository.findAll().size();

        // Update the inviaCarrPosOutput
        InviaCarrPosOutput updatedInviaCarrPosOutput = inviaCarrPosOutputRepository.findOne(inviaCarrPosOutput.getId());
        updatedInviaCarrPosOutput
                .esito(UPDATED_ESITO)
                .codiceErrore(UPDATED_CODICE_ERRORE)
                .descrizione(UPDATED_DESCRIZIONE)
                .idTransazione(UPDATED_ID_TRANSAZIONE)
                .url(UPDATED_URL)
                .iuvListaConcat(UPDATED_IUV_LISTA);

        restInviaCarrPosOutputMockMvc.perform(put("/api/invia-carr-pos-outputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInviaCarrPosOutput)))
            .andExpect(status().isOk());

        // Validate the InviaCarrPosOutput in the database
        List<InviaCarrPosOutput> inviaCarrPosOutputList = inviaCarrPosOutputRepository.findAll();
        assertThat(inviaCarrPosOutputList).hasSize(databaseSizeBeforeUpdate);
        InviaCarrPosOutput testInviaCarrPosOutput = inviaCarrPosOutputList.get(inviaCarrPosOutputList.size() - 1);
        assertThat(testInviaCarrPosOutput.getEsito()).isEqualTo(UPDATED_ESITO);
        assertThat(testInviaCarrPosOutput.getCodiceErrore()).isEqualTo(UPDATED_CODICE_ERRORE);
        assertThat(testInviaCarrPosOutput.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testInviaCarrPosOutput.getIdTransazione()).isEqualTo(UPDATED_ID_TRANSAZIONE);
        assertThat(testInviaCarrPosOutput.getUrl()).isEqualTo(UPDATED_URL);
        //assertThat(testInviaCarrPosOutput.getIuvLista()).isEqualTo(UPDATED_IUV_LISTA);
    }

    @Test
    @Transactional
    public void updateNonExistingInviaCarrPosOutput() throws Exception {
        int databaseSizeBeforeUpdate = inviaCarrPosOutputRepository.findAll().size();

        // Create the InviaCarrPosOutput

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInviaCarrPosOutputMockMvc.perform(put("/api/invia-carr-pos-outputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inviaCarrPosOutput)))
            .andExpect(status().isCreated());

        // Validate the InviaCarrPosOutput in the database
        List<InviaCarrPosOutput> inviaCarrPosOutputList = inviaCarrPosOutputRepository.findAll();
        assertThat(inviaCarrPosOutputList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInviaCarrPosOutput() throws Exception {
        // Initialize the database
        inviaCarrPosOutputRepository.saveAndFlush(inviaCarrPosOutput);
        int databaseSizeBeforeDelete = inviaCarrPosOutputRepository.findAll().size();

        // Get the inviaCarrPosOutput
        restInviaCarrPosOutputMockMvc.perform(delete("/api/invia-carr-pos-outputs/{id}", inviaCarrPosOutput.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InviaCarrPosOutput> inviaCarrPosOutputList = inviaCarrPosOutputRepository.findAll();
        assertThat(inviaCarrPosOutputList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
