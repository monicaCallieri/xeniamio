package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.StudentCourse;
import com.consoft.xenia.repository.StudentCourseRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StudentCourseResource REST controller.
 *
 * @see StudentCourseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class StudentCourseResourceIntTest {

    private static final LocalDate DEFAULT_REGISTRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REGISTRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private StudentCourseRepository studentCourseRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restStudentCourseMockMvc;

    private StudentCourse studentCourse;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StudentCourseResource studentCourseResource = new StudentCourseResource();
        ReflectionTestUtils.setField(studentCourseResource, "studentCourseRepository", studentCourseRepository);
        this.restStudentCourseMockMvc = MockMvcBuilders.standaloneSetup(studentCourseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StudentCourse createEntity(EntityManager em) {
        StudentCourse studentCourse = new StudentCourse()
                .registrationDate(DEFAULT_REGISTRATION_DATE);
        return studentCourse;
    }

    @Before
    public void initTest() {
        studentCourse = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudentCourse() throws Exception {
        int databaseSizeBeforeCreate = studentCourseRepository.findAll().size();

        // Create the StudentCourse

        restStudentCourseMockMvc.perform(post("/api/student-courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentCourse)))
            .andExpect(status().isCreated());

        // Validate the StudentCourse in the database
        List<StudentCourse> studentCourseList = studentCourseRepository.findAll();
        assertThat(studentCourseList).hasSize(databaseSizeBeforeCreate + 1);
        StudentCourse testStudentCourse = studentCourseList.get(studentCourseList.size() - 1);
        assertThat(testStudentCourse.getRegistrationDate()).isEqualTo(DEFAULT_REGISTRATION_DATE);
    }

    @Test
    @Transactional
    public void createStudentCourseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentCourseRepository.findAll().size();

        // Create the StudentCourse with an existing ID
        StudentCourse existingStudentCourse = new StudentCourse();
        existingStudentCourse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentCourseMockMvc.perform(post("/api/student-courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingStudentCourse)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<StudentCourse> studentCourseList = studentCourseRepository.findAll();
        assertThat(studentCourseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStudentCourses() throws Exception {
        // Initialize the database
        studentCourseRepository.saveAndFlush(studentCourse);

        // Get all the studentCourseList
        restStudentCourseMockMvc.perform(get("/api/student-courses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(studentCourse.getId().intValue())))
            .andExpect(jsonPath("$.[*].registrationDate").value(hasItem(DEFAULT_REGISTRATION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getStudentCourse() throws Exception {
        // Initialize the database
        studentCourseRepository.saveAndFlush(studentCourse);

        // Get the studentCourse
        restStudentCourseMockMvc.perform(get("/api/student-courses/{id}", studentCourse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(studentCourse.getId().intValue()))
            .andExpect(jsonPath("$.registrationDate").value(DEFAULT_REGISTRATION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStudentCourse() throws Exception {
        // Get the studentCourse
        restStudentCourseMockMvc.perform(get("/api/student-courses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudentCourse() throws Exception {
        // Initialize the database
        studentCourseRepository.saveAndFlush(studentCourse);
        int databaseSizeBeforeUpdate = studentCourseRepository.findAll().size();

        // Update the studentCourse
        StudentCourse updatedStudentCourse = studentCourseRepository.findOne(studentCourse.getId());
        updatedStudentCourse
                .registrationDate(UPDATED_REGISTRATION_DATE);

        restStudentCourseMockMvc.perform(put("/api/student-courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStudentCourse)))
            .andExpect(status().isOk());

        // Validate the StudentCourse in the database
        List<StudentCourse> studentCourseList = studentCourseRepository.findAll();
        assertThat(studentCourseList).hasSize(databaseSizeBeforeUpdate);
        StudentCourse testStudentCourse = studentCourseList.get(studentCourseList.size() - 1);
        assertThat(testStudentCourse.getRegistrationDate()).isEqualTo(UPDATED_REGISTRATION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingStudentCourse() throws Exception {
        int databaseSizeBeforeUpdate = studentCourseRepository.findAll().size();

        // Create the StudentCourse

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStudentCourseMockMvc.perform(put("/api/student-courses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentCourse)))
            .andExpect(status().isCreated());

        // Validate the StudentCourse in the database
        List<StudentCourse> studentCourseList = studentCourseRepository.findAll();
        assertThat(studentCourseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStudentCourse() throws Exception {
        // Initialize the database
        studentCourseRepository.saveAndFlush(studentCourse);
        int databaseSizeBeforeDelete = studentCourseRepository.findAll().size();

        // Get the studentCourse
        restStudentCourseMockMvc.perform(delete("/api/student-courses/{id}", studentCourse.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<StudentCourse> studentCourseList = studentCourseRepository.findAll();
        assertThat(studentCourseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
