package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;
import com.consoft.xenia.domain.Authority;
import com.consoft.xenia.domain.Question;
import com.consoft.xenia.domain.Response;
import com.consoft.xenia.domain.User;

import com.consoft.xenia.domain.UserInformation;
import com.consoft.xenia.repository.QuestionRepository;
import com.consoft.xenia.repository.ResponseRepository;
import com.consoft.xenia.repository.UserInformationRepository;
import com.consoft.xenia.repository.UserRepository;
import com.consoft.xenia.security.AuthoritiesConstants;
import com.consoft.xenia.service.UserService;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManagerFactory;
import org.apache.commons.lang3.RandomStringUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserInformationResource REST controller.
 *
 * @see UserInformationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class UserInformationResourceIntTest {
    
    private final Logger log = LoggerFactory.getLogger(UserInformationResourceIntTest.class);

    @Inject
    private UserInformationRepository userInformationRepository;
    
    /*@Autowired
    private UserService userService;
    
    @Mock
    private UserService mockUserService;*/
    
    @Inject
    private QuestionRepository questionRepository;
    
    @Inject
    private ResponseRepository responseRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager euim;
    
    @Inject
    private EntityManager eum;

    private MockMvc restUserInformationMockMvc;
    private MockMvc restUserInformationUserMockMvc;

    private UserInformation userInformation;
    private User user;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserInformationResource userInformationResource = new UserInformationResource();
        //UserResource userInformationUserResource = new UserResource();
        ReflectionTestUtils.setField(userInformationResource, "userInformationRepository", userInformationRepository);
        //ReflectionTestUtils.setField(userInformationUserResource, "userService", mockUserService);
        this.restUserInformationMockMvc = MockMvcBuilders.standaloneSetup(userInformationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
        /*this.restUserInformationUserMockMvc = MockMvcBuilders.standaloneSetup(userInformationUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();*/
    }
    
    
     /**
     * Create a User.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the User
     * entity.
     */
    /*public static User createUserEntity(EntityManager eum) {
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.STUDENT);
        authorities.add(authority);
        User user = new User();
        user.setLogin("john.doe@jhipter.com");
        user.setFirstName("john");
        user.setLastName("doe");
        user.setEmail("john.doe@jhipter.com");
        user.setAuthorities(authorities);
        return user;
    }*/

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserInformation createUserInformationEntity(EntityManager euim) {
        UserInformation userInformation = new UserInformation();
        return userInformation;
    }

    @Before
    public void initTest() {
        userInformation = createUserInformationEntity(euim);
        //user = createUserEntity(eum);
    }

    @Test
    @Transactional
    public void createUserInformation() throws Exception {
        int databaseSizeBeforeCreate = userInformationRepository.findAll().size();

        // Create the UserInformation

        restUserInformationMockMvc.perform(post("/api/user-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userInformation)))
            .andExpect(status().isCreated());

        // Validate the UserInformation in the database
        List<UserInformation> userInformationList = userInformationRepository.findAll();
        assertThat(userInformationList).hasSize(databaseSizeBeforeCreate + 1);
        UserInformation testUserInformation = userInformationList.get(userInformationList.size() - 1);
    }

    @Test
    @Transactional
    public void createUserInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userInformationRepository.findAll().size();

        // Create the UserInformation with an existing ID
        UserInformation existingUserInformation = new UserInformation();
        existingUserInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserInformationMockMvc.perform(post("/api/user-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingUserInformation)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserInformation> userInformationList = userInformationRepository.findAll();
        assertThat(userInformationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserInformations() throws Exception {
        // Initialize the database
        userInformationRepository.saveAndFlush(userInformation);

        // Get all the userInformationList
        restUserInformationMockMvc.perform(get("/api/user-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userInformation.getId().intValue())));
    }

    @Test
    @Transactional
    public void getUserInformation() throws Exception {
        // Initialize the database
        userInformationRepository.saveAndFlush(userInformation);

        // Get the userInformation
        restUserInformationMockMvc.perform(get("/api/user-informations/{id}", userInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userInformation.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingUserInformation() throws Exception {
        // Get the userInformation
        restUserInformationMockMvc.perform(get("/api/user-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserInformation() throws Exception {
        // Initialize the database
        userInformationRepository.saveAndFlush(userInformation);
        int databaseSizeBeforeUpdate = userInformationRepository.findAll().size();

        // Update the userInformation
        UserInformation updatedUserInformation = userInformationRepository.findOne(userInformation.getId());

        restUserInformationMockMvc.perform(put("/api/user-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserInformation)))
            .andExpect(status().isOk());

        // Validate the UserInformation in the database
        List<UserInformation> userInformationList = userInformationRepository.findAll();
        assertThat(userInformationList).hasSize(databaseSizeBeforeUpdate);
        UserInformation testUserInformation = userInformationList.get(userInformationList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingUserInformation() throws Exception {
        int databaseSizeBeforeUpdate = userInformationRepository.findAll().size();

        // Create the UserInformation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserInformationMockMvc.perform(put("/api/user-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userInformation)))
            .andExpect(status().isCreated());

        // Validate the UserInformation in the database
        List<UserInformation> userInformationList = userInformationRepository.findAll();
        assertThat(userInformationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserInformation() throws Exception {
        // Initialize the database
        userInformationRepository.saveAndFlush(userInformation);
        int databaseSizeBeforeDelete = userInformationRepository.findAll().size();

        // Get the userInformation
        restUserInformationMockMvc.perform(delete("/api/user-informations/{id}", userInformation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserInformation> userInformationList = userInformationRepository.findAll();
        assertThat(userInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    /*@Test
    @Transactional
    public void getUserInformationCorrect() throws Exception {
        restUserInformationUserMockMvc.perform(post("/api/users/john.doe@jhipter.com.com"));
        List<Question> questions = questionRepository.findAll();
        log.debug("Domanda 0: "+questions.get(0).getQuestion());
        List<Response> responses = responseRepository.findAll();
        log.debug("Risposta 0: "+responses.get(0).getResponse());
                when(mockUserService.getUserWithAuthorities()).thenReturn(user);
        UserInformation userInformation = new UserInformation()
                .question(questions.get(0))
                .response(responses.get(0))
                .user(user);
        userInformationRepository.saveAndFlush(userInformation);
        
        // Get the userInformation
        restUserInformationMockMvc.perform(get("/api/user-informations/{id}", userInformation.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(userInformation.getId().intValue()));
        log.debug("question size: " + questions.size());

        List<UserInformation> userInformationList = userInformationRepository.findByUserIsCurrentUser();
        log.debug("information size: " + userInformationList.size());
        //assertThat(userInformationList).hasSize(questions.size());
    }*/
}
