package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.StudentIntentions;
import com.consoft.xenia.domain.EducationalProposer;
import com.consoft.xenia.repository.StudentIntentionsRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.consoft.xenia.domain.enumeration.CourseLevel;
import com.consoft.xenia.domain.enumeration.State;
/**
 * Test class for the StudentIntentionsResource REST controller.
 *
 * @see StudentIntentionsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class StudentIntentionsResourceIntTest {

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final CourseLevel DEFAULT_LEVEL = CourseLevel.A1;
    private static final CourseLevel UPDATED_LEVEL = CourseLevel.A2;

    private static final LocalDate DEFAULT_REALIZATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REALIZATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final State DEFAULT_STATE = State.ENROLLED;
    private static final State UPDATED_STATE = State.NOTENROLLED;

    @Inject
    private StudentIntentionsRepository studentIntentionsRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restStudentIntentionsMockMvc;

    private StudentIntentions studentIntentions;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StudentIntentionsResource studentIntentionsResource = new StudentIntentionsResource();
        ReflectionTestUtils.setField(studentIntentionsResource, "studentIntentionsRepository", studentIntentionsRepository);
        this.restStudentIntentionsMockMvc = MockMvcBuilders.standaloneSetup(studentIntentionsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StudentIntentions createEntity(EntityManager em) {
        StudentIntentions studentIntentions = new StudentIntentions()
                .creationDate(DEFAULT_CREATION_DATE)
                .startDate(DEFAULT_START_DATE)
                .endDate(DEFAULT_END_DATE)
                .level(DEFAULT_LEVEL)
                .realizationDate(DEFAULT_REALIZATION_DATE)
                .state(DEFAULT_STATE);
        // Add required entity
        EducationalProposer educationalProposer = EducationalProposerResourceIntTest.createEntity(em);
        em.persist(educationalProposer);
        em.flush();
        studentIntentions.setEducationalProposer(educationalProposer);
        return studentIntentions;
    }

    @Before
    public void initTest() {
        studentIntentions = createEntity(em);
    }

    @Test
    @Transactional
    public void createStudentIntentions() throws Exception {
        int databaseSizeBeforeCreate = studentIntentionsRepository.findAll().size();

        // Create the StudentIntentions

        restStudentIntentionsMockMvc.perform(post("/api/student-intentions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentIntentions)))
            .andExpect(status().isCreated());

        // Validate the StudentIntentions in the database
        List<StudentIntentions> studentIntentionsList = studentIntentionsRepository.findAll();
        assertThat(studentIntentionsList).hasSize(databaseSizeBeforeCreate + 1);
        StudentIntentions testStudentIntentions = studentIntentionsList.get(studentIntentionsList.size() - 1);
        assertThat(testStudentIntentions.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testStudentIntentions.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testStudentIntentions.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testStudentIntentions.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testStudentIntentions.getRealizationDate()).isEqualTo(DEFAULT_REALIZATION_DATE);
        assertThat(testStudentIntentions.getState()).isEqualTo(DEFAULT_STATE);
    }

    @Test
    @Transactional
    public void createStudentIntentionsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentIntentionsRepository.findAll().size();

        // Create the StudentIntentions with an existing ID
        StudentIntentions existingStudentIntentions = new StudentIntentions();
        existingStudentIntentions.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentIntentionsMockMvc.perform(post("/api/student-intentions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingStudentIntentions)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<StudentIntentions> studentIntentionsList = studentIntentionsRepository.findAll();
        assertThat(studentIntentionsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = studentIntentionsRepository.findAll().size();
        // set the field null
        studentIntentions.setState(null);

        // Create the StudentIntentions, which fails.

        restStudentIntentionsMockMvc.perform(post("/api/student-intentions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentIntentions)))
            .andExpect(status().isBadRequest());

        List<StudentIntentions> studentIntentionsList = studentIntentionsRepository.findAll();
        assertThat(studentIntentionsList).hasSize(databaseSizeBeforeTest);
    }

    //@Test
    @Transactional
    public void getAllStudentIntentions() throws Exception {
        // Initialize the database
        studentIntentionsRepository.saveAndFlush(studentIntentions);

        // Get all the studentIntentionsList
        restStudentIntentionsMockMvc.perform(get("/api/student-intentions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(studentIntentions.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL.toString())))
            .andExpect(jsonPath("$.[*].realizationDate").value(hasItem(DEFAULT_REALIZATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())));
    }

    @Test
    @Transactional
    public void getStudentIntentions() throws Exception {
        // Initialize the database
        studentIntentionsRepository.saveAndFlush(studentIntentions);

        // Get the studentIntentions
        restStudentIntentionsMockMvc.perform(get("/api/student-intentions/{id}", studentIntentions.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(studentIntentions.getId().intValue()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL.toString()))
            .andExpect(jsonPath("$.realizationDate").value(DEFAULT_REALIZATION_DATE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStudentIntentions() throws Exception {
        // Get the studentIntentions
        restStudentIntentionsMockMvc.perform(get("/api/student-intentions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudentIntentions() throws Exception {
        // Initialize the database
        studentIntentionsRepository.saveAndFlush(studentIntentions);
        int databaseSizeBeforeUpdate = studentIntentionsRepository.findAll().size();

        // Update the studentIntentions
        StudentIntentions updatedStudentIntentions = studentIntentionsRepository.findOne(studentIntentions.getId());
        updatedStudentIntentions
                .creationDate(UPDATED_CREATION_DATE)
                .startDate(UPDATED_START_DATE)
                .endDate(UPDATED_END_DATE)
                .level(UPDATED_LEVEL)
                .realizationDate(UPDATED_REALIZATION_DATE)
                .state(UPDATED_STATE);

        restStudentIntentionsMockMvc.perform(put("/api/student-intentions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStudentIntentions)))
            .andExpect(status().isOk());

        // Validate the StudentIntentions in the database
        List<StudentIntentions> studentIntentionsList = studentIntentionsRepository.findAll();
        assertThat(studentIntentionsList).hasSize(databaseSizeBeforeUpdate);
        StudentIntentions testStudentIntentions = studentIntentionsList.get(studentIntentionsList.size() - 1);
        assertThat(testStudentIntentions.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testStudentIntentions.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testStudentIntentions.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testStudentIntentions.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testStudentIntentions.getRealizationDate()).isEqualTo(UPDATED_REALIZATION_DATE);
        assertThat(testStudentIntentions.getState()).isEqualTo(UPDATED_STATE);
    }

    @Test
    @Transactional
    public void updateNonExistingStudentIntentions() throws Exception {
        int databaseSizeBeforeUpdate = studentIntentionsRepository.findAll().size();

        // Create the StudentIntentions

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStudentIntentionsMockMvc.perform(put("/api/student-intentions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(studentIntentions)))
            .andExpect(status().isCreated());

        // Validate the StudentIntentions in the database
        List<StudentIntentions> studentIntentionsList = studentIntentionsRepository.findAll();
        assertThat(studentIntentionsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStudentIntentions() throws Exception {
        // Initialize the database
        studentIntentionsRepository.saveAndFlush(studentIntentions);
        int databaseSizeBeforeDelete = studentIntentionsRepository.findAll().size();

        // Get the studentIntentions
        restStudentIntentionsMockMvc.perform(delete("/api/student-intentions/{id}", studentIntentions.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<StudentIntentions> studentIntentionsList = studentIntentionsRepository.findAll();
        assertThat(studentIntentionsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
