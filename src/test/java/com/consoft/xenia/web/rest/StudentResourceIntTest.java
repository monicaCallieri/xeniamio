package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;
import com.consoft.xenia.domain.Authority;

import com.consoft.xenia.domain.Student;
import com.consoft.xenia.domain.User;
import com.consoft.xenia.repository.StudentRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.consoft.xenia.domain.enumeration.Sex;
import com.consoft.xenia.domain.enumeration.Qualification;
import com.consoft.xenia.domain.enumeration.Level;
import com.consoft.xenia.repository.UserRepository;
import com.consoft.xenia.security.AuthoritiesConstants;
import com.consoft.xenia.service.UserService;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManagerFactory;
import org.apache.commons.lang3.RandomStringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

/**
 * Test class for the StudentResource REST controller.
 *
 * @see StudentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class StudentResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SURNAME = "AAAAAAAAAA";
    private static final String UPDATED_SURNAME = "BBBBBBBBBB";

    private static final Sex DEFAULT_SEX = Sex.MALE;
    private static final Sex UPDATED_SEX = Sex.FEMALE;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CITY_OF_BIRTH = "AAAAAAAAAA";
    private static final String UPDATED_CITY_OF_BIRTH = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE_OR_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_STATE_OR_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_PROFESSION = "AAAAAAAAAA";
    private static final String UPDATED_PROFESSION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BIRTH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_BIRTH = LocalDate.now(ZoneId.systemDefault());

    private static final Qualification DEFAULT_QUALIFICATION = Qualification.MASTEROFSCIENCE;
    private static final Qualification UPDATED_QUALIFICATION = Qualification.HIGHSCHOOL;

    private static final Level DEFAULT_KNOWLEDGE_OF_ITALIAN = Level.SI;
    private static final Level UPDATED_KNOWLEDGE_OF_ITALIAN = Level.NO;

    private static final Boolean DEFAULT_ALREADY_STUDENT = false;
    private static final Boolean UPDATED_ALREADY_STUDENT = true;

    private static final Integer DEFAULT_PREVIOUS_ATTEND_YEAR = 1;
    private static final Integer UPDATED_PREVIOUS_ATTEND_YEAR = 2;

    private static final Boolean DEFAULT_SCHOLARSHIP = false;
    private static final Boolean UPDATED_SCHOLARSHIP = true;

    private static final LocalDate DEFAULT_SCHOLARSHIP_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SCHOLARSHIP_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_SCHOLARSHIP_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SCHOLARSHIP_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_SCHOLARSHIP_AWARDED_BY = "AAAAAAAAAA";
    private static final String UPDATED_SCHOLARSHIP_AWARDED_BY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_PRIVACY_AUTHORIZE = false;
    private static final Boolean UPDATED_PRIVACY_AUTHORIZE = true;

    private static final LocalDate DEFAULT_REGISTRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REGISTRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_STUDENT_NUMBER = 1;
    private static final Integer UPDATED_STUDENT_NUMBER = 2;

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_MOTHER_TONGUE = "AAAAAAAAAA";
    private static final String UPDATED_MOTHER_TONGUE = "BBBBBBBBBB";

    private static final String DEFAULT_OTHER_LANGUAGES = "AAAAAAAAAA";
    private static final String UPDATED_OTHER_LANGUAGES = "BBBBBBBBBB";

    private static final String DEFAULT_ITALIAN_STUDIED_IN = "AAAAAAAAAA";
    private static final String UPDATED_ITALIAN_STUDIED_IN = "BBBBBBBBBB";

    private static final String DEFAULT_ITALIAN_STUDIED_FOR = "AAAAAAAAAA";
    private static final String UPDATED_ITALIAN_STUDIED_FOR = "BBBBBBBBBB";

    private static final String DEFAULT_ITALIAN_STUDIED_HOUR_WEEK = "AAAAAAAAAA";
    private static final String UPDATED_ITALIAN_STUDIED_HOUR_WEEK = "BBBBBBBBBB";

    private static final String DEFAULT_ITALIAN_CERTIFICATION = "AAAAAAAAAA";
    private static final String UPDATED_ITALIAN_CERTIFICATION = "BBBBBBBBBB";

    private static final String DEFAULT_ITALIAN_LEVEL = "AAAAAAAAAA";
    private static final String UPDATED_ITALIAN_LEVEL = "BBBBBBBBBB";

    private static final Integer DEFAULT_HOW_LONG_STUDENT_AT_UNISTRAPG = 1;
    private static final Integer UPDATED_HOW_LONG_STUDENT_AT_UNISTRAPG = 2;

    private static final Boolean DEFAULT_ACCOMODATION = false;
    private static final Boolean UPDATED_ACCOMODATION = true;

    private static final String DEFAULT_LEARNING_DISABILITY = "AAAAAAAAAA";
    private static final String UPDATED_LEARNING_DISABILITY = "BBBBBBBBBB";

    private static final String DEFAULT_PHYSICAL_DISABILITY = "AAAAAAAAAA";
    private static final String UPDATED_PHYSICAL_DISABILITY = "BBBBBBBBBB";

    private static final String DEFAULT_OTHER_SPECIAL_REQUESTS = "AAAAAAAAAA";
    private static final String UPDATED_OTHER_SPECIAL_REQUESTS = "BBBBBBBBBB";

    @Inject
    private StudentRepository studentRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restStudentMockMvc;

    @Mock
    private UserService mockUserService;
    
    private Student student;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StudentResource studentResource = new StudentResource();
        ReflectionTestUtils.setField(studentResource, "studentRepository", studentRepository);
        //ReflectionTestUtils.setField(studentResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(studentResource, "userService", mockUserService);
        this.restStudentMockMvc = MockMvcBuilders.standaloneSetup(studentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Student createStudentEntity(EntityManager em) {
        Student student = new Student()
                .name(DEFAULT_NAME)
                .surname(DEFAULT_SURNAME)
                .sex(DEFAULT_SEX)
                .email(DEFAULT_EMAIL)
                .phoneNumber(DEFAULT_PHONE_NUMBER)
                .cityOfBirth(DEFAULT_CITY_OF_BIRTH)
                .nationality(DEFAULT_NATIONALITY)
                .address(DEFAULT_ADDRESS)
                .country(DEFAULT_COUNTRY)
                .stateOrProvince(DEFAULT_STATE_OR_PROVINCE)
                .city(DEFAULT_CITY)
                .profession(DEFAULT_PROFESSION)
                .dateOfBirth(DEFAULT_DATE_OF_BIRTH)
                .qualification(DEFAULT_QUALIFICATION)
                .knowledgeOfItalian(DEFAULT_KNOWLEDGE_OF_ITALIAN)
                .alreadyStudent(DEFAULT_ALREADY_STUDENT)
                .previousAttendYear(DEFAULT_PREVIOUS_ATTEND_YEAR)
                .scholarship(DEFAULT_SCHOLARSHIP)
                .scholarshipStartDate(DEFAULT_SCHOLARSHIP_START_DATE)
                .scholarshipEndDate(DEFAULT_SCHOLARSHIP_END_DATE)
                .scholarshipAwardedBy(DEFAULT_SCHOLARSHIP_AWARDED_BY)
                .privacyAuthorize(DEFAULT_PRIVACY_AUTHORIZE)
                .registrationDate(DEFAULT_REGISTRATION_DATE)
                .studentNumber(DEFAULT_STUDENT_NUMBER)
                .postal_code(DEFAULT_POSTAL_CODE)
                .mobile_phone(DEFAULT_MOBILE_PHONE)
                .mother_tongue(DEFAULT_MOTHER_TONGUE)
                .other_languages(DEFAULT_OTHER_LANGUAGES)
                .italian_studied_in(DEFAULT_ITALIAN_STUDIED_IN)
                .italian_studied_for(DEFAULT_ITALIAN_STUDIED_FOR)
                .italian_studied_hour_week(DEFAULT_ITALIAN_STUDIED_HOUR_WEEK)
                .italian_certification(DEFAULT_ITALIAN_CERTIFICATION)
                .italian_level(DEFAULT_ITALIAN_LEVEL)
                .how_long_student_at_unistrapg(DEFAULT_HOW_LONG_STUDENT_AT_UNISTRAPG)
                .accomodation(DEFAULT_ACCOMODATION)
                .learning_disability(DEFAULT_LEARNING_DISABILITY)
                .physical_disability(DEFAULT_PHYSICAL_DISABILITY)
                .other_special_requests(DEFAULT_OTHER_SPECIAL_REQUESTS);
        return student;
    }

    @Before
    public void initTest() {
        student = createStudentEntity(em);
    }

    @Test
    @Transactional
    public void createStudent() throws Exception {
        int databaseSizeBeforeCreate = studentRepository.findAll().size();

        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.STUDENT);
        authorities.add(authority);
        User user = new User();
        user.setLogin("test");
        user.setFirstName("john");
        user.setLastName("doe");
        user.setEmail("john.doe@jhipter.com");
        user.setAuthorities(authorities);
        when(mockUserService.getUserWithAuthorities()).thenReturn(user);
        
        
        // Create the Student

        restStudentMockMvc.perform(post("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(student)))
            .andExpect(status().isCreated());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeCreate + 1);
        Student testStudent = studentList.get(studentList.size() - 1);
        assertThat(testStudent.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testStudent.getSurname()).isEqualTo(DEFAULT_SURNAME);
        assertThat(testStudent.getSex()).isEqualTo(DEFAULT_SEX);
        assertThat(testStudent.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testStudent.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testStudent.getCityOfBirth()).isEqualTo(DEFAULT_CITY_OF_BIRTH);
        assertThat(testStudent.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testStudent.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testStudent.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testStudent.getStateOrProvince()).isEqualTo(DEFAULT_STATE_OR_PROVINCE);
        assertThat(testStudent.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testStudent.getProfession()).isEqualTo(DEFAULT_PROFESSION);
        assertThat(testStudent.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testStudent.getQualification()).isEqualTo(DEFAULT_QUALIFICATION);
        assertThat(testStudent.getKnowledgeOfItalian()).isEqualTo(DEFAULT_KNOWLEDGE_OF_ITALIAN);
        assertThat(testStudent.isAlreadyStudent()).isEqualTo(DEFAULT_ALREADY_STUDENT);
        assertThat(testStudent.getPreviousAttendYear()).isEqualTo(DEFAULT_PREVIOUS_ATTEND_YEAR);
        assertThat(testStudent.isScholarship()).isEqualTo(DEFAULT_SCHOLARSHIP);
        assertThat(testStudent.getScholarshipStartDate()).isEqualTo(DEFAULT_SCHOLARSHIP_START_DATE);
        assertThat(testStudent.getScholarshipEndDate()).isEqualTo(DEFAULT_SCHOLARSHIP_END_DATE);
        assertThat(testStudent.getScholarshipAwardedBy()).isEqualTo(DEFAULT_SCHOLARSHIP_AWARDED_BY);
        assertThat(testStudent.isPrivacyAuthorize()).isEqualTo(DEFAULT_PRIVACY_AUTHORIZE);
        assertThat(testStudent.getRegistrationDate()).isEqualTo(DEFAULT_REGISTRATION_DATE);
        assertThat(testStudent.getStudentNumber()).isEqualTo(DEFAULT_STUDENT_NUMBER);
        assertThat(testStudent.getPostal_code()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testStudent.getMobile_phone()).isEqualTo(DEFAULT_MOBILE_PHONE);
        assertThat(testStudent.getMother_tongue()).isEqualTo(DEFAULT_MOTHER_TONGUE);
        assertThat(testStudent.getOther_languages()).isEqualTo(DEFAULT_OTHER_LANGUAGES);
        assertThat(testStudent.getItalian_studied_in()).isEqualTo(DEFAULT_ITALIAN_STUDIED_IN);
        assertThat(testStudent.getItalian_studied_for()).isEqualTo(DEFAULT_ITALIAN_STUDIED_FOR);
        assertThat(testStudent.getItalian_studied_hour_week()).isEqualTo(DEFAULT_ITALIAN_STUDIED_HOUR_WEEK);
        assertThat(testStudent.getItalian_certification()).isEqualTo(DEFAULT_ITALIAN_CERTIFICATION);
        assertThat(testStudent.getItalian_level()).isEqualTo(DEFAULT_ITALIAN_LEVEL);
        assertThat(testStudent.getHow_long_student_at_unistrapg()).isEqualTo(DEFAULT_HOW_LONG_STUDENT_AT_UNISTRAPG);
        assertThat(testStudent.isAccomodation()).isEqualTo(DEFAULT_ACCOMODATION);
        assertThat(testStudent.getLearning_disability()).isEqualTo(DEFAULT_LEARNING_DISABILITY);
        assertThat(testStudent.getPhysical_disability()).isEqualTo(DEFAULT_PHYSICAL_DISABILITY);
        assertThat(testStudent.getOther_special_requests()).isEqualTo(DEFAULT_OTHER_SPECIAL_REQUESTS);
    }

    @Test
    @Transactional
    public void createStudentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = studentRepository.findAll().size();

        // Create the Student with an existing ID
        Student existingStudent = new Student();
        existingStudent.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStudentMockMvc.perform(post("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingStudent)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStudents() throws Exception {
        
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.ADMIN);
        authorities.add(authority);
        User user = new User();
        user.setLogin("test");
        user.setFirstName("john");
        user.setLastName("doe");
        user.setEmail("john.doe@jhipter.com");
        user.setAuthorities(authorities);
        when(mockUserService.getUserWithAuthorities()).thenReturn(user);
        
        studentRepository.saveAndFlush(student);

        // Get all the studentList
        restStudentMockMvc.perform(get("/api/students?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(student.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME.toString())))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].cityOfBirth").value(hasItem(DEFAULT_CITY_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].stateOrProvince").value(hasItem(DEFAULT_STATE_OR_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].profession").value(hasItem(DEFAULT_PROFESSION.toString())))
            .andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH.toString())))
            .andExpect(jsonPath("$.[*].qualification").value(hasItem(DEFAULT_QUALIFICATION.toString())))
            .andExpect(jsonPath("$.[*].knowledgeOfItalian").value(hasItem(DEFAULT_KNOWLEDGE_OF_ITALIAN.toString())))
            .andExpect(jsonPath("$.[*].alreadyStudent").value(hasItem(DEFAULT_ALREADY_STUDENT.booleanValue())))
            .andExpect(jsonPath("$.[*].previousAttendYear").value(hasItem(DEFAULT_PREVIOUS_ATTEND_YEAR)))
            .andExpect(jsonPath("$.[*].scholarship").value(hasItem(DEFAULT_SCHOLARSHIP.booleanValue())))
            .andExpect(jsonPath("$.[*].scholarshipStartDate").value(hasItem(DEFAULT_SCHOLARSHIP_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].scholarshipEndDate").value(hasItem(DEFAULT_SCHOLARSHIP_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].scholarshipAwardedBy").value(hasItem(DEFAULT_SCHOLARSHIP_AWARDED_BY.toString())))
            .andExpect(jsonPath("$.[*].privacyAuthorize").value(hasItem(DEFAULT_PRIVACY_AUTHORIZE.booleanValue())))
            .andExpect(jsonPath("$.[*].registrationDate").value(hasItem(DEFAULT_REGISTRATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].studentNumber").value(hasItem(DEFAULT_STUDENT_NUMBER)))
            .andExpect(jsonPath("$.[*].postal_code").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].mobile_phone").value(hasItem(DEFAULT_MOBILE_PHONE.toString())))
            .andExpect(jsonPath("$.[*].mother_tongue").value(hasItem(DEFAULT_MOTHER_TONGUE.toString())))
            .andExpect(jsonPath("$.[*].other_languages").value(hasItem(DEFAULT_OTHER_LANGUAGES.toString())))
            .andExpect(jsonPath("$.[*].italian_studied_in").value(hasItem(DEFAULT_ITALIAN_STUDIED_IN.toString())))
            .andExpect(jsonPath("$.[*].italian_studied_for").value(hasItem(DEFAULT_ITALIAN_STUDIED_FOR.toString())))
            .andExpect(jsonPath("$.[*].italian_studied_hour_week").value(hasItem(DEFAULT_ITALIAN_STUDIED_HOUR_WEEK.toString())))
            .andExpect(jsonPath("$.[*].italian_certification").value(hasItem(DEFAULT_ITALIAN_CERTIFICATION.toString())))
            .andExpect(jsonPath("$.[*].italian_level").value(hasItem(DEFAULT_ITALIAN_LEVEL.toString())))
            .andExpect(jsonPath("$.[*].how_long_student_at_unistrapg").value(hasItem(DEFAULT_HOW_LONG_STUDENT_AT_UNISTRAPG)))
            .andExpect(jsonPath("$.[*].accomodation").value(hasItem(DEFAULT_ACCOMODATION.booleanValue())))
            .andExpect(jsonPath("$.[*].learning_disability").value(hasItem(DEFAULT_LEARNING_DISABILITY.toString())))
            .andExpect(jsonPath("$.[*].physical_disability").value(hasItem(DEFAULT_PHYSICAL_DISABILITY.toString())))
            .andExpect(jsonPath("$.[*].other_special_requests").value(hasItem(DEFAULT_OTHER_SPECIAL_REQUESTS.toString())));
    }

    @Test
    @Transactional
    public void getStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);

        // Get the student
        restStudentMockMvc.perform(get("/api/students/{id}", student.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(student.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.surname").value(DEFAULT_SURNAME.toString()))
            .andExpect(jsonPath("$.sex").value(DEFAULT_SEX.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.toString()))
            .andExpect(jsonPath("$.cityOfBirth").value(DEFAULT_CITY_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.stateOrProvince").value(DEFAULT_STATE_OR_PROVINCE.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.profession").value(DEFAULT_PROFESSION.toString()))
            .andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH.toString()))
            .andExpect(jsonPath("$.qualification").value(DEFAULT_QUALIFICATION.toString()))
            .andExpect(jsonPath("$.knowledgeOfItalian").value(DEFAULT_KNOWLEDGE_OF_ITALIAN.toString()))
            .andExpect(jsonPath("$.alreadyStudent").value(DEFAULT_ALREADY_STUDENT.booleanValue()))
            .andExpect(jsonPath("$.previousAttendYear").value(DEFAULT_PREVIOUS_ATTEND_YEAR))
            .andExpect(jsonPath("$.scholarship").value(DEFAULT_SCHOLARSHIP.booleanValue()))
            .andExpect(jsonPath("$.scholarshipStartDate").value(DEFAULT_SCHOLARSHIP_START_DATE.toString()))
            .andExpect(jsonPath("$.scholarshipEndDate").value(DEFAULT_SCHOLARSHIP_END_DATE.toString()))
            .andExpect(jsonPath("$.scholarshipAwardedBy").value(DEFAULT_SCHOLARSHIP_AWARDED_BY.toString()))
            .andExpect(jsonPath("$.privacyAuthorize").value(DEFAULT_PRIVACY_AUTHORIZE.booleanValue()))
            .andExpect(jsonPath("$.registrationDate").value(DEFAULT_REGISTRATION_DATE.toString()))
            .andExpect(jsonPath("$.studentNumber").value(DEFAULT_STUDENT_NUMBER))
            .andExpect(jsonPath("$.postal_code").value(DEFAULT_POSTAL_CODE.toString()))
            .andExpect(jsonPath("$.mobile_phone").value(DEFAULT_MOBILE_PHONE.toString()))
            .andExpect(jsonPath("$.mother_tongue").value(DEFAULT_MOTHER_TONGUE.toString()))
            .andExpect(jsonPath("$.other_languages").value(DEFAULT_OTHER_LANGUAGES.toString()))
            .andExpect(jsonPath("$.italian_studied_in").value(DEFAULT_ITALIAN_STUDIED_IN.toString()))
            .andExpect(jsonPath("$.italian_studied_for").value(DEFAULT_ITALIAN_STUDIED_FOR.toString()))
            .andExpect(jsonPath("$.italian_studied_hour_week").value(DEFAULT_ITALIAN_STUDIED_HOUR_WEEK.toString()))
            .andExpect(jsonPath("$.italian_certification").value(DEFAULT_ITALIAN_CERTIFICATION.toString()))
            .andExpect(jsonPath("$.italian_level").value(DEFAULT_ITALIAN_LEVEL.toString()))
            .andExpect(jsonPath("$.how_long_student_at_unistrapg").value(DEFAULT_HOW_LONG_STUDENT_AT_UNISTRAPG))
            .andExpect(jsonPath("$.accomodation").value(DEFAULT_ACCOMODATION.booleanValue()))
            .andExpect(jsonPath("$.learning_disability").value(DEFAULT_LEARNING_DISABILITY.toString()))
            .andExpect(jsonPath("$.physical_disability").value(DEFAULT_PHYSICAL_DISABILITY.toString()))
            .andExpect(jsonPath("$.other_special_requests").value(DEFAULT_OTHER_SPECIAL_REQUESTS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStudent() throws Exception {
        // Get the student
        restStudentMockMvc.perform(get("/api/students/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);
        int databaseSizeBeforeUpdate = studentRepository.findAll().size();

        // Update the student
        Student updatedStudent = studentRepository.findOne(student.getId());
        updatedStudent
                .name(UPDATED_NAME)
                .surname(UPDATED_SURNAME)
                .sex(UPDATED_SEX)
                .email(UPDATED_EMAIL)
                .phoneNumber(UPDATED_PHONE_NUMBER)
                .cityOfBirth(UPDATED_CITY_OF_BIRTH)
                .nationality(UPDATED_NATIONALITY)
                .address(UPDATED_ADDRESS)
                .country(UPDATED_COUNTRY)
                .stateOrProvince(UPDATED_STATE_OR_PROVINCE)
                .city(UPDATED_CITY)
                .profession(UPDATED_PROFESSION)
                .dateOfBirth(UPDATED_DATE_OF_BIRTH)
                .qualification(UPDATED_QUALIFICATION)
                .knowledgeOfItalian(UPDATED_KNOWLEDGE_OF_ITALIAN)
                .alreadyStudent(UPDATED_ALREADY_STUDENT)
                .previousAttendYear(UPDATED_PREVIOUS_ATTEND_YEAR)
                .scholarship(UPDATED_SCHOLARSHIP)
                .scholarshipStartDate(UPDATED_SCHOLARSHIP_START_DATE)
                .scholarshipEndDate(UPDATED_SCHOLARSHIP_END_DATE)
                .scholarshipAwardedBy(UPDATED_SCHOLARSHIP_AWARDED_BY)
                .privacyAuthorize(UPDATED_PRIVACY_AUTHORIZE)
                .registrationDate(UPDATED_REGISTRATION_DATE)
                .studentNumber(UPDATED_STUDENT_NUMBER)
                .postal_code(UPDATED_POSTAL_CODE)
                .mobile_phone(UPDATED_MOBILE_PHONE)
                .mother_tongue(UPDATED_MOTHER_TONGUE)
                .other_languages(UPDATED_OTHER_LANGUAGES)
                .italian_studied_in(UPDATED_ITALIAN_STUDIED_IN)
                .italian_studied_for(UPDATED_ITALIAN_STUDIED_FOR)
                .italian_studied_hour_week(UPDATED_ITALIAN_STUDIED_HOUR_WEEK)
                .italian_certification(UPDATED_ITALIAN_CERTIFICATION)
                .italian_level(UPDATED_ITALIAN_LEVEL)
                .how_long_student_at_unistrapg(UPDATED_HOW_LONG_STUDENT_AT_UNISTRAPG)
                .accomodation(UPDATED_ACCOMODATION)
                .learning_disability(UPDATED_LEARNING_DISABILITY)
                .physical_disability(UPDATED_PHYSICAL_DISABILITY)
                .other_special_requests(UPDATED_OTHER_SPECIAL_REQUESTS);

        restStudentMockMvc.perform(put("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStudent)))
            .andExpect(status().isOk());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeUpdate);
        Student testStudent = studentList.get(studentList.size() - 1);
        assertThat(testStudent.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testStudent.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testStudent.getSex()).isEqualTo(UPDATED_SEX);
        assertThat(testStudent.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testStudent.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testStudent.getCityOfBirth()).isEqualTo(UPDATED_CITY_OF_BIRTH);
        assertThat(testStudent.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testStudent.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testStudent.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testStudent.getStateOrProvince()).isEqualTo(UPDATED_STATE_OR_PROVINCE);
        assertThat(testStudent.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testStudent.getProfession()).isEqualTo(UPDATED_PROFESSION);
        assertThat(testStudent.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testStudent.getQualification()).isEqualTo(UPDATED_QUALIFICATION);
        assertThat(testStudent.getKnowledgeOfItalian()).isEqualTo(UPDATED_KNOWLEDGE_OF_ITALIAN);
        assertThat(testStudent.isAlreadyStudent()).isEqualTo(UPDATED_ALREADY_STUDENT);
        assertThat(testStudent.getPreviousAttendYear()).isEqualTo(UPDATED_PREVIOUS_ATTEND_YEAR);
        assertThat(testStudent.isScholarship()).isEqualTo(UPDATED_SCHOLARSHIP);
        assertThat(testStudent.getScholarshipStartDate()).isEqualTo(UPDATED_SCHOLARSHIP_START_DATE);
        assertThat(testStudent.getScholarshipEndDate()).isEqualTo(UPDATED_SCHOLARSHIP_END_DATE);
        assertThat(testStudent.getScholarshipAwardedBy()).isEqualTo(UPDATED_SCHOLARSHIP_AWARDED_BY);
        assertThat(testStudent.isPrivacyAuthorize()).isEqualTo(UPDATED_PRIVACY_AUTHORIZE);
        assertThat(testStudent.getRegistrationDate()).isEqualTo(UPDATED_REGISTRATION_DATE);
        assertThat(testStudent.getStudentNumber()).isEqualTo(UPDATED_STUDENT_NUMBER);
        assertThat(testStudent.getPostal_code()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testStudent.getMobile_phone()).isEqualTo(UPDATED_MOBILE_PHONE);
        assertThat(testStudent.getMother_tongue()).isEqualTo(UPDATED_MOTHER_TONGUE);
        assertThat(testStudent.getOther_languages()).isEqualTo(UPDATED_OTHER_LANGUAGES);
        assertThat(testStudent.getItalian_studied_in()).isEqualTo(UPDATED_ITALIAN_STUDIED_IN);
        assertThat(testStudent.getItalian_studied_for()).isEqualTo(UPDATED_ITALIAN_STUDIED_FOR);
        assertThat(testStudent.getItalian_studied_hour_week()).isEqualTo(UPDATED_ITALIAN_STUDIED_HOUR_WEEK);
        assertThat(testStudent.getItalian_certification()).isEqualTo(UPDATED_ITALIAN_CERTIFICATION);
        assertThat(testStudent.getItalian_level()).isEqualTo(UPDATED_ITALIAN_LEVEL);
        assertThat(testStudent.getHow_long_student_at_unistrapg()).isEqualTo(UPDATED_HOW_LONG_STUDENT_AT_UNISTRAPG);
        assertThat(testStudent.isAccomodation()).isEqualTo(UPDATED_ACCOMODATION);
        assertThat(testStudent.getLearning_disability()).isEqualTo(UPDATED_LEARNING_DISABILITY);
        assertThat(testStudent.getPhysical_disability()).isEqualTo(UPDATED_PHYSICAL_DISABILITY);
        assertThat(testStudent.getOther_special_requests()).isEqualTo(UPDATED_OTHER_SPECIAL_REQUESTS);
    }

    @Test
    @Transactional
    public void updateNonExistingStudent() throws Exception {
        
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.STUDENT);
        authorities.add(authority);
        User user = new User();
        user.setLogin("test");
        user.setFirstName("john");
        user.setLastName("doe");
        user.setEmail("john.doe@jhipter.com");
        user.setAuthorities(authorities);
        when(mockUserService.getUserWithAuthorities()).thenReturn(user);
        
        
        int databaseSizeBeforeUpdate = studentRepository.findAll().size();

        // Create the Student

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStudentMockMvc.perform(put("/api/students")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(student)))
            .andExpect(status().isCreated());

        // Validate the Student in the database
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStudent() throws Exception {
        // Initialize the database
        studentRepository.saveAndFlush(student);
        int databaseSizeBeforeDelete = studentRepository.findAll().size();

        // Get the student
        restStudentMockMvc.perform(delete("/api/students/{id}", student.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Student> studentList = studentRepository.findAll();
        assertThat(studentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
