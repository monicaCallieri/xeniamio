package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.pagopa.webservicea.gestoreposizioni.wsdl.InserimentoPosizioneOutputType;
import com.consoft.xenia.repository.InserimentoPosizioneOutputTypeRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InserimentoPosizioneOutputTypeResource REST controller.
 *
 * @see InserimentoPosizioneOutputTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class InserimentoPosizioneOutputTypeResourceIntTest {

    private static final String DEFAULT_ESITO = "AAAAAAAAAA";
    private static final String UPDATED_ESITO = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_ERRORE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_ERRORE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_IDENTIFICATIVO_PRESENTAZIONE = "BBBBBBBBBB";

    @Inject
    private InserimentoPosizioneOutputTypeRepository inserimentoPosizioneOutputTypeRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restInserimentoPosizioneOutputTypeMockMvc;

    private InserimentoPosizioneOutputType inserimentoPosizioneOutputType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InserimentoPosizioneOutputTypeResource inserimentoPosizioneOutputTypeResource = new InserimentoPosizioneOutputTypeResource();
        ReflectionTestUtils.setField(inserimentoPosizioneOutputTypeResource, "inserimentoPosizioneOutputTypeRepository", inserimentoPosizioneOutputTypeRepository);
        this.restInserimentoPosizioneOutputTypeMockMvc = MockMvcBuilders.standaloneSetup(inserimentoPosizioneOutputTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InserimentoPosizioneOutputType createEntity(EntityManager em) {
        InserimentoPosizioneOutputType inserimentoPosizioneOutputType = new InserimentoPosizioneOutputType()
                .esito(DEFAULT_ESITO)
                .codiceErrore(DEFAULT_CODICE_ERRORE)
                .descrizione(DEFAULT_DESCRIZIONE)
                .identificativo_univoco_versamento(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO)
                .codice_identificativo_presentazione(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE);
        return inserimentoPosizioneOutputType;
    }

    @Before
    public void initTest() {
        inserimentoPosizioneOutputType = createEntity(em);
    }

    @Test
    @Transactional
    public void createInserimentoPosizioneOutputType() throws Exception {
        int databaseSizeBeforeCreate = inserimentoPosizioneOutputTypeRepository.findAll().size();

        // Create the InserimentoPosizioneOutputType

        restInserimentoPosizioneOutputTypeMockMvc.perform(post("/api/inserimento-posizione-output-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneOutputType)))
            .andExpect(status().isCreated());

        // Validate the InserimentoPosizioneOutputType in the database
        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypeList = inserimentoPosizioneOutputTypeRepository.findAll();
        assertThat(inserimentoPosizioneOutputTypeList).hasSize(databaseSizeBeforeCreate + 1);
        InserimentoPosizioneOutputType testInserimentoPosizioneOutputType = inserimentoPosizioneOutputTypeList.get(inserimentoPosizioneOutputTypeList.size() - 1);
        assertThat(testInserimentoPosizioneOutputType.getEsito()).isEqualTo(DEFAULT_ESITO);
        assertThat(testInserimentoPosizioneOutputType.getCodiceErrore()).isEqualTo(DEFAULT_CODICE_ERRORE);
        assertThat(testInserimentoPosizioneOutputType.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testInserimentoPosizioneOutputType.getIdentificativo_univoco_versamento()).isEqualTo(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
        assertThat(testInserimentoPosizioneOutputType.getCodice_identificativo_presentazione()).isEqualTo(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE);
    }

    @Test
    @Transactional
    public void createInserimentoPosizioneOutputTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inserimentoPosizioneOutputTypeRepository.findAll().size();

        // Create the InserimentoPosizioneOutputType with an existing ID
        InserimentoPosizioneOutputType existingInserimentoPosizioneOutputType = new InserimentoPosizioneOutputType();
        existingInserimentoPosizioneOutputType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInserimentoPosizioneOutputTypeMockMvc.perform(post("/api/inserimento-posizione-output-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingInserimentoPosizioneOutputType)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypeList = inserimentoPosizioneOutputTypeRepository.findAll();
        assertThat(inserimentoPosizioneOutputTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEsitoIsRequired() throws Exception {
        int databaseSizeBeforeTest = inserimentoPosizioneOutputTypeRepository.findAll().size();
        // set the field null
        inserimentoPosizioneOutputType.setEsito(null);

        // Create the InserimentoPosizioneOutputType, which fails.

        restInserimentoPosizioneOutputTypeMockMvc.perform(post("/api/inserimento-posizione-output-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneOutputType)))
            .andExpect(status().isBadRequest());

        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypeList = inserimentoPosizioneOutputTypeRepository.findAll();
        assertThat(inserimentoPosizioneOutputTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInserimentoPosizioneOutputTypes() throws Exception {
        // Initialize the database
        inserimentoPosizioneOutputTypeRepository.saveAndFlush(inserimentoPosizioneOutputType);

        // Get all the inserimentoPosizioneOutputTypeList
        restInserimentoPosizioneOutputTypeMockMvc.perform(get("/api/inserimento-posizione-output-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inserimentoPosizioneOutputType.getId().intValue())))
            .andExpect(jsonPath("$.[*].esito").value(hasItem(DEFAULT_ESITO.toString())))
            .andExpect(jsonPath("$.[*].codiceErrore").value(hasItem(DEFAULT_CODICE_ERRORE.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].identificativo_univoco_versamento").value(hasItem(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO.toString())))
            .andExpect(jsonPath("$.[*].codice_identificativo_presentazione").value(hasItem(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE.toString())));
    }

    @Test
    @Transactional
    public void getInserimentoPosizioneOutputType() throws Exception {
        // Initialize the database
        inserimentoPosizioneOutputTypeRepository.saveAndFlush(inserimentoPosizioneOutputType);

        // Get the inserimentoPosizioneOutputType
        restInserimentoPosizioneOutputTypeMockMvc.perform(get("/api/inserimento-posizione-output-types/{id}", inserimentoPosizioneOutputType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inserimentoPosizioneOutputType.getId().intValue()))
            .andExpect(jsonPath("$.esito").value(DEFAULT_ESITO.toString()))
            .andExpect(jsonPath("$.codiceErrore").value(DEFAULT_CODICE_ERRORE.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.identificativo_univoco_versamento").value(DEFAULT_IDENTIFICATIVO_UNIVOCO_VERSAMENTO.toString()))
            .andExpect(jsonPath("$.codice_identificativo_presentazione").value(DEFAULT_CODICE_IDENTIFICATIVO_PRESENTAZIONE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInserimentoPosizioneOutputType() throws Exception {
        // Get the inserimentoPosizioneOutputType
        restInserimentoPosizioneOutputTypeMockMvc.perform(get("/api/inserimento-posizione-output-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInserimentoPosizioneOutputType() throws Exception {
        // Initialize the database
        inserimentoPosizioneOutputTypeRepository.saveAndFlush(inserimentoPosizioneOutputType);
        int databaseSizeBeforeUpdate = inserimentoPosizioneOutputTypeRepository.findAll().size();

        // Update the inserimentoPosizioneOutputType
        InserimentoPosizioneOutputType updatedInserimentoPosizioneOutputType = inserimentoPosizioneOutputTypeRepository.findOne(inserimentoPosizioneOutputType.getId());
        updatedInserimentoPosizioneOutputType
                .esito(UPDATED_ESITO)
                .codiceErrore(UPDATED_CODICE_ERRORE)
                .descrizione(UPDATED_DESCRIZIONE)
                .identificativo_univoco_versamento(UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO)
                .codice_identificativo_presentazione(UPDATED_CODICE_IDENTIFICATIVO_PRESENTAZIONE);

        restInserimentoPosizioneOutputTypeMockMvc.perform(put("/api/inserimento-posizione-output-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInserimentoPosizioneOutputType)))
            .andExpect(status().isOk());

        // Validate the InserimentoPosizioneOutputType in the database
        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypeList = inserimentoPosizioneOutputTypeRepository.findAll();
        assertThat(inserimentoPosizioneOutputTypeList).hasSize(databaseSizeBeforeUpdate);
        InserimentoPosizioneOutputType testInserimentoPosizioneOutputType = inserimentoPosizioneOutputTypeList.get(inserimentoPosizioneOutputTypeList.size() - 1);
        assertThat(testInserimentoPosizioneOutputType.getEsito()).isEqualTo(UPDATED_ESITO);
        assertThat(testInserimentoPosizioneOutputType.getCodiceErrore()).isEqualTo(UPDATED_CODICE_ERRORE);
        assertThat(testInserimentoPosizioneOutputType.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testInserimentoPosizioneOutputType.getIdentificativo_univoco_versamento()).isEqualTo(UPDATED_IDENTIFICATIVO_UNIVOCO_VERSAMENTO);
        assertThat(testInserimentoPosizioneOutputType.getCodice_identificativo_presentazione()).isEqualTo(UPDATED_CODICE_IDENTIFICATIVO_PRESENTAZIONE);
    }

    @Test
    @Transactional
    public void updateNonExistingInserimentoPosizioneOutputType() throws Exception {
        int databaseSizeBeforeUpdate = inserimentoPosizioneOutputTypeRepository.findAll().size();

        // Create the InserimentoPosizioneOutputType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInserimentoPosizioneOutputTypeMockMvc.perform(put("/api/inserimento-posizione-output-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inserimentoPosizioneOutputType)))
            .andExpect(status().isCreated());

        // Validate the InserimentoPosizioneOutputType in the database
        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypeList = inserimentoPosizioneOutputTypeRepository.findAll();
        assertThat(inserimentoPosizioneOutputTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInserimentoPosizioneOutputType() throws Exception {
        // Initialize the database
        inserimentoPosizioneOutputTypeRepository.saveAndFlush(inserimentoPosizioneOutputType);
        int databaseSizeBeforeDelete = inserimentoPosizioneOutputTypeRepository.findAll().size();

        // Get the inserimentoPosizioneOutputType
        restInserimentoPosizioneOutputTypeMockMvc.perform(delete("/api/inserimento-posizione-output-types/{id}", inserimentoPosizioneOutputType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InserimentoPosizioneOutputType> inserimentoPosizioneOutputTypeList = inserimentoPosizioneOutputTypeRepository.findAll();
        assertThat(inserimentoPosizioneOutputTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
