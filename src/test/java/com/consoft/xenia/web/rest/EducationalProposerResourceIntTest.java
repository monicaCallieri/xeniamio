package com.consoft.xenia.web.rest;

import com.consoft.xenia.XeniaApp;

import com.consoft.xenia.domain.EducationalProposer;
import com.consoft.xenia.repository.EducationalProposerRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EducationalProposerResource REST controller.
 *
 * @see EducationalProposerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XeniaApp.class)
public class EducationalProposerResourceIntTest {

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_OF_COURSE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_COURSE = "BBBBBBBBBB";

    private static final Integer DEFAULT_DURATION = 1;
    private static final Integer UPDATED_DURATION = 2;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PERIODICITY = "AAAAAAAAAA";
    private static final String UPDATED_PERIODICITY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_STARTDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_STARTDATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    @Inject
    private EducationalProposerRepository educationalProposerRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restEducationalProposerMockMvc;

    private EducationalProposer educationalProposer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EducationalProposerResource educationalProposerResource = new EducationalProposerResource();
        ReflectionTestUtils.setField(educationalProposerResource, "educationalProposerRepository", educationalProposerRepository);
        this.restEducationalProposerMockMvc = MockMvcBuilders.standaloneSetup(educationalProposerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EducationalProposer createEntity(EntityManager em) {
        EducationalProposer educationalProposer = new EducationalProposer()
                .creationDate(DEFAULT_CREATION_DATE)
                .name(DEFAULT_NAME)
                .typeOfCourse(DEFAULT_TYPE_OF_COURSE)
                .duration(DEFAULT_DURATION)
                .description(DEFAULT_DESCRIPTION)
                .periodicity(DEFAULT_PERIODICITY)
                .startdate(DEFAULT_STARTDATE)
                .price(DEFAULT_PRICE);
        return educationalProposer;
    }

    @Before
    public void initTest() {
        educationalProposer = createEntity(em);
    }

    @Test
    @Transactional
    public void createEducationalProposer() throws Exception {
        int databaseSizeBeforeCreate = educationalProposerRepository.findAll().size();

        // Create the EducationalProposer

        restEducationalProposerMockMvc.perform(post("/api/educational-proposers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(educationalProposer)))
            .andExpect(status().isCreated());

        // Validate the EducationalProposer in the database
        List<EducationalProposer> educationalProposerList = educationalProposerRepository.findAll();
        assertThat(educationalProposerList).hasSize(databaseSizeBeforeCreate + 1);
        EducationalProposer testEducationalProposer = educationalProposerList.get(educationalProposerList.size() - 1);
        assertThat(testEducationalProposer.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testEducationalProposer.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEducationalProposer.getTypeOfCourse()).isEqualTo(DEFAULT_TYPE_OF_COURSE);
        assertThat(testEducationalProposer.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testEducationalProposer.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEducationalProposer.getPeriodicity()).isEqualTo(DEFAULT_PERIODICITY);
        assertThat(testEducationalProposer.getStartdate()).isEqualTo(DEFAULT_STARTDATE);
        assertThat(testEducationalProposer.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void createEducationalProposerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = educationalProposerRepository.findAll().size();

        // Create the EducationalProposer with an existing ID
        EducationalProposer existingEducationalProposer = new EducationalProposer();
        existingEducationalProposer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEducationalProposerMockMvc.perform(post("/api/educational-proposers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingEducationalProposer)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<EducationalProposer> educationalProposerList = educationalProposerRepository.findAll();
        assertThat(educationalProposerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEducationalProposers() throws Exception {
        // Initialize the database
        educationalProposerRepository.saveAndFlush(educationalProposer);

        // Get all the educationalProposerList
        restEducationalProposerMockMvc.perform(get("/api/educational-proposers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(educationalProposer.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].typeOfCourse").value(hasItem(DEFAULT_TYPE_OF_COURSE.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].periodicity").value(hasItem(DEFAULT_PERIODICITY.toString())))
            .andExpect(jsonPath("$.[*].startdate").value(hasItem(DEFAULT_STARTDATE.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())));
    }

    @Test
    @Transactional
    public void getEducationalProposer() throws Exception {
        // Initialize the database
        educationalProposerRepository.saveAndFlush(educationalProposer);

        // Get the educationalProposer
        restEducationalProposerMockMvc.perform(get("/api/educational-proposers/{id}", educationalProposer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(educationalProposer.getId().intValue()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.typeOfCourse").value(DEFAULT_TYPE_OF_COURSE.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.periodicity").value(DEFAULT_PERIODICITY.toString()))
            .andExpect(jsonPath("$.startdate").value(DEFAULT_STARTDATE.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEducationalProposer() throws Exception {
        // Get the educationalProposer
        restEducationalProposerMockMvc.perform(get("/api/educational-proposers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEducationalProposer() throws Exception {
        // Initialize the database
        educationalProposerRepository.saveAndFlush(educationalProposer);
        int databaseSizeBeforeUpdate = educationalProposerRepository.findAll().size();

        // Update the educationalProposer
        EducationalProposer updatedEducationalProposer = educationalProposerRepository.findOne(educationalProposer.getId());
        updatedEducationalProposer
                .creationDate(UPDATED_CREATION_DATE)
                .name(UPDATED_NAME)
                .typeOfCourse(UPDATED_TYPE_OF_COURSE)
                .duration(UPDATED_DURATION)
                .description(UPDATED_DESCRIPTION)
                .periodicity(UPDATED_PERIODICITY)
                .startdate(UPDATED_STARTDATE)
                .price(UPDATED_PRICE);

        restEducationalProposerMockMvc.perform(put("/api/educational-proposers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEducationalProposer)))
            .andExpect(status().isOk());

        // Validate the EducationalProposer in the database
        List<EducationalProposer> educationalProposerList = educationalProposerRepository.findAll();
        assertThat(educationalProposerList).hasSize(databaseSizeBeforeUpdate);
        EducationalProposer testEducationalProposer = educationalProposerList.get(educationalProposerList.size() - 1);
        assertThat(testEducationalProposer.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testEducationalProposer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEducationalProposer.getTypeOfCourse()).isEqualTo(UPDATED_TYPE_OF_COURSE);
        assertThat(testEducationalProposer.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testEducationalProposer.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEducationalProposer.getPeriodicity()).isEqualTo(UPDATED_PERIODICITY);
        assertThat(testEducationalProposer.getStartdate()).isEqualTo(UPDATED_STARTDATE);
        assertThat(testEducationalProposer.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void updateNonExistingEducationalProposer() throws Exception {
        int databaseSizeBeforeUpdate = educationalProposerRepository.findAll().size();

        // Create the EducationalProposer

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEducationalProposerMockMvc.perform(put("/api/educational-proposers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(educationalProposer)))
            .andExpect(status().isCreated());

        // Validate the EducationalProposer in the database
        List<EducationalProposer> educationalProposerList = educationalProposerRepository.findAll();
        assertThat(educationalProposerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEducationalProposer() throws Exception {
        // Initialize the database
        educationalProposerRepository.saveAndFlush(educationalProposer);
        int databaseSizeBeforeDelete = educationalProposerRepository.findAll().size();

        // Get the educationalProposer
        restEducationalProposerMockMvc.perform(delete("/api/educational-proposers/{id}", educationalProposer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EducationalProposer> educationalProposerList = educationalProposerRepository.findAll();
        assertThat(educationalProposerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
