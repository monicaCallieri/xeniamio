'use strict';

describe('Controller Tests', function() {

    describe('EducationalProposer Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockEducationalProposer, MockStudentIntentions;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockEducationalProposer = jasmine.createSpy('MockEducationalProposer');
            MockStudentIntentions = jasmine.createSpy('MockStudentIntentions');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'EducationalProposer': MockEducationalProposer,
                'StudentIntentions': MockStudentIntentions
            };
            createController = function() {
                $injector.get('$controller')("EducationalProposerDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:educationalProposerUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
