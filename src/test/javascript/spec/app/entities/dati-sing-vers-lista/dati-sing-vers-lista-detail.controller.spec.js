'use strict';

describe('Controller Tests', function() {

    describe('DatiSingVersLista Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockDatiSingVersLista, MockInviaCarrPosLista;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockDatiSingVersLista = jasmine.createSpy('MockDatiSingVersLista');
            MockInviaCarrPosLista = jasmine.createSpy('MockInviaCarrPosLista');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'DatiSingVersLista': MockDatiSingVersLista,
                'InviaCarrPosLista': MockInviaCarrPosLista
            };
            createController = function() {
                $injector.get('$controller')("DatiSingVersListaDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:datiSingVersListaUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
