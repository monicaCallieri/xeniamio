'use strict';

describe('Controller Tests', function() {

    describe('InviaCarrPosLista Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockInviaCarrPosLista, MockInviaCarrPosOutput, MockDatiSingVersLista;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockInviaCarrPosLista = jasmine.createSpy('MockInviaCarrPosLista');
            MockInviaCarrPosOutput = jasmine.createSpy('MockInviaCarrPosOutput');
            MockDatiSingVersLista = jasmine.createSpy('MockDatiSingVersLista');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'InviaCarrPosLista': MockInviaCarrPosLista,
                'InviaCarrPosOutput': MockInviaCarrPosOutput,
                'DatiSingVersLista': MockDatiSingVersLista
            };
            createController = function() {
                $injector.get('$controller')("InviaCarrPosListaDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:inviaCarrPosListaUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
