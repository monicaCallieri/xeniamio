'use strict';

describe('Controller Tests', function() {

    describe('StudentIntentions Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockStudentIntentions, MockUser, MockEducationalProposer;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockStudentIntentions = jasmine.createSpy('MockStudentIntentions');
            MockUser = jasmine.createSpy('MockUser');
            MockEducationalProposer = jasmine.createSpy('MockEducationalProposer');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'StudentIntentions': MockStudentIntentions,
                'User': MockUser,
                'EducationalProposer': MockEducationalProposer
            };
            createController = function() {
                $injector.get('$controller')("StudentIntentionsDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:studentIntentionsUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
