'use strict';

//$timeout, $scope, $stateParams, $uibModalInstance, $filter, entity, Course, StudentCourse, Lecturer
/*Test che si occua di verificare il corretto funzionamento del course-dialog.controller.js
 * Il test da implementare è per verificare che al momento della chiamata della creazione del
 * corso tutti i valori siano riempiti in modo opportuno.*/
/*describe('Controller Tests', function () {

    describe('Course Management Dialog Controller', function () {
        var $q, $scope, $stateParams;
        var MockTimeout, MockModalInstance, MockFilter, MockEntity, MockCourse, MockStudentCourse, MockLecturer;
        var createController;

        beforeEach(inject(function ($injector) {
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
            $stateParams= 'course.new';
            MockTimeout = jasmine.createSpy('MockTimeout');
            MockModalInstance = jasmine.createSpyObj('modalInstance', ['close', 'dismiss', 'result.then']);
            MockFilter = jasmine.createSpy('MockFilter');
            MockEntity = jasmine.createSpy('MockEntity');
            MockCourse = jasmine.createSpyObj('MockCourse', ['save']);
            MockStudentCourse = jasmine.createSpy('MockStudentCourse');
            MockLecturer = jasmine.createSpy('MockLecturer');

            var locals = {
                '$timout': MockTimeout,
                '$scope': $scope,
                '$stateParams': $stateParams,
                '$uibModalInstance':MockModalInstance,
                '$filter': MockFilter,
                'entity': MockEntity,
                'Course': MockCourse,
                'StudentCourse': MockStudentCourse,
                'Lecturer': MockLecturer
            };
            createController = function () {
                $injector.get('$controller')("CourseDetailController", locals);
            };
        }));


        describe('Course-dialog.controller', function () {
            it('should update success to OK after creating a Course', function () {
                MockCourse.save.and.returnValue($q.resolve());
                createController();
                $scope.vm.course.level= 'A1';
                $scope.vm.course.description = 'test di prova della creazione del corso';
                $scope.vm.course.intensive = true;
                $scope.vm.course.startDate = '2017-03-07';
                $scope.vm.course.endDate = '2017-06-09';
                $scope.vm.course.price = 1200;
                $scope.$apply($scope.vm.course); // $q promises require an $apply
                expect(MockCourse.save).toHaveBeenCalledWith({
                    level: 'A1',
                    description: 'test di prova della creazione del corso',
                    intensive: true,
                    startDate: '2017-03-07',
                    endDate: '2017-06-09',
                    price: 1200
                });
                expect($scope.vm.success).toEqual('OK');
                
            });
        });
    });

});*/



