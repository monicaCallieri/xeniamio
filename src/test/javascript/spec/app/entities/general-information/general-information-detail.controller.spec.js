'use strict';

describe('Controller Tests', function() {

    describe('GeneralInformation Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockGeneralInformation, MockUserInformation, MockResponse;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockGeneralInformation = jasmine.createSpy('MockGeneralInformation');
            MockUserInformation = jasmine.createSpy('MockUserInformation');
            MockResponse = jasmine.createSpy('MockResponse');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'GeneralInformation': MockGeneralInformation,
                'UserInformation': MockUserInformation,
                'Response': MockResponse
            };
            createController = function() {
                $injector.get('$controller')("GeneralInformationDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:generalInformationUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
