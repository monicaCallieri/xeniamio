'use strict';

describe('Controller Tests', function() {

    describe('UserInformation Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockUserInformation, MockQuestion, MockUser, MockResponse;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockUserInformation = jasmine.createSpy('MockUserInformation');
            MockQuestion = jasmine.createSpy('MockQuestion');
            MockUser = jasmine.createSpy('MockUser');
            MockResponse = jasmine.createSpy('MockResponse');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'UserInformation': MockUserInformation,
                'Question': MockQuestion,
                'User': MockUser,
                'Response': MockResponse
            };
            createController = function() {
                $injector.get('$controller')("UserInformationDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:userInformationUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
