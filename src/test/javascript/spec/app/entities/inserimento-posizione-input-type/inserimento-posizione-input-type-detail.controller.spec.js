'use strict';

describe('Controller Tests', function() {

    describe('InserimentoPosizioneInputType Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockInserimentoPosizioneInputType, MockInserimentoPosizioneOutputType;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockInserimentoPosizioneInputType = jasmine.createSpy('MockInserimentoPosizioneInputType');
            MockInserimentoPosizioneOutputType = jasmine.createSpy('MockInserimentoPosizioneOutputType');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'InserimentoPosizioneInputType': MockInserimentoPosizioneInputType,
                'InserimentoPosizioneOutputType': MockInserimentoPosizioneOutputType
            };
            createController = function() {
                $injector.get('$controller')("InserimentoPosizioneInputTypeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:inserimentoPosizioneInputTypeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
