'use strict';

describe('Controller Tests', function() {

    describe('Response Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockResponse, MockQuestion, MockUserInformation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockResponse = jasmine.createSpy('MockResponse');
            MockQuestion = jasmine.createSpy('MockQuestion');
            MockUserInformation = jasmine.createSpy('MockUserInformation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Response': MockResponse,
                'Question': MockQuestion,
                'UserInformation': MockUserInformation
            };
            createController = function() {
                $injector.get('$controller')("ResponseDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'xeniaApp:responseUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
