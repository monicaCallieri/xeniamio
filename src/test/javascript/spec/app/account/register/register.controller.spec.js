'use strict';

describe('Controller Tests', function () {

    beforeEach(mockApiAccountCall);
    beforeEach(mockI18nCalls);

    describe('RegisterController', function () {

        var $scope, $q; // actual implementations
        var MockTimeout, MockTranslate, MockAuth, MockQuestion, MockResponse, MockEntity, MockQuestionEntity; // mocks
        var createController; // local utility function
        var questions = [{
                id: 1,
                question: "Knowledge of Italian"
            }];
        var responses = [{
                id: 1,
                response: "beginner",
                generalInformation: {
                    id: 1,
                    question: "Knowledge of Italian"
                }
            }];
        
        var studentIntentions = ['2017-03-09', '2017-03-25', '2017-03-29'];

        beforeEach(inject(function ($injector) {
            $q = $injector.get('$q');
            $scope = $injector.get('$rootScope').$new();
            MockTimeout = jasmine.createSpy('MockTimeout');
            MockAuth = jasmine.createSpyObj('MockAuth', ['createAccount']);
            MockTranslate = jasmine.createSpyObj('MockTranslate', ['use']);
            MockEntity = jasmine.createSpy('MockEntity');
            MockQuestionEntity = jasmine.createSpy('MockQuestionEntity');
            //MockResponse = jasmine.createSpyObj('MockResponse', ['query']);

            // MockQuestion.query.and.returnValue(questions);
            /*MockResponse.query.and.returnValue([{
             id: 1,
             response: "beginner",
             generalInformation: {
             id: 1,
             question: "Knowledge of Italian"
             }
             }]);*/
            /*var responses = {
             query: function(callback) {
             callback([{
             id: 1,
             response: "beginner",
             generalInformation: {
             id: 1,
             question: "Knowledge of Italian"
             }
             }]);
             }
             };*/


            var locals = {
                'Auth': MockAuth,
                '$translate': MockTranslate,
                '$timeout': MockTimeout,
                '$scope': $scope,
                'response': responses,
                'question': questions
            };
            createController = function () {
                $injector.get('$controller')('RegisterController as vm', locals);
            };
        }));

        it('should ensure the two passwords entered match', function () {
            // given
            createController();
            $scope.vm.registerAccount.password = 'password';
            $scope.vm.confirmPassword = 'non-matching';
            // when
            $scope.vm.register();
            // then
            expect($scope.vm.doNotMatch).toEqual('ERROR');
        });

        it('should update success to OK after creating an account', function () {
            MockTranslate.use.and.returnValue('en');
            MockAuth.createAccount.and.returnValue($q.resolve());
            createController();
            $scope.vm.registerAccount.password = $scope.vm.registerAccount.login = $scope.vm.confirmPassword = 'password';
            $scope.vm.responseUser = ['1'];
            $scope.vm.studentIntentionsDate = studentIntentions;
            $scope.$apply($scope.vm.register); // $q promises require an $apply
            expect(MockAuth.createAccount).toHaveBeenCalledWith({
                password: 'password',
                responseUser: ['1'],
                studentIntentionsDate: studentIntentions,
                langKey: 'en',
                login: undefined
            });
            expect($scope.vm.success).toEqual('OK');
            expect($scope.vm.questions.length).toEqual($scope.vm.responseUser.length);
            expect($scope.vm.registerAccount.langKey).toEqual('en');
            expect(MockTranslate.use).toHaveBeenCalled();
            expect($scope.vm.errorUserExists).toBeNull();
            expect($scope.vm.errorEmailExists).toBeNull();
            expect($scope.vm.error).toBeNull();
        });

        it('should notify of user existence upon 400/login already in use', function () {
            // given
            MockAuth.createAccount.and.returnValue($q.reject({
                status: 400,
                data: 'login already in use'
            }));
            createController();
            $scope.vm.registerAccount.password = $scope.vm.confirmPassword = 'password';
            // when
            $scope.$apply($scope.vm.register); // $q promises require an $apply
            // then
            expect($scope.vm.errorUserExists).toEqual('ERROR');
            expect($scope.vm.errorEmailExists).toBeNull();
            expect($scope.vm.error).toBeNull();
        });

        it('should notify of email existence upon 400/e-mail address already in use', function () {
            // given
            MockAuth.createAccount.and.returnValue($q.reject({
                status: 400,
                data: 'e-mail address already in use'
            }));
            createController();
            $scope.vm.registerAccount.password = $scope.vm.confirmPassword = 'password';
            // when
            $scope.$apply($scope.vm.register); // $q promises require an $apply
            // then
            expect($scope.vm.errorEmailExists).toEqual('ERROR');
            expect($scope.vm.errorUserExists).toBeNull();
            expect($scope.vm.error).toBeNull();
        });

        it('should notify of generic error', function () {
            // given
            MockAuth.createAccount.and.returnValue($q.reject({
                status: 503
            }));
            createController();
            $scope.vm.registerAccount.password = $scope.vm.confirmPassword = 'password';
            // when
            $scope.$apply($scope.vm.register); // $q promises require an $apply
            // then
            expect($scope.vm.errorUserExists).toBeNull();
            expect($scope.vm.errorEmailExists).toBeNull();
            expect($scope.vm.error).toEqual('ERROR');
        });
        /**
         * Test che verifica che nella pagina di registrazione siano presenti domande e risposte
         * se queste sono presenti nel datamodel.
         */
        it('should verify register question are present', function () {
            // given
            createController();
            $scope.$apply(createController);
            //then
            expect($scope.vm.questions).toEqual(questions);
            expect($scope.vm.response).toEqual(responses);
            //expect($scope.vm.response[0].response).toEqual("beginner");
        });
        /**
         * Test che verifica che se è presente una domanda nella pagina di registrazione
         * allora ci deve essere esattamente una risposta.
         */
        it('should verify that every question in register have a response', function () {
            // given
            createController();
            $scope.vm.responseUser = ['1'];
            $scope.$apply($scope.vm.responseUser);
            //then
            expect($scope.vm.questions).toEqual(questions);
            expect($scope.vm.response).toEqual(responses);
            expect($scope.vm.questions.length).toEqual($scope.vm.responseUser.length);
            //expect($scope.vm.response[0].response).toEqual("beginner");
        });
        /**
         * Metodo che verifica che nessuna risposta sia selezionata di default
         */
        it('should verify that there is not response set by default', function () {
            // given
            createController();
            //then
            expect($scope.vm.questions).toEqual(questions);
            expect($scope.vm.response).toEqual(responses);
            expect($scope.vm.responseUser.length).toEqual(0);
            //expect($scope.vm.response[0].response).toEqual("beginner");
        });
    });
});
